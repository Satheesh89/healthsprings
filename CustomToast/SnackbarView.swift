//
//  SnackbarView.swift
//  Healthsprings
//
//  Created by Personal on 08/10/22.
//
import Foundation
import UIKit

class SnackbarView: UIView {
    let viewModel: SnackbarViewModel!
    private var handler: Handler?
    
    let label: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(16.0))
        return label
    }()
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        //imageView.setImageColor(color: .white)
        return imageView
    }()
    init(viewModel: SnackbarViewModel, frame: CGRect) {
        self.viewModel = viewModel
        super.init(frame: frame)
        addSubview(label)
        if viewModel.image != nil{
            addSubview(imageView)
        }
        backgroundColor = UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.3)
        //backgroundColor = .themeColor
        clipsToBounds = true
        layer.cornerRadius = 8
        layer.masksToBounds = true
        configure()
    }
    
    private func configure(){
        label.text = viewModel.text
        imageView.image = viewModel.image
        switch viewModel.type{
        case .action(let handler):
            self.handler = handler
            isUserInteractionEnabled = true
            let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapSnackbar))
            gesture.numberOfTapsRequired = 1
            gesture.numberOfTouchesRequired = 1
            addGestureRecognizer(gesture)
        case .info: break
        }
    }
    @objc private func didTapSnackbar(){
        handler?()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        if viewModel.image != nil{
            imageView.frame = CGRect(x: 45, y: 15, width: frame.height-30, height: frame.height-30)
            label.frame = CGRect(x: imageView.frame.origin.x+imageView.frame.size.width, y: 0, width: frame.size.width - imageView.frame.size.width - 80, height: frame.size.height)
        }else{
            label.frame = bounds
        }
    }
}
