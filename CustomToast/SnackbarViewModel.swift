//
//  SnackbarViewModel.swift
//  Healthsprings
//
//  Created by Personal on 08/10/22.
//

import UIKit
import Foundation

typealias Handler = (() -> Void)

enum SnackbarViewType{
    case info
    case action(handler: Handler)
}
struct SnackbarViewModel{
    let type: SnackbarViewType
    let text: String
    let image: UIImage?
}
