//
//  DoctorAvilableSlotsViewModel.swift
//  Healthsprings
//

import Foundation
import Alamofire
import SwiftyJSON

class DoctorAvilableSlotsViewModel {
    typealias notificationDetails = (_ notificationDetails: [NotificationsModel]?,_ status: Bool,_ message: String) -> Void
    var callBackNotificationDetails: notificationDetails?
    var notificationsModel = [NotificationsModel]()
    func getNotificationDetails() {
        let parameters: [String: Any] = [
            "apiToken": UserDetails.shared.getApiToken()
        ]
        AF.request(URL.getNotifications, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            guard let data = response.data else {
                self.callBackNotificationDetails?(nil, false, "")
                return}
            do {
                let jsonDecoder = JSONDecoder()
                let responseModel = try jsonDecoder.decode([NotificationsModel].self, from: data)
                print("responseModel === ",responseModel)
                self.callBackNotificationDetails?(responseModel, true,"")
            } catch {
                print("responseModel error ",error.localizedDescription)
                self.callBackNotificationDetails?(nil, false, error.localizedDescription)
            }
        }
    }
    func completionHandler(callBack: @escaping notificationDetails) {
        self.callBackNotificationDetails = callBack
    }
}
