
import Foundation
struct ClinicAvailableDays : Codable {
	let display_date : String?
	let date : String?
	let avail_from_time : String?
	let avail_to_time : String?
	let slots : Int?

	enum CodingKeys: String, CodingKey {
		case display_date = "display_date"
		case date = "date"
		case avail_from_time = "avail_from_time"
		case avail_to_time = "avail_to_time"
		case slots = "slots"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		display_date = try values.decodeIfPresent(String.self, forKey: .display_date)
		date = try values.decodeIfPresent(String.self, forKey: .date)
		avail_from_time = try values.decodeIfPresent(String.self, forKey: .avail_from_time)
		avail_to_time = try values.decodeIfPresent(String.self, forKey: .avail_to_time)
		slots = try values.decodeIfPresent(Int.self, forKey: .slots)
	}
}
