//
//  CompletedProfileViewModel.swift
//  Healthsprings
//
import Foundation
import UIKit
import SwiftyJSON

class CompletedProfileViewModel{
    
    weak var vc: ProfileSetttingViewController?
    var profileDataModel: ProfileDataModel!
    var profileData: ProfileData!
    var updateProfileDetails =  [UpdateProfileLoginDetails]()
    
    

    
    func updatePostRequest(getviewController: UIViewController,getupdateProfileDetails: [UpdateProfileLoginDetails],getProfile: Data,refreshToken: String) {
        LoadingIndicator.shared.show(forView: getviewController.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["apiToken"] = refreshToken
        paramsDictionary["person_name"] = "\(getupdateProfileDetails.first?.person_name ?? "")"
        paramsDictionary["profile_person"] = "\(getupdateProfileDetails.first?.profile_person ?? "")"
        paramsDictionary["email"] = "\(getupdateProfileDetails.first?.email ?? "")"
        paramsDictionary["mobile_code"] = "\(getupdateProfileDetails.first?.mobile_code ?? "")"
        paramsDictionary["mobile_number"] = "\(getupdateProfileDetails.first?.mobile_number ?? "")"
        paramsDictionary["street_name"] = "\(getupdateProfileDetails.first?.street_name ?? "")"
        paramsDictionary["country"] = "\(getupdateProfileDetails.first?.country ?? "")"
        paramsDictionary["city"] = "\(getupdateProfileDetails.first?.city ?? "")"
        paramsDictionary["state_id"] = "\(getupdateProfileDetails.first?.state_id ?? "")"
        paramsDictionary["pin_code"] = "\(getupdateProfileDetails.first?.pin_code ?? "")"
        paramsDictionary["gender"] = "\(getupdateProfileDetails.first?.gender ?? "")"
        paramsDictionary["emergency_contact"] = "\(getupdateProfileDetails.first?.emergency_contact ?? "")"
        paramsDictionary["marital_status"] = "\(getupdateProfileDetails.first?.marital_status ?? "")"
        paramsDictionary["dob"] = "\(getupdateProfileDetails.first?.dob ?? "")"
        paramsDictionary["height"] = "\(getupdateProfileDetails.first?.height ?? "")"
        paramsDictionary["weight"] = "\(getupdateProfileDetails.first?.weight ?? "")"
        paramsDictionary["blood_group"] = "\(getupdateProfileDetails.first?.blood_group ?? "")"
        paramsDictionary["nric_id"] = "\(getupdateProfileDetails.first?.nric_id ?? "")"
        paramsDictionary["full_address"] = "\(getupdateProfileDetails.first?.full_address ?? "")"
        print("paramsDictionary",paramsDictionary)
        
        HttpClientApi.instance().makeAPICall(url: URL.getupdateprofile, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("dataDict = ",dataDict)
                if  response?.statusCode == 200 {
                    doOnMain {
                        self.vc?.getProfileId = "\(dataDict.value(forKeyPath: "profile_id") ?? "")"
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                            let jsonDecoder = JSONDecoder()
                            let responseModel = try? jsonDecoder.decode(AuthModel.self, from: getProfile)
                            UserDetails.shared.setUserLoginData(data: try! JSONEncoder().encode(responseModel))
                            UserDetails.shared.setUserApiToken(apiTK: responseModel?.success?.token ?? "")
                            self.setTabPage()
                        }
                    }
                }else if  response?.statusCode == 401 {
                    doOnMain {LoadingIndicator.shared.hide()}
                    let tempArray = NSMutableArray()
                    for  i in 0..<((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.count ?? 0) {
                        let tArray  = ((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.allValues[i]) as? Array<Any>
                        tempArray.add(tArray?.first ?? "")
                    }
                    let errorStr = tempArray.componentsJoined(by: ",")
                    CustomToast.show(message: errorStr,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: getviewController,tarbarHeight: 0.0)
                }else{
                    doOnMain {LoadingIndicator.shared.hide()}
                    CustomToast.show(message: dataDict["error"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: getviewController,tarbarHeight: 0.0)
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
            CustomToast.show(message: "API call Failure" ,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: getviewController,tarbarHeight: 0.0)
        })
    }
    
    
    
    
    // MARK: Socail Register
    func socialSignUpApi(getViewcontroller: UIViewController,emailStr: String,nameStr: String,socailTokenStr: String,loginType:  Int,getupdateProfileDetails: [UpdateProfileLoginDetails]){
        var loginStr = ""
        if loginType == 1{
            loginStr = Key.UserDefaults.logintypeapple
        }else if loginType == 2{
            loginStr = Key.UserDefaults.logintypeFB
        }else if loginType == 3{
            loginStr = Key.UserDefaults.logintypeGM
        }
        LoadingIndicator.shared.show(forView: getViewcontroller.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["email"] = emailStr
        paramsDictionary["first_name"] = nameStr
        paramsDictionary["social_media_token"] = socailTokenStr
        paramsDictionary["device_token"] = UserDefaults.standard.object(forKey: Key.UserDefaults.deviceToken) ?? "35f86a221bfec1fe61b57c47bbba1e4bcc71ad760905dec6b900e333e52ff856"
        paramsDictionary["device_type"] = Key.UserDefaults.devicetype
        paramsDictionary["login_type"] = loginStr
        print(paramsDictionary)
        
        HttpClientApi.instance().makeAPICall(url: "\(URL.socialRegister)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("dataDict",dataDict)
                let response = response
                if  response?.statusCode == 200 {
                    doOnMain {
                        if "\(dataDict.value(forKeyPath: "message") ?? "")" ==  "user already Registered."{
                            self.socialLoginApi(getVC: getViewcontroller, emailStr: emailStr,socailTokenStr: socailTokenStr,loginType:  loginType)
                        }else{
                            let jsonDecoder = JSONDecoder()
                            let responseModel = try? jsonDecoder.decode(AuthModel.self, from: data)
                            self.updatePostRequest(getviewController: getViewcontroller,getupdateProfileDetails: getupdateProfileDetails, getProfile: data,refreshToken: responseModel?.success?.token ?? "")
                        }
                        LoadingIndicator.shared.hide()
                    }
                }else{
                    doOnMain {
                        CustomToast.show(message: "\(dataDict.value(forKeyPath: "message") ?? "")",bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: getViewcontroller,tarbarHeight: 0.0)
                        LoadingIndicator.shared.hide()
                    }
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
            doOnMain {
                CustomToast.show(message: "Error :- API call Failure",bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: getViewcontroller,tarbarHeight: 0.0)
                LoadingIndicator.shared.hide()
            }
        })
    }
    
    
    // MARK: Socail Login
    private func socialLoginApi(getVC: UIViewController,emailStr: String,socailTokenStr: String,loginType:  Int){
        var loginStr = ""
        if loginType == 1{
            loginStr = Key.UserDefaults.logintypeapple
        }else if loginType == 2{
            loginStr = Key.UserDefaults.logintypeFB
        }else if loginType == 3{
            loginStr = Key.UserDefaults.logintypeGM
        }
        LoadingIndicator.shared.show(forView: getVC.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["email"] = emailStr
        paramsDictionary["social_media_token"] = socailTokenStr
        paramsDictionary["device_token"] = UserDefaults.standard.object(forKey: Key.UserDefaults.deviceToken) ?? "35f86a221bfec1fe61b57c47bbba1e4bcc71ad760905dec6b900e333e52ff856"
        paramsDictionary["device_type"] = Key.UserDefaults.devicetype
        paramsDictionary["login_type"] = loginStr
        print(paramsDictionary)
        
        HttpClientApi.instance().makeAPICall(url: "\(URL.socialLogin)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("dataDict",dataDict)
                let response = response
                if  response?.statusCode == 200 {
                    doOnMain {
                        if "\(dataDict.value(forKeyPath: "message") ?? "")" ==  "user already Registered."{
                            CustomToast.show(message: "\(dataDict.value(forKeyPath: "message") ?? "user already Registered.")",bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: getVC,tarbarHeight: 0.0)
                        }else{
                            let jsonDecoder = JSONDecoder()
                            let responseModel = try? jsonDecoder.decode(AuthModel.self, from: data)
                            UserDetails.shared.setUserLoginData(data: try! JSONEncoder().encode(responseModel))
                            UserDetails.shared.setUserApiToken(apiTK: responseModel?.success?.token ?? "")
                            self.setTabPage()
                        }
                        LoadingIndicator.shared.hide()
                    }
                }else{
                    doOnMain {
                        CustomToast.show(message: "\(dataDict.value(forKeyPath: "message") ?? "")",bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: getVC,tarbarHeight: 0.0)
                        LoadingIndicator.shared.hide()
                    }
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
            doOnMain {
                CustomToast.show(message: "Error:- API call Failure",bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: getVC,tarbarHeight: 0.0)
                LoadingIndicator.shared.hide()
            }
        })
    }
    
    // MARK: - Tabbar Action
    private func setTabPage(){
        doOnMain {
            let tabStoryboard = UIStoryboard(name: "Tabbar", bundle: nil)
            let Permissionvc = tabStoryboard.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
            UIApplication.shared.windows.first?.rootViewController = Permissionvc
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }
}
