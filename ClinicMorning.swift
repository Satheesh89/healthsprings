//
//  ClinicMorning.swift
//

import Foundation
import SwiftyJSON

struct ClinicMorning {
	let time: String?
	init(_ json: JSON) {
		time = json["time"].stringValue
	}
}
