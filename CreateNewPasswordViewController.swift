//
//  CreateNewPasswordViewController.swift
//  Healthsprings
//
//  Created by Personal on 02/10/22.
//

import UIKit
import CRNotifications

class CreateNewPasswordViewController: UIViewController {
    
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordTextField: CustomTextField!
    @IBOutlet weak var repasswordView: UIView!
    @IBOutlet weak var repasswordTextField: CustomTextField!
    @IBOutlet weak var changePasswordButton: UIButton!
    
    @IBOutlet weak var passwordBottonConstraint: NSLayoutConstraint!
    @IBOutlet weak var repasswordTopConstraint: NSLayoutConstraint!

    var passwprdRightViewImageView: UIImageView!
    var reimageViewTextField: UIImageView!
    
    var getOtpStr: String!
    var getEmailStr: String!
    var passwordBool : Bool = true
    var reenterPasswordBool : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        passwordTextField.delegate = self
        repasswordTextField.delegate = self
        passwordTextField.becomeFirstResponder()
        initializeHideKeyboard()
        passwordView.viewBorderShadow()
        repasswordView.viewBorderShadow()
        passwordSetup()
        reEnterPasswordSetup()
        buttonError()
    }
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func passwordSetup(){
        passwordTextField.delegate = self
        passwordTextField.placeholder = "Password"
        
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        passwprdRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 20.68, height: 17))
        passwprdRightViewImageView.image = UIImage(named: "ic_password_hidden")
        passwprdRightViewImageView.contentMode = .scaleAspectFit
        outerView.addSubview(passwprdRightViewImageView)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(CreateNewPasswordViewController.passwordToggleAction))
        passwprdRightViewImageView.addGestureRecognizer(tap)
        passwprdRightViewImageView.isUserInteractionEnabled = true
        
        passwordTextField.rightView = outerView
        passwordTextField.rightViewMode = .always
        passwordTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        passwordTextField.addPadding(.left(18))
        passwordTextField.backgroundColor = .white
        passwordTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
//        validateData(statusTextField: passwordTextField)
    }
    
    func reEnterPasswordSetup(){
        repasswordTextField.delegate = self
        repasswordTextField.placeholder = "Re-enter password"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        reimageViewTextField = UIImageView(frame: CGRect(x: padding, y: 0, width: 20.68, height: 17))
        reimageViewTextField.image = UIImage(named: "ic_password_hidden")
        reimageViewTextField.contentMode = .scaleAspectFit
        outerView.addSubview(reimageViewTextField)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(CreateNewPasswordViewController.rePasswordToggleAction))
        reimageViewTextField.addGestureRecognizer(tap)
        reimageViewTextField.isUserInteractionEnabled = true
        
        repasswordTextField.rightView = outerView
        repasswordTextField.rightViewMode = .always
        repasswordTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        repasswordTextField.addPadding(.left(18))
        repasswordTextField.backgroundColor = .white
        repasswordTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
//        validateData(statusTextField: repasswordTextField)
    }
    
    @objc func passwordToggleAction(){
        if(passwordBool == true) {
            passwordTextField.isSecureTextEntry = false
            passwprdRightViewImageView.image = UIImage(named: "ic_password_visible")
        } else {
            passwordTextField.isSecureTextEntry = true
            passwprdRightViewImageView.image = UIImage(named: "ic_password_hidden")
        }
        passwordBool = !passwordBool
    }
    @objc func rePasswordToggleAction(){
        if(reenterPasswordBool == true) {
            repasswordTextField.isSecureTextEntry = false
            reimageViewTextField.image = UIImage(named: "ic_password_visible")
        } else {
            repasswordTextField.isSecureTextEntry = true
            reimageViewTextField.image = UIImage(named: "ic_password_hidden")
        }
        reenterPasswordBool = !reenterPasswordBool
    }
    
    @objc func textFieldDidChange(sender: UITextField) {
        validateData(statusTextField: sender)
    }
    
    func validateData(statusTextField: UITextField)  {
        if statusTextField == passwordTextField {
            if passwordTextField.text == ""{
                //buttonError()
                passwordTextField.showError(errorString: emptyMessage)
                repasswordTopConstraint.constant = 30
            }else if "\(passwordTextField.text ?? "")".isValidPassword() == false  {
                //buttonError()
                passwordTextField.showError(errorString: InvalidPasswordMessage)
                repasswordTopConstraint.constant = 60
            }
            else{
                if passwordTextField.text! !=  "" &&  "\(passwordTextField.text ?? "")".isValidPassword() == true  {
                    passwordTextField.hideError()
                    repasswordTopConstraint.constant = 20
                }
            }
            repasswordTopConstraint.isActive = true
        }
        
        let password = self.repasswordTextField.text == self.passwordTextField.text

        if statusTextField == repasswordTextField {
            if repasswordTextField.text == ""{
                //buttonError()
                repasswordTextField.showError(errorString: emptyMessage)
                passwordBottonConstraint.constant = 30
            }
//            else if "\(repasswordTextField.text ?? "")".isValidPassword() == false  {
//                //buttonError()
//                repasswordTextField.showError(errorString: InvalidPasswordMessage)
//                passwordBottonConstraint.constant = 60
//            }
            else{
                if password == true  {
                    repasswordTextField.hideError()
                    passwordBottonConstraint.constant = 20
                }else{
                    repasswordTextField.showError(errorString: InvalidConfirmPasswordMessage)
                    passwordBottonConstraint.constant = 30
                }
            }
            passwordBottonConstraint.isActive = true
        }

        if  passwordTextField.text != "" && "\(passwordTextField.text ?? "")".isValidPassword() == true && repasswordTextField.text != ""  && password == true  {
            buttonSuccess()
        }else{
            buttonError()
        }
    }
    
    
    func buttonSuccess(){
        changePasswordButton.backgroundColor = .themeColor
        changePasswordButton.setTitleColor(.white, for: .normal)
    }
    
    func buttonError() {
        self.changePasswordButton.backgroundColor = .loginGeryColor
        changePasswordButton.setTitleColor(UIColor.convertRGB(hexString: "#A0A0A0"), for: .normal)
    }
    
    @IBAction func didTapChangePasswordAction(_ sender: Any) {
        //        setFrontAccountView()
        if passwordTextField.text == ""{
            self.showCRNotificationsToast(toastType: CRNotifications.info, toastTitle: "Info", toastMeassage: "Passwords must not be empty")
        }else if repasswordTextField.text == ""{
            self.showCRNotificationsToast(toastType: CRNotifications.info, toastTitle: "Info", toastMeassage: "Confirm passwords must not be empty")
        }else if passwordTextField.text != repasswordTextField.text {
            self.showCRNotificationsToast(toastType: CRNotifications.info, toastTitle: "Info", toastMeassage: "Passwords Don't Match")
        }else{
            resetresetPassword()
        }
    }
    
    func resetresetPassword(){
        LoadingIndicator.shared.show(forView: self.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["email"] = getEmailStr
        paramsDictionary["password"] = passwordTextField.text ?? ""
        paramsDictionary["confirm_password"] = repasswordTextField.text ?? ""
        paramsDictionary["otp"] = getOtpStr
        
        HttpClientApi.instance().makeAPICall(url: URL.resetPassword, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                if  response?.statusCode == 200 {
                    doOnMain {
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                            self.setFrontPasswordView()
                        }
                    }
                }else{
                    delay(0.1) {
                        LoadingIndicator.shared.hide()
                        self.setFrontPasswordView()
                    }
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
            delay(0.1) {
                LoadingIndicator.shared.hide()
            }
        })
    }
    
    func setFrontPasswordView() {
        let mainstoryboard = UIStoryboard(name: "Menu", bundle: nil)
        let mainview = mainstoryboard.instantiateViewController(withIdentifier: "PasswordChangedViewController") as! PasswordChangedViewController
        self.navigationController?.pushViewController(mainview, animated: false)
    }
}
