//
//  InstantVideoAddCardViewController.swift
//  Healthsprings
//
//  Created by Personal on 17/10/22.
//

import UIKit

class InstantVideoAddCardViewController: UIViewController {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardTypeImageView: UIImageView!
    @IBOutlet weak var cardNameLabel: UILabel!
    @IBOutlet weak var cardNoLabel: UILabel!
    @IBOutlet weak var vaildLabel: UILabel!
    @IBOutlet weak var saveLabel: UILabel!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var accountNameLabel: UITextField!
    @IBOutlet weak var noView: UIView!
    @IBOutlet weak var accountNoLabel: UITextField!
    @IBOutlet weak var exprieLabel: UILabel!
    @IBOutlet weak var cvvLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var expView: UIView!
    @IBOutlet weak var cvvView: UIView!
    @IBOutlet weak var expTextField: UITextField!
    @IBOutlet weak var cvvTextField: UITextField!
    @IBOutlet weak var branchView: UIView!
    @IBOutlet weak var branchTextField: UITextField!
    var cardBool: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initializeHideKeyboard()
        accountNameLabel.delegate = self
        accountNoLabel.delegate = self
        expTextField.delegate = self
        cvvTextField.delegate = self
        branchTextField.delegate = self
        
//        accountNameLabel.text = "satheesh kumar"
//        accountNoLabel.text = "satheesh kumar"
//        expTextField.text = "12/22"
//        cvvTextField.text = "111"
//        branchTextField.text = "HDFC-Vadavalli"


        setStatusBar(color: UIColor.themeColor)
        expTextField.delegate = self
        cvvTextField.delegate = self
        accountNameLabel.delegate = self
        accountNoLabel.delegate = self
        expTextField.tag = 222
        cvvTextField.tag = 333
        accountNoLabel.tag = 444
        initializeHideKeyboard()
        //=cardNameLabel.text = UserDetails.shared.userName ?? ""
        vaildLabel.textColor = UIColor.convertRGB(hexString: "#8E8E8E")
        let attributedWithTextColor: NSAttributedString = "Valid  MM/YY".attributedStringWithColor([" MM/YY"], color: UIColor.black)
        vaildLabel.attributedText = attributedWithTextColor
        self.accountNoLabel.addTarget(self, action: #selector(didChangeText(textField:)), for: .editingChanged)
        //expTextField.addTarget(self, action: #selector(expirationDateDidChange), for: .editingChanged)
    }
    
    @objc func expirationDateDidChange() {
        let currentText = expTextField.text ?? ""
        // To remove " / " when user deletes the space after '/'
        if currentText.count == 4 {
            expTextField.text = String(currentText.prefix(2))
            return
        }
        var dateText = expTextField.text?.replacingOccurrences(of: "/", with: "") ?? ""
        dateText = dateText.replacingOccurrences(of: " ", with: "")
        var newText = ""
        for (index, character) in dateText.enumerated() {
            if index == 1 {
                newText = "\(newText)\(character) / "
            } else {
                newText.append(character)
            }
        }
        expTextField.text = newText
    }

    
    @objc func didChangeText(textField:UITextField) {
        textField.text = self.modifyCreditCardString(creditCardString: textField.text!)
        if textField.text?.count ?? 0 >= 19{
            let str = textField.text
            self.cardNoLabel.text = "****       ****       ****       \(str?.onlyLastCharacters(4) ?? "")"
        }else{
            self.cardNoLabel.text = "****       ****       ****       1234"
        }
    }
    
    func modifyCreditCardString(creditCardString : String) -> String {
        let trimmedString = creditCardString.components(separatedBy: .whitespaces).joined()
        let arrOfCharacters = Array(trimmedString)
        var modifiedCreditCardString = ""
        if(arrOfCharacters.count > 0) {
            for i in 0...arrOfCharacters.count-1 {
                modifiedCreditCardString.append(arrOfCharacters[i])
                if((i+1) % 4 == 0 && i+1 != arrOfCharacters.count){
                    modifiedCreditCardString.append("  ")
                }
            }
        }
        return modifiedCreditCardString
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        cardView.viewBorderShadow()
        cardView.layer.cornerRadius = 5
        noView.viewBorderShadow()
        expView.viewBorderShadow()
        cvvView.viewBorderShadow()
        nameView.viewBorderShadow()
        branchView.viewBorderShadow()
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func didTapLeftAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func didTapSaveAction(_ sender: Any) {
        if "\(accountNameLabel.text ?? "")" == ""{
            self.showCustomToast(toastTitle: "Error",toastMeassage: "Please enter your account holder name",status: 0)
        }else if "\(accountNoLabel.text ?? "")" == ""{
            self.showCustomToast(toastTitle: "Error",toastMeassage: "Please enter your card number",status: 0)
        }else if "\(expTextField.text ?? "")" == ""{
            self.showCustomToast(toastTitle: "Error",toastMeassage: "Please enter your expiry date and month",status: 0)
        }else if expTextField.text?.count != 5{
            self.showCustomToast(toastTitle: "Error",toastMeassage: "Please enter your expiry date and month",status: 0)
        }  else if "\(cvvTextField.text ?? "")" == ""{
            self.showCustomToast(toastTitle: "Error",toastMeassage: "Please enter your CVV",status: 0)
        }else{
            self.addCardApi()
        }
    }
    func sliceString(str: String, start: Int, end: Int) -> String {
        let data = Array(str)
        return String(data[start..<end])
    }
    
    func addCardApi()  {
        //        let monthStr = sliceString(str: expTextField.text ?? "", start: 0, end: 2)
        //        let yearStr = sliceString(str: expTextField.text ?? "", start: 3, end: 7)
        var paramsDictionary = [String:Any]()
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()
        paramsDictionary["cardholder_name"] = accountNameLabel.text ?? ""
        paramsDictionary["card_number"] = accountNoLabel.text ?? ""
        paramsDictionary["card_id"] = cvvTextField.text ?? ""
        //        paramsDictionary["expiration_year"] = yearStr
        //        paramsDictionary["expiration_month"] = monthStr
        paramsDictionary["expiration_year"] = expTextField.text ?? ""
        paramsDictionary["expiration_month"] = "2022"
        paramsDictionary["branch"] = branchTextField.text ?? ""
        
        HttpClientApi.instance().makeAPICall(url: URL.addCard, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                let response = response
                if  response?.statusCode == 200 {
                    doOnMain{
                        self.reDirectToAppointmentView(cardId: dataDict.object(forKey:"card_id") as? Int ?? 0)
                    }
                }else if  response?.statusCode == 401 {
                    let tempArray = NSMutableArray()
                    for  i in 0..<((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.count ?? 0) {
                        let tArray  = ((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.allValues[i]) as? Array<Any>
                        tempArray.add(tArray?.first ?? "")
                    }
                    let errorStr = tempArray.componentsJoined(by: ",")
                    CustomToast.show(message: errorStr,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }
                else{
                    CustomToast.show(message: dataDict["error"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
            self.showCustomToast(toastTitle: "Error",toastMeassage: "API call Failure",status: 1)
        })
    }
    func reDirectToAppointmentView(cardId: Int) {
        let paymentListVC = PaymentListViewController.instantiateFromAppStoryboard(appStoryboard: .Tabbar)
        paymentListVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(paymentListVC, animated: false)
  }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 222{
            guard let oldText = textField.text, let r = Range(range, in: oldText) else {
                return true
            }
            let updatedText = oldText.replacingCharacters(in: r, with: string)
            let attributedWithTextColor: NSAttributedString = "Valid  \(updatedText)".attributedStringWithColor([" \(updatedText)"], color: UIColor.black)
            vaildLabel.attributedText = attributedWithTextColor
            if string == "" {
                if updatedText.count == 2 {
                    textField.text = "\(updatedText.prefix(1))"
                    return false
                }
            } else if updatedText.count == 1 {
                if updatedText > "1" {
                    return false
                }
            } else if updatedText.count == 2 {
                if updatedText <= "12" { //Prevent user to not enter month more than 12
                    textField.text = "\(updatedText)/" //This will add "/" when user enters 2nd digit of month
                }
                return false
            } else if updatedText.count == 5 {
                self.expDateValidation(dateStr: updatedText)
            } else if updatedText.count > 5 {
                return false
            }
            return true
        }else if textField.tag == 333{
            let maxLength = 3
            let currentString = (textField.text ?? "") as NSString
            let newString = currentString.replacingCharacters(in: range, with: string)
            return newString.count <= maxLength
        }else if textField.tag == 444{
            let maxLength = 22
            let currentString = (textField.text ?? "") as NSString
            let newString = currentString.replacingCharacters(in: range, with: string)
            return newString.count <= maxLength
        }
        //        else if textField.tag == 444{
        //            let maxLength = 16
        //            let currentString = (textField.text ?? "") as NSString
        //            let newString = currentString.replacingCharacters(in: range, with: string)
        //            if newString.count > 12{
        //                if  newString.count <= maxLength {
        //                    let last4 = newString.substring(from:newString.index(newString.endIndex, offsetBy: -4))
        //                    cardNoLabel.text = "****       ****       ****       \(last4)"
        //                    cardNoLabel.textColor = UIColor.convertRGB(hexString: "#8E8E8E")
        //                    let attributedWithTextColor: NSAttributedString = "****       ****       ****       1234".attributedStringWithColor(["\(last4)"], color: UIColor.black)
        //                    cardNoLabel.attributedText = attributedWithTextColor
        //                }else{
        //                    cardNoLabel.text = "****       ****       ****       1234"
        //                    cardNoLabel.textColor = UIColor.convertRGB(hexString: "#8E8E8E")
        //                    let attributedWithTextColor: NSAttributedString = "****       ****       ****       1234".attributedStringWithColor(["1234"], color: UIColor.black)
        //                    cardNoLabel.attributedText = attributedWithTextColor
        //                }
        //            }else{
        //                cardNoLabel.text = "****       ****       ****       1234"
        //                cardNoLabel.textColor = UIColor.convertRGB(hexString: "#8E8E8E")
        //                let attributedWithTextColor: NSAttributedString = "****       ****       ****       1234".attributedStringWithColor(["1234"], color: UIColor.black)
        //                cardNoLabel.attributedText = attributedWithTextColor
        //            }
        //            return newString.count <= maxLength
        //        }
        else{
            let currentString = (textField.text ?? "") as NSString
            let newString = currentString.replacingCharacters(in: range, with: string)
            cardNameLabel.text = newString
        }
        return true
    }
    
    func expDateValidation(dateStr:String) {
        let currentYear = Calendar.current.component(.year, from: Date()) % 100   // This will give you current year (i.e. if 2019 then it will be 19)
        let currentMonth = Calendar.current.component(.month, from: Date()) // This will give you current month (i.e if June then it will be 6)
        let enteredYear = Int(dateStr.suffix(2)) ?? 0 // get last two digit from entered string as year
        let enteredMonth = Int(dateStr.prefix(2)) ?? 0 // get first two digit from entered string as month
        print(dateStr) // This is MM/YY Entered by user
        if enteredYear > currentYear {
            if (1 ... 12).contains(enteredMonth) {
                print("Entered Date Is Right")
            } else {
                print("Entered Date Is Wrong")
                self.showCustomToast(toastTitle: "Error",toastMeassage: "Invalid card expiry date or month",status: 1)
            }
        } else if currentYear == enteredYear {
            if enteredMonth >= currentMonth {
                if (1 ... 12).contains(enteredMonth) {
                    print("Entered Date Is Right")
                } else {
                    print("Entered Date Is Wrong")
                    self.showCustomToast(toastTitle: "Error",toastMeassage: "Invalid card expiry date or month",status: 1)
                }
            } else {
                self.showCustomToast(toastTitle: "Error",toastMeassage: "Invalid card expiry date or month",status: 1)
                print("Entered Date Is Wrong")
            }
        } else {
            self.showCustomToast(toastTitle: "Error",toastMeassage: "Invalid card expiry date or month",status: 1)
            print("Entered Date Is Wrong")
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 222{
            guard let text = textField.text else { return }
            let isUserInputValid = validate(string: text)
            if !isUserInputValid {
                //TODO: Notify user that expiration date is invalid
            }
        }
    }
    
    func validate(string: String) -> Bool {
        let regex = try! NSRegularExpression(pattern: "^(20)\\d\\d[/](0[1-9]|1[012])$")
        return regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil
    }
}
