//
//  CompletedProfileViewController.swift
//  Healthsprings
//
//  Created by Personal on 06/12/22.
//

import UIKit
import SwiftyJSON
import Alamofire
import CoreLocation
import iOSDropDown


class CompletedProfileViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: CustomTextField!
    @IBOutlet weak var userNameTextField: CustomTextField!
    @IBOutlet weak var mobileTextField: CustomTextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var selectCountry: CustomTextField!
    @IBOutlet weak var postalCodeTextField: CustomTextField!
    @IBOutlet weak var flatNoTextField: CustomTextField!
    @IBOutlet weak var locationTextField: CustomTextField!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var nricTextField: CustomTextField!
    @IBOutlet weak var mobileView: UIView!
    @IBOutlet weak var dateofbrithTextField: CustomTextField!
    @IBOutlet weak var heightTextField: CustomTextField!
    @IBOutlet weak var weightTextField: CustomTextField!
    @IBOutlet weak var emergencyNoTextField: CustomTextField!
    
    @IBOutlet weak var stateTextField: CustomTextField!
    @IBOutlet weak var cityTextField: CustomTextField!
    @IBOutlet weak var fullAddressTextField: CustomTextField!
    
    
    
    
    var emailRightViewImageView : UIImageView!
    var nameRightViewImageView : UIImageView!
    var mobileRightViewImageView : UIImageView!
    var nricRightViewImageView : UIImageView!
    var countryRightViewImageView : UIImageView!
    var postalRightViewImageView : UIImageView!
    var flatRightViewImageView : UIImageView!
    var addressRightViewImageView : UIImageView!
    
    
    var dobRightViewImageView : UIImageView!
    var heightRightViewImageView : UIImageView!
    var weightRightViewImageView : UIImageView!
    var emergenyNoRightViewImageView : UIImageView!
    
    var cityRightViewImageView : UIImageView!
    var stateRightViewImageView : UIImageView!
    var fulladdressRightViewImageView : UIImageView!
    
    var completedProfileViewModel = CompletedProfileViewModel()
    var getSocialDetails = [SocialLoginDetails]()
    
    @IBOutlet weak var emailBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var nameBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var mobileBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var nriceBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var countryBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var postalBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var flatBottomConstraint: NSLayoutConstraint!
    
    var countrycodepicker = UIPickerView()
    var countrypicker = UIPickerView()
    var countryListModel = [GetCountryList]()
    var countryCodeStr : String = "199"
    var locationManager:CLLocationManager!
    @IBOutlet weak var personDropdown: DropDown!
    @IBOutlet weak var bloodGroupView: UIView!
    
    @IBOutlet weak var maritalView: UIView!
    @IBOutlet weak var maritalDropdown: DropDown!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        countryTextField.delegate = self
        countryTextField.inputView = self.countrycodepicker
        countryTextField.tintColor = .clear
        countrycodepicker.dataSource = self
        countrycodepicker.delegate = self
        countrycodepicker.backgroundColor = .themeColor
        
        selectCountry.delegate = self
        selectCountry.inputView = self.countrypicker
        selectCountry.tintColor = .clear
        countrypicker.dataSource = self
        countrypicker.delegate = self
        countrypicker.backgroundColor = .themeColor
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        DispatchQueue.global().async {
            if CLLocationManager.locationServicesEnabled() {
                self.locationManager.startUpdatingLocation()
            }
        }
        getCountryListApi( )
        emailSetup()
        mobileSetup()
        usernameSetup()
        nricSetup()
        countrySetup()
        postalCodeSetup()
        //streetSetup()
//        AreaLocationSetup()
        dateofBrithSetup()
        heightSetup()
        weightSetup()
        enoMobileSetup()
        citySetup()
        stateSetup()
        fulladdressSetup()
        personDropDown()
        maritalSetupDropdown()
    }
    
    func personDropDown()  {
        bloodGroupView.dropShadowWithCornerRaduis()
        OperationQueue.main.addOperation({
            self.personDropdown.dropShadowWithCornerRaduis()
            self.personDropdown.placeholder = "Select blood group"
            self.personDropdown.addPadding(.left(18))
            self.personDropdown.optionArray = ["A+","O+","AB+","A-","O-","B-","AB-"]
            self.personDropdown.resignFirstResponder()
            //self.personDropdown.font = UIFont(name:"Lato-Medium", size: 12)!
            self.personDropdown.didSelect{(selectedText , index ,id) in
                print("Select-index",index)
            }
        })
    }
    
    
    func maritalSetupDropdown()  {
        maritalView.dropShadowWithCornerRaduis()
        OperationQueue.main.addOperation({
            self.maritalDropdown.dropShadowWithCornerRaduis()
            self.maritalDropdown.placeholder = "Marital Status"
            self.maritalDropdown.addPadding(.left(18))
            self.maritalDropdown.optionArray = ["Married","Unmarried"]
            self.maritalDropdown.resignFirstResponder()
            //self.personDropdown.font = UIFont(name:"Lato-Medium", size: 12)!
            self.maritalDropdown.didSelect{(selectedText , index ,id) in
                print("Select-index",index)
            }
        })
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func getCountryListApi( ) {
        LoadingIndicator.shared.show(forView: view)
        AF.request( "\(URL.getCountrylist)").response { [self] response in
            if let data = response.data {
                do{
                    let userResponse = try JSONDecoder().decode([GetCountryList].self, from: data)
                    self.countryListModel.removeAll()
                    self.countryListModel.append(contentsOf: userResponse)
                    let filterArray = self.countryListModel.filter({($0.country_id! == 199)})
                    self.countryListModel.removeAll()
                    self.countryListModel = filterArray
                    if filterArray.count > 0 {
                        self.selectCountry.text = "\(self.countryListModel[0].country ?? "")"
                        self.countryLabel.text = "+ \(self.countryListModel[0].country_phone_code ?? 0)"
                    }else{
                        self.selectCountry.text = ""
                        self.countryLabel.text = "+ 65"
                    }
                    doOnMain {
                        LoadingIndicator.shared.hide()
                    }
                }catch let err{
                    print(err.localizedDescription)
                }
            }
        }
    }
    
    func emailSetup(){
        emailTextField.delegate = self
        emailTextField.placeholder = "ENTER EMAIL ID"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        emailRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 17.34, height: 12.63))
        emailRightViewImageView.image = UIImage(named: "email")
        outerView.addSubview(emailRightViewImageView)
        
        emailTextField.rightView = outerView
        emailTextField.rightViewMode = .always
        emailTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        emailTextField.addPadding(.left(18))
        emailTextField.backgroundColor = .white
        emailTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        emailTextField.text = getSocialDetails.first?.email
        emailTextField.isEnabled = false
    }
    
    func usernameSetup(){
        userNameTextField.delegate = self
        userNameTextField.placeholder = "Enter your full name"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        nameRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 14.97, height: 19.24))
        nameRightViewImageView.image = UIImage(named: "useraccount")
        outerView.addSubview(nameRightViewImageView)
        
        userNameTextField.rightView = outerView
        userNameTextField.rightViewMode = .always
        userNameTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        userNameTextField.addPadding(.left(18))
        userNameTextField.backgroundColor = .white
        userNameTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        userNameTextField.text = getSocialDetails.first?.name
    }
    
    func mobileSetup(){
        mobileTextField.delegate = self
        mobileTextField.placeholder = "Enter your mobile number"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        mobileRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 19, height: 19.21))
        mobileRightViewImageView.image = UIImage(named: "message")
        outerView.addSubview(mobileRightViewImageView)
        mobileView.dropShadowWithCornerRaduis()
        mobileTextField.rightView = outerView
        mobileTextField.rightViewMode = .always
        mobileTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        mobileTextField.addPadding(.left(18))
        mobileTextField.backgroundColor = .white
        mobileTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        doOnMain {
            self.mobileTextField.layer.masksToBounds = false
            self.mobileTextField.layer.shadowRadius = 4
            self.mobileTextField.layer.shadowOpacity = 0.5
            self.mobileTextField.layer.shadowColor = UIColor.whiteColor.cgColor
            self.mobileTextField.layer.cornerRadius = 0
        }
    }
    func nricSetup(){
        nricTextField.delegate = self
        nricTextField.placeholder = "Enter your NRIC Number"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        nricRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 19, height: 19.21))
        nricRightViewImageView.image = UIImage(named: "noun-payment-1800088")
        nricRightViewImageView.contentMode = .scaleAspectFit
        outerView.addSubview(nricRightViewImageView)
        
        nricTextField.rightView = outerView
        nricTextField.rightViewMode = .always
        nricTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        nricTextField.addPadding(.left(18))
        nricTextField.backgroundColor = .white
        nricTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        nricTextField.autocapitalizationType = .allCharacters
    }
    
    func countrySetup(){
        selectCountry.delegate = self
        selectCountry.placeholder = "Select Country"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        countryRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 11, height: 8))
        countryRightViewImageView.image = UIImage(named: "Polygon")
        countryRightViewImageView.contentMode = .scaleAspectFit
        outerView.addSubview(countryRightViewImageView)
        
        selectCountry.rightView = outerView
        selectCountry.rightViewMode = .always
        selectCountry.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        selectCountry.addPadding(.left(18))
        selectCountry.backgroundColor = .white
        selectCountry.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        //        selectCountry.autocapitalizationType = .allCharacters
    }
    func postalCodeSetup(){
        postalCodeTextField.delegate = self
        postalCodeTextField.placeholder = " Enter Postal Code"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        postalRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 19, height: 19.21))
        postalRightViewImageView.image = UIImage(named: "postalcode")
        postalRightViewImageView.contentMode = .scaleAspectFit
        outerView.addSubview(postalRightViewImageView)
        postalCodeTextField.rightView = outerView
        postalCodeTextField.rightViewMode = .always
        postalCodeTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        postalCodeTextField.addPadding(.left(18))
        postalCodeTextField.backgroundColor = .white
        postalCodeTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        //        postalCodeTextField.autocapitalizationType = .allCharacters
    }
    func streetSetup(){
        flatNoTextField.delegate = self
        flatNoTextField.placeholder = "Enter your blood group"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        flatRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 19, height: 19.21))
        flatRightViewImageView.image = UIImage(named: "bloodgroup")
        flatRightViewImageView.contentMode = .scaleAspectFit
        outerView.addSubview(flatRightViewImageView)
        
        flatNoTextField.rightView = outerView
        flatNoTextField.rightViewMode = .always
        flatNoTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        flatNoTextField.addPadding(.left(18))
        flatNoTextField.backgroundColor = .white
        flatNoTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        //        flatNoTextField.autocapitalizationType = .allCharacters
        flatNoTextField.isHidden = true
    }
    func AreaLocationSetup(){
        locationTextField.delegate = self
        locationTextField.placeholder = "Enter your marital status"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        addressRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 19, height: 19.21))
        addressRightViewImageView.image = UIImage(named: "maritalstatus")
        addressRightViewImageView.contentMode = .scaleAspectFit
        outerView.addSubview(addressRightViewImageView)
        locationTextField.rightView = outerView
        locationTextField.rightViewMode = .always
        locationTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        locationTextField.addPadding(.left(18))
        locationTextField.backgroundColor = .white
        locationTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        //locationTextField.autocapitalizationType = .allCharacters
        locationTextField.isHidden = true
    }
    func dateofBrithSetup(){
        dateofbrithTextField.delegate = self
        dateofbrithTextField.placeholder = "Enter your DOB"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        dobRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 18, height: 24))
        dobRightViewImageView.image = UIImage(named: "calender")
        dobRightViewImageView.contentMode = .scaleAspectFit
        outerView.addSubview(dobRightViewImageView)
        
        dateofbrithTextField.rightView = outerView
        dateofbrithTextField.rightViewMode = .always
        dateofbrithTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        dateofbrithTextField.addPadding(.left(18))
        dateofbrithTextField.backgroundColor = .white
        dateofbrithTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        //        dateofbrithTextField.autocapitalizationType = .allCharacters
        dateofbrithTextField.addInputViewDatePicker(target: self, selector: #selector(tapDone)) //1
    }
    @objc func tapDone() {
        if let datePicker = self.dateofbrithTextField.inputView as? UIDatePicker {
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let selectedDate: String = dateFormatter.string(from: datePicker.date)
            dateofbrithTextField.text = selectedDate
        }
        validateData(statusTextField: dateofbrithTextField)
        dateofbrithTextField.resignFirstResponder()
    }
    func heightSetup(){
        heightTextField.delegate = self
        heightTextField.placeholder = "Enter your height"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        heightRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 18, height: 24))
        heightRightViewImageView.image = UIImage(named: "union")
        //        countryRightViewImageView.contentMode = .scaleAspectFit
        outerView.addSubview(heightRightViewImageView)
        
        heightTextField.rightView = outerView
        heightTextField.rightViewMode = .always
        heightTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        heightTextField.addPadding(.left(18))
        heightTextField.backgroundColor = .white
        heightTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        //        heightTextField.autocapitalizationType = .allCharacters
    }
    
    func weightSetup(){
        weightTextField.delegate = self
        weightTextField.placeholder = "Enter your weight"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        weightRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 18, height: 18.67))
        weightRightViewImageView.image = UIImage(named: "weight")
        weightRightViewImageView.contentMode = .scaleAspectFit
        outerView.addSubview(weightRightViewImageView)
        
        weightTextField.rightView = outerView
        weightTextField.rightViewMode = .always
        weightTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        weightTextField.addPadding(.left(18))
        weightTextField.backgroundColor = .white
        weightTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        //        weightTextField.autocapitalizationType = .allCharacters
    }
    
    func enoMobileSetup(){
        emergencyNoTextField.delegate = self
        emergencyNoTextField.placeholder = "Enter your emergency contact"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        emailRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 19, height: 19.21))
        emailRightViewImageView.image = UIImage(named: "emptyMessage")
        outerView.addSubview(emailRightViewImageView)
        
        emergencyNoTextField.rightView = outerView
        emergencyNoTextField.rightViewMode = .always
        emergencyNoTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        emergencyNoTextField.addPadding(.left(18))
        emergencyNoTextField.backgroundColor = .white
        emergencyNoTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
    }
    
    
    
    func citySetup(){
        cityTextField.delegate = self
        cityTextField.placeholder = "Enter your city"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        cityRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 18, height: 24))
        cityRightViewImageView.image = UIImage(named: "location")
        //        countryRightViewImageView.contentMode = .scaleAspectFit
        outerView.addSubview(cityRightViewImageView)
        
        cityTextField.rightView = outerView
        cityTextField.rightViewMode = .always
        cityTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        cityTextField.addPadding(.left(18))
        cityTextField.backgroundColor = .white
        cityTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        //        cityTextField.autocapitalizationType = .allCharacters
    }
    
    func stateSetup(){
        stateTextField.delegate = self
        stateTextField.placeholder = "Enter your state"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        stateRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 18, height: 24))
        stateRightViewImageView.image = UIImage(named: "location")
        //        countryRightViewImageView.contentMode = .scaleAspectFit
        outerView.addSubview(stateRightViewImageView)
        
        stateTextField.rightView = outerView
        stateTextField.rightViewMode = .always
        stateTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        stateTextField.addPadding(.left(18))
        stateTextField.backgroundColor = .white
        stateTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        //        stateTextField.autocapitalizationType = .allCharacters
    }
    
    func fulladdressSetup(){
        fullAddressTextField.delegate = self
        fullAddressTextField.placeholder = "Enter your full address"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        fulladdressRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 18, height: 24))
        fulladdressRightViewImageView.image = UIImage(named: "location")
        //        countryRightViewImageView.contentMode = .scaleAspectFit
        outerView.addSubview(fulladdressRightViewImageView)
        
        fullAddressTextField.rightView = outerView
        fullAddressTextField.rightViewMode = .always
        fullAddressTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        fullAddressTextField.addPadding(.left(18))
        fullAddressTextField.backgroundColor = .white
        fullAddressTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        //        fullAddressTextField.autocapitalizationType = .allCharacters
    }
    
    @objc func textFieldDidChange(sender: UITextField) {
        validateData(statusTextField: sender)
    }
    
    
    @IBAction func didTapUpdateAction(_ sender: Any) {
        updateProfile()
    }
    
    func updateProfile(){
        let get_OutputDateStr = dateofbrithTextField.text ?? ""
        let addObject = UpdateProfileLoginDetails(person_name: userNameTextField.text ?? "", profile_person: "self", email: emailTextField.text ?? "", loginType: getSocialDetails.first?.loginType ?? 0, mobile_code: countryLabel.text ?? "", mobile_number: mobileTextField.text ?? "", street_name: stateTextField.text ?? "", country: countryCodeStr, state_id: stateTextField.text ?? "", city: cityTextField.text ?? "", pin_code: postalCodeTextField.text ?? "", gender: "male", emergency_contact: emergencyNoTextField.text ?? "", marital_status: maritalDropdown.text ?? "", dob: get_OutputDateStr, height: heightTextField.text ?? "", weight: weightTextField.text ?? "", blood_group:  personDropdown.text ?? "", nric_id: nricTextField.text ?? "", full_address: fullAddressTextField.text ?? "")
        var updateProfileLoginDetails = [UpdateProfileLoginDetails]()
        updateProfileLoginDetails.append(addObject)
        completedProfileViewModel.socialSignUpApi(getViewcontroller: self, emailStr: "\(getSocialDetails.first?.email ?? "")", nameStr: "\(getSocialDetails.first?.name ?? "")", socailTokenStr: "\(getSocialDetails.first?.token ?? "")", loginType: getSocialDetails.first?.loginType ?? 0, getupdateProfileDetails: updateProfileLoginDetails)
        //        let get_OutputDateStr = dateofbrithTextField.text ?? ""
        //        completedProfileViewModel.updatePostRequest(getviewController: self, getPersonName: userNameTextField.text ?? "", getEmail: emailTextField.text ?? "", getMobilecode: countryLabel.text ?? "", getMobilenumber: mobileTextField.text ?? "", getStreetname: "", getCountry: countryCodeStr, getCity: cityTextField.text ?? "", getState: stateTextField.text ?? "", getPincode: postalCodeTextField.text ?? "", getGender: "", getEmergencycontact: emergencyNoTextField.text ?? "", getMaritalstatus: locationTextField.text ?? "", getDob: get_OutputDateStr,getHeight: heightTextField.text ?? "",getWeight: weightTextField.text ?? "",getBloodgroup: flatNoTextField.text ?? "", getNricid: nricTextField.text ?? "",fullAddress: fullAddressTextField.text ?? "")
    }
    
    func validateData(statusTextField: UITextField)  {
        if statusTextField == emailTextField {
            if emailTextField.text! == ""{
                buttonError()
                emailTextField.showError(errorString: emptyMessage)
                emailRightViewImageView.setImageColor(color: .red)
                emailBottomConstraint.constant = 30
            }else if  isValidEmail(emailID: emailTextField.text ?? "") == false {
                buttonError()
                emailTextField.showError(errorString: invaildEmailMessage)
                emailRightViewImageView.setImageColor(color: .red)
                emailBottomConstraint.constant = 30
            }else if emailTextField.text! !=  "" &&  isValidEmail(emailID: emailTextField.text ?? "") == true {
                emailRightViewImageView.setImageColor(color: .black)
                emailTextField.hideError()
                emailBottomConstraint.constant = 20
            }
            emailBottomConstraint.isActive = true
        }
        
        if statusTextField == userNameTextField {
            if userNameTextField.text! == ""{
                buttonError()
                userNameTextField.showError(errorString: emptyMessage)
                nameRightViewImageView.setImageColor(color: .red)
                nameBottomConstraint.constant = 30
            }else if userNameTextField.text?.count ?? 0 > 0{
                nameRightViewImageView.setImageColor(color: .black)
                userNameTextField.hideError()
                nameBottomConstraint.constant = 20
            }
            nameBottomConstraint.isActive = true
        }
        
        if statusTextField == mobileTextField {
            if mobileTextField.text! == ""{
                buttonError()
                mobileTextField.showMobileError(errorString: phoneNoEmptyMessage)
                mobileRightViewImageView.setImageColor(color: .red)
                mobileView.layer.borderWidth = 1
                mobileView.layer.borderColor = UIColor.red.cgColor
                mobileBottomConstraint.constant = 30
            }   else if mobileTextField.text?.count ?? 0 < 8  {
                buttonError()
                mobileTextField.showMobileError(errorString: invaildPhoneNoMessage)
                mobileRightViewImageView.setImageColor(color: .red)
                mobileView.layer.borderWidth = 1
                mobileView.layer.borderColor = UIColor.red.cgColor
                mobileBottomConstraint.constant = 30
            }else{
                if mobileTextField.text! !=  "" &&  mobileTextField.text?.count ?? 0 == 8 {
                    mobileRightViewImageView.setImageColor(color: .black)
                    mobileTextField.hideError()
                    mobileView.layer.borderWidth = 0
                    mobileView.layer.borderColor = UIColor.white.cgColor
                    mobileBottomConstraint.constant = 20
                }
            }
            mobileBottomConstraint.isActive = true
        }
        
        if statusTextField == nricTextField {
            if  nricTextField.text! == "" {
                buttonError()
                nricTextField.showError(errorString: emptyMessage)
                nricRightViewImageView.setImageColor(color: .red)
                nriceBottomConstraint.constant = 30
            }else if "\(nricTextField.text ?? "")".nricVal == false {
                buttonError()
                nricTextField.showError(errorString: nricMessage)
                nricRightViewImageView.setImageColor(color: .red)
                nriceBottomConstraint.constant = 60
            }else{
                if nricTextField.text! !=  "" &&  "\(nricTextField.text ?? "")".nricVal == true {
                    nricRightViewImageView.setImageColor(color: .black)
                    nricTextField.hideError()
                    nriceBottomConstraint.constant = 20
                }
            }
            nriceBottomConstraint.isActive = true
        }
        if statusTextField == selectCountry {
            if selectCountry.text! == ""{
                buttonError()
                selectCountry.showError(errorString: emptyMessage)
                countryRightViewImageView.setImageColor(color: .red)
                countryBottomConstraint.constant = 30
            }else if selectCountry.text?.count ?? 0 > 0{
                countryRightViewImageView.setImageColor(color: .black)
                selectCountry.hideError()
                countryBottomConstraint.constant = 20
            }
            countryBottomConstraint.isActive = true
        }
        if statusTextField == postalCodeTextField {
            if postalCodeTextField.text! == ""{
                buttonError()
                postalCodeTextField.showError(errorString: emptyMessage)
                postalRightViewImageView.setImageColor(color: .red)
                postalBottomConstraint.constant = 30
            }else if postalCodeTextField.text?.count ?? 0 > 0{
                postalRightViewImageView.setImageColor(color: .black)
                postalCodeTextField.hideError()
                postalBottomConstraint.constant = 20
            }
            postalBottomConstraint.isActive = true
        }
        //        if statusTextField == flatNoTextField {
        //            if flatNoTextField.text! == ""{
        //                buttonError()
        //                flatNoTextField.showError(errorString: emptyMessage)
        //                flatRightViewImageView.setImageColor(color: .red)
        //                flatBottomConstraint.constant = 30
        //            }else if flatNoTextField.text?.count ?? 0 > 0{
        //                flatRightViewImageView.setImageColor(color: .black)
        //                flatNoTextField.hideError()
        //                flatBottomConstraint.constant = 20
        //            }
        //            flatBottomConstraint.isActive = true
        //        }
        //        if statusTextField == locationTextField {
        //            if locationTextField.text! == ""{
        //                buttonError()
        //                locationTextField.showError(errorString: emptyMessage)
        //                addressRightViewImageView.setImageColor(color: .red)
        //            }else if locationTextField.text?.count ?? 0 > 0{
        //                addressRightViewImageView.setImageColor(color: .black)
        //                locationTextField.hideError()
        //            }
        //        }
        if statusTextField == dateofbrithTextField {
            if dateofbrithTextField.text! == ""{
                buttonError()
                dateofbrithTextField.showError(errorString: emptyMessage)
                dobRightViewImageView.setImageColor(color: .red)
            }else if dateofbrithTextField.text?.count ?? 0 > 0{
                dobRightViewImageView.setImageColor(color: .black)
                dateofbrithTextField.hideError()
            }
        }
        //        if statusTextField == heightTextField {
        //            if heightTextField.text! == ""{
        //                buttonError()
        //                heightTextField.showError(errorString: emptyMessage)
        //                heightRightViewImageView.setImageColor(color: .red)
        //            }else if heightTextField.text?.count ?? 0 > 0{
        //                heightRightViewImageView.setImageColor(color: .black)
        //                heightTextField.hideError()
        //            }
        //        }
        if statusTextField == weightTextField {
            if weightTextField.text! == ""{
                buttonError()
                weightTextField.showError(errorString: emptyMessage)
                weightRightViewImageView.setImageColor(color: .red)
            }else if weightTextField.text?.count ?? 0 > 0{
                weightRightViewImageView.setImageColor(color: .black)
                weightTextField.hideError()
            }
        }
        if statusTextField == emergencyNoTextField {
            if emergencyNoTextField.text! == ""{
                buttonError()
                emergencyNoTextField.showMobileError(errorString: phoneNoEmptyMessage)
                emailRightViewImageView.setImageColor(color: .red)
            }   else if emergencyNoTextField.text?.count ?? 0 < 8  {
                buttonError()
                emergencyNoTextField.showMobileError(errorString: invaildPhoneNoMessage)
                emailRightViewImageView.setImageColor(color: .red)
            }else{
                if emergencyNoTextField.text! !=  "" &&  emergencyNoTextField.text?.count ?? 0 == 8 {
                    emailRightViewImageView.setImageColor(color: .black)
                    emergencyNoTextField.hideError()
                }
            }
            mobileBottomConstraint.isActive = true
        }
        if statusTextField == cityTextField {
            if cityTextField.text! == ""{
                buttonError()
                cityTextField.showError(errorString: emptyMessage)
                cityRightViewImageView.setImageColor(color: .red)
            }else if cityTextField.text?.count ?? 0 > 0{
                cityRightViewImageView.setImageColor(color: .black)
                cityTextField.hideError()
            }
        }
        if statusTextField == stateTextField {
            if stateTextField.text! == ""{
                buttonError()
                stateTextField.showError(errorString: emptyMessage)
                stateRightViewImageView.setImageColor(color: .red)
            }else if stateTextField.text?.count ?? 0 > 0{
                stateRightViewImageView.setImageColor(color: .black)
                stateTextField.hideError()
            }
        }
        if statusTextField == fullAddressTextField {
            if fullAddressTextField.text! == ""{
                buttonError()
                fullAddressTextField.showError(errorString: emptyMessage)
                fulladdressRightViewImageView.setImageColor(color: .red)
            }else if fullAddressTextField.text?.count ?? 0 > 0{
                fulladdressRightViewImageView.setImageColor(color: .black)
                fullAddressTextField.hideError()
            }
        }
        if emailTextField.text! != "" && isValidEmail(emailID: emailTextField.text ?? "") == true && userNameTextField.text! != "" && mobileTextField.text! != "" && mobileTextField.text?.count ?? 0 == 8  && nricTextField.text! != "" && "\(nricTextField.text ?? "")".nricVal == true && selectCountry.text! != "" && postalCodeTextField.text! != "" && cityTextField.text! != "" && stateTextField.text! != "" && fullAddressTextField.text! != ""{
            buttonSuccess()
        }else{
            buttonError()
        }
    }
    
    //MARK:- Email Check
    func isValidEmail(emailID:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailID)
    }
    
    
    func buttonError() {
        DispatchQueue.main.async {
            self.updateButton.backgroundColor = .loginGeryColor
            self.updateButton.tintColor = UIColor.convertRGB(hexString: "#A0A0A0")
            self.updateButton.titleLabel?.textColor = UIColor.convertRGB(hexString: "#A0A0A0")
            self.updateButton.setTitleColor(UIColor.convertRGB(hexString: "#A0A0A0"), for: .normal)
        }
    }
    
    func buttonSuccess(){
        self.updateButton.backgroundColor = .themeColor
        updateButton.setTitleColor(.white, for: .normal)
    }
}


extension CompletedProfileViewController{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileTextField || textField == emergencyNoTextField{
            return range.location < 8
        }else if textField == nricTextField{
            return range.location < 9
        }else if textField == userNameTextField{
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let Regex = "[a-z A-Z ]+"
            let predicate = NSPredicate.init(format: "SELF MATCHES %@", Regex)
            if predicate.evaluate(with: text) || string == ""{
                //textField.text = textField.text?.replacingOccurrences(of: " ", with: "")
                return true
            }
            else{
                return false
            }
        }else if textField == postalCodeTextField{
            return range.location < 8
        }else if textField == heightTextField{
            return range.location < 3
        }else if textField == weightTextField{
            return range.location < 3
        }
        return true
    }
}
extension CompletedProfileViewController:  UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.countryListModel.count
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(20.0))
            pickerLabel?.textAlignment = .center
        }
        if pickerView == countrypicker{
            pickerLabel?.text = "\(countryListModel[row].country ?? "")"
        }else{
            pickerLabel?.text = "(+ \(countryListModel[row].country_phone_code ?? 0)) \(countryListModel[row].country ?? "")"
        }
        pickerLabel?.textColor = UIColor.white
        return pickerLabel!
    }
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == countrypicker{
            selectCountry?.text = "\(countryListModel[row].country ?? "")"
            validateData(statusTextField: selectCountry)
        }else{
            self.countryLabel.text = "+ \(self.countryListModel[row].country_phone_code ?? 0)"
            countryCodeStr = "\(self.countryListModel[row].country_id ?? 0)"
        }
    }
}
extension CompletedProfileViewController{
    //MARK: - location delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0] as CLLocation
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count>0{
                let placemark = placemarks![0]
                self.fullAddressTextField.text = "\(placemark.locality!), \(placemark.administrativeArea!), \(placemark.country!)"
            }
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
}

extension CompletedProfileViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            print("not determined - hence ask for Permission")
            manager.requestWhenInUseAuthorization()
        case .restricted, .denied:
            print("permission denied")
        case .authorizedAlways, .authorizedWhenInUse:
            print("Apple delegate gives the call back here once user taps Allow option, Make sure delegate is set to self")
        }
    }
}
