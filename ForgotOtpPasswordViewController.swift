//
//  ForgotOtpPasswordViewController.swift
//  Healthsprings
//
//  Created by Personal on 02/10/22.
//

import UIKit

class ForgotOtpPasswordViewController: UIViewController {
    @IBOutlet weak var otpCode: DPOTPView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var registerLabel: UILabel!
    @IBOutlet weak var otpexpireLabel: UILabel!
    @IBOutlet weak var resendLabel: UILabel!
    
    
    var emailStr: String!
    var timer : Timer!
    var second = 300
    
    var getEmail: String = ""
    var getMobile: String = ""
    var getName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initializeHideKeyboard()
        timer = Timer()
        otpCode.fontTextField = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(20.0))!
        otpCode.dismissOnLastEntry = true
        otpCode.isCursorHidden = true
        otpCode.dpOTPViewDelegate = self
        otpCode.textColorTextField = .themeColor
        otpCode.becomeFirstResponder()
        otpCode.dpOTPViewDelegate = self
        registerLabel.text = "OTP has been sent to your registered Email ID"
        resendLabel.textColor = .lightGray
        timerUpdate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    //    //MARK :- Timer function
    func timerUpdate() {
        second = 300
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(update), userInfo: nil, repeats: true)
    }
    
    //    //MARK:- Calculate timer
    func timeString(time: TimeInterval) -> String {
        //        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
        //        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    @objc func update() {
        resendLabel.textColor = .lightGray
        let time = timeString(time: TimeInterval(second))
        second = second - 1
        let str = String(time)
        DispatchQueue.main.async {
            self.otpexpireLabel.text = "Your OTP will expire within \(str) minutes"
            let attributedWithTextColor: NSAttributedString = "Your OTP will expire within \(str) minutes".attributedStringWithColor(["\(str) minutes"], color: UIColor.themeColor)
            self.otpexpireLabel.attributedText = attributedWithTextColor
        }
        
        if second == 0 {
            resendLabel.textColor = .themeColor
            let endStr  = "00:00"
            self.otpexpireLabel.text = "Your OTP will expire within \(endStr) minutes"
            let attributedWithTextColor: NSAttributedString = "Your OTP will expire within \(endStr) minutes".attributedStringWithColor(["\(endStr) minutes"], color: UIColor.themeColor)
            self.otpexpireLabel.attributedText = attributedWithTextColor
            timer.invalidate()
        }
    }
    
    
    // MARK: - Resend Action
    @IBAction func didTapresendAction(_ sender: Any) {
        second = 300
        timerUpdate()
    }
    
    // MARK: - Close Action
    @IBAction func didTapCloseAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    func matchOTPPage(){
        LoadingIndicator.shared.show(forView: self.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["email"] = emailStr
        paramsDictionary["otp"] = otpCode.text ?? ""
        
        HttpClientApi.instance().makeAPICall(url: URL.getMatchtoken, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                if  response?.statusCode == 200 {
                    doOnMain {
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                                self.setFrontAccountView()
                        }
                    }
                }else if  response?.statusCode == 401 {
                    doOnMain {
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                        }
                    }
                    let tempArray = NSMutableArray()
                    for  i in 0..<((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.count ?? 0) {
                        let tArray  = ((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.allValues[i]) as? Array<Any>
                        tempArray.add(tArray?.first ?? "")
                    }
                    let errorStr = tempArray.componentsJoined(by: ",")
                    CustomToast.show(message: errorStr,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                } else{
                    doOnMain {
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                        }
                    }
                    CustomToast.show(message: dataDict["error"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
            doOnMain {
                delay(0.1) {
                    LoadingIndicator.shared.hide()
                        self.setFrontAccountView()
                    CustomToast.show(message: error as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }
            }
        })
    }
}
extension ForgotOtpPasswordViewController : DPOTPViewDelegate {
    func dpOTPViewAddText(_ text: String, at position: Int) {
        print("addText:- " + text + " at:- \(position)" )
        if text.count == 6 {
            matchOTPPage()
        }
    }
    func setFrontAccountView() {
        let mainstoryboard = UIStoryboard(name: "Menu", bundle: nil)
        let mainview = mainstoryboard.instantiateViewController(withIdentifier: "CreateNewPasswordViewController") as! CreateNewPasswordViewController
        mainview.getEmailStr = emailStr
        mainview.getOtpStr = otpCode.text ?? ""
        self.navigationController?.pushViewController(mainview, animated: false)
    }
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        print("removeText:- " + text + " at:- \(position)" )
    }
    func dpOTPViewChangePositionAt(_ position: Int) {
        print("at:-\(position)")
    }
    func dpOTPViewBecomeFirstResponder() {
    }
    func dpOTPViewResignFirstResponder() {
    }
}
