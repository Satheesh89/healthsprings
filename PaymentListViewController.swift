//
//  PaymentListViewController.swift
//  Healthsprings
//
import UIKit


class PaymentListViewController: UIViewController {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var paymentSettingTableView: UITableView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addNextCardButton: UIButton!
    @IBOutlet weak var proceedButton: UIButton!
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var insidepopupView: UIView!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var closedButton: UIButton!

    
    var selectIndexpath : IndexPath = IndexPath()
    var getCardModel = GetCardViewModel()
    var cardIdStr: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        proceedButton.layer.cornerRadius = 5
//        popupViewLoadSetup()
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func loadSetup(){
        getCardModel.paymentListvc = self
        getCardModel.getCardListApi( apitokenStr: "apiToken=\(UserDetails.shared.getApiToken())",loadview: self)
    }
        
    //MARK: Load Setup
    func alertViewLoadSetup()  {
        alertView.backgroundColor = .alphaColor
        alertView.layer.cornerRadius = 5
        alertView.isHidden = false
        cancelButton.layer.cornerRadius = 5
        okButton.layer.cornerRadius = 5
    }
    
    
    //MARK: Load Setup
    func popupViewLoadSetup(getCardIdStr: Int)  {
        popupView.backgroundColor = UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6)
        insidepopupView.layer.cornerRadius = 5
        popupView.isHidden = false
        closedButton.layer.cornerRadius = 5
        yesButton.layer.cornerRadius = 5
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        loadSetup()
        tableViewSetup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: TableView Setup
    func tableViewSetup()  {
        setStatusBar(color: UIColor.themeColor)
        paymentSettingTableView.register(DefaultEmptyCell.nib, forCellReuseIdentifier: DefaultEmptyCell.identifier)
        paymentSettingTableView.register(PaymentDefaultCell.nib, forCellReuseIdentifier: PaymentDefaultCell.identifier)
        paymentSettingTableView.delegate = self
        paymentSettingTableView.dataSource = self
        paymentSettingTableView.tableFooterView = UIView()
        paymentSettingTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        paymentSettingTableView.showsHorizontalScrollIndicator = false
        paymentSettingTableView.showsVerticalScrollIndicator = false
        paymentSettingTableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        paymentSettingTableView.backgroundColor = .clear
        paymentSettingTableView.contentInsetAdjustmentBehavior = .never
        selectIndexpath = IndexPath(item:0, section:0)
    }
    
    @IBAction func didTabLeftAction(_ sender: Any) {
//        self.navigationController?.popViewController(animated: false)
        let story = UIStoryboard(name: "Tabbar", bundle:nil)
        let vc = story.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    @IBAction func didTapRemoveAction(_ sender: UIButton) {
        cardIdStr = getCardModel.cardList[sender.tag].id ?? 0
        alertViewLoadSetup()
    }
    
    @IBAction func didTapProceedAction(_ sender: UIButton) {
        cardIdStr = getCardModel.cardList[sender.tag].id ?? 0
        self.redirectMakeRequestView ()
    }
    
    @IBAction func didTapYesAction(_ sender: Any) {
        self.getCardRemoveListApi( apitokenStr: "apiToken=\(UserDetails.shared.getApiToken())",loadview: self, cardId: cardIdStr)
    }
    
    func getDefualtApi( loadview: UIViewController,cardId: Int) {
        LoadingIndicator.shared.show(forView: loadview.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()
        paramsDictionary["card_id"] = cardId
        HttpClientApi.instance().makeAPICall(url: "\(URL.setDefaultCard)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("dataDict",dataDict)
                let response = response
                if  response?.statusCode == 200 {
                    doOnMain {
                        LoadingIndicator.shared.hide()
                        self.alertView.isHidden = true
                        self.loadSetup()
                        self.popupView.isHidden = true
                        self.showCustomToast(toastTitle: "Info",toastMeassage: "\(dataDict.value(forKeyPath: "message") ?? "")",status: 0)
                    }
                }else if  response?.statusCode == 401 {
                    let tempArray = NSMutableArray()
                    for  i in 0..<((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.count ?? 0) {
                        let tArray  = ((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.allValues[i]) as? Array<Any>
                        tempArray.add(tArray?.first ?? "")
                    }
                    let errorStr = tempArray.componentsJoined(by: ",")
                    CustomToast.show(message: errorStr,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }
                else{
                    CustomToast.show(message: dataDict["error"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }

            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
    
    private func redirectMakeRequestView (){
        let findDoctorVC = MakeRequestViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        findDoctorVC.hidesBottomBarWhenPushed = true
        findDoctorVC.getCardId = cardIdStr
        findDoctorVC.getCartBool = true
        self.navigationController?.pushViewController(findDoctorVC, animated: false)
    }
    
    func getCardRemoveListApi( apitokenStr: String,loadview: UIViewController,cardId: Int) {
        LoadingIndicator.shared.show(forView: loadview.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()
        paramsDictionary["card_id"] = cardId
        HttpClientApi.instance().makeAPICall(url: "\(URL.getRemovecard)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("dataDict",dataDict)
                let response = response
                if  response?.statusCode == 200 {
                    doOnMain {
                        LoadingIndicator.shared.hide()
                        self.alertView.isHidden = true
                        self.loadSetup()
                    }
                }else if  response?.statusCode == 401 {
                    let tempArray = NSMutableArray()
                    for  i in 0..<((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.count ?? 0) {
                        let tArray  = ((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.allValues[i]) as? Array<Any>
                        tempArray.add(tArray?.first ?? "")
                    }
                    let errorStr = tempArray.componentsJoined(by: ",")
                    CustomToast.show(message: errorStr,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }
                else{
                    CustomToast.show(message: dataDict["error"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }

            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
    
    @IBAction func didTapNoAction(_ sender: Any) {
        alertView.isHidden = true
    }
    
    @IBAction func didTapAddNewCardButton(_ sender: Any) {
        let profileVC = InstantVideoAddCardViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        profileVC.hidesBottomBarWhenPushed = true
        profileVC.cardBool = true
        self.navigationController?.pushViewController(profileVC, animated: false)
    }
    
    @IBAction func didTapProceedButton(_ sender: Any) {
        self.redirectMakeRequestView ()
    }
    
    @IBAction func didTapOkButton(_ sender: Any) {
        self.getDefualtApi( loadview: self,cardId: cardIdStr)
        paymentSettingTableView.reloadData()
    }
    
    @IBAction func didTapClosedButton(_ sender: Any) {
        popupView.isHidden = true
    }
}

extension PaymentListViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getCardModel.cardList.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        let label = UILabel()
        label.frame = CGRect.init(x: 24, y: 0, width: headerView.frame.width-10, height: headerView.frame.height)
        headerView.addSubview(label)
        label.text = "Setup your card for payment"
        if ( UIDevice.current.model.range(of: "iPad") != nil){
            label.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(10.0))
        } else {
            label.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(16.0))
        }
        label.textColor = .blackColor
        
        if getCardModel.cardList.count > 0{
            headerView.frame = CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50)
        }else{
            headerView.frame = CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 0)
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let footerView = UIView.init(frame: CGRect.init(x: 24, y: 0, width: tableView.frame.size.width, height: 100))
//        let centerView = UIView.init(frame: CGRect.init(x: 24, y: 15, width: 170, height: 44))
//        centerView.backgroundColor = .themeColor
//        centerView.layer.cornerRadius = 5
//        footerView.addSubview(centerView)
//        let label = UILabel()
//        label.frame = CGRect.init(x: 10, y: (centerView.frame.height/2)-22, width: 30, height: centerView.frame.height)
//        label.text = " + "
//        label.textColor = .white
//        centerView.addSubview(label)
//
//        let addButton = UIButton()
//        addButton.frame = CGRect.init(x: label.frame.origin.x+label.frame.size.width, y: 0, width: centerView.frame.width, height: centerView.frame.height)
//        if ( UIDevice.current.model.range(of: "iPad") != nil){
//            addButton.titleLabel?.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(10.0))
//            label.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(13.0))
//        } else {
//            addButton.titleLabel?.font = UIFont(name:Font.FontName.LatoMedium.rawValue, size: Utility.dynamicSize(15.0))
//            label.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(24.0))
//        }
//        addButton.contentHorizontalAlignment = .left
//        centerView.addSubview(addButton)
//        addButton.setTitle("Add New Card", for: .normal)
//        addButton.setTitleColor(.white, for: .normal)
//        addButton.addTarget(self, action: #selector(self.didSelectRowIndex), for: .touchUpInside)
//        return footerView
//    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 100
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if getCardModel.cardList[indexPath.row].default_card ?? 0 == 1 {
                return availableTableViewCell(tableView: tableView, indexPath: indexPath)
            }else{
                return emptyTableViewCell(tableView: tableView, indexPath: indexPath)
            }
    }
    
    func emptyTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DefaultEmptyCell.identifier) as? DefaultEmptyCell else {
            return UITableViewCell()
        }
        cell.selectRadioImage(getIndexpath: selectIndexpath,normalIndex: indexPath)
        cell.getCardModel =  getCardModel.cardList[indexPath.row]
        return cell
    }
    
    func availableTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentDefaultCell.identifier) as? PaymentDefaultCell else {
            return UITableViewCell()
        }
        cell.selectRadioImage(getIndexpath: selectIndexpath,normalIndex: indexPath)
        cell.getCardModel =  getCardModel.cardList[indexPath.row]
        cell.proceedButton.addTarget(self, action: #selector(self.didTapProceedAction), for: .touchUpInside)
        cell.proceedButton.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectIndexpath = indexPath
        cardIdStr = getCardModel.cardList[indexPath.row].id ?? 0
//        getDefualtApi( loadview: self,cardId: cardIdStr)
//        paymentSettingTableView.reloadData()
        popupViewLoadSetup(getCardIdStr: cardIdStr)
    }
    
    @objc func didSelectRowIndex(sender : UIButton) {
        let profileVC = InstantVideoAddCardViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        profileVC.hidesBottomBarWhenPushed = true
        profileVC.cardBool = true
        self.navigationController?.pushViewController(profileVC, animated: false)
    }
}
