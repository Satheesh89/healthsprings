//
//  ArticlesWebViewController.swift
//  Healthsprings
//
//  Created by Personal on 05/12/22.
//

import UIKit
import WebKit

class ArticlesWebViewController: UIViewController,WKUIDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    var titleStr: String = ""
    var webViewUrl: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setStatusBar(color: UIColor.themeColor)
        titleLabel.text = titleStr
        LoadingIndicator.shared.show(forView: view)
        let url = URL(string: webViewUrl)
        webView.load(URLRequest(url: url!))
        delay(1.0) {
            LoadingIndicator.shared.hide()
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    // MARK: - Left Action
    @IBAction func didTapleftAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}


extension ArticlesWebViewController: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("Finished loading")
    }
}
