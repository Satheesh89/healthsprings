//
//  FAQViewModel.swift
//  Healthsprings
//
import Foundation
import ObjectMapper
import Alamofire
import SwiftyJSON

class FAQViewModel {
    typealias faqDetails = (_ itemBillDetails: FAQModel?,_ status: Bool,_ message: String) -> Void
    var callBackFAQDetails: faqDetails?
    func getFAQDetails() {
        AF.request(URL.getFaqs, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil).response { (responseData) in
            if let data = responseData.data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("UpcomingModel >",dataDict)
                if  responseData.response?.statusCode == 200 {
                    self.callBackFAQDetails?(FAQModel(JSON(dataDict) ), true,"")
                }else{
                    self.callBackFAQDetails?(nil, false, "error")
                }
            }
        }
    }
    
    func completionHandler(callBack: @escaping faqDetails) {
        self.callBackFAQDetails = callBack
    }
}
