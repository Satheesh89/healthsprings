//
//  ConfirmPopupViewController.swift
//  Healthsprings
//
//  Created by Personal on 11/12/22.
//

import UIKit

class ConfirmPopupViewController: UIViewController {
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var bottomUpView: UIView!
    
    @IBOutlet weak var requiredView: UIView!
    @IBOutlet weak var notRequiredView: UIView!

    @IBOutlet weak var medicineRequiedLabel: UILabel!
    @IBOutlet weak var medicineRequiedDetailLabel: UILabel!

    @IBOutlet weak var notMedicineRequiedLabel: UILabel!
    @IBOutlet weak var notMedicineRequiedDetailLabel: UILabel!

    @IBOutlet weak var medicineRequiedButton: UIButton!
    @IBOutlet weak var notMedicineRequiedDetailButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!

    var getRequestDetailModel: RequestDetailModel!
    var getRequestdetail: [Requestdetail]?

    var selectTime = String()
    var selectDate = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

//        popupView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        requiredView.layer.cornerRadius = 5
        notRequiredView.layer.cornerRadius = 5
        requiredView.layer.borderWidth = 1
        requiredView.layer.borderColor = UIColor.lightGray.cgColor
        notRequiredView.layer.borderWidth = 1
        notRequiredView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func didTapMedicineAction(_ sender: UIButton) {
        if sender.tag == 500{
            medicineRequiedButton.setImage(UIImage(named: "radiocheck"), for: .normal)
            notMedicineRequiedDetailButton.setImage(UIImage(named: "radiouncheck"), for: .normal)
            requiredView.layer.borderWidth = 1
            requiredView.layer.borderColor = UIColor.themeColor.cgColor
            notRequiredView.layer.borderWidth = 1
            notRequiredView.layer.borderColor = UIColor.lightGray.cgColor
        }else if sender.tag == 501{
            medicineRequiedButton.setImage(UIImage(named: "radiouncheck"), for: .normal)
            notMedicineRequiedDetailButton.setImage(UIImage(named: "radiocheck"), for: .normal)
            requiredView.layer.borderWidth = 1
            requiredView.layer.borderColor = UIColor.lightGray.cgColor
            notRequiredView.layer.borderWidth = 1
            notRequiredView.layer.borderColor = UIColor.themeColor.cgColor
        }
     }
    
    @IBAction func didTapCloseAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }

    @IBAction func didTapConfirmAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
//        let requestSummaryVC = RequestSummaryViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
//        requestSummaryVC.hidesBottomBarWhenPushed = true
//        requestSummaryVC.getRequestDetailModel = getRequestDetailModel
//        requestSummaryVC.getRequestdetail = getRequestdetail
//        self.navigationController?.pushViewController(requestSummaryVC, animated: false)
    }
}
