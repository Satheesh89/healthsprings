

import Foundation
struct Itemised_bill : Codable {
	let video_id : String?
	let items : [ItemsBill]?
	let sub_total : String?
	let tax_service : Int?
	let total_paid : Int?
	let payment_date : String?
	let card_no : Int?

	enum CodingKeys: String, CodingKey {

		case video_id = "video_id"
		case items = "items"
		case sub_total = "sub_total"
		case tax_service = "tax_service"
		case total_paid = "total_paid"
		case payment_date = "payment_date"
		case card_no = "card_no"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		video_id = try values.decodeIfPresent(String.self, forKey: .video_id)
		items = try values.decodeIfPresent([ItemsBill].self, forKey: .items)
		sub_total = try values.decodeIfPresent(String.self, forKey: .sub_total)
		tax_service = try values.decodeIfPresent(Int.self, forKey: .tax_service)
		total_paid = try values.decodeIfPresent(Int.self, forKey: .total_paid)
		payment_date = try values.decodeIfPresent(String.self, forKey: .payment_date)
		card_no = try values.decodeIfPresent(Int.self, forKey: .card_no)
	}

}
