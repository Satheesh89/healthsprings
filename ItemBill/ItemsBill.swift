
import Foundation
struct ItemsBill : Codable {
	let name : String?
	let qty : String?
	let price : String?

	enum CodingKeys: String, CodingKey {

		case name = "name"
		case qty = "qty"
		case price = "price"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		qty = try values.decodeIfPresent(String.self, forKey: .qty)
		price = try values.decodeIfPresent(String.self, forKey: .price)
	}

}
