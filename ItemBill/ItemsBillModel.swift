
import Foundation

struct ItemsBillModel : Codable {
	let itemised_bill : Itemised_bill?
	enum CodingKeys: String, CodingKey {
		case itemised_bill = "itemised_bill"
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		itemised_bill = try values.decodeIfPresent(Itemised_bill.self, forKey: .itemised_bill)
	}
}
