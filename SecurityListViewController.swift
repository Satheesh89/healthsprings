//
//  SecurityListViewController.swift
//  Healthsprings
//

import UIKit
class SecurityListViewController: UIViewController {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var setcurityTableView: UITableView!
    
    var TitleLblRef = NSArray()
    var leftIconArray = NSArray()
    var selectIndexpath : IndexPath = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initializeHideKeyboard()
        TitleLblRef = ["Screen Lock","Enable Touch ID","Enable Face ID","Change Password"]
        leftIconArray = ["mobile-lock","fingerprint","face-id","key"]
        tableViewSetup()
        selectIndexpath = IndexPath(item:7, section:0)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: TableView Setup
    func tableViewSetup()  {
        setStatusBar(color: UIColor.themeColor)
        setcurityTableView.delegate = self
        setcurityTableView.dataSource = self
        setcurityTableView.tableFooterView = UIView()
        setcurityTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        setcurityTableView.showsHorizontalScrollIndicator = false
        setcurityTableView.showsVerticalScrollIndicator = true
        setcurityTableView.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
        setcurityTableView.backgroundColor = .clear
        setcurityTableView.contentInsetAdjustmentBehavior = .never
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func didTapLeftAction(_ sender: Any) {
        self.navigationController?.popToViewController(ofClass: SettingsViewController.self)
    }
}

extension SecurityListViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TitleLblRef.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getSettingTableViewCell(tableView: tableView, indexPath: indexPath)
    }
    func getSettingTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? SecurityListCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.separatorInset = UIEdgeInsets(top: 0, left: cell.bounds.size.width, bottom: 0, right: 0)
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        cell.TitleLblRef.text = self.TitleLblRef[indexPath.row] as? String
        cell.leftImgRef.image = UIImage.init(named:leftIconArray[indexPath.row] as! String)
        cell.leftImgRef.setImageColor(color: UIColor.convertRGB(hexString: "#215460"))
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: cell.frame.size.width))
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(self.buttonTapped), for: .touchUpInside)
        button.tag = indexPath.row
        cell.addSubview(button)
        cell.bgView.backgroundColor = .white
        
        cell.bgView.layer.cornerRadius = 5
        cell.bgView.layer.borderWidth = 1
        if selectIndexpath.row == indexPath.row{
            cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#A2D6E3").cgColor
        }else{
            //cell.bgView.viewBorderShadow()
            cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#E6E6E6").cgColor
        }
        if indexPath.row == 3{
            cell.arrowImgRef.isHidden = false
            cell.arrowImgRef.image = UIImage(named: "themeColorRightArrow")
            cell.TitleImgRef.isHidden = true
        }else{
            cell.TitleImgRef.isHidden = false
            cell.arrowImgRef.isHidden = true
            cell.TitleImgRef.image = UIImage(named: "switch_off")
        }
        return cell
    }
    
    @objc func buttonTapped(sender : UIButton) {
        selectIndexpath = IndexPath(item: sender.tag, section:0)
        setcurityTableView.reloadData()
        delayWithSeconds(0.5) {
            if sender.tag == 3{
                doOnMain{
                    self.tabBarController?.tabBar.isHidden = true
                    var menuStoryboard = UIStoryboard(name: "Setting", bundle: nil)
                    var viewName = UIViewController()
                    viewName = menuStoryboard.instantiateViewController(withIdentifier: "PinSettingViewController") as! PinSettingViewController
                    self.navigationController?.pushViewController(viewName, animated: false)
                }
            }else{
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}


