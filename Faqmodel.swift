import Foundation

struct Faqmodel : Codable {
	let category_name : String?
	let question : String?
	let answer : String?
    
    var expanded: Bool

	enum CodingKeys: String, CodingKey {
		case category_name = "category_name"
		case question = "question"
		case answer = "answer"
        case expanded = "expanded"
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		category_name = try values.decodeIfPresent(String.self, forKey: .category_name)
		question = try values.decodeIfPresent(String.self, forKey: .question)
		answer = try values.decodeIfPresent(String.self, forKey: .answer)
        expanded = false
	}
}
