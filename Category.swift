//
//  Category.swift
//

import Foundation
import SwiftyJSON

struct Category {
    let categoryName: String?
    let faq: [Faq]?

    init(_ json: JSON) {
        categoryName = json["category_name"].stringValue
        faq = json["faq"].arrayValue.map { Faq($0) }
    }
}
