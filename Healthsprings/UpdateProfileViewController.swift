//
//  UpdateProfileViewController.swift
//  Healthsprings
//
//  Created by Personal on 17/06/22.
//

import UIKit
import SwiftyJSON

class UpdateProfileViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var mobileNoTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var postalCodeTextField: UITextField!
    @IBOutlet weak var flatNoTextField: UITextField!
    @IBOutlet weak var areaTextField: UITextField!
    @IBOutlet weak var profileUpdateButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var mobileView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var postalCodeView: UIView!
    @IBOutlet weak var flatView: UIView!
    @IBOutlet weak var areaView: UIView!
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var countryLabel: UILabel!
    
    let countrycodepicker = UIPickerView()
    var countryModel: CountryModel!
    var countryCode: [Code] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initializeHideKeyboard()
        textFieldSetup()
    }
    

    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - loadJsonData
    func loadJsonData()  {
        if let localData = self.readLocalFile(forName: "Country") {
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: localData, options: .mutableContainers)
                self.countryModel = CountryModel(JSON(jsonResponse))
                self.countryCode = self.countryModel?.countryDetail?.code ?? []
                print(self.countryCode.count)
            }catch let error{
                print(error)
            }
        }
    }
    // MARK: - readLocalFile
    private func readLocalFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name,
                                                 ofType: "json"),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        return nil
    }
    
    // MARK: - TextField Setup
    func textFieldSetup(){
        emailTextField.delegate = self
        nameTextField.delegate = self
        mobileNoTextField.delegate = self
        passwordTextField.delegate = self
        countryTextField.delegate = self
        postalCodeTextField.delegate = self
        flatNoTextField.delegate = self
        areaTextField.delegate = self
        
        emailTextField.tag = 100
        nameTextField.tag = 101
        mobileNoTextField.tag = 102
        passwordTextField.tag = 103
        countryTextField.tag = 104
        postalCodeTextField.tag = 105
        flatNoTextField.tag = 106
        areaTextField.tag = 107
        
        self.countryCodeTextField.delegate = self
        self.countryCodeTextField.inputView = self.countrycodepicker
        self.countryCodeTextField.tintColor = .clear
        self.countrycodepicker.dataSource = self
        self.countrycodepicker.delegate = self
        self.countrycodepicker.backgroundColor = .themeColor
        loadJsonData()
    }
    
    // MARK: - Profile Action
    @IBAction func didTabProfileAction(_ sender: Any) {
    }

    // MARK: - Skip Action
    @IBAction func didTapSkipAction(_ sender: Any) {
    }
    
}


extension UpdateProfileViewController {
    
    // MARK: - TextField BeginEditing
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.viewStatus(status: textField.tag)
    }
    
    // MARK: - TextField Begin
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewRemoveStatus(status: textField.tag)
    }
    
    // MARK: - TextField Add border
    func viewStatus(status: Int)  {
        if status == 100{
            emailView.layer.borderColor = UIColor.themeColor.cgColor
            emailView.layer.borderWidth = 1.0
        }else if status == 101{
            nameView.layer.borderColor = UIColor.themeColor.cgColor
            nameView.layer.borderWidth = 1.0
        }else if status == 102{
            mobileView.layer.borderColor = UIColor.themeColor.cgColor
            mobileView.layer.borderWidth = 1.0
        }else if status == 103{
            passwordView.layer.borderColor = UIColor.themeColor.cgColor
            passwordView.layer.borderWidth = 1.0
        }else if status == 104{
            countryView.layer.borderColor = UIColor.themeColor.cgColor
            countryView.layer.borderWidth = 1.0
        }else if status == 105{
            postalCodeView.layer.borderColor = UIColor.themeColor.cgColor
            postalCodeView.layer.borderWidth = 1.0
        }else if status == 106{
            flatView.layer.borderColor = UIColor.themeColor.cgColor
            flatView.layer.borderWidth = 1.0
        }else if status == 107{
            areaView.layer.borderColor = UIColor.themeColor.cgColor
            areaView.layer.borderWidth = 1.0
        }
    }
    
    // MARK: - TextField border Remove
    func viewRemoveStatus(status: Int)  {
        if status == 100{
            emailView.layer.borderWidth = 0.0
        }else if status == 101{
            nameView.layer.borderWidth = 0.0
        }else if status == 102{
            mobileView.layer.borderWidth = 0.0
        }else if status == 103{
            passwordView.layer.borderWidth = 0.0
        }else if status == 104{
            countryView.layer.borderWidth = 0.0
        }else if status == 105{
            postalCodeView.layer.borderWidth = 0.0
        }else if status == 106{
            flatView.layer.borderWidth = 0.0
        }else if status == 107{
            areaView.layer.borderWidth = 0.0
        }
    }
}


extension UpdateProfileViewController:  UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.countryCode.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(20.0))
            pickerLabel?.textAlignment = .center
        }
        pickerLabel?.text = countryCode[row].dialCode
        pickerLabel?.textColor = UIColor.white
        return pickerLabel!
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.countryLabel.text = "\(self.countryCode[row].dialCode ?? "")"
    }
}


