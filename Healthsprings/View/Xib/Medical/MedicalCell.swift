//
//  MedicalCell.swift
//  MVVM+Json
//
//  Created by Yogesh Patel on 4/8/20.
//  Copyright © 2020 Yogesh Patel. All rights reserved.
//

import UIKit

class MedicalCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var detailLabel: UILabel!
  
    var medicalModel: Symptoms?{
        didSet{
            userConfiguration()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func userConfiguration(){
        titleLabel.text = medicalModel?.name
        detailLabel.text = medicalModel?.details
    }
    
    static var nib: UINib {
      return UINib(nibName: "MedicalCell", bundle: nil)
    }
    
    static var identifier: String {
      return String(describing: self)
    }
    
}
