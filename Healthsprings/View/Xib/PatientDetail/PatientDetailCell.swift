//
//  PatientDetailCell.swift
//  Healthsprings
//
//  Created by Personal on 30/06/22.
//

import UIKit

class PatientDetailCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var patientLabel: UILabel!
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var symptomsTitleLabel: UILabel!
    @IBOutlet weak var symptomsLabel: UILabel!
    @IBOutlet weak var patientLocationLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var totalPaidLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var dateandtimeLabel: UILabel!
    @IBOutlet weak var paidcardLabel: UILabel!
    @IBOutlet weak var cardNoLabel: UILabel!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var rescheduleView: UIView!
    @IBOutlet weak var paidButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var rescheduleButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        rescheduleView.layer.cornerRadius = 5
        rescheduleView.clipsToBounds = false
        paidButton.layer.cornerRadius = 5
        paidButton.clipsToBounds = false
        rescheduleView.layer.borderWidth = 1
        rescheduleView.layer.borderColor = UIColor.themeColor.cgColor
        cancelView.layer.cornerRadius = 5
        cancelView.clipsToBounds = false
        cancelView.layer.borderWidth = 1
        cancelView.layer.borderColor = UIColor.lightGray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    static var nib: UINib {
      return UINib(nibName: "PatientDetailCell", bundle: nil)
    }
    static var identifier: String {
      return String(describing: self)
    }
}
