//
//  DeliveryTimeTableViewCell.swift
//  TabaoGo
//
//  Created by PremKumar on 01/06/18.
//  Copyright © 2018 sadeeshmac. All rights reserved.
//

import UIKit

class TimeTableViewCell: UITableViewCell {
    @IBOutlet weak var timeCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setCollectionViewDataSourceDelegate
        <D: UICollectionViewDataSource & UICollectionViewDelegate>
        (dataSourceDelegate: D, forRow row: Int) {
        
        timeCollectionView.delegate = dataSourceDelegate
        timeCollectionView.dataSource = dataSourceDelegate
        timeCollectionView.tag = row
        timeCollectionView.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
