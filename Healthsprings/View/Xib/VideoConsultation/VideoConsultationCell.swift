//
//  VideoConsultationCell.swift
//  Healthsprings
//
//  Created by Personal on 30/06/22.
//

import UIKit

class VideoConsultationCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var doctorImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var videoTitleLabel: UILabel!
    @IBOutlet weak var joinView: UIView!
    @IBOutlet weak var joinLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var circleImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bgView.layer.cornerRadius = 10
        bgView.clipsToBounds = false
//        shadowView.layer.cornerRadius = 10
//        shadowView.clipsToBounds = false
        bgView.layer.borderWidth = 1
        bgView.layer.borderColor = UIColor.convertRGB(hexString: "#E6E6E6").cgColor
        joinView.layer.cornerRadius = 5
        joinView.clipsToBounds = false
        
        circleImageView.layer.cornerRadius = circleImageView.frame.size.width / 2
        circleImageView.clipsToBounds = false
        
        doOnMain {
//            self.bgView.dropShadowWithCornerRaduis()
            self.bgView.viewBorderShadow()
        }
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib {
      return UINib(nibName: "VideoConsultationCell", bundle: nil)
    }
    
    static var identifier: String {
      return String(describing: self)
    }

}
