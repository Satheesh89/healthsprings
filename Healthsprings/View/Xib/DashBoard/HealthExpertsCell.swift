//
//  HealthExpertsCell.swift
//  Docis
//
//  Created by Personal on 03/03/22.
//

import UIKit

class HealthExpertsCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collView: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: "HealthExpertsCell", bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
