//
//  SettingHeaderTableViewCell.swift
//  Healthsprings
//
//  Created by Personal on 18/06/22.
//

import UIKit

class SettingHeaderTableViewCell: UITableViewCell {

    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profilenameLabel: UILabel!    
    @IBOutlet weak var detailLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        headerView.layer.cornerRadius = 10
        headerView.clipsToBounds = false
        headerView.layer.borderWidth = 1
        headerView.layer.borderColor = UIColor.convertRGB(hexString: "#E6E6E6").cgColor

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
      return UINib(nibName: "SettingHeaderTableViewCell", bundle: nil)
    }
    
    static var identifier: String {
      return String(describing: self)
    }
}
