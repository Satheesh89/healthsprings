//
//  SelectCardCell.swift
//  Healthsprings
//
//  Created by Personal on 01/08/22.
//

import UIKit

class SelectCardCell: UITableViewCell {
    
    @IBOutlet weak var cardNoLabel: UILabel!
    @IBOutlet weak var vaildDateLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: "SelectCardCell", bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
