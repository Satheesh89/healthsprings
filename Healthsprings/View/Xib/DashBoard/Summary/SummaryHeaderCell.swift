//
//  SummaryHeaderCell.swift
//  Healthsprings
//
//  Created by Personal on 01/08/22.
//

import UIKit

class SummaryHeaderCell: UITableViewCell {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var selectView: UIView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var symptomsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectView.layer.cornerRadius = 5
        selectView.clipsToBounds = true
        selectView.layer.borderWidth = 1
        selectView.layer.borderColor = UIColor.themeColor.cgColor
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: "SummaryHeaderCell", bundle: nil)
    }
    static var identifier: String {
        return String(describing: self)
    }
}
