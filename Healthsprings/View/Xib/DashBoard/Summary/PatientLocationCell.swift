//
//  PatientLocationCell.swift
//  Healthsprings
//
//  Created by Personal on 01/08/22.
//

import UIKit

class PatientLocationCell: UITableViewCell {
    
    @IBOutlet weak var patientView: UIView!
    @IBOutlet weak var patientLocationCollectionView: UICollectionView!
    @IBOutlet weak var patientBgview: UIView!
    
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        patientBgview.backgroundColor = .white
        patientBgview.layer.borderWidth = 1
        patientBgview.layer.borderColor = UIColor.convertRGB(hexString: "#3ED222").cgColor
        patientBgview.layer.cornerRadius = 5
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: "PatientLocationCell", bundle: nil)
    }
    static var identifier: String {
        return String(describing: self)
    }
}
