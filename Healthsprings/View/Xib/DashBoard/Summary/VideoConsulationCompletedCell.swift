//
//  VideoConsulationCompletedCell.swift
//  Healthsprings
//
//  Created by Personal on 19/09/22.
//

import UIKit

class VideoConsulationCompletedCell: UITableViewCell {
    
    
    @IBOutlet weak var bgview: UIView!
    @IBOutlet weak var appointmentPriceLabel: UILabel!
    @IBOutlet weak var shdowView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var yearExpLabel: UILabel!
    @IBOutlet weak var dateandtimeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        doOnMain {
//            self.shdowView.backgroundColor = .white
//            self.shdowView.viewBorderShadow()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: "VideoConsulationCompletedCell", bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
