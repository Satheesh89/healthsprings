//
//  DocumentverifyCell.swift
//  Healthsprings
//
//  Created by Personal on 19/09/22.
//

import UIKit

class DocumentverifyCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var documentView: UIView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var documentTitleLabel : UILabel!
    @IBOutlet weak var documnetImageView : UIImageView!
    
    @IBOutlet weak var fullName : UITextField!
    @IBOutlet weak var documnetNumber : UITextField!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nameView.viewBorderShadow()
        documentView.viewBorderShadow()
        buttonView.viewBorderShadow()
        uploadButton.layer.cornerRadius = 5
        uploadButton.titleLabel?.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(13.0))
        captureButton.layer.cornerRadius = 5
        captureButton.titleLabel?.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(13.0))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: "DocumentverifyCell", bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
