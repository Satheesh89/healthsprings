//
//  PrescriptionMCCell.swift
//  Healthsprings
//
//  Created by Personal on 19/09/22.
//

import UIKit

class PrescriptionMCCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var historyButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: "PrescriptionMCCell", bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
