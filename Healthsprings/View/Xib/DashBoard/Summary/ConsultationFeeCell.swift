//
//  ConsultationFeeCell.swift
//  Healthsprings
//
//  Created by Personal on 01/08/22.
//

import UIKit

class ConsultationFeeCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var consultationFeeLabel: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: "ConsultationFeeCell", bundle: nil)
    }
    static var identifier: String {
        return String(describing: self)
    }
}
