//
//  IDVerificationDocumentTypeCell.swift
//  Healthsprings
//
//  Created by Personal on 19/09/22.
//

import UIKit
import iOSDropDown

class IDVerificationDocumentTypeCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var typeDropView: UIView!
    @IBOutlet weak var documentDropdown: DropDown!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        typeDropView.viewBorderShadow()

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: "IDVerificationDocumentTypeCell", bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
