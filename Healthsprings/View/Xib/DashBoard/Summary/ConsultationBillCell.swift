//
//  ConsultationBillCell.swift
//  Healthsprings
//
//  Created by Personal on 19/09/22.
//

import UIKit

class ConsultationBillCell: UITableViewCell {
    
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var lineLabel: UILabel!
    @IBOutlet weak var bottomVIew: UIView!
    @IBOutlet weak var paidButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var swipeButton: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.bottomVIew.layer.masksToBounds = false
        self.bottomVIew?.layer.shadowColor = UIColor.blackColor.cgColor
        self.bottomVIew?.layer.shadowOffset =  CGSize.zero
        self.bottomVIew?.layer.shadowOpacity = 0.5
        self.bottomVIew?.layer.shadowRadius = 4
        self.bottomVIew?.backgroundColor = .white
        
        doOnMain {
            self.paidButton.layer.cornerRadius = 5
            self.lineLabel.layer.cornerRadius = self.lineLabel.frame.size.height / 2
            self.lineLabel.backgroundColor = UIColor.appDBBorderColor
            self.lineLabel.clipsToBounds = true
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: "ConsultationBillCell", bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
