//
//  MedicalServicesCell.swift
//  Docis
//
//  Created by Personal on 03/03/22.
//

import UIKit

class MedicalServicesCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var doctorView: UIView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var findDoctorImageView: UIImageView!
    @IBOutlet weak var instantVideoImageView: UIImageView!
    @IBOutlet weak var findDoctorButton: UIButton!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var testLabel: UILabel!
    @IBOutlet weak var testDetailLabel: UILabel!
    @IBOutlet weak var findDoctorBtn: UIButton!
    @IBOutlet weak var videoButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        testLabel.font = UIFont(name:Font.FontName.PTSansBold.rawValue, size: Utility.dynamicSize(16.0))
//        testDetailLabel.font = UIFont(name:Font.FontName.PTSansBold.rawValue, size: Utility.dynamicSize(12.0))
        
        
        testLabel.text = "COVID 19 TESTING AT YOUR HOME"
        testLabel.textColor = UIColor.blackColor
        let durationattributedWithTextColor: NSAttributedString = "COVID 19 TESTING AT YOUR HOME".attributedStringWithColor(["COVID 19 TESTING"], color: UIColor.themeColor)
        testLabel.attributedText = durationattributedWithTextColor


        view1.layer.cornerRadius = 10
        view2.layer.cornerRadius = 10

//        view1.clipsToBounds = true
//        view1.layer.cornerRadius = 100
//        view1.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
//
//        view2.clipsToBounds = true
//        view2.layer.cornerRadius = 100
//        view2.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
//
//
//        view3.clipsToBounds = true
//        view3.layer.cornerRadius = 100
//        view3.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
//
        
//        doOnMain {
//            self.setHorizontalGradientColor(view: self.stautsBgView)
//            self.stautsBgView.layer.masksToBounds = false
//            self.stautsBgView.layer.shadowRadius = 4
//            self.stautsBgView.layer.shadowOpacity = 0.5
//            self.stautsBgView.layer.shadowColor = UIColor.themeColor.cgColor
//            self.stautsBgView.layer.shadowOffset = CGSize(width: 0 , height:1)
//            self.stautsBgView.layer.cornerRadius = 5
//            self.stautsBgView.backgroundColor = .clear
//        }
        doOnMain {
            self.statusView.layer.cornerRadius = 5
            self.statusView.layer.borderWidth = 0.5
            self.statusView.layer.borderColor = UIColor.themeColor.cgColor
            self.statusView.backgroundColor = .white
            self.setHorizontalGradientColor(view: self.statusView)
        }
    }
    
    func setHorizontalGradientColor(view: UIView) {
        let gradientLayer = CAGradientLayer()
        var updatedFrame = view.bounds
        updatedFrame.size.height += 20
        gradientLayer.frame = updatedFrame
        gradientLayer.colors = [UIColor.white.cgColor, UIColor.themeColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.2)
        gradientLayer.endPoint = CGPoint(x: 4.0, y: 0.5)
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: "MedicalServicesCell", bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async {
            self.view1.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10.0)
            self.view2.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10.0)

            self.findDoctorImageView.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
            self.instantVideoImageView.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
            
            if self.findDoctorImageView.frame.size.width > self.findDoctorImageView.frame.size.height {
                self.findDoctorImageView.contentMode = .scaleAspectFit
                //since the width > height we may fit it and we'll have bands on top/bottom
            } else {
                self.findDoctorImageView.contentMode = .scaleToFill
              //width < height we fill it until width is taken up and clipped on top/bottom
            }

            //            self.findDoctorImageView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 100.0)
//            self.instantVideoImageView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 100.0)
//            self.remoteHealthImageView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 100.0)
//            self.aboutImageView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 100.0)
//
//
//            self.findDoctorImageView.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
//            self.instantVideoImageView.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
//            self.remoteHealthImageView.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
//            self.aboutImageView.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
            
//            self.doctorView.layer.borderWidth = 0.5
//            self.doctorView.layer.cornerRadius = 2
//            self.doctorView.layer.borderColor = UIColor.themeColor.cgColor
//
//            self.videoView.layer.borderWidth = 0.5
//            self.videoView.layer.cornerRadius = 2
//            self.videoView.layer.borderColor = UIColor.themeColor.cgColor
//
//            self.aboutView.layer.borderWidth = 0.5
//            self.aboutView.layer.cornerRadius = 2
//            self.aboutView.layer.borderColor = UIColor.themeColor.cgColor
//
//            self.remoteView.layer.borderWidth = 0.5
//            self.remoteView.layer.cornerRadius = 2
//            self.remoteView.layer.borderColor = UIColor.themeColor.cgColor
        }
    }
    
}
