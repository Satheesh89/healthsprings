//
//  Header Cell.swift
//  Docis
//
//  Created by Personal on 11/03/22.
//

import UIKit
import SDWebImage

class HeaderCell: UITableViewCell {

    @IBOutlet weak var HeaderCell: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    
    override func awakeFromNib() {
          super.awakeFromNib()
          // Initialization code
        profileImageView.layoutIfNeeded()
//        profileImageView.layer.cornerRadius = profileImageView.frame.height / 2
//        profileImageView.clipsToBounds = true
//        profileImageView.layer.borderWidth = 1
//        profileImageView.layer.borderColor = UIColor.themeColor.cgColor
      }

      override func setSelected(_ selected: Bool, animated: Bool) {
          super.setSelected(selected, animated: animated)
          // Configure the view for the selected state
      }
    
    static var nib: UINib {
      return UINib(nibName: "HeaderCell", bundle: nil)
    }
    
    func cellCofig(ttitle: String,imageUrl: String){
        detailsLabel.text = ttitle
//        profileImageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "accountplaceholder"))
    }
    
    static var identifier: String {
      return String(describing: self)
    }
}
