//
//  HistoryTableViewCell.swift
//  Healthsprings
//
//  Created by Personal on 18/06/22.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var symptomsLabel: UILabel!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var patientLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    var button = UIButton()

    var historyDetails: History_details?{
        didSet{
            userConfiguration()
        }
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        doOnMain {
//            self.bgView.dropShadowWithCornerRaduis()
            self.bgView.backgroundColor = .white
            self.bgView.viewBorderShadow()
        }
        button = UIButton(frame: CGRect(x: 0, y: 0, width: bgView.frame.size.width, height: bgView.frame.size.width))
        button.backgroundColor = .clear
        bgView.addSubview(button)

//        bgView.layer.cornerRadius = 10
//        bgView.clipsToBounds = false
//        bgView.layer.borderWidth = 1
//        bgView.layer.borderColor = UIColor.convertRGB(hexString: "#E6E6E6").cgColor
//
    }
    
    func userConfiguration(){
        symptomsLabel.text = "\(historyDetails?.disease ?? "")"
        doctorNameLabel.text = "Dr. \(historyDetails?.doctor ?? "")"
        
        doctorNameLabel.textColor = UIColor.convertRGB(hexString: "#969696")
        patientLabel.textColor = UIColor.convertRGB(hexString: "#969696")
        let attributedWithTextColor: NSAttributedString = "Patient: \(historyDetails?.patient ?? "")".attributedStringWithColor(["\(historyDetails?.patient ?? "")"], color: UIColor.black)
        patientLabel.attributedText = attributedWithTextColor

        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd MM yyyy"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd"
        let date: NSDate? = dateFormatterGet.date(from: "\(historyDetails?.appointment_date ?? "")") as NSDate?
        dayLabel.text = dateFormatterPrint.string(from: date! as Date)
        
        let dateFormatterGet1 = DateFormatter()
        dateFormatterGet1.dateFormat = "dd MM yyyy"
        let dateFormatterPrint1 = DateFormatter()
        dateFormatterPrint1.dateFormat = "MMM"
        let date1: NSDate? = dateFormatterGet1.date(from: "\(historyDetails?.appointment_date ?? "")") as NSDate?
        monthLabel.text = dateFormatterPrint1.string(from: date1! as Date)
        
        let dateFormatterGet2 = DateFormatter()
        dateFormatterGet2.dateFormat = "dd MM yyyy"
        let dateFormatterPrint2 = DateFormatter()
        dateFormatterPrint2.dateFormat = "yyyy"
        let date2: NSDate? = dateFormatterGet2.date(from: "\(historyDetails?.appointment_date ?? "")") as NSDate?
        yearLabel.text = dateFormatterPrint2.string(from: date2! as Date)
        
//        let dateFormatter3 = DateFormatter()
////        dateFormatter3.dateFormat = "HH:mm:ss"
//        dateFormatter3.dateFormat = "hh:mm:ss a"
//        let dateFromStr = dateFormatter3.date(from: "\(historyDetails?.appointment_time ?? "")")
//        print("dateFromStr",dateFromStr)
        

        let dateFormatterGet5 = DateFormatter()
        dateFormatterGet5.dateFormat = "HH:mm:ss"

        let dateFormatterPrint4 = DateFormatter()
        dateFormatterPrint4.dateFormat = "h:mm a"

        var timeStr = ""
        if let date = dateFormatterGet5.date(from: "\(historyDetails?.appointment_time ?? "")") {
            print(dateFormatterPrint4.string(from: date))
            timeStr = dateFormatterPrint4.string(from: date)
        } else {
            timeStr = "\(historyDetails?.appointment_time ?? "")"
           print("There was an error decoding the string")
        }
//

        
        timeLabel.textColor = UIColor.blackColor
        let durationattributedWithTextColor: NSAttributedString = "Time: \(timeStr)    Duration: \(historyDetails?.duration ?? "") Mnts".attributedStringWithColor(["Time: ","    Duration: "], color: UIColor.convertRGB(hexString: "#969696"))
        timeLabel.attributedText = durationattributedWithTextColor

        print("duration = \(historyDetails?.duration ?? "")")


//
//        let dateFormatterGet3 = DateFormatter()
//        dateFormatterGet3.dateFormat = "dd MM yyyy"
//        let dateFormatterPrint = DateFormatter()
//        dateFormatterPrint3.dateFormat = "yyyy"
//        let date3: NSDate? = dateFormatterGet3.date(from: "\(historyDetails?.appointment_date ?? "")") as NSDate?
//        yearLabel.text = dateFormatterPrint3.string(from: date3! as Date)

        
    }
    


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    static var nib: UINib {
      return UINib(nibName: "HistoryTableViewCell", bundle: nil)
    }
    
    static var identifier: String {
      return String(describing: self)
    }
}
