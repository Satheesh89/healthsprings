//
//  DashBoardSearchDoctorCell.swift
//  Docis
//
//  Created by Personal on 02/03/22.
//

import UIKit

class DashBoardSearchDoctorCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var filterView: UIView!

    override func awakeFromNib() {
          super.awakeFromNib()
          // Initialization code
        
        searchView.dropShadowWithCornerRaduisWithColor(setColor: .themeColor)
        filterView.dropShadowWithCornerRaduisWithColor(setColor: .lightGray)

//        searchView.backgroundColor = .white
//        searchView.layer.borderWidth = 0.6
//        searchView.layer.cornerRadius = 5
//        searchView.layer.borderColor = UIColor.themeColor.cgColor
//
//        filterView.backgroundColor = .white
//        filterView.layer.borderWidth = 0.6
//        filterView.layer.cornerRadius = 5
//        filterView.layer.borderColor = UIColor.filterBgColor.cgColor
      }
      override func setSelected(_ selected: Bool, animated: Bool) {
          super.setSelected(selected, animated: animated)
          // Configure the view for the selected state
      }
    static var nib: UINib {
      return UINib(nibName: "DashBoardSearchDoctorCell", bundle: nil)
    }
    static var identifier: String {
      return String(describing: self)
    }
  }
