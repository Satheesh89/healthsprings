//
//  AppointmentTableViewCell.swift
//  Healthsprings
//
//  Created by Personal on 18/06/22.
//

import UIKit
class AppointmentTableViewCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var doctorImageView: UIImageView!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var patientLabel: UILabel!
    @IBOutlet weak var patientHoursLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var joinView: UIView!

    
    var button = UIButton()
    
    var upcomingDetails: UpcomingDetails?{
        didSet{
            cellConfiguration()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
        joinView.layer.cornerRadius = 5
        joinView.clipsToBounds = false
        joinView.backgroundColor = .themeColor
        
        button = UIButton(frame: CGRect(x: 0, y: 0, width: bgView.frame.size.width, height: bgView.frame.size.width))
        button.backgroundColor = .clear
        bgView.addSubview(button)
        
        doOnMain {
            self.bgView.viewBorderShadow()
        }
//        doOnMain {
//            self.bgView.viewBorderShadow()
//            self.bgView.layer.cornerRadius = 5
//            self.bgView.layer.shadowColor = UIColor.convertRGB(hexString: "#FFFFFF").cgColor
//            self.bgView.layer.shadowOffset = .zero
//            self.bgView.layer.shadowOpacity = 0.9
//            self.bgView.layer.shadowRadius = 10
//            self.bgView.layer.borderWidth = 1
//            self.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#D6D6D6").cgColor

//            self.bgView.dropShadowWithCornerRaduisWithColor(setColor: UIColor.convertRGB(hexString: "#A2D6E3"))
//            self.bgView.backgroundColor = .white
//            self.bgView.layer.cornerRadius = 5
//            self.bgView.clipsToBounds = false
//            self.bgView.layer.borderWidth = 1
//            self.bgView.layer.borderColor = UIColor.lightGray.cgColor
//        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: "AppointmentTableViewCell", bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    func cellConfiguration(){
        doctorNameLabel.text = "Dr \(upcomingDetails?.doctorName ?? "")"
        detailLabel.text = "\(upcomingDetails?.doctorDepartment ?? "")"
        //        hoursLabel.text = ""
        patientLabel.text = "\(upcomingDetails?.patientName ?? "")"
        //        patientHoursLabel.text = "\(upcomingDetails?.disease ?? "")"
        if  "\(upcomingDetails?.appointmentFromTime ?? "")" == ""{
            dateLabel.text = ""
        }else{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd MMM hh:mm a"
            let date: NSDate? = dateFormatterGet.date(from: "\(upcomingDetails?.appointmentFromTime ?? "")") as NSDate?
            dateLabel.text = dateFormatterPrint.string(from: date! as Date)
        }
        let status = self.getStatusColor()
        joinView.backgroundColor = status.1
        
        
        if  "\(upcomingDetails?.appointmentStatus ?? "")" == "In Queue"{
            joinView.backgroundColor = status.1
        }else if  "\(upcomingDetails?.appointmentStatus ?? "")" == "Completed"{
            joinView.backgroundColor = status.1
        }else if  "\(upcomingDetails?.appointmentStatus ?? "")" == "Cancel"{
            joinView.backgroundColor = .red
        }else if  "\(upcomingDetails?.appointmentStatus ?? "")" == "Completed"{
            joinView.backgroundColor = status.1
        }
    }
    
    //MARK: StatusColor
    func getStatusColor() -> (String, UIColor){
        if upcomingDetails?.video_consultation_available ?? 0 == 1{
            return ("COMPLETED", UIColor.themeColor)
        }else{
            return ("NOT COMPLETED", UIColor.joinStatusBgColor)
        }
    }

    //MARK: Status Index
    func getIndexStatusColor(getIndexpath: IndexPath,normalIndex: IndexPath){
        doOnMain {
            if getIndexpath == normalIndex{
                self.bgView.backgroundColor = .white
                self.bgView.layer.cornerRadius = 5
                self.bgView.layer.shadowColor = UIColor.themeColor.cgColor
                self.bgView.layer.shadowOffset = .zero
                self.bgView.layer.shadowOpacity = 0.3
                self.bgView.layer.shadowRadius = 10
                self.bgView.layer.borderWidth = 1
                self.bgView.layer.borderColor = UIColor.themeColor.cgColor
            }else{
                self.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#E6E6E6").cgColor
            }
        }
    }
    
}
