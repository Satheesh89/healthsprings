//
//  PaymentSettingCell.swift
//  Healthsprings
//
//  Created by Personal on 30/06/22.
//

import UIKit

class PaymentSettingCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var accountNoLabel: UILabel!
    @IBOutlet weak var cardNameLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var closeImageView: UIImageView!
    @IBOutlet weak var defualtView: UIView!
    @IBOutlet weak var tickImageView: UIImageView!

    var getCardModel: GetCardModel?{
        didSet{
            cellConfiguration()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func configCell()  {
        backgroundColor = .clear
        selectionStyle = .none
        bgView.backgroundColor = .white
        bgView.layer.borderWidth = 1
        bgView.layer.cornerRadius = 10
        checkImageView.setImageColor(color: .themeColor)
    }
 
    //MARK: Select Radio Image
    func selectRadioImage(getIndexpath: IndexPath,normalIndex: IndexPath)  {
        doOnMain {
            if self.getCardModel?.default_card ?? 0 == 1{
                self.bgView.layer.borderColor = UIColor.themeColor.cgColor
                self.checkImageView.image = UIImage(named: "radiocheck")
                self.proceedButton.isHidden = false
                self.defualtView.isHidden = false
                self.removeButton.isHidden = true
                self.closeImageView.isHidden = true
                self.proceedButton.setTitleColor(.themeColor, for: .normal)
            }else{
                self.checkImageView.image = UIImage(named: "radiouncheck")
                self.bgView.layer.borderColor = UIColor.selectborderColor.cgColor
                self.proceedButton.isHidden = true
                self.defualtView.isHidden = true
                self.removeButton.isHidden = false
                self.closeImageView.isHidden = false
            }
            self.checkImageView.setImageColor(color: .themeColor)
            self.tickImageView.setImageColor(color: UIColor.convertRGB(hexString: "#669A04"))
        }
    }
    
    func cellConfiguration(){
        titleLabel.text = "\(getCardModel?.branch ?? "")"
        accountNoLabel.text = "Saving A/c No. \(getCardModel?.card_number ?? 0)"
        cardNameLabel.text = "A/c Holder Name: \(getCardModel?.cardholder_name ?? "")"
    }
    
    static var nib: UINib {
      return UINib(nibName: "PaymentSettingCell", bundle: nil)
    }
    
    static var identifier: String {
      return String(describing: self)
    }
}



