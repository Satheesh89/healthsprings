//
//  DefaultEmptyCell.swift
//  Healthsprings
//
//  Created by Personal on 17/10/22.
//

import UIKit

class DefaultEmptyCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var accountNoLabel: UILabel!
    @IBOutlet weak var cardNameLabel: UILabel!
    
    var getCardModel: GetCardModel?{
        didSet{
            cellConfiguration()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func configCell()  {
        backgroundColor = .clear
        selectionStyle = .none
        bgView.backgroundColor = .white
        bgView.layer.borderWidth = 1
        bgView.layer.cornerRadius = 10
        checkImageView.setImageColor(color: .themeColor)
    }
 
    //MARK: Select Radio Image
    func selectRadioImage(getIndexpath: IndexPath,normalIndex: IndexPath)  {
        doOnMain {
//            if getIndexpath == normalIndex {
            if self.getCardModel?.default_card ?? 0 == 1{
                self.bgView.layer.borderColor = UIColor.themeColor.cgColor
                self.checkImageView.image = UIImage(named: "radiocheck")
            }else{
                self.checkImageView.image = UIImage(named: "radiouncheck")
                self.bgView.layer.borderColor = UIColor.selectborderColor.cgColor
            }
            self.checkImageView.setImageColor(color: .themeColor)
        }
    }
    
    func cellConfiguration(){
        titleLabel.text = "\(getCardModel?.branch ?? "")"
        accountNoLabel.text = "Saving A/c No. \(getCardModel?.card_number ?? 0)"
        cardNameLabel.text = "A/c Holder Name: \(getCardModel?.cardholder_name ?? "")"
    }
    
    static var nib: UINib {
      return UINib(nibName: "DefaultEmptyCell", bundle: nil)
    }
    
    static var identifier: String {
      return String(describing: self)
    }
}
