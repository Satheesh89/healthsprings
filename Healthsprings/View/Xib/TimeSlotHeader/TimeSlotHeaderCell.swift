//
//  TimeSlotHeaderCell.swift
//  Healthsprings
//
//  Created by Personal on 13/07/22.
//

import UIKit

class TimeSlotHeaderCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var specialtyLabel: UILabel!
    @IBOutlet weak var doctorImageButton: UIImageView!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var slotCollectionView: UICollectionView!
    @IBOutlet weak var selectTimeLabel: UILabel!

      var getSearchData: SearchData?{
          didSet{
              userConfiguration()
          }
      }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        bookAppointmentButton.layer.cornerRadius = 5
//        bookAppointmentButton.clipsToBounds = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func userConfiguration(){
//        doctorNameLabel.text = "Dr \(getSearchData?.doctorName ?? "")"
//        specialtyLabel.text = getSearchData?.specialty
//        timeLabel.text = getSearchData?.nextAvailTime
//        yearLabel.text = getSearchData?.nextAvailDate
    }
    
    static var nib: UINib {
        return UINib(nibName: "TimeSlotHeaderCell", bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
