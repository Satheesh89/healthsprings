//
//  FindDoctorCell.swift
//  Healthsprings
//
//  Created by Personal on 01/07/22.
//

import UIKit

class FindDoctorCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var specialtyLabel: UILabel!
    @IBOutlet weak var nextAvilableatLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var bookAppointmentButton: UIButton!
    @IBOutlet weak var clockButton: UIImageView!
    @IBOutlet weak var doctorImageButton: UIImageView!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    
      var getSearchData: SearchDetails?{
          didSet{
              userConfiguration()
          }
      }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bookAppointmentButton.layer.cornerRadius = 5
        bookAppointmentButton.clipsToBounds = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func userConfiguration(){
        doctorNameLabel.text = "Dr \(getSearchData?.doctorName ?? "")"
        specialtyLabel.text = getSearchData?.specialty
        timeLabel.text = getSearchData?.nextAvailTime
        yearLabel.text = getSearchData?.nextAvailDate
    }
    
    static var nib: UINib {
        return UINib(nibName: "FindDoctorCell", bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
