//
//  NotificationsTableViewCell.swift
//  Healthsprings
//
//  Created by Personal on 19/06/22.
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var circleImageView: UIImageView!
    @IBOutlet weak var titleNameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var subdetailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        bgView.layer.cornerRadius = 10
//        bgView.clipsToBounds = false
//        bgView.layer.borderWidth = 1
//        bgView.layer.borderColor = UIColor.convertRGB(hexString: "#E6E6E6").cgColor
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    var getNotificationModel: NotificationsModel?{
        didSet{
            userConfiguration()
        }
    }
    
    func userConfiguration(){
        titleNameLabel.text = getNotificationModel?.sub_type
        detailLabel.text = getNotificationModel?.message
        if  "\(getNotificationModel?.created_at ?? "")" == ""{
            subdetailLabel.text = ""
        }else{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd MMM hh:mm a"
            let date: NSDate? = dateFormatterGet.date(from: "\(getNotificationModel?.created_at ?? "")") as NSDate?
            subdetailLabel.text = dateFormatterPrint.string(from: date! as Date)
        }
    }
    
    static var nib: UINib {
      return UINib(nibName: "NotificationsTableViewCell", bundle: nil)
    }
    
    static var identifier: String {
      return String(describing: self)
    }
}
