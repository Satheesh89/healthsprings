//
//  CommonCollectionViewCell.swift
//  Healthsprings
//
//  Created by Personal on 20/07/22.
//

import UIKit

class CommonCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var subBgView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var plusButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        subBgView.layer.borderWidth = 1
        subBgView.layer.borderColor = UIColor.convertRGB(hexString: "#D3D3D3").cgColor
        subBgView.layer.cornerRadius = subBgView.frame.size.height / 2
        subBgView.clipsToBounds = true
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension UIView{
    func buttonSetUp() {
        doOnMain {
            self.layer.cornerRadius = self.frame.size.height/2
            self.clipsToBounds = true
        }
    }
}

