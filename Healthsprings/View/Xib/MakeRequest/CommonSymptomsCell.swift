//
//  CommonSymptomsCell.swift
//  Healthsprings
//
//  Created by Personal on 20/07/22.
//

import UIKit

class CommonSymptomsCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var radiusView: UIView!

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var commonSymptomsCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: "CommonSymptomsCell", bundle: nil)
    }
    static var identifier: String {
        return String(describing: self)
    }
}
