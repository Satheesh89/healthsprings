//
//  AddReasonCell.swift
//  Healthsprings
//
//  Created by Personal on 20/07/22.
//

import UIKit

class AddReasonCell: UITableViewCell, UITextFieldDelegate {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var patientView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var addReasonTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        addView.dropShadowWithCornerRaduis()
//        patientView.dropShadowWithCornerRaduis()
        shadowView.dropShadowWithCornerRaduis()
        addReasonTextField.delegate = self
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: "AddReasonCell", bundle: nil)
    }
    static var identifier: String {
        return String(describing: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
