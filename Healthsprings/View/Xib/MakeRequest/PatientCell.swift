//
//  PatientCell.swift
//  Healthsprings
//
//  Created by Personal on 20/07/22.
//
import UIKit
import iOSDropDown

class PatientCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var patientView: UIView!
    @IBOutlet weak var patientDropDown: DropDown!
    @IBOutlet weak var addButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addView.dropShadowWithCornerRaduis()
        patientView.dropShadowWithCornerRaduis()
        addView.layer.borderWidth = 1
        addView.layer.borderColor = UIColor.themeColor.cgColor
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: "PatientCell", bundle: nil)
    }
    static var identifier: String {
        return String(describing: self)
    }
}
