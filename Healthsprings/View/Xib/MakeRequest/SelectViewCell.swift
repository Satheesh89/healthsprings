//
//  SelectViewCell.swift
//  Healthsprings
//
//  Created by Personal on 20/07/22.
//

import UIKit

class SelectViewCell: UICollectionViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var subBgView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        subBgView.layer.masksToBounds = true
        subBgView.layer.borderWidth = 1
//        subBgView.layer.cornerRadius = 25
        subBgView.layer.cornerRadius = subBgView.frame.size.height / 2
        subBgView.clipsToBounds = true

//subBgView.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner]
        subBgView.backgroundColor = UIColor.convertRGB(hexString: "#D0F6FF")
        subBgView.layer.borderColor = UIColor.convertRGB(hexString: "#86D0E2").cgColor
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
