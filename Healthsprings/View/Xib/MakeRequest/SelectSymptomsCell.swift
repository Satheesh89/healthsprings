//
//  SelectSymptomsCell.swift
//  Healthsprings
//
//  Created by Personal on 20/07/22.
//

import UIKit

class SelectSymptomsCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectSymptomsCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: "SelectSymptomsCell", bundle: nil)
    }
    static var identifier: String {
        return String(describing: self)
    }
}
