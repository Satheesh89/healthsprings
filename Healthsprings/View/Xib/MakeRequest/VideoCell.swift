//
//  VideoconsultationCell.swift
//  Healthsprings
//
//  Created by Personal on 20/07/22.
//

import UIKit
import iOSDropDown

class VideoCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var patientView: UIView!
    @IBOutlet weak var videoDropDown: DropDown!
    @IBOutlet weak var shadowView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        addView.dropShadowWithCornerRaduis()
//        patientView.dropShadowWithCornerRaduis()
        addView.layer.cornerRadius = 5
        shadowView.dropShadowWithCornerRaduis()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: "VideoCell", bundle: nil)
    }
    static var identifier: String {
        return String(describing: self)
    }
}
