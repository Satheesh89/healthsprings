//
//  PaymentDetailsCell.swift
//  Healthsprings
//
//  Created by Personal on 21/07/22.
//

import UIKit

class PaymentDetailsCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var subTotal: UILabel!
    @IBOutlet weak var taxServicesTotal: UILabel!
    @IBOutlet weak var subTotalPriceLabel: UILabel!
    @IBOutlet weak var taxPriceLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var dateandtimeLabel: UILabel!
    @IBOutlet weak var cardNoLabel: UILabel!
    @IBOutlet weak var paidButton: UIButton!
    @IBOutlet weak var downloadBillView: UIView!
    @IBOutlet weak var downloadPrescriptionView: UIView!
    @IBOutlet weak var downloadMedicalCertificateView: UIView!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var prescriptionButton: UIButton!
    @IBOutlet weak var medicalCerficateButton: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        downloadBillView.dropShadowWithCornerRaduis()
//        downloadPrescriptionView.dropShadowWithCornerRaduis()
//        downloadMedicalCertificateView.dropShadowWithCornerRaduis()

        downloadBillView.viewBorderShadow()
        downloadPrescriptionView.viewBorderShadow()
        downloadMedicalCertificateView.viewBorderShadow()

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    static var nib: UINib {
      return UINib(nibName: "PaymentDetailsCell", bundle: nil)
    }
    static var identifier: String {
      return String(describing: self)
    }
}
