//
//  HistoryDetailHeaderCell.swift
//  Healthsprings
//
//  Created by Personal on 21/07/22.
//

import UIKit

class HistoryDetailHeaderCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var patientView: UIView!
    @IBOutlet weak var doctorImageView: UIImageView!
    @IBOutlet weak var identityImageView: UIImageView!
    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var doctorDetail: UILabel!
    @IBOutlet weak var patientName: UILabel!
    @IBOutlet weak var detailName: UILabel!
    @IBOutlet weak var calendarImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var clockImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var videoTitleLabel: UILabel!
    @IBOutlet weak var videoCodeLabel: UILabel!
    @IBOutlet weak var symptomsTitleLabel: UILabel!
    @IBOutlet weak var symptomsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        patientView.backgroundColor = .white
//        patientView.dropShadowWithCornerRaduis()
//        patientView.layer.borderWidth = 0.3
//        patientView.layer.borderColor = UIColor.lightGray.cgColor
        patientView.viewBorderShadow()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    static var nib: UINib {
      return UINib(nibName: "HistoryDetailHeaderCell", bundle: nil)
    }
    static var identifier: String {
      return String(describing: self)
    }
}
