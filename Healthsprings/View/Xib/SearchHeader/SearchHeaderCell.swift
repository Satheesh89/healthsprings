//
//  SearchHeaderCell.swift
//  Healthsprings
//
//  Created by Personal on 01/07/22.
//

import UIKit

class SearchHeaderCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var totalCountLabel: UILabel!
    @IBOutlet weak var filterLabel: UILabel!
    @IBOutlet weak var filterButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
      return UINib(nibName: "SearchHeaderCell", bundle: nil)
    }
    
    static var identifier: String {
      return String(describing: self)
    }

}
