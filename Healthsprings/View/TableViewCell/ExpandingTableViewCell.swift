//
//  ExpandingTableViewCell.swift
//  ExpandingCell
//
//  Created by Alexis Creuzot on 13/11/2016.
//  Copyright © 2016 alexiscreuzot. All rights reserved.
//

import UIKit

class ExpandingTableViewCellContent {
    var title: String?
    var subtitle: String?
    var expanded: Bool

    init(title: String, subtitle: String) {
        self.title = title
        self.subtitle = subtitle
        self.expanded = false
    }
}

class ExpandingTableViewCell: UITableViewCell {
    
    @IBOutlet var bgView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.cornerRadius = 5
        bgView.layer.borderWidth = 1
        bgView.layer.borderColor = UIColor.themeColor.cgColor
        bgView.clipsToBounds = true
        if ( UIDevice.current.model.range(of: "iPad") != nil){
            titleLabel.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(10.0))
            subtitleLabel.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(08.0))
        } else {
            titleLabel.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))
            subtitleLabel.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(12.0))
        }
    }
    
    //    func set(content: ExpandingTableViewCellContent) {
    //        self.titleLabel.text = content.title
    //        self.subtitleLabel.text = content.expanded ? content.subtitle : ""
    //    }

    func set(content: Faqmodel) {
        self.titleLabel.text = content.question
        self.subtitleLabel.text = content.expanded ? content.answer : ""
    }
}
