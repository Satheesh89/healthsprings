//
//  HealthSpringMenuTableViewCell.swift
//  Healthsprings
//
//  Created by Personal on 15/06/22.
//

import UIKit

class HealthSpringMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var HeaderCell: UIView!
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleLabel.textColor = .themeColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    static var nib: UINib {
      return UINib(nibName: "HealthSpringMenuTableViewCell", bundle: nil)
    }
    
    static var identifier: String {
      return String(describing: self)
    }
    
    func configure(model: HealthSpringMenuModel, index: Int) {
        titleLabel.text = model.title
    }
}
