//
//  RequestSummaryViewController.swift
//  Healthsprings
//
//  Created by Personal on 01/08/22.
//

import UIKit
import Alamofire
import StripePaymentSheet


class RequestSummaryViewController: UIViewController {
    @IBOutlet weak var summaryTableview: UITableView!    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var continueButton: UIButton!
    
    var getDoctorUuid: String!
    var getDoctorName: String!
    var getDoctorId: String!
    var getBookingType: String!
    var getDate: String!
    var getTIme: String!
    var getProfileId: String!
    var getEditBool: Bool!
    var symtomList = [MakeRequestModel]()
    var bookType: String!
    var deliveryType: String!
    var symptoms: String!
    var personname: String!
    var cardnumber: String!
    var expirationyear: String!
    var expirationmonth: String!
    var currency: String!
    var consultationfee: String!
    var getRequestDetailModel: RequestDetailModel!
    var getRequestdetail: [Requestdetail]?
    
    // Stripe PaymentGate
    var paymentSheet: PaymentSheet?
    let backendCheckoutUrl = URL(string: "https://stripe-mobile-payment-sheet.glitch.me/checkout")!  // An example backend endpoint
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.getRequestSummaryApi(loadview: self)
        alertViewLoadSetup()
        tableViewSetup()
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: TableView Setup
    func tableViewSetup()  {
        setStatusBar(color: UIColor.convertRGB(hexString: "#F4F4F4"))
        summaryTableview.register(SummaryHeaderCell.nib, forCellReuseIdentifier: SummaryHeaderCell.identifier)
        summaryTableview.register(SelectCardCell.nib, forCellReuseIdentifier: SelectCardCell.identifier)
        summaryTableview.register(ConsultationFeeCell.nib, forCellReuseIdentifier: ConsultationFeeCell.identifier)
        summaryTableview.register(PatientLocationCell.nib, forCellReuseIdentifier: PatientLocationCell.identifier)
        summaryTableview.delegate = self
        summaryTableview.dataSource = self
        summaryTableview.tableFooterView = UIView()
        summaryTableview.separatorStyle = UITableViewCell.SeparatorStyle.none
        summaryTableview.showsHorizontalScrollIndicator = false
        summaryTableview.showsVerticalScrollIndicator = false
        summaryTableview.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        summaryTableview.backgroundColor = .clear
        summaryTableview.contentInsetAdjustmentBehavior = .never
    }
    
    //MARK: Get Request Summary Api
    func getRequestSummaryApi(loadview: UIViewController) {
        LoadingIndicator.shared.show(forView: loadview.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["Uuid"] = "\(self.getRequestdetail?[0].uuid ?? "")"
        paramsDictionary["profile_id"] = "\(self.getRequestdetail?[0].profileId ?? "")"
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()
        
        HttpClientApi.instance().makeAPICall(url: "\(URL.requestsummary)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as? [ Any]
                let response = response
                if  response?.statusCode == 200 {
                    self.bookType = "\((dataDict?[0] as? NSDictionary)?.value(forKeyPath: "booking_type") ?? "")"
                    self.deliveryType = "\((dataDict?[0] as? NSDictionary)?.value(forKeyPath: "type") ?? "")"
                    self.symptoms = "\((dataDict?[0] as? NSDictionary)?.value(forKeyPath: "symptoms") ?? "")"
                    self.personname = "\((dataDict?[0] as? NSDictionary)?.value(forKeyPath: "person_name") ?? "")"
                    self.cardnumber = "\((dataDict?[0] as? NSDictionary)?.value(forKeyPath: "card_number") ?? "")"
                    self.expirationyear = "\((dataDict?[0] as? NSDictionary)?.value(forKeyPath: "expiration_year") ?? "")"
                    self.expirationmonth = "\((dataDict?[0] as? NSDictionary)?.value(forKeyPath: "expiration_month") ?? "")"
                    self.currency = "\((dataDict?[0] as? NSDictionary)?.value(forKeyPath: "currency") ?? "")"
                    self.consultationfee = "\((dataDict?[0] as? NSDictionary)?.value(forKeyPath: "consultation_fee") ?? "")"
                    doOnMain {delay(0.1) {
                        self.summaryTableview.reloadData()
                        LoadingIndicator.shared.hide()
                    }}
                }else{
                    delay(0.1) {
                        self.showCustomToast(toastTitle: "Error",toastMeassage: "error",status: 1)
                        LoadingIndicator.shared.hide()
                    }
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
    
    //MARK: - Back
    @IBAction func didTapLeftAction(_ sender: Any) {
        self.navigationController?.popToViewController(ofClass: MakeRequestViewController.self)
    }
    
    
    @IBAction func didTapContinueAction(_ sender: Any) {
        alertView.isHidden = true
        var details = [confirmDetails]()
        print("\(self.getRequestdetail?.first?.uuid ?? "")")
        print("\(self.getRequestdetail?.first?.profileId ?? "")")
        let confirm = confirmDetails(getUuidStr: "\(self.getRequestdetail?.first?.uuid ?? "")", profileIdStr: "\(self.getRequestdetail?.first?.profileId ?? "")",consultationfee: consultationfee ?? "",currency: currency ?? "",date: "\(getTIme ?? "")",time: "\((getDate ?? ""))")
        details.append(confirm)
        
        print(details)
        setupTabbar(details: details)
    }
    
    //MARK: Load Setup
    func alertViewLoadSetup()  {
        alertView.backgroundColor = UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.7)
        alertView.layer.cornerRadius = 5
        popupView.layer.cornerRadius = 5
        continueButton.layer.cornerRadius = 5
        alertView.isHidden = true
    }
    
}

extension RequestSummaryViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            return summaryHeaderTableViewCell(tableView: tableView, indexPath: indexPath)
        }else  if indexPath.row == 1{
            return patientLocationCell(tableView: tableView, indexPath: indexPath)
        }else  if indexPath.row == 2{
            return selectCardCell(tableView: tableView, indexPath: indexPath)
        }else  if indexPath.row == 3{
            return consultationFeeCellCell(tableView: tableView, indexPath: indexPath)
        }
        return UITableViewCell()
    }
    
    func summaryHeaderTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SummaryHeaderCell.identifier) as? SummaryHeaderCell else {
            return UITableViewCell()
        }
        if bookType == "video_consultation"{
            cell.typeLabel.text = "Video Consultation"
        }else{
            cell.typeLabel.text = bookType ?? ""
        }
        if deliveryType ?? "" == "asap"{
            cell.dateLabel.text =  "ASAP"
            cell.timeLabel.text =  "Immediate"
        }else{
            //            if deliveryType == "time_based"{
            //                 cell.dateLabel.text = "Schedule Time"
            //            }
            cell.dateLabel.text = "\((getDate ?? ""))"
            cell.timeLabel.text =  "\(getTIme ?? "")"
        }
        cell.patientNameLabel.text =  personname ?? ""
        cell.symptomsLabel.text =  symptoms ?? ""
        cell.editButton.addTarget(self, action: #selector(didTapEditAction), for: .touchUpInside)
        return cell
    }
    
    @IBAction func didTapEditAction(_ sender: UIButton) {
        //        if sender.tag == 2001{
        self.navigationController?.popToViewController(ofClass: MakeRequestViewController.self)
        //        }else  if sender.tag == 2000{
        //            self.navigationController?.popToViewController(ofClass: AddCardViewController.self)
        //        }
    }
    
    func patientLocationCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PatientLocationCell.identifier) as? PatientLocationCell else {
            return UITableViewCell()
        }
        //cell.patientLocationCollectionView.delegate = self
        //cell.patientLocationCollectionView.dataSource = self
        //cell.patientLocationCollectionView.reloadData()
        print(UserDetails.shared.getFullAddress ?? "")
        cell.addressLabel.text = "\(UserDetails.shared.getFullAddress ?? "") \(UserDetails.shared.getcity ?? "") \(UserDetails.shared.getstreet_name ?? "") \(UserDetails.shared.getarea_location ?? "")"
        return cell
    }
    
    func selectCardCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SelectCardCell.identifier) as? SelectCardCell else {
            return UITableViewCell()
        }
        cell.cardNoLabel.text = "****   ****    ****    ****    \(cardnumber ?? "")"
        //cell.vaildDateLabel.text = "Valid \(expirationmonth ?? "") / \(expirationyear ?? "")"
        cell.vaildDateLabel.textColor = UIColor.black
        let attributedWithTextColor: NSAttributedString = "Valid \(expirationmonth ?? "") / \(expirationyear ?? "")".attributedStringWithColor(["Valid"], color: UIColor.convertRGB(hexString: "#8E8E8E"))
        cell.vaildDateLabel.attributedText = attributedWithTextColor
        cell.editButton.isHidden = true
        return cell
    }
    
    func consultationFeeCellCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ConsultationFeeCell.identifier) as? ConsultationFeeCell else {
            return UITableViewCell()
        }
        
        if currency == "SGD"{
            cell.consultationFeeLabel.text = "S$ \(consultationfee ?? "")"
        }else if currency == "USD"{
            cell.consultationFeeLabel.text = "$ \(consultationfee ?? "")"
        }else{
            cell.consultationFeeLabel.text = "$ \(consultationfee ?? "")"
        }
        
        cell.confirmButton.addTarget(self, action: #selector(didTapConFirmAction), for: .touchUpInside)
        return cell
    }
    
    @IBAction func didTapConFirmAction(_ sender: UIButton) {
        //        self.getConfirmApi(getUuidStr: "\(getDoctorUuid ?? "")",profileIdStr: "\(getProfileId ?? "")",loadview: self)
        //        createCustomer()
        alertView.isHidden = false
    }
    
    //Stripe Payment Gateway
    func createCustomer(){
        var request = URLRequest(url: backendCheckoutUrl)
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(
            with: request,
            completionHandler: { [weak self] (data, response, error) in
                guard let data = data,
                      let json = try? JSONSerialization.jsonObject(with: data, options: [])
                        as? [String: Any],
                      let customerId = json["customer"] as? String,
                      let customerEphemeralKeySecret = json["ephemeralKey"] as? String,
                      let paymentIntentClientSecret = json["paymentIntent"] as? String,
                      let publishableKey = json["publishableKey"] as? String,
                      let self = self
                else {
                    // Handle error
                    return
                }
                // MARK: Set your Stripe publishable key - this allows the SDK to make requests to Stripe for your account
                STPAPIClient.shared.publishableKey = publishableKey
                
                // MARK: Create a PaymentSheet instance
                var configuration = PaymentSheet.Configuration()
                configuration.merchantDisplayName = "Example, Inc."
                configuration.applePay = .init(
                    merchantId: "com.foo.example", merchantCountryCode: "US")
                configuration.customer = .init(
                    id: customerId, ephemeralKeySecret: customerEphemeralKeySecret)
                configuration.returnURL = "payments-example://stripe-redirect"
                // Set allowsDelayedPaymentMethods to true if your business can handle payment methods that complete payment after a delay, like SEPA Debit and Sofort.
                configuration.allowsDelayedPaymentMethods = true
                self.paymentSheet = PaymentSheet(
                    paymentIntentClientSecret: paymentIntentClientSecret,
                    configuration: configuration)
                DispatchQueue.main.async {
                    //                    self.buyButton.isEnabled = true
                }
            })
        task.resume()
        didTapCheckoutButton()
    }
    
    @objc
    func didTapCheckoutButton() {
        // MARK: Start the checkout process
        paymentSheet?.present(from: self) { paymentResult in
            // MARK: Handle the payment result
            switch paymentResult {
            case .completed:
                self.displayAlert("Your order is confirmed!")
            case .canceled:
                print("Canceled!")
            case .failed(let error):
                print(error)
                self.displayAlert("Payment failed: \n\(error.localizedDescription)")
            }
        }
    }
    
    func displayAlert(_ message: String) {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            alertController.dismiss(animated: true) {
                //                self.getConfirmApi(getUuidStr: "\(self.getDoctorUuid ?? "")",profileIdStr: "\(self.getProfileId ?? "")",loadview: self)
                //self.navigationController?.popViewController(animated: true)
            }
        }
        alertController.addAction(OKAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
    func  setupTabbar(details: [confirmDetails])  {
        DispatchQueue.main.async {
            let mainstoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let mainview = mainstoryboard.instantiateViewController(withIdentifier: "WaitingViewController") as! WaitingViewController
            mainview.getConfirmDetails =  details
            if self.deliveryType ?? "" == "asap"{
                mainview.timerBool = false
            }else{
                mainview.timerBool = true
            }
            self.navigationController?.pushViewController(mainview, animated: false)
        }
    }
    
    //    func getConfirmApi(getUuidStr: String,profileIdStr: String,loadview: UIViewController) {
    //        LoadingIndicator.shared.show(forView: loadview.view)
    //        var paramsDictionary = [String:Any]()
    //        paramsDictionary["Uuid"] = "\(self.getRequestdetail?[0].uuid ?? "")"
    //        paramsDictionary["confirmed"] = "yes"
    //        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()
    //        paramsDictionary["timezone"] = "asia/kolkata"
    //        HttpClientApi.instance().makeAPICall(url: "\(URL.confirmappointment)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
    //            if let data = data {
    //                let json = try? JSONSerialization.jsonObject(with: data, options: [])
    //                let dataDict = json as? NSDictionary
    //                let response = response
    //                if  response?.statusCode == 200 {
    //                    doOnMain {delay(0.1) {
    //                        LoadingIndicator.shared.hide()
    //                        self.setupTabbar()
    //                    }}
    //                }else{
    //                    delay(0.1) {
    //                        self.showCustomToast(toastTitle: "Error",toastMeassage: "error",status: 1)
    //                        LoadingIndicator.shared.hide()
    //                    }
    //                }
    //            }
    //        }, failure: { (data, response, error) in
    //            // API call Failure
    //        })
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}


extension DashBoard: UIDocumentMenuDelegate,UIDocumentPickerDelegate{
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        print("import result : \(myURL)")
        let data = NSData(contentsOf: myURL)
        do{
            //            self.Doc(url: strUrl, docData: try Data(contentsOf: myURL), parameters: ["club_file": "file" as AnyObject], fileName: myURL.lastPathComponent, token: token)
            //            self.docText.text = myURL.lastPathComponent
            //uploadActionDocument(documentURLs: myURL, pdfName: myURL.lastPathComponent)
        }catch{
            print(error)
        }
    }
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
}
