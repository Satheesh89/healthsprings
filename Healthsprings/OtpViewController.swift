//
//  OtpViewController.swift
//  Healthsprings
//
//  Created by Personal on 17/06/22.
//

import UIKit

class OtpViewController: UIViewController {
    @IBOutlet weak var otpCode: DPOTPView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var registerLabel: UILabel!
    @IBOutlet weak var otpexpireLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var resendUILabel: UILabel!

    var timer : Timer!
    var second = 30
    
    var getEmail: String = ""
    var getMobile: String = ""
    var getName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        timer = Timer()
        otpCode.fontTextField = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(20.0))!
        otpCode.dismissOnLastEntry = true
        otpCode.isCursorHidden = true
        otpCode.dpOTPViewDelegate = self
        otpCode.textColorTextField = .themeColor
        registerLabel.text = "OTP has been sent to your registered Email-ID : \(getMobile)"
        sendOTPPage()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    //MARK :- Timer function
    func timerUpdate() {
        second = 300
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.calculateSeconds), userInfo: nil, repeats: true)
    }
    //MARK:- Calculate timer
    @objc func calculateSeconds() {
//        second -= 1
//        let str = String(second)
//        DispatchQueue.main.async {
//            self.otpexpireLabel.text = "Your OTP will expire within \(str) minutes"
//            let attributedWithTextColor: NSAttributedString = "Your OTP will expire within \(str) minutes".attributedStringWithColor(["\(str) minutes"], color: UIColor.themeColor)
//            self.otpexpireLabel.attributedText = attributedWithTextColor
//        }
        
        if(second > 0){
            resendUILabel.textColor = .lightGray
            let minutes = String(second / 60)
            let seconds = String(second % 60)
            let str = minutes + ":" + seconds
            DispatchQueue.main.async {
                self.otpexpireLabel.text = "Your OTP will expire within \(str) minutes"
                let attributedWithTextColor: NSAttributedString = "Your OTP will expire within \(str) minutes".attributedStringWithColor(["\(str) minutes"], color: UIColor.themeColor)
                self.otpexpireLabel.attributedText = attributedWithTextColor
            }
            second -= 1
        }

        if second == 0 {
            timer.invalidate()
            resendUILabel.textColor = .themeColor
            let str = "00 : 00"
            DispatchQueue.main.async {
                self.otpexpireLabel.text = "Your OTP will expire within \(str) minutes"
                let attributedWithTextColor: NSAttributedString = "Your OTP will expire within \(str) minutes".attributedStringWithColor(["\(str) minutes"], color: UIColor.themeColor)
                self.otpexpireLabel.attributedText = attributedWithTextColor
            }
        }
    }
    // MARK: - Resend Action
    @IBAction func didTapresendAction(_ sender: Any) {
        reSendOTPPage()
        timerUpdate()
    }
    // MARK: - Close Action
    @IBAction func didTapCloseAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    func sendOTPPage(){
        LoadingIndicator.shared.show(forView: self.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["phone"] = getMobile
        paramsDictionary["email"] = getMobile

        HttpClientApi.instance().makeAPICall(url: URL.getSendotp, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                if  response?.statusCode == 200 {
                    doOnMain {
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                            self.timerUpdate()
                        }
                    }
                }else if  response?.statusCode == 401 {
                    
                    doOnMain {
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                            self.timerUpdate()
                        }
                    }
                    let tempArray = NSMutableArray()
                    for  i in 0..<((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.count ?? 0) {
                        let tArray  = ((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.allValues[i]) as? Array<Any>
                        tempArray.add(tArray?.first ?? "")
                    }
                    let errorStr = tempArray.componentsJoined(by: ",")
                    CustomToast.show(message: errorStr,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                } else{
                    doOnMain {
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                            self.timerUpdate()
                        }
                    }
                    CustomToast.show(message: dataDict["error"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }

            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
    
    func checkSendOTPPage(){
        LoadingIndicator.shared.show(forView: self.view)
        var paramsDictionary = [String:Any]()
//        paramsDictionary["phone"] = getMobile
        paramsDictionary["otp"] = "\(otpCode.text ?? "")"
        paramsDictionary["email"] = getMobile

        
        HttpClientApi.instance().makeAPICall(url: URL.checkOtp, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                if  response?.statusCode == 200 {
                    doOnMain {
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                            self.showCustomToast(toastTitle: "Info",toastMeassage: dataDict["success"] as! String,status: 0)
                            self.setFrontAccountView()
                        }
                    }
                }else if  response?.statusCode == 401 {
                    doOnMain {
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                        }
                    }
                    let tempArray = NSMutableArray()
                    for  i in 0..<((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.count ?? 0) {
                        let tArray  = ((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.allValues[i]) as? Array<Any>
                        tempArray.add(tArray?.first ?? "")
                    }
                    let errorStr = tempArray.componentsJoined(by: ",")
                    CustomToast.show(message: errorStr,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                } else{
                    doOnMain {
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                        }
                    }
                    CustomToast.show(message: dataDict["error"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
            
            doOnMain {
                delay(0.1) {
                    LoadingIndicator.shared.hide()
                }
            }
        })
    }

    func reSendOTPPage(){
        LoadingIndicator.shared.show(forView: self.view)
        var paramsDictionary = [String:Any]()
//        paramsDictionary["phone"] = getMobile
//        paramsDictionary["otp"] = "\(otpCode.text ?? "")"
        paramsDictionary["email"] = "\(getMobile )"
        
        HttpClientApi.instance().makeAPICall(url: URL.resendOtp, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                if  response?.statusCode == 200 {
                    doOnMain {
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                            CustomToast.show(message: dataDict["success"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                        }
                    }
                }else if  response?.statusCode == 401 {
                    delay(0.1) {
                        LoadingIndicator.shared.hide()
                    }
                    let tempArray = NSMutableArray()
                    for  i in 0..<((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.count ?? 0) {
                        let tArray  = ((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.allValues[i]) as? Array<Any>
                        tempArray.add(tArray?.first ?? "")
                    }
                    let errorStr = tempArray.componentsJoined(by: ",")
                    CustomToast.show(message: errorStr,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                } else{
                    delay(0.1) {
                        LoadingIndicator.shared.hide()
                    }
                    CustomToast.show(message: dataDict["error"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
    
}
extension OtpViewController : DPOTPViewDelegate {
    func dpOTPViewAddText(_ text: String, at position: Int) {
        print("addText:- " + text + " at:- \(position)" )
        if text.count == 6 {
            self.checkSendOTPPage()
        }
    }
        
    func setFrontAccountView() {
        //        let mainstoryboard = UIStoryboard(name: "Menu", bundle: nil)
        //        let mainview = mainstoryboard.instantiateViewController(withIdentifier: "AccountSuccessfullyViewController") as! AccountSuccessfullyViewController
        //        mainview.getName = getName
        //        self.navigationController?.pushViewController(mainview, animated: false)
        DispatchQueue.main.async {
            let mainview = SignupViewController.instantiateFromAppStoryboard(appStoryboard: .Menu)
            mainview.getEmail = self.getMobile
            mainview.getName = self.getName
            self.navigationController?.pushViewController(mainview, animated: false)
        }
    }
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        print("removeText:- " + text + " at:- \(position)" )
    }
    func dpOTPViewChangePositionAt(_ position: Int) {
        print("at:-\(position)")
    }
    func dpOTPViewBecomeFirstResponder() {
    }
    func dpOTPViewResignFirstResponder() {
    }
}
