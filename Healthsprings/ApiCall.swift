//
//  ApiCall.swift
//  Healthsprings
//

import Foundation
import UIKit

class ApiCall: NSObject {
    
    func apiCallWitoutActivity(url:String, parameter:String, methodType: String, isActivity: Bool, CompletionHandler:@escaping(NSDictionary) ->()) {
        if GlobalClass.sharedInstance.hasConnectivity(){
            let encodeURL : String = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            var headers = NSDictionary()
            if isActivity {
                GlobalClass.sharedInstance.activity()
            }
            headers = ["Accept-Language": "en",
                       "Authorization" : "Bearer accessToken",
                       "Accept" : "application/json","Content-Type" : "application/x-www-form-urlencoded"]
            //print(headers)
            let sampleCall2 : SSHTTPClient = SSHTTPClient(url: encodeURL , method: methodType, httpBody:parameter, headerFieldsAndValues: headers as? [String : String])
            sampleCall2.getJsonData { (obj, error) -> Void in
                if isActivity {
                    GlobalClass.sharedInstance.removeActivity()
                }
                if error != nil {
                    //print(error ?? "no error")
                }else {
                    //print(obj ?? "")
                    let dataDict = obj as! NSDictionary
                    //print(dataDict.object(forKey:"status") ?? "")
                    if dataDict.object(forKey:"status") as! Int == 200 || dataDict.object(forKey:"status") as! Int == 419 {
                        CompletionHandler(dataDict)
                    } else {
                        DispatchQueue.main.async {
                            self.showToast(message:dataDict.object(forKey:"message") as! String)
                        }
                    }
                }
            }
        } else {
            //self.showToast(message:"Please check your internet connection")
            //GlobalClass.sharedInstance.popUpView()
        }
    }
    
    func apiCall(url:String, parameter:String, methodType: String, ishidden: Bool,viewController: UIViewController, CompletionHandler:@escaping(NSDictionary) ->()) {
        if GlobalClass.sharedInstance.hasConnectivity(){
            let encodeURL : String = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print(encodeURL)
            print(parameter)
            var headers = NSDictionary()
            if ishidden == true{
            }else{
                GlobalClass.sharedInstance.activity()
            }
            headers = ["Accept-Language": "en",
                       "Authorization" : "Bearer accessToken",
                       "Accept" : "application/json","Content-Type" : "application/x-www-form-urlencoded"]
            let sampleCall2 : SSHTTPClient = SSHTTPClient(url: encodeURL , method: methodType, httpBody:parameter, headerFieldsAndValues: headers as? [String : String])
            sampleCall2.getJsonData { (obj, error) -> Void in
                if ishidden == true{
                }else{
                    GlobalClass.sharedInstance.removeActivity()
                }
                print(error?.localizedDescription ?? "no error")
                if error != nil {
                    //print(error ?? "no error")
                } else {
                    let dataDict = obj as? NSDictionary ?? [:]
                    print(dataDict)
                    if dataDict.object(forKey:"status") as? Int ?? 0 == 200 || dataDict.object(forKey:"status") as? Int ?? 0 == 419 {
                        CompletionHandler(dataDict)
                    } else {
                        DispatchQueue.main.async {
                            if dataDict.object(forKey:"success") as? Int ?? 0 == 1 {
                                self.showToast(message: "email has already been taken.")
                            }else if dataDict.object(forKey:"error") as? String ?? "" == "user already Registered" {
                                self.showToast(message: dataDict.object(forKey:"error") as? String ?? "")
                            }else{
                                self.showToast(message: dataDict.object(forKey:"message") as? String ?? "")
                            }
                        }
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                GlobalClass.sharedInstance.popUpView()
            }
        }
    }
    
    func apiCallRaw(url:String, parameter:NSMutableDictionary, methodType: String,ishidden: Bool,viewController: UIViewController, CompletionHandler:@escaping(NSDictionary) ->()) {
        if GlobalClass.sharedInstance.hasConnectivity() {
            let encodeURL : String = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            var headers = NSDictionary()
            GlobalClass.sharedInstance.activity()
            headers = ["Accept-Language": "en",
                       "Authorization" : "Bearer accessToken",
                       "Accept" : "application/json","Content-Type" : "application/x-www-form-urlencoded"]
            let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
            var param: String? = ""
            if let aData = jsonData {
                param = String(data: aData, encoding: .utf8)
                //print(param ?? "")
            }
            let sampleCall2 : SSHTTPClient = SSHTTPClient(url: encodeURL , method: methodType, httpBody:param, headerFieldsAndValues: headers as? [String : String])
            sampleCall2.getJsonData { (obj, error) -> Void in
                GlobalClass.sharedInstance.removeActivity()
                if error != nil {
                    //print(error ?? "no error")
                }else {
                    //print(obj ?? "")
                    let dataDict = obj as! NSDictionary
                    //print(dataDict.object(forKey:"status") ?? "")
                    if dataDict.object(forKey:"status") as! Int == 200 || dataDict.object(forKey:"status") as! Int == 419 {
                        CompletionHandler(dataDict)
                    } else {
                        DispatchQueue.main.async {
                            self.showToast(message:dataDict.object(forKey:"message") as! String)
                        }
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                GlobalClass.sharedInstance.popUpView()
            }
        }
    }
    
    //MARK:- Upload Multipart data
    func apiCallMultipart(url:String, parameter:[String: String]?, methodType: String, image:UIImage, CompletionHandler:@escaping(NSDictionary) ->()) {
        //GlobalClass.sharedInstance.showLoading()
        let encodeURL : String = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string:encodeURL);
        let request = NSMutableURLRequest(url: url!);
        request.httpMethod = "POST"
        let boundary = "Boundary-\(NSUUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = ["Accept-Language": "appLanguage",
                                       "Authorization" : "Bearer accessToken"]
        let imageData = image.jpegData(compressionQuality: 0.50)
        if (imageData == nil) {
            //print("UIImageJPEGRepresentation return nil")
            return
        }
        let body = NSMutableData()
        if parameter!.count > 0 {
            //        if parameter != nil {
            for (key, value) in parameter! {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format:"Content-Disposition: form-data; name=\"profile_image\"; filename=\"testfromios.jpg\"\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "Content-Type: application/octet-stream\r\n\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(imageData!)
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        request.httpBody = body as Data
        
        let task =  URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            (data, response, error) -> Void in
            //            GlobalClass.sharedInstance.hideLoading()
            if let data = data {
                ////print(response as Any)
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                //print(dataDict)
                if dataDict.object(forKey:"status") as! Int == 200 {
                    CompletionHandler(dataDict)
                } else {
                    DispatchQueue.main.async {
                        self.showToast(message:dataDict.object(forKey:"message") as! String)
                    }
                }
            } else if let error = error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    //MARK:- Toast view
    func showToast(message : String) {
        let lblHeight = self.height(constraintedWidth:UIScreen.main.bounds.size.width-40 , font: UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))!,text:message)
        let toastLabel = UILabel(frame: CGRect(x: 20, y: UIScreen.main.bounds.size.height-100, width: UIScreen.main.bounds.size.width-40, height:lblHeight+30))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        toastLabel.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        toastLabel.numberOfLines = 0
        let view = UIApplication.shared.keyWindow!
        view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    //MARK:- label height calculate
    func height(constraintedWidth width: CGFloat, font: UIFont, text:String) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = text
        label.font = font
        label.sizeToFit()
        return label.frame.height
    }
}
