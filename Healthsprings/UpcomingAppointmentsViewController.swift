//
//  UpcomingAppointmentsViewController.swift
//  Healthsprings
//
//  Created by Personal on 18/06/22.
//

import UIKit

class UpcomingAppointmentsViewController: UIViewController {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var UpcomingAppointTableView: UITableView!
    var upcomingViewModel = UpcomingViewModel()
    var selectIndexpath : IndexPath = IndexPath()
    @IBOutlet weak var emptyImageView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initializeHideKeyboard()
 
    }
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func loadSetup(){
        print(["apiToken": UserDetails.shared.getApiToken()])
        upcomingViewModel.vc = self
        upcomingViewModel.getUpcomingListApi(getviewController: self.tabBarController!, viewC: self)
//        upcomingViewModel.getUpcomingListApi(params: ["apiToken": UserDetails.shared.getApiToken()], loadview: self.tabBarController!)
    }
    
    //MARK: TableView Setup
    func tableViewSetup()  {
        selectIndexpath = IndexPath(item:0, section:0)
        setStatusBar(color: UIColor.themeColor)
        UpcomingAppointTableView.register(AppointmentTableViewCell.nib, forCellReuseIdentifier: AppointmentTableViewCell.identifier)
//        UpcomingAppointTableView.delegate = self
        UpcomingAppointTableView.dataSource = self
        UpcomingAppointTableView.tableFooterView = UIView()
        UpcomingAppointTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        UpcomingAppointTableView.showsHorizontalScrollIndicator = false
        UpcomingAppointTableView.showsVerticalScrollIndicator = true
        //serviceTableView.setContentOffset(.zero, animated: true)
        UpcomingAppointTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        UpcomingAppointTableView.backgroundColor = .clear
        UpcomingAppointTableView.contentInset.top = 5
        UpcomingAppointTableView.allowsMultipleSelection = true
        UpcomingAppointTableView.contentInsetAdjustmentBehavior = .never
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        loadSetup()
        tableViewSetup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
}

extension UpcomingAppointmentsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if upcomingViewModel.upcomingDetails?.count == 0 {
            //            tableView.setEmptyMessage("no data found!")
            emptyImageView.isHidden = false
        } else {
            emptyImageView.isHidden = true
            tableView.restore()
        }
        return upcomingViewModel.upcomingDetails?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getDoctorTableViewCell(tableView: tableView, indexPath: indexPath)
    }
    func getDoctorTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AppointmentTableViewCell.identifier) as? AppointmentTableViewCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.isUserInteractionEnabled = true
        cell.bgView.isUserInteractionEnabled = true
        cell.upcomingDetails = upcomingViewModel.upcomingDetails?[indexPath.row]
        cell.button.addTarget(self, action: #selector(self.didSelectRowIndex), for: .touchUpInside)
        cell.button.tag = indexPath.row
        cell.getIndexStatusColor(getIndexpath: selectIndexpath,normalIndex: indexPath)
//        cell.bgView.backgroundColor = .white
//        cell.bgView.layer.cornerRadius = 5
//        cell.bgView.layer.shadowColor = UIColor.blackColor.cgColor
//        cell.bgView.layer.shadowOffset = .zero
//        cell.bgView.layer.shadowOpacity = 0.8
//        cell.bgView.layer.shadowRadius = 10
//        cell.bgView.layer.borderWidth = 1
//        cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#D6D6D6").cgColor
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectIndexpath = indexPath
//        paymentSettingTableView.reloadData()
    }
    @objc func didSelectRowIndex(sender : UIButton) {
        selectIndexpath = IndexPath(item: sender.tag, section:0)
        UpcomingAppointTableView.reloadData()
        moveDetailPage(getIndex: sender.tag)
    }
    func moveDetailPage(getIndex: Int){
        delay(0.1) {
            let profileVC = AppointmentsDetailsViewController.instantiateFromAppStoryboard(appStoryboard: .Tabbar)
            profileVC.getAppointmentid = self.upcomingViewModel.upcomingDetails?[getIndex].appointmentId ?? 0
            profileVC.getProfileid = "\(self.upcomingViewModel.upcomingDetails?[getIndex].profileId ?? "")"
            self.navigationController?.pushViewController(profileVC, animated: false)
        }
    }
    @objc func didTapProfileAction(sender : UIButton) {
        let profileVC = AppointmentsDetailsViewController.instantiateFromAppStoryboard(appStoryboard: .Tabbar)
        self.navigationController?.pushViewController(profileVC, animated: false)
    }
}
