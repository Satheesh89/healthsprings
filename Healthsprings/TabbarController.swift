//
//  TabbarController.swift
//  Healthsprings
//
//  Created by Personal on 18/06/22.
//

import UIKit

class TabbarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if #available(iOS 13.0, *){
            let appearance = tabBar.standardAppearance
            appearance.stackedLayoutAppearance.normal.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.init(red: 180/255, green: 180/255, blue: 180/255, alpha: 1.0)]
            appearance.stackedLayoutAppearance.selected.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.init(red: 13/255, green: 133/255, blue: 163/255, alpha: 1.0)]
            appearance.stackedLayoutAppearance.normal.iconColor = UIColor.init(red: 180/255, green: 180/255, blue: 180/255, alpha: 1.0)
//            appearance.stackedLayoutAppearance.selected.iconColor = UIColor.init(red: 13/255, green: 133/255, blue: 163/255, alpha: 1.0)
            appearance.stackedLayoutAppearance.selected.iconColor = UIColor.tababarthemeColor
            tabBar.standardAppearance = appearance
        }else{
            tabBar.unselectedItemTintColor = UIColor.init(red: 180/255, green: 180/255, blue: 180/255, alpha: 1.0)
            tabBar.tintColor = UIColor.init(red: 13/255, green: 133/255, blue: 163/255, alpha: 1.0)
        }
        tabBar.backgroundColor = .white
        
        tabBar.frame.size.height = 95
        tabBar.frame.origin.y = view.frame.height - 95
        tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: .tababarthemeColor, size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count), height:  tabBar.frame.height), lineWidth: 3.0, yOrginSize: tabBar.frame.origin.y)
        
        // Remove default line
        tabBar.shadowImage = UIImage()
        tabBar.backgroundImage = UIImage()
        tabBar.backgroundColor = UIColor.white

        // Add only shadow
        tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        tabBar.layer.shadowRadius = 8
        tabBar.layer.shadowColor = UIColor.themeColor.cgColor
        tabBar.layer.shadowOpacity = 0.2
    }
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
}
