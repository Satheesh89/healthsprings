//
//  IDVerificationViewController.swift
//  Healthsprings
//
//  Created by Personal on 18/09/22.
//

import UIKit
import Alamofire
import AVFoundation
import Photos
import MobileCoreServices


class IDVerificationViewController: UIViewController {
    @IBOutlet weak var IDVerificationTableView: UITableView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var doctorVerifictionLabel: UILabel!
    private var documentTitleString: String = ""
    private var captureImage = UIImage(named: "patientplaceholder")
    var showMyIDDoctor: Bool = false
    var getconfirmdetails =  [confirmDetails]()
    
    var nameStr =  ""
    var number =  ""
    var dropdownStr =  "select document type"

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if showMyIDDoctor == true{
            doctorVerifictionLabel.isHidden = false
        }else{
            doctorVerifictionLabel.isHidden = true
            tableViewSetup()
            isValidation()
        }
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK: TableView Setup
    func tableViewSetup()  {
        IDVerificationTableView.register(IDVerificationDocumentTypeCell.nib, forCellReuseIdentifier: IDVerificationDocumentTypeCell.identifier)
        IDVerificationTableView.register(DocumentverifyCell.nib, forCellReuseIdentifier: DocumentverifyCell.identifier)
        IDVerificationTableView.delegate = self
        IDVerificationTableView.dataSource = self
        IDVerificationTableView.tableFooterView = UIView()
        IDVerificationTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        IDVerificationTableView.showsHorizontalScrollIndicator = false
        IDVerificationTableView.showsVerticalScrollIndicator = false
        IDVerificationTableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        IDVerificationTableView.backgroundColor = .clear
        IDVerificationTableView.contentInsetAdjustmentBehavior = .never
    }
    
    //MARK: - Back
    @IBAction func didTapLeftAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc  func textFieldDidChange(_ textField: UITextField) {
        if textField.tag == 100{
            nameStr = textField.text ?? ""
        }else if textField.tag == 101{
            number = textField.text ?? ""
        }
        isValidation()
    }
    
    @IBAction func didTapConfirmAction(_ sender: Any) {
        getConfirmApi(loadview: self)
    }
    
    private func isValidation() {
        if documentTitleString != "" && nameStr != "" && number != "" {
            if dropdownStr == "" || dropdownStr == "select document type"{
                confirmButton.isUserInteractionEnabled = false
                confirmButton.backgroundColor = .loginGeryColor
                confirmButton.tintColor = UIColor.convertRGB(hexString: "#A0A0A0")
                confirmButton.titleLabel?.textColor = UIColor.convertRGB(hexString: "#A0A0A0")
            }else{
                confirmButton.backgroundColor = .themeColor
                confirmButton.tintColor = .white
                confirmButton.titleLabel?.textColor = .white
                confirmButton.setTitleColor(.white, for: .normal)
                confirmButton.isUserInteractionEnabled = true
            }
        }else{
            confirmButton.isUserInteractionEnabled = false
            confirmButton.backgroundColor = .loginGeryColor
            confirmButton.tintColor = UIColor.convertRGB(hexString: "#A0A0A0")
            confirmButton.titleLabel?.textColor = UIColor.convertRGB(hexString: "#A0A0A0")
        }
    }
    
    
    private func getConfirmApi(loadview: UIViewController) {
        LoadingIndicator.shared.show(forView: loadview.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["Uuid"] = "\(getconfirmdetails.first?.getUuidStr ?? "")"
        paramsDictionary["confirmed"] = "yes"
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()
        paramsDictionary["timezone"] = "asia/kolkata"
        HttpClientApi.instance().makeAPICall(url: "\(URL.confirmappointment)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as? NSDictionary
                let response = response
                if  response?.statusCode == 200 {
                    doOnMain {delay(0.1) {
                        LoadingIndicator.shared.hide()
                        if self.getconfirmdetails.first?.date != "" && self.getconfirmdetails.first?.time != ""{
                            let videoConsulationVC = VideoConsulationViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                            videoConsulationVC.hidesBottomBarWhenPushed = true
                            videoConsulationVC.getconfirmdetails =  self.getconfirmdetails
                            self.navigationController?.pushViewController(videoConsulationVC, animated: false)
                        }else{
                            self.redirectJoinCallVC()
                        }
                    }}
                }else if  response?.statusCode == 401 {
                    doOnMain {delay(0.1) {
                        LoadingIndicator.shared.hide()
                    }
                    }
                    let tempArray = NSMutableArray()
                    for  i in 0..<((dataDict?.value(forKeyPath: "field_error")as? NSDictionary)?.count ?? 0) {
                        let tArray  = ((dataDict?.value(forKeyPath: "field_error")as? NSDictionary)?.allValues[i]) as? Array<Any>
                        tempArray.add(tArray?.first ?? "")
                    }
                    let errorStr = tempArray.componentsJoined(by: ",")
                    CustomToast.show(message: errorStr,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }
                else{
                    CustomToast.show(message: dataDict?["error"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }

            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
    
    private func redirectJoinCallVC(){
        DispatchQueue.main.async {
            let mainstoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let mainview = mainstoryboard.instantiateViewController(withIdentifier: "JoinCallViewController") as! JoinCallViewController
            mainview.getconfirmdetails = self.getconfirmdetails
            self.navigationController?.pushViewController(mainview, animated: false)
        }
    }
    
    //    func getConfirmIdVerificationApiCall(){
    //        LoadingIndicator.shared.show(forView: loadview.view)
    //        var paramsDictionary = [String:Any]()
    //        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()
    //        paramsDictionary["document_type"] = cardId
    //        paramsDictionary["document"] = cardId
    //        paramsDictionary["full_name"] = cardId
    //        paramsDictionary["document_number"] = cardId
    //
    //        HttpClientApi.instance().makeAPICall(url: "\(URL.getIdVerification)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
    //            if let data = data {
    //                let json = try? JSONSerialization.jsonObject(with: data, options: [])
    //                let dataDict = json as! NSDictionary
    //                print("dataDict",dataDict)
    //                let response = response
    //                if  response?.statusCode == 200 {
    //                    doOnMain {
    //                        LoadingIndicator.shared.hide()
    //                        self.loadSetup()
    //                        self.showCustomToast(toastTitle: "Info",toastMeassage: "\(dataDict.value(forKeyPath: "message") ?? "")",status: 0)
    //                    }
    //                }else{
    //                    self.showCustomToast(toastTitle: "Error",toastMeassage: "error",status: 1)
    //                }
    //            }
    //        }, failure: { (data, response, error) in
    //            // API call Failure
    //        })
    //    }
}


extension IDVerificationViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            return IDVerificationTableViewCell(tableView: tableView, indexPath: indexPath)
        }else {
            return documentverifyCell(tableView: tableView, indexPath: indexPath)
        }
    }
    
    func IDVerificationTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: IDVerificationDocumentTypeCell.identifier) as? IDVerificationDocumentTypeCell else {
            return UITableViewCell()
        }
        OperationQueue.main.addOperation({
            cell.documentDropdown.text = "select document type"
            cell.documentDropdown.optionArray = ["Passport","Drivers license"]
            cell.documentDropdown.resignFirstResponder()
            cell.documentDropdown.textColor = .lightGray
            cell.documentDropdown.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))
            cell.documentDropdown.didSelect{(selectedText , index ,id) in
                print("Select-index",index)
                self.dropdownStr = selectedText
                self.isValidation()
            }
        })
        return cell
    }
    
    func documentverifyCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DocumentverifyCell.identifier) as? DocumentverifyCell else {
            return UITableViewCell()
        }
        cell.uploadButton.addTarget(self, action: #selector(didTapUploadButton), for: .touchUpInside)
        cell.captureButton.addTarget(self, action: #selector(didTapCaptureButton), for: .touchUpInside)
        cell.documentTitleLabel.text = documentTitleString
        let sampleImage = image(with: captureImage!, scaledTo: CGSize(width: cell.documnetImageView?.frame.size.width ?? 64.67, height:  cell.documnetImageView?.frame.size.height ?? 74.67))
        cell.documnetImageView?.image = captureImage
        cell.documnetImageView.contentMode = .scaleAspectFill
        
        cell.fullName.addTarget(self, action: #selector(IDVerificationViewController.textFieldDidChange(_:)), for: .editingChanged)
        cell.fullName.tag = 100
        cell.documnetNumber.addTarget(self, action: #selector(IDVerificationViewController.textFieldDidChange(_:)), for: .editingChanged)
        cell.documnetNumber.tag = 101
        cell.fullName.text = nameStr
        cell.documnetNumber.text = number
        return cell
    }
    
    func paymentPriceCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentPriceDetailsCell.identifier) as? PaymentPriceDetailsCell else {
            return UITableViewCell()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    @objc func didTapUploadButton() {
        let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF),String(kUTTypeText)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    @objc func didTapCaptureButton() {
        checkCameraPermission()
    }
    
    
    
    // MARK: - Camera Permission
    func checkCameraPermission()  {
        let cameraMediaType = AVMediaType.video
        AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
            if granted {
                print("Granted access for camera")
                DispatchQueue.main.async {
                    ImagePickerManager().pickImage(self){ image in
                        doOnMain {
                            self.captureImage = image
                            self.cellReloadRow()
                            //self.profileImageView.image = image
                            //self.imageUploadB64(uploadImd: self.profileImageView.image!)
                        }
                    }
                }
            } else {
                self.noCameraFound()
                print("Denied access for camera ")
            }
        }
    }
    
    //  MARK: No Camera Found
    func noCameraFound(){
        let alert = UIAlertController(title: "Pingle User", message: "Please allow camera access in phone settings", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {(action:UIAlertAction) in
        }));
        alert.addAction(UIAlertAction(title: "Open setting ", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
            UIApplication.shared.open(NSURL(string:UIApplication.openSettingsURLString)! as URL, options: [:], completionHandler: nil)
        }));
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Image UploadB 64
    //    func imageUploadB64(uploadImd:UIImage) {
    //        let sampleImage = image(with: uploadImd, scaledTo: CGSize(width: 216.0, height: 216.0))
    //        let url = NSURL(string:  URL.getUploaduserimage)
    //        let request = NSMutableURLRequest(url: url! as URL)
    //        request.httpMethod = "POST"
    //        var headers = ["":""]
    //        headers = ["Accept-Language": "en",
    //                   "apiToken" : UserDetails.shared.getApiToken()]
    //        request.allHTTPHeaderFields = headers
    //        let boundary = generateBoundaryString()
    //        //define the multipart request type
    //        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
    //        if (profileImageView.image == nil){
    //            return
    //        }
    //        let image_data = sampleImage.pngData()
    //        if(image_data == nil){
    //            return
    //        }
    //        let body = NSMutableData()
    //        let fname = "image.png"
    //        let mimetype = "image/png"
    //        //define the data post parameter
    //        let parameters = ["apiToken": UserDetails.shared.getApiToken(),
    //                          "profile_id": getProfileId] as [String : Any]
    //        if parameters != nil {
    //            for (key, value) in parameters {
    //                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
    //                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
    //                body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
    //            }
    //        }
    //        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
    //        body.append("Content-Disposition:form-data; name=\"test\"\r\n\r\n".data(using: String.Encoding.utf8)!)
    //        body.append("hi\r\n".data(using: String.Encoding.utf8)!)
    //        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
    //        body.append("Content-Disposition:form-data; name=\"image\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
    //        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
    //        body.append(image_data!)
    //        body.append("\r\n".data(using: String.Encoding.utf8)!)
    //        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
    //        request.httpBody = body as Data
    //        let session = URLSession.shared
    //        session.dataTask(with: request as URLRequest) { (data, response, error) in
    //            if let response = response {
    //                print(response)
    //            }
    //            if let data = data {
    //                do {
    //                    let json = try JSONSerialization.jsonObject(with: data, options: [])
    //                    doOnMain {
    //                        self.showCustomToast(toastTitle: "Info",toastMeassage: ((json as? NSDictionary)?.value(forKey: "message") ?? "") as! String,status: 0)
    //                    }
    //                } catch {
    //                    print(error)
    //                }
    //            }
    //        }.resume()
    //    }
    // MARK: - Boundary Value
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func image(with image: UIImage, scaledTo newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        //        profileImageView.image = newImage
        return newImage ?? UIImage()
    }
    
    
    //MARK:-
    //    func fileUploadApiCall (){
    //        let param: [String:Any] = ["your_parameters"]
    //        var image = UIImage()
    //        image = UIImage(named: "edit.png")!
    //        let imageData = image.jpegData(compressionQuality: 0.50)
    //        print(image, imageData!)
    //
    //        AF.upload(multipartFormData: { (multipartFormData) in
    //                multipartFormData.append(imageData!, withName: "file", fileName: "swift_file.png", mimeType: "image/png")
    //                for (key, value) in param {
    //                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
    //                }
    //            }, to: "your_url")
    //            { (result) in
    //                switch result {
    //                case .success(let upload, _, _):
    //                    upload.uploadProgress(closure: { (progress) in
    //                        //Print progress
    //                        print("uploading \(progress)")
    //                    })
    //                    upload.responseJSON { response in
    //                        //print response.result
    //                    }
    //                case .failure( _): break
    //                    //print encodingError.description
    //                }
    //            }
    //        }
    
    
}


extension IDVerificationViewController: UIDocumentMenuDelegate,UIDocumentPickerDelegate{
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        print("import result : \(myURL)")
        let data = NSData(contentsOf: myURL)
        do{
            //self.Doc(url: strUrl, docData: try Data(contentsOf: myURL), parameters: ["club_file": "file" as AnyObject], fileName: myURL.lastPathComponent, token: token)
            //            self.docText.text = myURL.lastPathComponent
            //uploadActionDocument(documentURLs: myURL, pdfName: myURL.lastPathComponent)
            documentTitleString = myURL.lastPathComponent
            let imageThumbnail = self.thumbnailFromPdf(withUrl: myURL)
            self.captureImage = imageThumbnail
            self.cellReloadRow()
        }catch{
            print(error)
        }
    }
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    private func thumbnailFromPdf(withUrl url: URL, pageNumber: Int = 1, width: CGFloat = 240) -> UIImage? {
        guard
            let pdf = CGPDFDocument(url as CFURL),
            let page = pdf.page(at: pageNumber)
        else { return nil }
        var pageRect = page.getBoxRect(.mediaBox)
        let pdfScale = width / pageRect.size.width
        pageRect.size = CGSize(width: pageRect.size.width * pdfScale, height: pageRect.size.height * pdfScale)
        pageRect.origin = .zero
        UIGraphicsBeginImageContext(pageRect.size)
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(UIColor.white.cgColor)
            context.fill(pageRect)
            context.saveGState()
            context.translateBy(x: 0.0, y: pageRect.size.height)
            context.scaleBy(x: 1.0, y: -1.0)
            context.concatenate(page.getDrawingTransform(.mediaBox, rect: pageRect, rotate: 0, preserveAspectRatio: true))
            context.drawPDFPage(page)
            context.restoreGState()
        }
        let image = UIGraphicsGetImageFromCurrentImageContext()
        defer { UIGraphicsEndImageContext() }
        return image
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        captureImage = UIImage(named: "patientplaceholder")
        self.documentTitleString = ""
        self.cellReloadRow()
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Cell Reload
    func cellReloadRow(){
        let indexPathRow:Int = 1
        let indexPosition = IndexPath(row: indexPathRow, section: 0)
        IDVerificationTableView.reloadRows(at: [indexPosition], with: .none)
    }
}
