//
//  OnBoardVC.swift
//  Healthsprings
//
//  Created by Personal on 07/09/22.
//

import UIKit

class OnBoardVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: ISPageControl!
    @IBOutlet weak var getStartedButton: UIButton!

    
    var slides: [OnboardingSlide] = []
    
    var currentPage = 0 {
        didSet {
            pageControl.currentPage = currentPage
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        slides = [
            OnboardingSlide(title: "Your healthcare service is now one click away", description: "We're so happy to have you on our team, name. We're pleased you decided to join us, and we hope you'll come to us for help when you need it.", image: #imageLiteral(resourceName: "onboard")),
            OnboardingSlide(title: "Your healthcare service is now one click away", description: "We're so happy to have you on our team, name. We're pleased you decided to join us, and we hope you'll come to us for help when you need it.", image: #imageLiteral(resourceName: "onboard")),
            OnboardingSlide(title: "Your healthcare service is now one click away", description: "We're so happy to have you on our team, name. We're pleased you decided to join us, and we hope you'll come to us for help when you need it.", image: #imageLiteral(resourceName: "onboard"))
        ]
        pageControl.numberOfPages = slides.count
        pageControl.radius = 5 // Circle Height
        pageControl.padding = 4 // Space
        pageControl.inactiveTintColor = UIColor.white.withAlphaComponent(0.4)
        pageControl.currentPageTintColor = UIColor.themeColor // Current Page Color
        pageControl.borderWidth = 1
        pageControl.borderColor = UIColor.themeColor // Circle Color
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        if UIApplication.isFirstLaunch()  == false{
            setRootViewController()
        }
    }
    
    func setRootViewController() {
        print("isFirstLaunch==>",UIApplication.isFirstLaunch())
        if UserDetails.shared.isLoggedIn {
            setTabPage()
        } else {
            moveLoginPage()
        }
    }
    
    // MARK: - Tabbar Action
    func moveLoginPage(){
        DispatchQueue.main.async {
            let mainstoryboard = UIStoryboard(name: "Menu", bundle: nil)
            let mainview = mainstoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            UserDefaults.standard.hasOnboarded = true
            self.navigationController?.pushViewController(mainview, animated: false)
        }
    }
    
    // MARK: - Tabbar Action
    func setTabPage(){
        let tabStoryboard = UIStoryboard(name: "Tabbar", bundle: nil)
        let tabbarPage = tabStoryboard.instantiateViewController(withIdentifier: "TabbarController")
//        tabbarPage.modalPresentationStyle = .fullScreen
//        tabbarPage.modalTransitionStyle = .flipHorizontal
        self.navigationController?.pushViewController(tabbarPage, animated: false)
    }
    
    @IBAction func nextBtnClicked(_ sender: UIButton) {
        if currentPage == slides.count - 1 {
//            let controller = storyboard?.instantiateViewController(identifier: "HomeNC") as! UINavigationController
//            controller.modalPresentationStyle = .fullScreen
//            controller.modalTransitionStyle = .flipHorizontal
//            UserDefaults.standard.hasOnboarded = true
//            present(controller, animated: true, completion: nil)
        } else {
            currentPage += 1
            let indexPath = IndexPath(item: currentPage, section: 0)
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    @IBAction func didTapNextButtonAction(_ sender: UIButton) {
        let mainstoryboard = UIStoryboard(name: "Menu", bundle: nil)
        let mainview = mainstoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        UserDefaults.standard.hasOnboarded = true
        self.navigationController?.pushViewController(mainview, animated: false)
    }
}

extension OnBoardVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return slides.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OnboardingCollectionViewCell.identifier, for: indexPath) as! OnboardingCollectionViewCell
        cell.setup(slides[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let width = scrollView.frame.width
        currentPage = Int(scrollView.contentOffset.x / width)
    }
}
