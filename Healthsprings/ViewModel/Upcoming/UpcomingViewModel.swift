//
//  UpcomingViewModel.swift
//  Healthsprings
//
//  Created by Personal on 30/06/22.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire

class UpcomingViewModel {
    weak var vc: UpcomingAppointmentsViewController?
    weak var vcDashBoard: DashBoard?

    var upcomingModel : UpcomingModel?
    var upcomingDetails: [UpcomingDetails]?

    func getUpcomingListApi(getviewController: UITabBarController,viewC: UIViewController) {
        LoadingIndicator.shared.show(forView: getviewController.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()

        HttpClientApi.instance().makeAPICall(url: URL.getappointments, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("UpcomingModel >",dataDict)
                let response = response
                if  response?.statusCode == 200 {
                    self.upcomingModel = UpcomingModel(JSON(dataDict) )
                    self.upcomingDetails = self.upcomingModel?.upcoming?.upcomingDetails
                    doOnMain {
                        self.vc?.UpcomingAppointTableView.reloadData()
                        self.vcDashBoard?.upcomingSetupFrame()
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                        }
                    }
                }else if  response?.statusCode == 401 {
                    let tempArray = NSMutableArray()
                    for  i in 0..<((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.count ?? 0) {
                        let tArray  = ((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.allValues[i]) as? Array<Any>
                        tempArray.add(tArray?.first ?? "")
                    }
                    let errorStr = tempArray.componentsJoined(by: ",")
                    CustomToast.show(message: errorStr,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: viewC,tarbarHeight: 0.0)
                } else{
                    CustomToast.show(message: dataDict["error"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: viewC,tarbarHeight: 0.0)
                }

            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
    
//    func getUpcomingListApi(params: Parameters ,loadview: UITabBarController) {
//        LoadingIndicator.shared.show(forView: loadview.view)
//        self.getUpcomingList(input: params) { (result: UpcomingModel?, alert: AlertMessage?) in
//            if let result = result {
//                self.upcoming_details = result.upcoming?.upcoming_details
//                doOnMain {
//                    self.vc?.UpcomingAppointTableView.reloadData()
//                    delay(0.1) {
//                        LoadingIndicator.shared.hide()
//                    }
//                }
//            } else if let alert = alert {
//                print(alert.statusCode)
//                print(alert.errorMessage)
//            }
//        }
//    }
}

extension UpcomingViewModel {
    func getUpcomingList(input: Parameters,
                      handler: @escaping (_ user: UpcomingModel?, _ error: AlertMessage?)->()) {
//        UpcomingAppointmentsAPIManager().getUpcomingList(input: input, handler: handler)
    }
}


