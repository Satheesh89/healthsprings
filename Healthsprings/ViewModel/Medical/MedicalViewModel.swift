//
//  MedicalViewModel.swift
//  Healthsprings
//

import Foundation
import SwiftyJSON

class MedicalViewModel{
    
    var medicalModel: MedicalModel!
    var symptoms: [Symptoms] = []
    
    weak var vc: ProfileSetttingViewController?
    
    func loadJsonData()  {
        if let localData = self.readLocalFile(forName: "Medical") {
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: localData, options: .mutableContainers)
                self.medicalModel = MedicalModel(JSON(jsonResponse))
                self.symptoms = self.medicalModel?.medicalDetail?.symptoms ?? []
                print(self.symptoms.count)
                DispatchQueue.main.async{
                    self.vc?.medicalTableView.reloadData()
                }
            }catch let error{
                print(error)
            }
        }
    }

    private func readLocalFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name,
                                                 ofType: "json"),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        return nil
    }
    
}

