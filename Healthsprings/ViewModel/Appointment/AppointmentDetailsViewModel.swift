//
//  AppointmentDetailsViewModel.swift
//  Healthsprings
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class AppointmentDetailsViewModel {
    weak var vc: AppointmentsDetailsViewController?
    var appointmentDetailsModelList: AppointmentDetailsModel!
    var appointmentDetails: AppointmentDetails!
    
    func getAppointmentDetailsListApi( apitokenStr: String,loadview: UIViewController,profileid: String,appointmentid: Int,viewC: UIViewController) {
        LoadingIndicator.shared.show(forView: loadview.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["appointment_id"] = appointmentid
        paramsDictionary["profile_id"] = profileid
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()

        HttpClientApi.instance().makeAPICall(url: "\(URL.getappointmentdetails)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                let response = response
                if  response?.statusCode == 200 {
                    self.appointmentDetailsModelList = AppointmentDetailsModel(JSON(dataDict))
                    self.appointmentDetails = self.appointmentDetailsModelList.appointmentDetails
                    doOnMain {
                        self.vc?.appointmentDetailTableview.reloadData()
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                        }
                    }
                }else if  response?.statusCode == 401 {
                    let tempArray = NSMutableArray()
                    for  i in 0..<((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.count ?? 0) {
                        let tArray  = ((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.allValues[i]) as? Array<Any>
                        tempArray.add(tArray?.first ?? "")
                    }
                    let errorStr = tempArray.componentsJoined(by: ",")
                    CustomToast.show(message: errorStr,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: viewC,tarbarHeight: 0.0)
                } else{
                    CustomToast.show(message: dataDict["error"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: viewC,tarbarHeight: 0.0)
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
        })

//        AF.request( "\(URL.getappointmentdetails)").response { response in
//            if let data = response.data {
//                do{
//                    let userResponse = try JSONDecoder().decode([MakeRequestModel].self, from: data)
//                    self.symtomList.append(contentsOf: userResponse)
//                    doOnMain {
//                        self.vc?.requestTableView.reloadData()
//                        LoadingIndicator.shared.hide()
//                    }
//                }catch let err{
//                    print(err.localizedDescription)
//                }
//            }
//        }
    }
}
