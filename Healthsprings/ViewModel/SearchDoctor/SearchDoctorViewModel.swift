//
//  SearchDoctorViewModel.swift
//  Healthsprings
//

import Foundation
import Alamofire
import SwiftyJSON

class SearchDoctorViewModel {
    weak var vc: FindDoctorViewController?
    var searchDetailModel : SearchDetailModel?
    var searchDetails: [SearchDetails]?
    var searchDetailsCopy: [SearchDetails]?
    
    func postRequest(getviewController: UIViewController) {
        LoadingIndicator.shared.show(forView: getviewController.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()
        
        HttpClientApi.instance().makeAPICall(url: URL.searchdoctor, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                let response = response
                if  response?.statusCode == 200 {
                    self.searchDetailModel = SearchDetailModel(JSON(dataDict) )
                    self.searchDetails = self.searchDetailModel?.search?.searchDetails
                    self.searchDetailsCopy = self.searchDetailModel?.search?.searchDetails
                    doOnMain {
                        self.vc?.searchTableView.reloadData()
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                        }
                    }
                }else{
                    //self.showAlertMessage(title: "Alert", message: dataDict["error"] as! String ) {
                    //self.hideActivityIndicator()
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
}
