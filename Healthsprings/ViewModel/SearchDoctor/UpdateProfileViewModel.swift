//
//  UpdateProfileViewModel.swift
//  Healthsprings
//

import Foundation
import UIKit
import SwiftyJSON

class UpdateProfileViewModel{
    
    weak var vc: ProfileSetttingViewController?
    var profileDataModel: ProfileDataModel!
    var profileData: ProfileData!
    
    func updatePostRequest(getviewController: UIViewController, getPersonName: String,getEmail: String,getMobilecode: String,getMobilenumber: String,getStreetname: String,getCountry: String,getCity: String,getState: String,getPincode: String,getGender: String,getEmergencycontact: String,getMaritalstatus: String,getDob: String,getHeight: String,getWeight: String,getBloodgroup: String,getNricid: String) {
        LoadingIndicator.shared.show(forView: getviewController.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()
        paramsDictionary["person_name"] = getPersonName
        paramsDictionary["profile_person"] = "self"
        paramsDictionary["email"] = getEmail
        paramsDictionary["mobile_code"] = getMobilecode
        paramsDictionary["mobile_number"] = getMobilenumber
        paramsDictionary["street_name"] = getStreetname
        paramsDictionary["country"] = getCountry
        paramsDictionary["city"] = getCity
        paramsDictionary["state"] = getState
        paramsDictionary["pin_code"] = getPincode
        paramsDictionary["gender"] = "male"
        paramsDictionary["emergency_contact"] = getEmergencycontact
        paramsDictionary["marital_status"] = getMaritalstatus
        paramsDictionary["dob"] = getDob
        paramsDictionary["height"] = getHeight
        paramsDictionary["weight"] = getWeight
        paramsDictionary["blood_group"] = getBloodgroup
        paramsDictionary["nric_id"] = getNricid
        paramsDictionary["full_address"] = UserDetails.shared.getFullAddress ?? ""
        paramsDictionary["state"] = UserDetails.shared.getFullAddress ?? ""
        print("paramsDictionary",paramsDictionary)
        
        HttpClientApi.instance().makeAPICall(url: URL.getupdateprofile, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("dataDict = ",dataDict)
                if  response?.statusCode == 200 {
                    doOnMain {
                        self.vc?.getProfileId = "\(dataDict.value(forKeyPath: "profile_id") ?? "")"
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                        }
                    }
                }else{
                    //self.showAlertMessage(title: "Alert", message: dataDict["error"] as! String ) {
                    //self.hideActivityIndicator()
//                    GlobalClass.sharedInstance.showCustomToast(toastTitle: "Info",toastMeassage: dataDict["error"] as! String,status: 0)
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
//            self.showCustomToast(toastTitle: "Info",toastMeassage: "API call Failure",status: 0)
        })
    }
    
    
    func getProfileGetRequest(getviewController: UIViewController) {
        print("\(URL.getProfile)apiToken=\(UserDetails.shared.getApiToken())")
        LoadingIndicator.shared.show(forView: getviewController.view)
        HttpClientApi.instance().makeAPICall(url: "\(URL.getProfile)apiToken=\(UserDetails.shared.getApiToken())", params:nil, method: .GET, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print(dataDict)
                if  response?.statusCode == 200 {
                    self.profileDataModel = ProfileDataModel(JSON(dataDict))
                    self.profileData = self.profileDataModel.profileData
                    doOnMain {
                        self.vc?.nameTexField.text = self.profileData.patientName
                        self.vc?.emailTextField.text = self.profileData.patientEmail
                        self.vc?.phoneTextField.text = self.profileData.mobileNo
                        self.vc?.dobTextField.text = self.profileData.dateOfBirth
                        self.vc?.countryTextField.text = self.profileData.country
                        self.vc?.postalTextField.text = self.profileData.pinCode
                        self.vc?.bloodGroupTextField.text = self.profileData.bloodGroup
                        self.vc?.maritalStatusTextField.text = self.profileData.maritalStatus
                        self.vc?.heightTextField.text = self.profileData.height
                        self.vc?.weightTextField.text = self.profileData.weight
                        self.vc?.econtactTextField.text = self.profileData.emergencyContact
                        self.vc?.nricTextField.text = self.profileData.nric_id
                        self.vc?.getProfileId = self.profileData.profile_id ?? ""
                        self.vc?.profileImageView.sd_setImage(with: URL(string: self.profileData.profileImage ?? ""), placeholderImage: UIImage(named: "accountplaceholder"))
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                        }
                    }
                }else{
                    //self.showAlertMessage(title: "Alert", message: dataDict["error"] as! String ) {
                    //self.hideActivityIndicator()
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
}
