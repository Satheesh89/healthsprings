//
//  GetCardViewModel.swift
//  Healthsprings
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON

class GetCardViewModel {
    var cardList = [GetCardModel]()
    
    weak var paymentListvc: PaymentListViewController?
    weak var vc: PaymentSettingViewController?
    weak var vcDashBoard: DashBoard?

    
    func getCardListApi( apitokenStr: String,loadview: UIViewController) {
        LoadingIndicator.shared.show(forView: loadview.view)
        print("\(URL.getcard)\(apitokenStr)")
        AF.request( "\(URL.getcard)\(apitokenStr)").response { response in
            if let data = response.data {
                do{
                    let userResponse = try JSONDecoder().decode([GetCardModel].self, from: data)
                    self.cardList.removeAll()
                    self.cardList.append(contentsOf: userResponse)
                    doOnMain {
                        LoadingIndicator.shared.hide()
                        self.paymentListvc?.paymentSettingTableView.reloadData()
                        self.vc?.paymentSettingTableView.reloadData()
                    }
                }catch let err{
                    print(err.localizedDescription)
                }
            }
        }
    }
}
extension GetCardViewModel {
    func getCardListDetails(url: String, handler: @escaping (_ user: GetCardModel?, _ error: AlertMessage?)->()) {
        CardAPIManager().getCardListDetails(url: url, completionHandler: handler)
    }
}
