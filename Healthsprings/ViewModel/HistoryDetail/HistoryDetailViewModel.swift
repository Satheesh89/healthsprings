//
//  HistoryDetailViewModel.swift
//  Healthsprings
//
//  Created by Personal on 05/09/22.
//

import Foundation
import Alamofire
import ObjectMapper
import SwiftyJSON

class HistoryDetailViewModel {
    typealias historyDetails = (_ historyDetails: HistoryModelDetails?,_ status: Bool,_ message: String) -> Void
    var callBackHistoryDetails: historyDetails?
    func getAppoinmentDetails(appointmentId: String,profileId: String) {
        let parameters: [String: Any] = [
            "apiToken": UserDetails.shared.getApiToken(),
            "appointment_id": appointmentId,
            "profile_id": profileId
        ]
        print(parameters)
        AF.request(URL.historydetails, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil, interceptor: nil).response { (responseData) in
            if let data = responseData.data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("History Details >",dataDict)
                if  responseData.response?.statusCode == 200 {
                    self.callBackHistoryDetails?(HistoryModelDetails(JSON(dataDict) ), true,"")
                }else{
                    self.callBackHistoryDetails?(nil, false, "error")
                }
            }
        }
    }
    func completionHandler(callBack: @escaping historyDetails) {
        self.callBackHistoryDetails = callBack
    }
}


