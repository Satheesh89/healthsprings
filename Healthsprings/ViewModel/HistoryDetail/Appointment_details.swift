import Foundation
struct Appointment_details : Codable {
    let doctor_name : String?
    let speciality : String?
    let appointment_date : String?
    let appointment_time : String?
    let patient_name : String?
    let video_id : String?
    let symptom : String?
    let items : [Items]?
    let sub_total : Int?
    let tax_service : String?
    let total_paid : Int?
    let currency : String?
    let payment_date : String?
    let card_no : Int?
    let street_name : String?
    let area_location : String?
    let state : String?
    let city : String?
    let country : String?
    let pin_code : String?
    let avatar : String?
    let bill : String?
    let prescription : String?
    let medical_certificate : String?
    let full_address : String?

    enum CodingKeys: String, CodingKey {

        case doctor_name = "doctor_name"
        case speciality = "speciality"
        case appointment_date = "appointment_date"
        case appointment_time = "appointment_time"
        case patient_name = "patient_name"
        case video_id = "video_id"
        case symptom = "symptom"
        case items = "items"
        case sub_total = "sub_total"
        case tax_service = "tax_service"
        case total_paid = "total_paid"
        case currency = "currency"
        case payment_date = "payment_date"
        case card_no = "card_no"
        case street_name = "street_name"
        case area_location = "area_location"
        case state = "state"
        case city = "city"
        case country = "country"
        case pin_code = "pin_code"
        case avatar = "avatar"
        case bill = "bill"
        case prescription = "prescription"
        case medical_certificate = "medical_certificate"
        case full_address = "full_address"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        doctor_name = try values.decodeIfPresent(String.self, forKey: .doctor_name)
        speciality = try values.decodeIfPresent(String.self, forKey: .speciality)
        appointment_date = try values.decodeIfPresent(String.self, forKey: .appointment_date)
        appointment_time = try values.decodeIfPresent(String.self, forKey: .appointment_time)
        patient_name = try values.decodeIfPresent(String.self, forKey: .patient_name)
        video_id = try values.decodeIfPresent(String.self, forKey: .video_id)
        symptom = try values.decodeIfPresent(String.self, forKey: .symptom)
        items = try values.decodeIfPresent([Items].self, forKey: .items)
        sub_total = try values.decodeIfPresent(Int.self, forKey: .sub_total)
        tax_service = try values.decodeIfPresent(String.self, forKey: .tax_service)
        total_paid = try values.decodeIfPresent(Int.self, forKey: .total_paid)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        payment_date = try values.decodeIfPresent(String.self, forKey: .payment_date)
        card_no = try values.decodeIfPresent(Int.self, forKey: .card_no)
        street_name = try values.decodeIfPresent(String.self, forKey: .street_name)
        area_location = try values.decodeIfPresent(String.self, forKey: .area_location)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        pin_code = try values.decodeIfPresent(String.self, forKey: .pin_code)
        avatar = try values.decodeIfPresent(String.self, forKey: .avatar)
        bill = try values.decodeIfPresent(String.self, forKey: .bill)
        prescription = try values.decodeIfPresent(String.self, forKey: .prescription)
        medical_certificate = try values.decodeIfPresent(String.self, forKey: .medical_certificate)
        full_address = try values.decodeIfPresent(String.self, forKey: .full_address)
    }
}
