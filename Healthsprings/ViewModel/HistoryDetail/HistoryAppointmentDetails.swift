import Foundation
import ObjectMapper

struct HistoryAppointmentDetails : Codable {
    let appointment_details : Appointment_details?
    enum CodingKeys: String, CodingKey {
        case appointment_details = "appointment_details"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        appointment_details = try values.decodeIfPresent(Appointment_details.self, forKey: .appointment_details)
    }
}


