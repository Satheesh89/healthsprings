//
//  HistoryViewModel.swift
//  Healthsprings
//
//  Created by Personal on 29/06/22.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class HistoryViewModel {
    
    weak var vc: HistoryViewController?
    var historyModel : HistoryModel?
    var history_details : [History_details]?

    func getHistoryList(params: Parameters ,loadview: UITabBarController) {
        LoadingIndicator.shared.show(forView: loadview.view)
        self.getHistoryDetails(input: params) { (result: HistoryModel?, alert: AlertMessage?) in
            if let result = result {
                self.history_details = result.history?.history_details
                doOnMain {
                    self.vc?.historyTableView.reloadData()
                    delay(0.1) {
                        LoadingIndicator.shared.hide()
                    }
                }
            } else if let alert = alert {
                print(alert.statusCode)
                print(alert.errorMessage)
            }
        }
    }
    
}

extension HistoryViewModel {
    func getHistoryDetails(input: Parameters,
                      handler: @escaping (_ user: HistoryModel?, _ error: AlertMessage?)->()) {
        HistoryAPIManager().getHistoryDetails(input: input, handler: handler)
    }
}
