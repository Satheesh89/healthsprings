//
//  HealthBlogModelView.swift
//  Healthsprings
//

import Foundation
import ObjectMapper
import Alamofire
import SwiftyJSON

class HealthBlogModelView {
    typealias healthBlogDetails = (_ itemBillDetails: HealthBlogModel?,_ status: Bool,_ message: String) -> Void
    var callBackHealthBlogDetails: healthBlogDetails?
    func getHealthBlogDetails() {
        
        let parameters: [String: Any] = [
            "apiToken": UserDetails.shared.getApiToken()
        ]
        print(parameters)
        AF.request(URL.getHealthBlog, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil, interceptor: nil).response { (responseData) in
            if let data = responseData.data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("HealthBlog --->",dataDict)
                if  responseData.response?.statusCode == 200 {
                    self.callBackHealthBlogDetails?(HealthBlogModel(JSON(dataDict) ), true,"")
                }else{
                    self.callBackHealthBlogDetails?(nil, false, "error")
                }
            }
        }
    }
    
    func completionHandler(callBack: @escaping healthBlogDetails) {
        self.callBackHealthBlogDetails = callBack
    }
}
