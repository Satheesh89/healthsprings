//
//  MakeRequestViewModel.swift
//  Healthsprings
//
//  Created by Personal on 20/07/22.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class MakeRequestViewModel {
    weak var vc: MakeRequestViewController?
    var symtomList = [MakeRequestModel]()
    
    var getProfileDataListModel: GetAllProfileModel!
    var profileData: [GetProfileData]?

    func getSymptomListApi( apitokenStr: String,loadview: UIViewController) {
        LoadingIndicator.shared.show(forView: loadview.view)
        AF.request( "\(URL.getsymptom)").response { response in
            if let data = response.data {
                do{
                    let userResponse = try JSONDecoder().decode([MakeRequestModel].self, from: data)
                    self.symtomList.append(contentsOf: userResponse)
                    doOnMain {
                        self.vc?.requestTableView.reloadData()
                        LoadingIndicator.shared.hide()
                    }
                }catch let err{
                    print(err.localizedDescription)
                }
            }
        }
    }
    
    
    func getAllProfileListApi( apitokenStr: String,loadview: UIViewController) {        
        LoadingIndicator.shared.show(forView: loadview.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()

        HttpClientApi.instance().makeAPICall(url: "\(URL.getallprofile)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                let response = response
                if  response?.statusCode == 200 {
                    self.getProfileDataListModel = GetAllProfileModel(JSON(dataDict))
                    self.profileData = self.getProfileDataListModel?.profileData ?? []
                    doOnMain {
                        if self.getProfileDataListModel.profileData?.count ?? 0 > 0{
                            self.vc?.getProfileId = self.getProfileDataListModel.profileData?[0].profileId ?? ""
                        }
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                        }
                    }
                }else if  response?.statusCode == 401 {
                    let tempArray = NSMutableArray()
                    for  i in 0..<((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.count ?? 0) {
                        let tArray  = ((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.allValues[i]) as? Array<Any>
                        tempArray.add(tArray?.first ?? "")
                    }
                    let errorStr = tempArray.componentsJoined(by: ",")
                    CustomToast.show(message: errorStr,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: loadview,tarbarHeight: 0.0)
                } else{
                    CustomToast.show(message: dataDict["error"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: loadview,tarbarHeight: 0.0)
                }

            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
}
