//
//  TimeSlotViewModel.swift
//  Healthsprings
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class TimeSlotViewModel {
    weak var vc: TimeSlotViewController?
    var availabilitiesModelList: AvailabilitiesModel!
    var availabilities: [Availabilities]?
    var selectDate: String?
    var morningSection: [MorningSection]?
    var afternoonSection: [AfternoonSection]?
    var eveningSection: [EveningSection]?
    var nightSection: [NightSection]?

    weak var scheduleVC: ScheduleTimeViewController?
    var clinicAvailableDaysList = [ClinicAvailableDays]()

    var clinicAvailableSlotsModel: ClinicAvailableSlotsModel!
    var clinicMorning: [ClinicMorning]?
    var clinicAfternoon: [ClinicAfternoon]?
    var clinicEvening: [ClinicEvening]?
    var clinicNight: [ClinicNight]?

    func getTimeSlotListApi( loadview: UIViewController,doctorid: Int,dateStr: String) {
        LoadingIndicator.shared.show(forView: loadview.view)
        var paramsDictionary = [String:Any]()
//        paramsDictionary["date"] = dateStr
        paramsDictionary["doctor_id"] = doctorid
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()

        HttpClientApi.instance().makeAPICall(url: "\(URL.getdoctoravailability)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                let response = response
                if  response?.statusCode == 200 {
                    self.availabilitiesModelList = AvailabilitiesModel(JSON(dataDict))
                    self.availabilities = self.availabilitiesModelList.availabilities
                    self.morningSection = self.availabilitiesModelList.morningSection
                    self.afternoonSection = self.availabilitiesModelList.afternoonSection
                    self.eveningSection = self.availabilitiesModelList.eveningSection
                    self.nightSection = self.availabilitiesModelList.nightSection
                    doOnMain {
                        self.vc?.timeSlotTbleView.reloadData()
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                        }
                    }
                }else if  response?.statusCode == 401 {
                    let tempArray = NSMutableArray()
                    for  i in 0..<((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.count ?? 0) {
                        let tArray  = ((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.allValues[i]) as? Array<Any>
                        tempArray.add(tArray?.first ?? "")
                    }
                    let errorStr = tempArray.componentsJoined(by: ",")
                    CustomToast.show(message: errorStr,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: loadview,tarbarHeight: 0.0)
                } else{
                    CustomToast.show(message: dataDict["error"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: loadview,tarbarHeight: 0.0)
                }

            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
    
    //ClinicAvailableDays
    func getClinicAvailableDaysApi(loadview: UIViewController) {
        LoadingIndicator.shared.show(forView: loadview.view)
        let parameters: [String: Any] = [
            "apiToken": UserDetails.shared.getApiToken()]
        print(parameters)
        AF.request("\(URL.clinicAvailableDays)", method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil, interceptor: nil).response { response in
            if let data = response.data {
                do{
                    let userResponse = try JSONDecoder().decode([ClinicAvailableDays].self, from: data)
                    self.clinicAvailableDaysList.removeAll()
                    self.clinicAvailableDaysList.append(contentsOf: userResponse)
                    doOnMain {
                        LoadingIndicator.shared.hide()
                        if self.clinicAvailableDaysList.count > 0{
                            self.scheduleVC?.selectSlotStr = "\(self.clinicAvailableDaysList.first?.display_date ?? "")"
                            self.scheduleVC?.selectTimeSlotIndexpath = IndexPath(item:0, section:0)
                            self.scheduleVC?.selectDate = self.clinicAvailableDaysList.first?.date ?? ""
                            self.getClinicTimeSlotListApi( loadview: loadview,dateStr: self.clinicAvailableDaysList.first?.date ?? "")
                        }
                        self.scheduleVC?.timeSlotTbleView.reloadData()
                    }
                }catch let err{
                    print(err.localizedDescription)
                }
            }
        }
    }
    
    //ClinicAvailableDays
    func getClinicTimeSlotListApi( loadview: UIViewController,dateStr: String) {
        LoadingIndicator.shared.show(forView: loadview.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["date"] = dateStr
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()

        HttpClientApi.instance().makeAPICall(url: "\(URL.clinicAvailableSlots)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                let response = response
                if  response?.statusCode == 200 {
                    self.clinicAvailableSlotsModel = ClinicAvailableSlotsModel(JSON(dataDict))
                    self.clinicMorning = self.clinicAvailableSlotsModel.morning
                    self.clinicAfternoon = self.clinicAvailableSlotsModel.afternoon
                    self.clinicEvening = self.clinicAvailableSlotsModel.evening
                    self.clinicNight = self.clinicAvailableSlotsModel.night
                    doOnMain {
                        self.scheduleVC?.timeSlotTbleView.reloadData()
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                        }
                    }
                }else if  response?.statusCode == 401 {
                    let tempArray = NSMutableArray()
                    for  i in 0..<((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.count ?? 0) {
                        let tArray  = ((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.allValues[i]) as? Array<Any>
                        tempArray.add(tArray?.first ?? "")
                    }
                    let errorStr = tempArray.componentsJoined(by: ",")
                    CustomToast.show(message: errorStr,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: loadview,tarbarHeight: 0.0)
                } else{
                    CustomToast.show(message: dataDict["error"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: loadview,tarbarHeight: 0.0)
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
}
