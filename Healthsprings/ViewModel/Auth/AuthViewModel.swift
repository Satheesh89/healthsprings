//
//  AuthViewModel.swift
//  Healthsprings
//
//  Created by Personal on 27/06/22.
//

import Foundation
import Alamofire

class AuthViewModel {
    func requestLogin(input: Parameters,
                      handler: @escaping (_ user: AuthModel?, _ error: AlertMessage?)->()) {
        AuthApiManager().requestLogIn(input: input, handler: handler)
    }

    
    func requestSignUp(input: Parameters,
                      handler: @escaping (_ user: AuthModel?, _ error: AlertMessage?)->()) {
        AuthApiManager().requestSignUp(input: input, handler: handler)
    }
    
    
    func requestNormalLogin(input: Parameters,
                      handler: @escaping (_ user: AuthModel?, _ error: AlertMessage?)->()) {
        AuthApiManager().requestNormalLogin(input: input, handler: handler)
    }
    
}
