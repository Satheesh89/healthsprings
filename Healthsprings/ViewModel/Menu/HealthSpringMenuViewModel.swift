//
//  HealthSpringMenuViewModel.swift
//  Healthsprings
//
//  Created by Personal on 15/06/22.
//

import Foundation

class HealthSpringMenuViewModel {
    
    var healthMenu: [HealthSpringMenuModel] = []
    init(){}
    
    func initializeData()  {
        healthMenu.removeAll()
        healthMenu.append(HealthSpringMenuModel(title: "Login"))
        healthMenu.append(HealthSpringMenuModel(title: "New User - Register"))
        healthMenu.append(HealthSpringMenuModel(title: "Login with singpass"))
        healthMenu.append(HealthSpringMenuModel(title: "Video Consultation Flow"))
        healthMenu.append(HealthSpringMenuModel(title: "Search Doctors"))
        healthMenu.append(HealthSpringMenuModel(title: "Apointments"))
        healthMenu.append(HealthSpringMenuModel(title: "History"))
        healthMenu.append(HealthSpringMenuModel(title: "Setting"))
        healthMenu.append(HealthSpringMenuModel(title: "Profile Settings"))
        healthMenu.append(HealthSpringMenuModel(title: "Payment Setting"))
        healthMenu.append(HealthSpringMenuModel(title: "Notifications"))
        healthMenu.append(HealthSpringMenuModel(title: "Notifications Empty State"))
        healthMenu.append(HealthSpringMenuModel(title: "FAQ"))
        healthMenu.append(HealthSpringMenuModel(title: "Feedback"))
        healthMenu.append(HealthSpringMenuModel(title: "End call Feedback"))
    }

    // MARK: - Get Items Count
    func numberOfItems()-> Int{
        return healthMenu.count
    }
    
    // MARK: - Get Index
    func getItemIndex(index: Int) -> HealthSpringMenuModel {
        return healthMenu[index]
    }
    
}
