//
//  NotificationViewController.swift
//  Healthsprings
//
//  Created by Personal on 19/06/22.
//

import UIKit

class NotificationViewController: UIViewController {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var notificationTableView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var viewAllButton: UIButton!
    var notificationsModel : [NotificationsModel]!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initializeHideKeyboard()
        loadViewModel()
    }

    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func  loadViewModel()  {
        LoadingIndicator.shared.show(forView: self.view)
        let notificationsViewModel = NotificationsViewModel()
        notificationsViewModel.getNotificationDetails()
        notificationsViewModel.completionHandler{[weak self] (details, status, message)in
            if status {
                guard let self = self else {return}
                guard let _notifications = details else {return}
                self.notificationsModel = _notifications
                self.notificationTableView.reloadData()
                self.tableViewSetup()
                doOnMain {
                    LoadingIndicator.shared.hide()}}}
    }
    
    //MARK: TableView Setup
    func tableViewSetup()  {
        notificationTableView.register(NotificationsTableViewCell.nib, forCellReuseIdentifier: NotificationsTableViewCell.identifier)
        notificationTableView.delegate = self
        notificationTableView.dataSource = self
        notificationTableView.tableFooterView = UIView()
        notificationTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        notificationTableView.showsHorizontalScrollIndicator = false
        notificationTableView.showsVerticalScrollIndicator = true
        //serviceTableView.setContentOffset(.zero, animated: true)
        notificationTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        notificationTableView.backgroundColor = .clear
        notificationTableView.contentInset.top = 5
        notificationTableView.contentInsetAdjustmentBehavior = .never
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setStatusBar(color: UIColor.themeColor)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    // MARK: - Left Action
    @IBAction func didTapleftAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    // MARK: - Notification Action
    @IBAction func didTapNotificationAction(_ sender: Any) {
    }
}


extension NotificationViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificationsModel.count 
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            return getNotificationTableViewCell(tableView: tableView, indexPath: indexPath)
    }
    
    func getNotificationTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NotificationsTableViewCell.identifier) as? NotificationsTableViewCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.getNotificationModel = self.notificationsModel[indexPath.row]
        return cell
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
