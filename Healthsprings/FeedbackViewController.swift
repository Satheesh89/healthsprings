//
//  FeedbackViewController.swift
//  Healthsprings
//
//  Created by Personal on 19/06/22.
//

import UIKit

class FeedbackViewController: UIViewController {
    
    @IBOutlet weak var navVIew: UIView!
    @IBOutlet weak var leftButton: UIButton!
    
    @IBOutlet weak var terribleView: UIView!
    @IBOutlet weak var terribleImageView: UIImageView!
    @IBOutlet weak var terribleLabel: UILabel!
    
    @IBOutlet weak var badView: UIView!
    @IBOutlet weak var badImageView: UIImageView!
    @IBOutlet weak var badLabel: UILabel!
    
    @IBOutlet weak var okayView: UIView!
    @IBOutlet weak var okayImageView: UIImageView!
    @IBOutlet weak var okayLabel: UILabel!
    
    @IBOutlet weak var goodView: UIView!
    @IBOutlet weak var goodImageView: UIImageView!
    @IBOutlet weak var goodLabel: UILabel!
    
    @IBOutlet weak var excellentView: UIView!
    @IBOutlet weak var excellentImageView: UIImageView!
    @IBOutlet weak var excellentLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    var checkBool = Bool()

    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var describleView: UIView!

    
    @IBOutlet var textView : UITextView!
    var placeholderLabel : UILabel!
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setStatusBar(color: UIColor.themeColor)
        setUpCheckBoxButton()
        setUpFeebBack()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    // MARK: - Left Action
    @IBAction func didTapleftAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func didTapCheckAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        checkBool = sender.isSelected
        setUpCheckBoxButton()
    }
    
    @IBAction func didTapSubmitAction(_ sender: Any) {
        if "\(textView.text ?? "")" == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter your feedback",status: 0)
        }else{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Successfully submit your feedback",status: 0)
        }
    }
    
    @IBAction func didTapCancelAction(_ sender: Any) {
    }
    
    //MARK:- Check
    func setUpCheckBoxButton() {
        checkButton.backgroundColor = UIColor.clear
        if checkBool{
            checkButton.setImage(UIImage(named: "check"), for: .normal)
        }else{
            checkButton.setImage(UIImage(named: "uncheck"), for: .normal)
        }
    }
    
    //MARK:- Check
    func setUpFeebBack() {
        terribleView.layer.cornerRadius = 5
        terribleView.clipsToBounds = false
        terribleView.layer.borderWidth = 1
        terribleView.layer.borderColor = UIColor.convertRGB(hexString: "#E6E6E6").cgColor

        badView.layer.cornerRadius = 5
        badView.clipsToBounds = false
        badView.layer.borderWidth = 1
        badView.layer.borderColor = UIColor.convertRGB(hexString: "#E6E6E6").cgColor

        okayView.layer.cornerRadius = 5
        okayView.clipsToBounds = false
        okayView.layer.borderWidth = 1
        okayView.layer.borderColor = UIColor.convertRGB(hexString: "#E6E6E6").cgColor

        goodView.layer.cornerRadius = 5
        goodView.clipsToBounds = false
        goodView.layer.borderWidth = 1
        goodView.layer.borderColor = UIColor.convertRGB(hexString: "#E6E6E6").cgColor

        excellentView.layer.cornerRadius = 5
        excellentView.clipsToBounds = false
        excellentView.layer.borderWidth = 1
        excellentView.layer.borderColor = UIColor.convertRGB(hexString: "#E6E6E6").cgColor
        
        submitButton.layer.cornerRadius = 5
        submitButton.clipsToBounds = true
        
        cancelButton.layer.cornerRadius = 5
        cancelButton.clipsToBounds = false
        cancelButton.layer.borderWidth = 1
        cancelButton.layer.borderColor = UIColor.convertRGB(hexString: "#E6E6E6").cgColor

        describleView.layer.cornerRadius = 5
        describleView.clipsToBounds = false
        describleView.layer.borderWidth = 1
        describleView.layer.borderColor = UIColor.convertRGB(hexString: "#E6E6E6").cgColor
        
        textView.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel.text = "enter your feedback..."
        
        if ( UIDevice.current.model.range(of: "iPad") != nil){
            placeholderLabel.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(08.0))
        } else {
            placeholderLabel.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))
        }

        placeholderLabel.sizeToFit()
        textView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (textView.font?.pointSize)! / 2)
        placeholderLabel.textColor = .themeColor
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    //MARK:- feedback Status
    @IBAction func didTapfeedbackRateAction(_ sender: UIButton) {
        if sender.tag == 1{
            sender.isSelected = !sender.isSelected
            if sender.isSelected {
                terribleView.backgroundColor = .themeColor
                terribleLabel.textColor = .white
                terribleImageView.image = UIImage(named: "terrible")?.imageWithColor(color: .white)
            }else{
                feedBackViewRemoveStatus()
            }
        }else if sender.tag == 2{
            sender.isSelected = !sender.isSelected
            if sender.isSelected {
                terribleView.backgroundColor = .themeColor
                terribleLabel.textColor = .white
                terribleImageView.image = UIImage(named: "terrible")?.imageWithColor(color: .white)
                
                badView.backgroundColor = .themeColor
                badLabel.textColor = .white
                badImageView.image = UIImage(named: "bad")?.imageWithColor(color: .white)
            }else{
                feedBackViewRemoveStatus()
            }
        }else if sender.tag == 3{
            sender.isSelected = !sender.isSelected
            if sender.isSelected {
                terribleView.backgroundColor = .themeColor
                terribleLabel.textColor = .white
                terribleImageView.image = UIImage(named: "terrible")?.imageWithColor(color: .white)
                
                badView.backgroundColor = .themeColor
                badLabel.textColor = .white
                badImageView.image = UIImage(named: "bad")?.imageWithColor(color: .white)

                terribleView.backgroundColor = .themeColor
                terribleLabel.textColor = .white
                terribleImageView.image = UIImage(named: "terrible")?.imageWithColor(color: .white)
                
                okayView.backgroundColor = .themeColor
                okayLabel.textColor = .white
                okayImageView.image = UIImage(named: "okay")?.imageWithColor(color: .white)
            }else{
                feedBackViewRemoveStatus()
            }
        }else if sender.tag == 4{
            sender.isSelected = !sender.isSelected
            if sender.isSelected {
                terribleView.backgroundColor = .themeColor
                terribleLabel.textColor = .white
                terribleImageView.image = UIImage(named: "terrible")?.imageWithColor(color: .white)
                
                badView.backgroundColor = .themeColor
                badLabel.textColor = .white
                badImageView.image = UIImage(named: "bad")?.imageWithColor(color: .white)

                terribleView.backgroundColor = .themeColor
                terribleLabel.textColor = .white
                terribleImageView.image = UIImage(named: "terrible")?.imageWithColor(color: .white)
                
                okayView.backgroundColor = .themeColor
                okayLabel.textColor = .white
                okayImageView.image = UIImage(named: "okay")?.imageWithColor(color: .white)

                goodView.backgroundColor = .themeColor
                goodLabel.textColor = .white
                goodImageView.image = UIImage(named: "good")?.imageWithColor(color: .white)
            }else{
                feedBackViewRemoveStatus()
            }
        }else if sender.tag == 5{
            sender.isSelected = !sender.isSelected
            if sender.isSelected {
                terribleView.backgroundColor = .themeColor
                terribleLabel.textColor = .white
                terribleImageView.image = UIImage(named: "terrible")?.imageWithColor(color: .white)
                
                badView.backgroundColor = .themeColor
                badLabel.textColor = .white
                badImageView.image = UIImage(named: "bad")?.imageWithColor(color: .white)

                terribleView.backgroundColor = .themeColor
                terribleLabel.textColor = .white
                terribleImageView.image = UIImage(named: "terrible")?.imageWithColor(color: .white)
                
                okayView.backgroundColor = .themeColor
                okayLabel.textColor = .white
                okayImageView.image = UIImage(named: "okay")?.imageWithColor(color: .white)

                goodView.backgroundColor = .themeColor
                goodLabel.textColor = .white
                goodImageView.image = UIImage(named: "good")?.imageWithColor(color: .white)
                
                excellentView.backgroundColor = .themeColor
                excellentLabel.textColor = .white
                excellentImageView.image = UIImage(named: "excellent")?.imageWithColor(color: .white)
            }else{
                feedBackViewRemoveStatus()
            }
        }
    }
    
    //MARK:- feedback Status
    func feedBackViewRemoveStatus(){
       let removeColor =  UIColor.convertRGB(hexString: "#6E6E6E")
        terribleView.backgroundColor = .white
        terribleLabel.textColor = removeColor
        terribleImageView.image = UIImage(named: "terrible")?.imageWithColor(color: removeColor)
        
        badView.backgroundColor = .white
        badLabel.textColor = removeColor
        badImageView.image = UIImage(named: "bad")?.imageWithColor(color: removeColor)

        terribleView.backgroundColor = .white
        terribleLabel.textColor = removeColor
        terribleImageView.image = UIImage(named: "terrible")?.imageWithColor(color: removeColor)
        
        okayView.backgroundColor = .white
        okayLabel.textColor = removeColor
        okayImageView.image = UIImage(named: "okay")?.imageWithColor(color: removeColor)

        goodView.backgroundColor = .white
        goodLabel.textColor = removeColor
        goodImageView.image = UIImage(named: "good")?.imageWithColor(color: removeColor)
        
        excellentView.backgroundColor = .white
        excellentLabel.textColor = removeColor
        excellentImageView.image = UIImage(named: "excellent")?.imageWithColor(color: removeColor)
    }
    
}


extension FeedbackViewController : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
}
