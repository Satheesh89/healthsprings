//
//  PaymentHeaderCell.swift
//  Healthsprings
//
//  Created by Personal on 12/09/22.
//

import UIKit

class PaymentHeaderCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib {
      return UINib(nibName: "PaymentHeaderCell", bundle: nil)
    }
    static var identifier: String {
      return String(describing: self)
    }
}
