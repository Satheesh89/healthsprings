//
//  AddProfileViewController.swift
//  Healthsprings
//
//  Created by Personal on 15/08/22.
//

import UIKit
import SwiftyJSON
import MDatePickerView
import iOSDropDown

class AddProfileViewController: UIViewController ,MDatePickerViewDelegate{
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var personNameTextField: UITextField!
    @IBOutlet weak var mobileCodeTextField: UITextField!
    @IBOutlet weak var mobileNoTextField: UITextField!
    @IBOutlet weak var dobTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var streetNameTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var areaLocationTextField: UITextField!
    @IBOutlet weak var pincodeTextField: UITextField!
    @IBOutlet weak var personDropdown: DropDown!
    
    let countrycodepicker = UIPickerView()
    let mobileCodepicker = UIPickerView()
    
    var countryCodeArray = NSArray()
    var countryModel: CountryModel!
    
    var countryCode: [Code] = []
    var mobileCode: [Code] = []
    var countryCodeStr = String()
    
    //MARK:- DatePicker
    lazy var dobBgView : UIView = {
        let dobdate = UIView()
        dobdate.backgroundColor = UIColor(red: 13/255, green: 133/255, blue: 163/255, alpha: 1)
        dobdate.translatesAutoresizingMaskIntoConstraints = false
        dobdate.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return dobdate
    }()
    
    lazy var MDate : MDatePickerView = {
        let mdate = MDatePickerView()
        mdate.delegate = self
        mdate.Color = UIColor(red: 13/255, green: 133/255, blue: 163/255, alpha: 1)
        mdate.cornerRadius = 14
        mdate.translatesAutoresizingMaskIntoConstraints = false
        mdate.from = 1920
        return mdate
    }()
    
    let Today : UIButton = {
        let but = UIButton(type:.system)
        but.setTitle("ToDay", for: .normal)
        but.setTitleColor(.white, for: .normal)
        but.titleLabel?.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(16.0))
        but.addTarget(self, action: #selector(today), for: .touchUpInside)
        but.translatesAutoresizingMaskIntoConstraints = false
        but.layer.cornerRadius = 14
        but.backgroundColor = UIColor(red: 13/255, green: 133/255, blue: 163/255, alpha: 1)
        return but
    }()
    
    let Label : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "Select Date of brith"
        label.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(16.0))
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    @objc func today() {
        MDate.selectDate = Date()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.countryTextField.delegate = self
        self.countryTextField.inputView = self.countrycodepicker
        self.countryTextField.tintColor = .clear
        self.countrycodepicker.dataSource = self
        self.countrycodepicker.delegate = self
        self.countrycodepicker.backgroundColor = .themeColor
        self.countrycodepicker.tag = 1000
        
        self.mobileCodeTextField.delegate = self
        self.mobileCodeTextField.inputView = self.mobileCodepicker
        self.mobileCodeTextField.tintColor = .clear
        self.mobileCodepicker.dataSource = self
        self.mobileCodepicker.delegate = self
        self.mobileCodepicker.backgroundColor = .themeColor
        self.mobileCodepicker.tag = 1001
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        dobBgView.addGestureRecognizer(tap)
        dobBgView.isUserInteractionEnabled = true
        dobBgView.addSubview(view)
        
        
        loadJsonData()
        personDropDown()
        loadCountryJsonData()
    }
    
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: - Back
    @IBAction func didClosedPatientAction(_ sender: UIButton) {
        self.navigationController?.popToViewController(ofClass: MakeRequestViewController.self)
    }
    
    func loadCountryJsonData()  {
        if let localData = self.readLocalFile(forName: "GetCountryList") {
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: localData, options: .mutableContainers)
                self.countryModel = CountryModel(JSON(jsonResponse))
                self.countryCode = self.countryModel?.countryDetail?.code ?? []
                print(self.countryCode)
                //self.mobileCode = self.countryModel?.countryDetail?.code ?? []
            }catch let error{
                print(error)
            }
        }
    }
    
    func loadJsonData()  {
        if let localData = self.readLocalFile(forName: "Country") {
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: localData, options: .mutableContainers)
                self.countryModel = CountryModel(JSON(jsonResponse))
//                self.countryCode = self.countryModel?.countryDetail?.code ?? []
                self.mobileCode = self.countryModel?.countryDetail?.code ?? []
            }catch let error{
                print(error)
            }
        }
    }
    
    private func readLocalFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name,
                                                 ofType: "json"),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        return nil
    }
    
    @IBAction func didTapAddProfileAction(_ sender: Any) {
        if nameTextField!.text == "" {
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter name",status: 0)
        } else if personNameTextField.text == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter the person name",status: 0)
        }else if emailTextField.text == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter the email id",status: 0)
        }else if mobileCodeTextField.text == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter the mobile code",status: 0)
        }else if mobileNoTextField.text == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter the mobile number",status: 0)
        }else if countryCodeStr == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter the country code",status: 0)
        } else if streetNameTextField.text == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter the street name",status: 0)
        }else if cityTextField.text == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter the city",status: 0)
        }else if stateTextField.text == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter the state",status: 0)
        }else if areaLocationTextField.text == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter the area",status: 0)
        }else if pincodeTextField.text == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter the pincode",status: 0)
        }else if dobTextField.text == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter the DOB",status: 0)
        }else{
            getAddProfileListApi(loadview: self)
        }
    }
    
    @IBAction func DOBAction(_ sender: Any) {
        showDatePIcker()
        dobBgView.isHidden = false
    }
    
    func showDatePIcker(){
        view.addSubview(dobBgView)
        NSLayoutConstraint.activate([
            dobBgView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            dobBgView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            dobBgView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            dobBgView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        ])
        dobBgView.addSubview(MDate)
        NSLayoutConstraint.activate([
            MDate.centerYAnchor.constraint(equalTo: dobBgView.centerYAnchor, constant: 0),
            MDate.centerXAnchor.constraint(equalTo: dobBgView.centerXAnchor, constant: 0),
            MDate.widthAnchor.constraint(equalTo: dobBgView.widthAnchor, multiplier: 0.7),
            MDate.heightAnchor.constraint(equalTo: dobBgView.heightAnchor, multiplier: 0.4)
        ])
        
        dobBgView.addSubview(Today)
        Today.centerXAnchor.constraint(equalTo: dobBgView.centerXAnchor, constant: 0).isActive = true
        Today.topAnchor.constraint(equalTo: MDate.bottomAnchor, constant: 20).isActive = true
        Today.leftAnchor.constraint(equalTo: MDate.leftAnchor, constant: 0).isActive = true
        Today.trailingAnchor.constraint(equalTo: MDate.trailingAnchor, constant: 0).isActive = true
        //        Today.heightAnchor.constraint(equalTo: MDate.heightAnchor, constant: 10).isActive = true
        
        dobBgView.addSubview(Label)
        Label.topAnchor.constraint(equalTo: Today.bottomAnchor, constant: 10).isActive = true
        Label.centerXAnchor.constraint(equalTo: dobBgView.centerXAnchor, constant: 0).isActive = true
    }
    
    func mdatePickerView(selectDate: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.string(from: selectDate)
        Label.text = date
        dobTextField.text = date
        print(date)
        dobBgView.isHidden = true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: MDate) == true {
            return false
        }
        return true
    }
    
    //MARK: handleTap
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        dobBgView.isHidden = true
    }
    
    // MARK: - barDropDown
    func personDropDown()  {
        OperationQueue.main.addOperation({
            self.personDropdown.optionArray = ["self","son","daughter","spouse","father","mother","brother","sister","son-in-law","daughter-in-law","mother-in-law","father-in-law","friend","guardian","paternal-grandfather","paternal-grandmother","maternal-grandfather","maternal-grandmother"]
            self.personDropdown.resignFirstResponder()
            self.personDropdown.textColor = .lightGray
            self.personDropdown.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))
            self.personDropdown.didSelect{(selectedText , index ,id) in
                print("Select-index",index)
                self.personNameTextField.text = selectedText
            }
        })
    }
    
    // MARK: - Add Profile
    func getAddProfileListApi(loadview: UIViewController) {
        LoadingIndicator.shared.show(forView: loadview.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()
        paramsDictionary["person_name"] = "\(nameTextField.text ?? "")"
        paramsDictionary["profile_person"] = "\(personNameTextField.text ?? "")"
        paramsDictionary["email"] = "\(emailTextField.text ?? "")"
//        paramsDictionary["mobile_code"] = "\(mobileCodeTextField.text ?? "+91")"
        paramsDictionary["mobile_code"] = "23"
        paramsDictionary["mobile_number"] = "\(mobileNoTextField.text ?? "")"
        paramsDictionary["street_name"] = "\(streetNameTextField.text ?? "")"
        print(countryCodeStr.removeSpecialCharacters().condensedWhitespace)
        paramsDictionary["country"] = countryCodeStr.removeSpecialCharacters().condensedWhitespace
        paramsDictionary["city"] = "\(cityTextField.text ?? "")"
        paramsDictionary["state"] = "\(stateTextField.text ?? "")"
        paramsDictionary["area_location"] = "\(areaLocationTextField.text ?? "")"
        paramsDictionary["pin_code"] = "\(pincodeTextField.text ?? "")"
        paramsDictionary["dob"] = "\(dobTextField.text ?? "")"
        print(paramsDictionary)
        
        HttpClientApi.instance().makeAPICall(url: "\(URL.getaddprofile)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("dataDict",dataDict)
                let response = response
                if  response?.statusCode == 200 {
                    doOnMain {
                        LoadingIndicator.shared.hide()
                        self.navigationController?.popToViewController(ofClass: MakeRequestViewController.self)
                    }
                }else{
                    doOnMain {
                        self.showToast(message: "error")
                        LoadingIndicator.shared.hide()
                        self.navigationController?.popToViewController(ofClass: MakeRequestViewController.self)
                    }
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
    
}

extension AddProfileViewController:  UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1000{
            return mobileCode.count
        }else{
            return self.countryCode.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(20.0))
            pickerLabel?.textAlignment = .center
        }
        if pickerView.tag == 1000{
            pickerLabel?.text = countryCode[row].name
        }else{
            pickerLabel?.text = mobileCode[row].dialCode
        }
        pickerLabel?.textColor = UIColor.white
        return pickerLabel!
    }
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1000{
            countryTextField.text = "\(countryCode[row].name ?? "")"
            countryCodeStr = "\(countryCode[row].dialCode ?? "")"
        }else if pickerView.tag == 1001{
            mobileCodeTextField.text = mobileCode[row].dialCode
//            mobileCode = mobileCode[row].dialCode
        }
    }
}
