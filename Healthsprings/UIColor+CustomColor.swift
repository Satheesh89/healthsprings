//
//  UIColor+CustomColor.swift
//  Healthsprings
//
//  Created by Personal on 15/06/22.
//

import UIKit

extension UIColor {
    
    class var darkGrayColor: UIColor {
        let color = #colorLiteral(red: 0.1999788582, green: 0.2000134587, blue: 0.1999712586, alpha: 1)
        return color
    }
    class var grayColor: UIColor {
        let color = #colorLiteral(red: 0.5414620042, green: 0.5408209562, blue: 0.5626216531, alpha: 1)
        return color
    }

    
    class var appLightColor: UIColor {
        let color = UIColor.darkGray
        return color
    }
    
    class var appLightTextFieldBoderColor: UIColor {
        let color = UIColor.init(red: 128/255, green: 128/255, blue: 128/255, alpha: 1.0)
        return color
    }
    
    class var blueColor: UIColor {
        let color = #colorLiteral(red: 0, green: 0.4787920713, blue: 0.9984667897, alpha: 1)
        return color
    }
    class var blackColor: UIColor {
        let color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        return color
    }
    class var whiteColor: UIColor {
        let color = #colorLiteral(red: 0.999904573, green: 1, blue: 0.9998808503, alpha: 1)
        return color
    }
    
    class var appBgViewLightColor: UIColor {
        let color = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.5)
        return color
    }
    
    class var appHighlightGreenColor: UIColor {
        let color = UIColor(red: 59.00/255.0, green: 186.00/255.00, blue: 107.00/255.00, alpha: 1.0)
        return color
    }
    
    class var appHighlightColor: UIColor {
        let color = UIColor(red: 237/255, green: 34/255, blue: 144/255, alpha: 1.0)
        return color
    }
    
    class var disableNextBtnColor: UIColor {
        let color = UIColor.init(red: 102/255, green: 102/255, blue: 102/255, alpha: 1.0)
        return color
    }
    
    class var termBtnColor: UIColor {
        let color = UIColor.init(red: 176/255, green: 25/255, blue: 126/255, alpha: 1.0)
        return color
    }
    
    
    class var enableBgColor: UIColor {
        let color = UIColor.init(red: 11/255, green: 123/255, blue: 136/255, alpha: 1.0)
        return color
    }
    
    class var bottomBgColor: UIColor {
        let color = UIColor.init(red: 247/255, green: 247/255, blue: 247/255, alpha: 1.0)
        return color
    }
    
    class var switchBgColor: UIColor {
        let color = UIColor.init(red: 113/255, green: 45/255, blue: 139/255, alpha: 1.0)
        return color
    }
    
    class var allowBtnBgColor: UIColor {
        let color = UIColor.init(red: 11/255, green: 123/255, blue: 136/255, alpha: 1.0)
        return color
    }
    
    class var lightBorderColor: UIColor {
        let color = UIColor.init(red: 211/255, green: 211/255, blue: 211/255, alpha: 0.5)
        return color
    }
    
    class var themeColor: UIColor {
        return UIColor.convertRGB(hexString: "#109CBF")
    }
    
    class var selectborderColor: UIColor {
        return UIColor.convertRGB(hexString: "#E6E6E6")
    }
    
    class var tababarthemeColor: UIColor {
        return UIColor.convertRGB(hexString: "#11849E")
    }
    
    class var pageControlColor: UIColor {
        return UIColor.convertRGB(hexString: "#506EAE")
    }
    
    class var loginGeryColor: UIColor {
        return UIColor.convertRGB(hexString: "#D0D0D0")
    }
    
    class var loginLightGeryColor: UIColor {
        return UIColor.convertRGB(hexString: "#A0A0A0")
    }
    
    class var filterBgColor: UIColor {
        return UIColor.convertRGB(hexString: "#E2E2E2")
    }
    
    class var joinStatusBgColor: UIColor {
        return UIColor.convertRGB(hexString: "#C1C1C1")
    }
    
    class var bgColor: UIColor {
        let color = UIColor.init(red: 233/255, green: 233/255, blue: 233/255, alpha: 1.0)
        return color
    }
    
    class var customAlphaColor: UIColor {
        return UIColor(red: 57/255, green: 57/255, blue: 57/255, alpha: 0.7)
    }
    
    class var lightAlphaColor: UIColor {
        return UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1.0)
    }
    
    class var alphaColor: UIColor {
        return UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.1)
    }
    
    
    class var linkBgColor: UIColor {
        return UIColor.convertRGB(hexString: "#516cb3")
    }

    //App color
    
    class var appBlueColor: UIColor {
        return UIColor.convertRGB(hexString: "#4060ac")
    }
    
    class var appPinkColor: UIColor {
        return  UIColor.convertRGB(hexString: "#ed2490")
    }
    
    class var appGreenColor: UIColor {
        return UIColor.convertRGB(hexString: "#62BD45")
    }
    
    class var appLightGreyColor: UIColor {
        return  UIColor.convertRGB(hexString: "#F3F5F9")
    }
    
    class var appDarkGreyColor: UIColor {
        return UIColor.convertRGB(hexString: "#787D86")
    }
    
    class var appLightGreenColor: UIColor {
        return UIColor.convertRGB(hexString: "#F4FFF1")
    }
    
    class var appLightRedColor: UIColor {
        return UIColor.convertRGB(hexString: "#FFF2F2")
    }
    
    class var appNameTagColor: UIColor {
        return UIColor.convertRGB(hexString: "#e6e8ed")
    }
    
    class var appCommonBorderColor: UIColor {
        return UIColor.convertRGB(hexString: "#D4D6DC")
    }
    
    class var appFormInputBorderColor: UIColor {
        return UIColor.convertRGB(hexString: "#C6C9CF")
    }
    
    class var appBlackColor: UIColor {
        return UIColor.convertRGB(hexString: "#202020")
    }
    
    class var appGreyLightColor: UIColor {
        return UIColor.convertRGB(hexString: "#808080")
    }
    
    class var appGreyDarkColor: UIColor {
        return UIColor.convertRGB(hexString: "#666666")
    }
    
    class var appRedColor: UIColor {
        return UIColor.convertRGB(hexString: "#ec1616")
    }
    
    class var appDashedBorderColor: UIColor {
        return UIColor.convertRGB(hexString: "#ed2290")
    }
    
    class var viewBorderColor: UIColor {
        return UIColor.convertRGB(hexString: "#00000029")
    }
   
    class var appBackgroundColor: UIColor {
        return UIColor.convertRGB(hexString: "#fff4fa")
    }
    
    class var appMaleColor: UIColor {
        return UIColor.convertRGB(hexString: "#69a7e0")
    }
    
    class var appFemaleColor: UIColor {
        return UIColor.convertRGB(hexString: "#f35dae")
    }
    
    class var appDeliveredStatusColor: UIColor {
        return UIColor.convertRGB(hexString: "#62BD45")
    }
    
    class var appOnWayStatusColor: UIColor {
        return UIColor.convertRGB(hexString: "#ed2490")
    }
    
    class var appPreparingStatusColor: UIColor {
        return UIColor.convertRGB(hexString: "#28a7f7")
    }
    
    class var appConfirmedStatusColor: UIColor {
        return UIColor.convertRGB(hexString: "#a63cff")
    }
    
    class var appPendingStatusColor: UIColor {
        return UIColor.convertRGB(hexString: "#eda019")
    }
    
    
    class var appDropShadowColor: UIColor {
        return UIColor.convertRGB(hexString: "#00000029")
    }
    
    class var appDBBorderColor: UIColor {
        return UIColor.convertRGB(hexString: "#DBDBDB")
    }
    
    class var textFieldSelectColor: UIColor {
        return UIColor.convertRGB(hexString: "#EA4E2C")
    }

    class var textFieldUnSelectColor: UIColor {
        return UIColor.convertRGB(hexString: "#E6E6E6")
    }
    
    class var textFieldUnSelectImageviewColor: UIColor {
        return UIColor.convertRGB(hexString: "#555555")
    }
    
    class var videoConsultationbgColor: UIColor {
        return UIColor.convertRGB(hexString: "#F7F7F7")
    }
    
    
    class var inactiveBorderColor: UIColor {
        return UIColor(red: 209/255, green: 211/255, blue: 212/255, alpha: 1)
    }
    
    
    //MARK:-Hexadecimal to RGB
    public class func convertRGB(hexString: String) -> UIColor {
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            let hexColor = String(hexString[start...])
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            if scanner.scanHexInt64(&hexNumber) {
                return UIColor(
                    red: CGFloat((hexNumber & 0xFF0000) >> 16) / 255.0,
                    green: CGFloat((hexNumber & 0x00FF00) >> 8) / 255.0,
                    blue: CGFloat(hexNumber & 0x0000FF) / 255.0,
                    alpha: CGFloat(1.0)
                )
            }
        }
        return UIColor.black
    }
}
