//
//  LoginViewController.swift
//  Healthsprings
//
//  Created by Personal on 16/06/22.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import Alamofire
import AuthenticationServices
import CRNotifications
import CocoaTextField

class LoginViewController: UIViewController  {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailBgView: UIView!
    @IBOutlet weak var emailTextField: CustomTextField!
    @IBOutlet weak var emailImageView: UIImageView!
    @IBOutlet weak var signOrLabel: UILabel!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var emailView: UIView!
    
    @IBOutlet weak var appleView: UIView!
    @IBOutlet weak var facebookView: UIView!
    @IBOutlet weak var signpassView: UIView!
    @IBOutlet weak var nextButtonTopConstraint: NSLayoutConstraint!
    
    
    var userId: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var email: String = ""
    private var authViewModel = AuthViewModel()
    
    var emailRightViewImageView : UIImageView!
    let emailMessage = NSLocalizedString("This filed is required", comment: "")
    let invaildMessage = NSLocalizedString("Enter valid Email", comment: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //emailTextField.text = "priyanka10028@yopmail.com"
        //emailTextField.text = "patient@docis.io"
        //emailTextField.text = "wejig2@probdd.com"
        
        setupEmailSetup()
        //setFrontUpdateProfileView()
    }
        
    private func heightErrorTopAlignment(){
        nextButtonTopConstraint.constant = 30
        nextButtonTopConstraint.isActive = true
    }
    
    private func heightNormalTopAlignment(){
        nextButtonTopConstraint.constant = 13
        nextButtonTopConstraint.isActive = true
    }
    
    private func setupEmailSetup(){
        initializeHideKeyboard()
        nextView.layer.cornerRadius = 5
        appleView.dropShadowWithCornerRaduis()
        facebookView.dropShadowWithCornerRaduis()
        signpassView.dropShadowWithCornerRaduis()
        emailView.dropShadowWithCornerRaduis()
        
        emailTextField.delegate = self
        emailTextField.placeholder = "ENTER EMAIL ID"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        emailRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 17.34, height: 12.63))
        emailRightViewImageView.image = UIImage(named: "email")
        outerView.addSubview(emailRightViewImageView)
        
        emailTextField.rightView = outerView
        emailTextField.rightViewMode = .always
        emailTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        emailTextField.addPadding(.left(18))
        emailTextField.backgroundColor = .white
        emailTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        
//        guard validateData() else {
//            return
//        }
    }
    
    
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: -  Login
    @IBAction func didTapNextButton(_ sender: Any) {
        guard validateData() else {
            return
        }
        callCheckEmailApiCall()
    }
    
    //MARK:- Email Check
    private func isValidEmail(emailID:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailID)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    private func callCheckEmailApiCall(){
        var paramsDictionary = [String:Any]()
        paramsDictionary["email"] = self.emailTextField.text ?? ""
        paramsDictionary["login_type"] = "normal"

        HttpClientApi.instance().makeAPICall(url: URL.checkemail, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                if dataDict.object(forKey:"success") as? Int ?? 0 == 1 {
                    self.setupLogin()
                }else{
                    self.setupRegister(name: "")
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
    
    private func callCheckSocialEmailApiCall(loginDetails:   [SocialLoginDetails]){
        var paramsDictionary = [String:Any]()
        paramsDictionary["email"] = loginDetails.first?.email ?? ""
        var loginStr = ""
        if loginDetails.first?.loginType == 1{
            loginStr = Key.UserDefaults.logintypeapple
        }else if loginDetails.first?.loginType == 2{
            loginStr = Key.UserDefaults.logintypeFB
        }else if loginDetails.first?.loginType == 3{
            loginStr = Key.UserDefaults.logintypeGM
        }
        paramsDictionary["login_type"] = loginStr
        
        HttpClientApi.instance().makeAPICall(url: URL.checkemail, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("dataDict",dataDict)
                print("\(dataDict.value(forKeyPath: "message") ?? "")")
                let response = response
                if  response?.statusCode == 200 || "\(dataDict.value(forKeyPath: "message") ?? "")" == "mail already register" || dataDict.object(forKey:"success") as? Int ?? 0 == 1{
                    doOnMain {
                        LoadingIndicator.shared.hide()
                        self.socialLoginApi(emailStr: "\(loginDetails.first?.email ?? "")",socailTokenStr: "\(loginDetails.first?.token ?? "")",loginType:  loginDetails.first?.loginType ?? 0)
                    }
                }else if  response?.statusCode == 404 || "\(dataDict.value(forKeyPath: "message") ?? "")" == "mail already not register" {
                    self.self.setFrontUpdateProfileView(loginDetails: loginDetails)
                }else if  response?.statusCode == 401 {
                    doOnMain {LoadingIndicator.shared.hide()}
                    let tempArray = NSMutableArray()
                    for  i in 0..<((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.count ?? 0) {
                        let tArray  = ((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.allValues[i]) as? Array<Any>
                        tempArray.add(tArray?.first ?? "")
                    }
                    let errorStr = tempArray.componentsJoined(by: ",")
                    CustomToast.show(message: errorStr,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }else{
                    doOnMain {LoadingIndicator.shared.hide()}
                    CustomToast.show(message: dataDict["error"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
    
    
    // MARK: -  Login
    private func setupLogin(){
        DispatchQueue.main.async {
            let mainview = LoginVC.instantiateFromAppStoryboard(appStoryboard: .Menu)
            mainview.getEmail = self.emailTextField.text ?? ""
            self.navigationController?.pushViewController(mainview, animated: false)
        }
    }
    
    // MARK: -  Login
    private func setupRegister(name: String){
        DispatchQueue.main.async {
//            let mainview = SignupViewController.instantiateFromAppStoryboard(appStoryboard: .Menu)
//            mainview.getEmail = self.emailTextField.text ?? ""
//            mainview.getName = name
//            self.navigationController?.pushViewController(mainview, animated: false)
            let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
            let registerPage = menuStoryboard.instantiateViewController(withIdentifier: "OtpViewController") as! OtpViewController
            registerPage.getMobile = self.emailTextField.text ?? ""
            self.navigationController?.pushViewController(registerPage, animated: false)
        }
    }
    
    

    
    // MARK: - Apple Login
    @IBAction func didTapappleButton(_ sender: Any) {
        self .actionHandleAppleSignin()
    }
    
    @available(iOS 13.0, *)
    func getCredentialState() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        appleIDProvider.getCredentialState(forUserID: "USER_ID") { (credentialState, error) in
            switch credentialState {
            case .authorized:
                // Credential is valid
                // Continiue to show 'User's Profile' Screen
                break
            case .revoked:
                // Credential is revoked.
                // Show 'Sign In' Screen
                break
            case .notFound:
                // Credential not found.
                // Show 'Sign In' Screen
                break
            default:
                break
            }
        }
    }
    
    @objc func actionHandleAppleSignin() {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            request.requestedOperation = .operationLogin
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        }
    }
    
    // MARK: - Facebook Login
    @IBAction func didTapFacebookButton(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile ,.email ], viewController: self) { (loginResult) in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("Logged in! \(grantedPermissions) \(declinedPermissions) \(String(describing: accessToken))")
                self.getFBUserData()
            }
        }
    }
    
    // MARK: - Facebook Details
    private func getFBUserData(){
        print("AccessToken=",AccessToken.current as Any)
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"]).start(completion: { (connection, result, error) -> Void in
                if (error == nil){
                    self.emailTextField.text = ((result! as AnyObject).value(forKey: "email") as? String) ?? ""
                    var socialLoginDetails = [SocialLoginDetails]()
                    let socialLogin = SocialLoginDetails(email: ((result! as AnyObject).value(forKey: "email") as? String ?? ""), name: ((result! as AnyObject).value(forKey: "name") as? String) ?? "", token: ((result! as AnyObject).value(forKey: "id") as? String) ?? "", loginType: 2)
                     socialLoginDetails.append(socialLogin)
//                    self.socialSignUpApi(loginDetails: socialLoginDetails)
                    self.callCheckSocialEmailApiCall(loginDetails:  socialLoginDetails)
                }
            })
        }
    }
    
    // MARK: Socail Register
    private func socialSignUpApi(loginDetails:   [SocialLoginDetails]){
        var loginStr = ""
        if loginDetails.first?.loginType == 1{
            loginStr = Key.UserDefaults.logintypeapple
        }else if loginDetails.first?.loginType == 2{
            loginStr = Key.UserDefaults.logintypeFB
        }else if loginDetails.first?.loginType == 3{
            loginStr = Key.UserDefaults.logintypeGM
        }
        LoadingIndicator.shared.show(forView: self.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["email"] = "\(loginDetails.first?.email ?? "")"
        paramsDictionary["first_name"] = "\(loginDetails.first?.name ?? "")"
        paramsDictionary["social_media_token"] = "\(loginDetails.first?.token ?? "")"
        paramsDictionary["device_token"] = UserDefaults.standard.object(forKey: Key.UserDefaults.deviceToken) ?? "35f86a221bfec1fe61b57c47bbba1e4bcc71ad760905dec6b900e333e52ff856"
        paramsDictionary["device_type"] = Key.UserDefaults.devicetype
        paramsDictionary["login_type"] = loginStr
        print(paramsDictionary)
        
        HttpClientApi.instance().makeAPICall(url: "\(URL.socialRegister)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("dataDict",dataDict)
                let response = response
                if  response?.statusCode == 200 {
                    doOnMain {
                        if "\(dataDict.value(forKeyPath: "message") ?? "")" ==  "user already Registered."{
                            self.socialLoginApi(emailStr: "\(loginDetails.first?.email ?? "")",socailTokenStr: "\(loginDetails.first?.token ?? "")",loginType:  loginDetails.first?.loginType ?? 0)
                        }else{
                            //let jsonDecoder = JSONDecoder()
                            //let responseModel = try? jsonDecoder.decode(AuthModel.self, from: data)
                            //UserDetails.shared.setUserLoginData(data: try! JSONEncoder().encode(responseModel))
                            //UserDetails.shared.setUserApiToken(apiTK: responseModel?.success?.token ?? "")
                            //self.setTabPage()
                            self.self.setFrontUpdateProfileView(loginDetails: loginDetails)
                        }
                        LoadingIndicator.shared.hide()
                    }
                }else{
                    doOnMain {
                        self.showCustomToast(toastTitle: "Info",toastMeassage: "\(dataDict.value(forKeyPath: "message") ?? "")",status: 0)
                        LoadingIndicator.shared.hide()
                    }
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
            doOnMain {
                self.showCustomToast(toastTitle: "Error",toastMeassage: "API call Failure",status: 1)
                LoadingIndicator.shared.hide()
            }
        })
    }
    
    // MARK: Socail Login
    private func socialLoginApi(emailStr: String,socailTokenStr: String,loginType:  Int){
        var loginStr = ""
        if loginType == 1{
            loginStr = Key.UserDefaults.logintypeapple
        }else if loginType == 2{
            loginStr = Key.UserDefaults.logintypeFB
        }else if loginType == 3{
            loginStr = Key.UserDefaults.logintypeGM
        }
        LoadingIndicator.shared.show(forView: self.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["email"] = emailStr
        paramsDictionary["social_media_token"] = socailTokenStr
        paramsDictionary["device_token"] = UserDefaults.standard.object(forKey: Key.UserDefaults.deviceToken) ?? "35f86a221bfec1fe61b57c47bbba1e4bcc71ad760905dec6b900e333e52ff856"
        paramsDictionary["device_type"] = Key.UserDefaults.devicetype
        paramsDictionary["login_type"] = loginStr
        print(paramsDictionary)
        
        HttpClientApi.instance().makeAPICall(url: "\(URL.socialLogin)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("dataDict",dataDict)
                let response = response
                if  response?.statusCode == 200 {
                    doOnMain {
                        if "\(dataDict.value(forKeyPath: "message") ?? "")" ==  "user already Registered."{
                            self.showCRNotificationsToast(toastType: CRNotifications.error, toastTitle: "Error", toastMeassage: "\(dataDict.value(forKeyPath: "message") ?? "user already Registered.")")
                        }else{
                            let jsonDecoder = JSONDecoder()
                            let responseModel = try? jsonDecoder.decode(AuthModel.self, from: data)
                            UserDetails.shared.setUserLoginData(data: try! JSONEncoder().encode(responseModel))
                            UserDetails.shared.setUserApiToken(apiTK: responseModel?.success?.token ?? "")
                            self.setTabPage()
                        }
                        LoadingIndicator.shared.hide()
                    }
                }else{
                    doOnMain {
                        self.showCustomToast(toastTitle: "Info",toastMeassage: "\(dataDict.value(forKeyPath: "message") ?? "")",status: 0)
                        LoadingIndicator.shared.hide()
                    }
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
            doOnMain {
                self.showCustomToast(toastTitle: "Error",toastMeassage: "API call Failure",status: 1)
                LoadingIndicator.shared.hide()
            }
        })
    }
    
    // MARK: - Tabbar Action
    private func setTabPage(){
        doOnMain {
            let tabStoryboard = UIStoryboard(name: "Tabbar", bundle: nil)
            let Permissionvc = tabStoryboard.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
            UIApplication.shared.windows.first?.rootViewController = Permissionvc
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }
    
    // MARK: - Gmail Login
    @IBAction func didTapGmailButton(_ sender: Any) {
        let signInConfig = GIDConfiguration(clientID: "1056163698692-80qn6smih4qet6odog92rd9l76fnv3ar.apps.googleusercontent.com")
        GIDSignIn.sharedInstance.signIn(with: signInConfig, presenting: self) { user, error in
            guard error == nil else { return }
            let userId = user?.userID // For client-side use only!
            let name = user?.profile?.name
            let email = user?.profile!.email
            
            var socialLoginDetails = [SocialLoginDetails]()
            let socialLogin = SocialLoginDetails(email: email ?? "", name: name ?? "", token: userId ?? "", loginType: 3)
             socialLoginDetails.append(socialLogin)
            self.callCheckSocialEmailApiCall(loginDetails:  socialLoginDetails)
//            self.socialSignUpApi(loginDetails: socialLoginDetails)
        }
    }
    
    
    
    // MARK: - Singpass Login
    @IBAction func didTapSingpassButton(_ sender: Any) {
        let mainstoryboard = UIStoryboard(name: "Menu", bundle: nil)
        let mainview = mainstoryboard.instantiateViewController(withIdentifier: "SingpassViewController") as! SingpassViewController
        self.navigationController?.pushViewController(mainview, animated: false)
    }
    
    func setFrontUpdateProfileView(loginDetails:  [SocialLoginDetails]) {
        doOnMain{
            let mainStoryBoard = UIStoryboard(name: "Menu", bundle: nil)
            let loginVC = mainStoryBoard.instantiateViewController(withIdentifier: "CompletedProfileViewController") as! CompletedProfileViewController
            loginVC.getSocialDetails = loginDetails
            UIApplication.shared.windows.first?.rootViewController = loginVC
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }

}

extension LoginViewController {
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard validateData() else {
            return
        }
        if validateData() == true{
            doOnMain {
                self.emailTextField.hideError()
                self.buttonSuccess()
            }
        }
    }
    
    @objc func textFieldDidChange(sender: UITextField) {
        if !emailTextField.text!.isEmptyStr  {
            guard validateData() else {
                return
            }
        }else{
            //            emailTextField.setError(errorString: emailMessage)
        }
        if validateData() == true{
            doOnMain {
                self.emailTextField.hideError()
                self.buttonSuccess()
            }
        }
    }
    
    private func validateData() -> Bool {
        guard !emailTextField.text!.isEmptyStr else {
            emailTextField.showError(errorString: emailMessage)
            emailRightViewImageView.setImageColor(color: UIColor.convertRGB(hexString: "#707070"))
            heightErrorTopAlignment()
            buttonError()
            return false
        }
        guard !isValidEmail(emailID: emailTextField.text ?? "") == false else {
            emailTextField.showError(errorString: invaildMessage)
            emailRightViewImageView.setImageColor(color: .red)
            heightErrorTopAlignment()
            buttonError()
            return false
        }
        emailRightViewImageView.setImageColor(color: .black)
        emailTextField.hideError()
        heightNormalTopAlignment()
        return true
    }
    func buttonError(){
        nextView.backgroundColor = .loginGeryColor
        nextButton.tintColor = UIColor.convertRGB(hexString: "#A0A0A0")
        nextButton.titleLabel?.textColor = UIColor.convertRGB(hexString: "#A0A0A0")
    }
    func buttonSuccess(){
        nextView.backgroundColor = .themeColor
        nextButton.tintColor = .white
        nextButton.titleLabel?.textColor = .white
        nextButton.setTitleColor(.white, for: .normal)
    }
    func clearForm() {
        self.emailTextField.text = ""
    }
}

extension LoginViewController: ASAuthorizationControllerPresentationContextProviding {
    // For present window
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
extension LoginViewController: ASAuthorizationControllerDelegate {
    // Authorization Failed
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    // Authorization Succeeded
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            // Get user data with Apple ID credentitial
            let appleId = appleIDCredential.user
            userId = appleId
            let appleUserFirstName = appleIDCredential.fullName?.givenName
            firstName = appleUserFirstName ?? ""
            let appleUserLastName = appleIDCredential.fullName?.familyName
            lastName = appleUserLastName ?? ""
            let appleUserEmail = appleIDCredential.email
            email = appleUserEmail ?? ""
//            self.socialSignUpApi(emailStr: email,nameStr: "\(firstName)\(lastName)",socailTokenStr: userId,loginType:  1)
            
            var socialLoginDetails = [SocialLoginDetails]()
            let socialLogin = SocialLoginDetails(email: email, name: "\(firstName)\(lastName)", token: userId, loginType: 1)
             socialLoginDetails.append(socialLogin)
            self.socialSignUpApi(loginDetails: socialLoginDetails)

        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Get user data using an existing iCloud Keychain credential
            let appleUsername = passwordCredential.user
            let applePassword = passwordCredential.password
            // Write your code
        }
    }
}
