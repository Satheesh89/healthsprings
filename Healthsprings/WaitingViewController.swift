//
//  WaitingViewController.swift
//  Healthsprings
//
//  Created by Personal on 18/09/22.
//

import UIKit

class WaitingViewController: BaseViewController {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var doctorDetailLabel: UILabel!
    @IBOutlet weak var subDetailsLabel: UILabel!
    @IBOutlet weak var verifyLabel: UILabel!
    @IBOutlet weak var cancelBookingButton: UIButton!
    @IBOutlet weak var roomBgView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var verifyBgView: UIView!
    @IBOutlet weak var doctorView: UIView!
    var getConfirmDetails =  [confirmDetails]()
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var detailLabel: UILabel!
    
    @IBOutlet weak var roomTopConstraint: NSLayoutConstraint!
    
    
    var timer: Timer?
    var totalTime = 60
    var timerBool : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        roomBgView.backgroundColor = .white
        roomBgView.viewBorderShadow()
        verifyBgView.layer.cornerRadius = 5
        verifyBgView.layer.borderWidth = 1
        verifyBgView.layer.borderColor = UIColor.themeColor.cgColor
        doctorView.layer.cornerRadius = 5
        doctorView.layer.borderWidth = 1
        doctorView.layer.borderColor = UIColor.themeColor.cgColor
        startOtpTimer()
        
        if timerBool == true{
            timeView.isHidden = true
            detailLabel.isHidden = true
//            roomTopConstraint.constant = 66
        }else{
            timeView.isHidden = false
            detailLabel.isHidden = false
//            roomTopConstraint.constant = 66
        }
//        roomTopConstraint.isActive = true
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: - Back
    @IBAction func didTapLeftAction(_ sender: Any) {
        self.navigationController?.popToViewController(ofClass: RequestSummaryViewController.self)
    }
    
    private func startOtpTimer() {
        self.totalTime = 10 * 60
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        print(self.totalTime)
        self.doctorDetailLabel.text =  "Doctor will see you shortly with in \(self.timeFormatted(self.totalTime)) mins" // will show timer
        self.timeLabel.text = self.timeFormatted(self.totalTime) // will show timer
        if totalTime != 0 {
            totalTime -= 1  // decrease counter timer
        } else {
            if let timer = self.timer {
                timer.invalidate()
                self.timer = nil
            }
        }
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    @IBAction func didTapCancelBookAction(_ sender: Any) {
    }
    
    @IBAction func didTapVerifyAction(_ sender: Any) {
        DispatchQueue.main.async {
            let mainstoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let mainview = mainstoryboard.instantiateViewController(withIdentifier: "IDVerificationViewController") as! IDVerificationViewController
            mainview.showMyIDDoctor = false
            mainview.getconfirmdetails = self.getConfirmDetails
            self.navigationController?.pushViewController(mainview, animated: false)
        }
    }
    
    
    @IBAction func didTapDoctorShowIDAction(_ sender: Any) {
        DispatchQueue.main.async {
            let mainstoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let mainview = mainstoryboard.instantiateViewController(withIdentifier: "IDVerificationViewController") as! IDVerificationViewController
            mainview.showMyIDDoctor = true
            mainview.getconfirmdetails = self.getConfirmDetails
            self.navigationController?.pushViewController(mainview, animated: false)
        }
    }
}
