//
//  PinSettingViewController.swift
//  Healthsprings
//
//  Created by Personal on 19/06/22.
//

import UIKit

class PinSettingViewController: UIViewController {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var currentPasswordView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var rePasswordView: UIView!
    @IBOutlet weak var currentPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var reEnterPasswordTextField: UITextField!
    @IBOutlet weak var currentPasswordButton: UIButton!
    @IBOutlet weak var newPasswordButton: UIButton!
    @IBOutlet weak var reEnterPasswordButton: UIButton!
    
    @IBOutlet weak var currentImgView: UIImageView!
    @IBOutlet weak var newPasswordImgView: UIImageView!
    @IBOutlet weak var reEnterPasswordImgView: UIImageView!
    
    var passwordiconClick = true
    var confirmPasswordiconClick = true
    var reEnterPasswordiconClick = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setStatusBar(color: UIColor.themeColor)
        initializeHideKeyboard()
        currentPasswordTextField.delegate = self
        newPasswordTextField.delegate = self
        reEnterPasswordTextField.delegate = self
        currentPasswordView.dropShadowWithCornerRaduis()
        passwordView.dropShadowWithCornerRaduis()
        rePasswordView.dropShadowWithCornerRaduis()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - Left Action
    @IBAction func didTapleftAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    // MARK: - Update Action
    @IBAction func didTapUpdateAction(_ sender: Any) {
        if "\(currentPasswordTextField.text ?? "")" == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter your current password",status: 0)
        }else if "\(newPasswordTextField.text ?? "")" == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter your new password",status: 0)
        }else if "\(reEnterPasswordTextField.text ?? "")" == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter your re enter password",status: 0)
        } else{
            self.updatePasswordApi()
            //self.showCustomToast(toastTitle: "Info",toastMeassage: "Successfully save the new password",status: 0)
        }
    }
    
    func updatePasswordApi()  {
        var paramsDictionary = [String:Any]()
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()
        paramsDictionary["current_password"] = currentPasswordTextField.text ?? ""
        paramsDictionary["new_password"] = newPasswordTextField.text ?? ""
        paramsDictionary["confirm_new_password"] = reEnterPasswordTextField.text ?? ""
        
        HttpClientApi.instance().makeAPICall(url: URL.getUpdatePassword, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                let response = response
                if  response?.statusCode == 200 {
                    doOnMain{
                        self.reDirectToAppointmentView()
                    }
                }else{
                    self.showCustomToast(toastTitle: "Error",toastMeassage: dataDict["error"] as! String,status: 1)
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
            self.showCustomToast(toastTitle: "Error",toastMeassage: "API call Failure",status: 1)
        })
    }
    
    // MARK: - re DirectTo Setting
    func reDirectToAppointmentView() {
        self.navigationController?.popViewController(animated: false)
    }
    
    // MARK: - Forgot Password Action
    @IBAction func didTapforgotPasswordAction(_ sender: Any) {
    }
    
    // MARK: - Password Hide and Show Action
    @IBAction func didTapPasswordShowAction(_ sender: UIButton) {
        if sender.tag == 1000{
            if(passwordiconClick == true) {
                currentPasswordTextField.isSecureTextEntry = false
                currentImgView.image = UIImage(named: "show")
            } else {
                currentPasswordTextField.isSecureTextEntry = true
                currentImgView.image = UIImage(named: "hideshow")
            }
            passwordiconClick = !passwordiconClick
        }else if sender.tag == 1001{
            if(confirmPasswordiconClick == true) {
                newPasswordTextField.isSecureTextEntry = false
                newPasswordImgView.image = UIImage(named: "show")
            } else {
                newPasswordTextField.isSecureTextEntry = true
                newPasswordImgView.image = UIImage(named: "hideshow")
            }
            confirmPasswordiconClick = !confirmPasswordiconClick
        }else if sender.tag == 1002{
            if(reEnterPasswordiconClick == true) {
                reEnterPasswordTextField.isSecureTextEntry = false
                reEnterPasswordImgView.image = UIImage(named: "show")
            } else {
                reEnterPasswordTextField.isSecureTextEntry = true
                reEnterPasswordImgView.image = UIImage(named: "hideshow")
            }
            reEnterPasswordiconClick = !confirmPasswordiconClick
        }
    }
}
