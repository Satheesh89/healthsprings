//
//  AddCardViewController.swift
//  Healthsprings
//
//  Created by Personal on 30/06/22.
//

import UIKit

class AddCardViewController: UIViewController {

    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardTypeImageView: UIImageView!
    @IBOutlet weak var cardNameLabel: UILabel!
    @IBOutlet weak var cardNoLabel: UILabel!
    @IBOutlet weak var vaildLabel: UILabel!
    @IBOutlet weak var saveLabel: UILabel!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var accountNameLabel: UITextField!
    @IBOutlet weak var noView: UIView!
    @IBOutlet weak var accountNoLabel: UITextField!
    @IBOutlet weak var exprieLabel: UILabel!
    @IBOutlet weak var cvvLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var expView: UIView!
    @IBOutlet weak var cvvView: UIView!
    @IBOutlet weak var expTextField: UITextField!
    @IBOutlet weak var cvvTextField: UITextField!


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setStatusBar(color: UIColor.themeColor)
        expTextField.delegate = self
        cvvTextField.delegate = self
        accountNameLabel.delegate = self
        accountNoLabel.delegate = self
        expTextField.tag = 222
        initializeHideKeyboard()
        cardNameLabel.text = UserDetails.shared.userName ?? ""
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        cardView.dropShadowWithCornerRaduis()
        noView.dropShadowWithCornerRaduis()
        expView.dropShadowWithCornerRaduis()
        cvvView.dropShadowWithCornerRaduis()
        nameView.dropShadowWithCornerRaduis()
        vaildLabel.textColor = UIColor.convertRGB(hexString: "#8E8E8E")
        let attributedWithTextColor: NSAttributedString = "Valid  08/26".attributedStringWithColor([" 08/26"], color: UIColor.black)
        vaildLabel.attributedText = attributedWithTextColor
    }
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func didTapLeftAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func didTapSaveAction(_ sender: Any) {
        if "\(accountNameLabel.text ?? "")" == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter your name",status: 0)
        }else if "\(accountNoLabel.text ?? "")" == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter your card number",status: 0)
        }else if "\(expTextField.text ?? "")" == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter your expiry date and month",status: 0)
        }else if "\(cvvTextField.text ?? "")" == ""{
            self.showCustomToast(toastTitle: "Info",toastMeassage: "Please enter your CVV",status: 0)
        }else{
            self.addCardApi()
        }
    }
    
    func sliceString(str: String, start: Int, end: Int) -> String {
        let data = Array(str)
        return String(data[start..<end])
    }
    
    
    func addCardApi()  {
        let monthStr = sliceString(str: expTextField.text ?? "", start: 0, end: 2)
        let yearStr = sliceString(str: expTextField.text ?? "", start: 3, end: 7)
        
        var paramsDictionary = [String:Any]()
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()
        paramsDictionary["cardholder_name"] = accountNameLabel.text ?? ""
        paramsDictionary["card_number"] = accountNoLabel.text ?? ""
        paramsDictionary["card_id"] = cvvTextField.text ?? ""
        paramsDictionary["expiration_year"] = yearStr
        paramsDictionary["expiration_month"] = monthStr

        HttpClientApi.instance().makeAPICall(url: URL.addCard, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                let response = response
                if  response?.statusCode == 200 {
                    doOnMain{
                        self.navigationController?.popViewController(animated: false)
                    }
                }else{
                    //self.showAlertMessage(title: "Alert", message: dataDict["error"] as! String ) {
                    //self.hideActivityIndicator()
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
        
        
//        let json :[String: Any] = ["email": email]
//        let jsonData = try? JSONSerialization.data(withJSONObject: json)
////        self.showActivityIndicator()
//        let url = URL(string: URL.sendotptopinuser)!
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//        request.setValue("\(String(describing: jsonData?.count))", forHTTPHeaderField: "Content-Length")
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        // insert json data to the request
//        request.httpBody = jsonData
//        let task = URLSession.shared.dataTask(with: request) { data, response, error in
//            if let data = data {
//                let json = try? JSONSerialization.jsonObject(with: data, options: [])
//                let dataDict = json as! NSDictionary
//                let response = response
//                if  response?.statusCode == 200 {
//                }else{
//                    //self.showAlertMessage(title: "Alert", message: dataDict["error"] as! String ) {
//                    //self.hideActivityIndicator()
//                }
//            }
//        }
//        task.resume()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 222{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            let characterSet = CharacterSet(charactersIn: string)

            if text.count == 2, !string.isEmpty {
                textField.text = text + "/"
            }
            return CharacterSet.decimalDigits.isSuperset(of: characterSet) && newLength <= 7
        }
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 222{
            guard let text = textField.text else { return }
            let isUserInputValid = validate(string: text)
            if !isUserInputValid {
                //TODO: Notify user that expiration date is invalid
            }
        }
    }
    
    func validate(string: String) -> Bool {
        let regex = try! NSRegularExpression(pattern: "^(20)\\d\\d[/](0[1-9]|1[012])$")
        return regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil
    }

}
