//
//  SearchAPIManger.swift
//  Healthsprings
//

import Foundation
import Alamofire
import SwiftyJSON

enum SearchAPI{
    case searchAPI
}
extension SearchAPI: EndPointType{
    var path: String {
        switch self {
        case .searchAPI:
            return "search-doctor?"
        }
    }
    var httpMethod: HTTPMethod {
        return .post
    }
}
class SearchAPIManger{
    func getSearchDoctorList(input: Parameters, handler: @escaping (_ result: FindDoctorModel?, _ error: AlertMessage?)->()) {
        APIManager.shared().call(type: SearchAPI.searchAPI, params: input) { (result: FindDoctorModel?, message: AlertMessage?) in
            if let result = result {
                handler(result, nil)
            } else {
                handler(nil, message!)
            }
        }
    }
}
