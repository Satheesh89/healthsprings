//
//  HistoryAPIManager.swift
//  Healthsprings
//

import Foundation
import Alamofire

enum HistoryApi{
    case history
}

extension HistoryApi: EndPointType{
    var path: String {
        switch self {
        case .history:
            return  "consultation-history?"
        }
    }
    var httpMethod: HTTPMethod {
        switch self {
        case .history:
            return .post
        }
    }
}


class HistoryAPIManager{
    func getHistoryDetails(input: Parameters, handler: @escaping (_ result: HistoryModel?, _ error: AlertMessage?)->()) {
        APIManager.shared().call(type: HistoryApi.history, params: input) { (result: HistoryModel?, message: AlertMessage?) in
            if let result = result {
                handler(result, nil)
            } else {
                handler(nil, message!)
            }
        }
    }
}
