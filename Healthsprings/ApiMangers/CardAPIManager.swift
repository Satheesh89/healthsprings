//
//  CardAPIManager.swift
//  Healthsprings
//
//  Created by Personal on 30/06/22.
//

import Foundation
import Alamofire

enum CardAPI{
    case cardAPI
}

extension CardAPI: EndPointType{
    var path: String {
        switch self {
        case .cardAPI:
            return "get-card?"
        }
    }
    var httpMethod: HTTPMethod {
        return .get
    }
}


class CardAPIManager{
    func getCardListDetails(url: String, completionHandler : @escaping (_ serviceClass: GetCardModel?, _ error: AlertMessage?) -> ()) {
        APIManager.shared().call(type: CardAPI.cardAPI, urlParams: url) { (result: GetCardModel?,message: AlertMessage?) in
            if let result = result {
                completionHandler(result, nil)
            } else {
                completionHandler(nil, message!)
            }
        }
    }
}



