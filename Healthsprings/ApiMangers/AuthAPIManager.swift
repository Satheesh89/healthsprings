//
//  AuthAPIManager.swift
//  Healthsprings
//

import Foundation
import Alamofire

enum AuthApi {
    case login
    case socialsignin
    case socialsignup
    
}

// MARK: - EndPointType
extension AuthApi: EndPointType {
    var httpMethod: HTTPMethod {
        switch self {
            //        case .getAccount, .GetInfoByItemType, .getProfile: return .get
            //        case .updateUserProfile: return .put
        default:
            return .post
        }
    }
    
    // MARK: - Vars & Lets
    var path: String {
        switch self {
        case .login:
            return "booking-login?"
        case .socialsignin:
            return "social-signin"
        case .socialsignup:
            return "social-signup"
        }
    
    var encoding: ParameterEncoding {
        switch self {
        default:
            return JSONEncoding.default
        }
    }
}
}

class AuthApiManager {
    func requestLogIn(input: Parameters, handler: @escaping (_ result: AuthModel?, _ error: AlertMessage?)->()) {
        APIManager.shared().call(type: AuthApi.socialsignin, params: input) { (result: AuthModel?, message: AlertMessage?) in
            if let result = result {
                handler(result, nil)
                UserDetails.shared.setUserLoginData(data: try! JSONEncoder().encode(result))
                UserDetails.shared.setUserApiToken(apiTK: result.success?.token ?? "")
            } else {
                handler(nil, message!)
            }
        }
    }
    
    
    func requestSignUp(input: Parameters, handler: @escaping (_ result: AuthModel?, _ error: AlertMessage?)->()) {
        APIManager.shared().call(type: AuthApi.socialsignup, params: input) { (result: AuthModel?, message: AlertMessage?) in
            if let result = result {
                handler(result, nil)
                UserDetails.shared.setUserLoginData(data: try! JSONEncoder().encode(result))
            } else {
                handler(nil, message!)
            }
        }
    }
    
    func requestNormalLogin(input: Parameters, handler: @escaping (_ result: AuthModel?, _ error: AlertMessage?)->()) {
        APIManager.shared().call(type: AuthApi.login, params: input) { (result: AuthModel?, message: AlertMessage?) in
            if let result = result {
                handler(result, nil)
                UserDetails.shared.setUserLoginData(data: try! JSONEncoder().encode(result))
                UserDetails.shared.setUserApiToken(apiTK: result.success?.token ?? "")
            } else {
                handler(nil, message!)
            }
        }
    }
}

