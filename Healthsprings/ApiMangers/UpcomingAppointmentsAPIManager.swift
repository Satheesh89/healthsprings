//
//  UpcomingAppointmentsAPIManager.swift
//  Healthsprings
//
//  Created by Personal on 29/06/22.
//

import Foundation
import Alamofire

enum UpcomingAppointmentsApi {
    case upcomingappointments
}

extension UpcomingAppointmentsApi: EndPointType {
    var path: String {
        switch self {
        case .upcomingappointments:
            return "appointments?"
        }
    }
    var httpMethod: HTTPMethod {
        return .post
    }
}

//class UpcomingAppointmentsAPIManager{
//    func getUpcomingList(input: Parameters, handler: @escaping (_ result: UpcomingModel?, _ error: AlertMessage?)->()) {
//        APIManager.shared().call(type: UpcomingAppointmentsApi.upcomingappointments, params: input) { (result: UpcomingModel?, message: AlertMessage?) in
//            if let result = result {
//                handler(result, nil)
//            } else {
//                handler(nil, message!)
//            }
//        }
//    }
//}
