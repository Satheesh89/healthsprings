//
//  FindDoctorViewController.swift
//  Healthsprings
//
//  Created by Personal on 01/07/22.
//
import UIKit
class FindDoctorViewController: UIViewController {
    
    @IBOutlet weak var searchBgView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var leftButton: UIButton!

    var searchBarButtonItem: UIBarButtonItem?
    var searchDoctorViewModel = SearchDoctorViewModel()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        searchBgView.isHidden = false
        searchTextField.delegate = self
        searchTextField.addTarget(self, action: #selector(FindDoctorViewController.textFieldDidChange(_:)),
                                  for: .editingChanged)
        initializeHideKeyboard()
        setGradient()
        showSearchBar()
        loadSetup()
        tableViewSetup()
        
//        topViewConstraint.constant = 44
//        topViewConstraint.isActive = true

        
//        searchBgView.translatesAutoresizingMaskIntoConstraints = false
//        searchBgView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
//        searchBgView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
//        searchBgView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
//        searchBgView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
//        searchBgView.backgroundColor = .red
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        searchBgView.layer.cornerRadius = 5
        searchBgView.clipsToBounds = false
        searchBgView.dropShadowWithCornerRaduis()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc func textFieldDidChange(_ textField: UITextField) {
//        facilityDict.filter{$0.value.contains{$0.value.contains("AR", options: .caseInsensitive)}}.forEach{filtered[$0.key] = $0.value}

        
//        let filterArray = searchDoctorViewModel.searchDetailsCopy?.filter({($0.doctorName!.contains(textField.text ?? ""))})
        let filterArray = searchDoctorViewModel.searchDetailsCopy?.filter({($0.doctorName!.localizedStandardContains((textField.text ?? "")))})
        if textField.text == "" {
            searchDoctorViewModel.searchDetails = searchDoctorViewModel.searchDetailsCopy
        }else{
            searchDoctorViewModel.searchDetails?.removeAll()
            searchDoctorViewModel.searchDetails = filterArray
        }
        doOnMain {
            self.searchTableView.reloadData()
        }
    }
    
    @IBAction func didTapLeftButtonAction(_ sender: Any) {
        self.navigationController?.popToViewController(ofClass: DashBoard.self)
    }
    
}

extension FindDoctorViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchDoctorViewModel.searchDetails?.count == 0 {
            tableView.setEmptyMessage("no data found!")
        } else {
            tableView.restore()
        }
        return self.searchDoctorViewModel.searchDetails?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getDoctorTableViewCell(tableView: tableView, indexPath: indexPath)
    }
    func getDoctorTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FindDoctorCell.identifier) as? FindDoctorCell else {
            return UITableViewCell()
        }
        cell.getSearchData = searchDoctorViewModel.searchDetails?[indexPath.row]
        cell.bookAppointmentButton.addTarget(self, action: #selector(didTaptoBookAppointmentAction), for: .touchUpInside)
        cell.bookAppointmentButton.tag = indexPath.row
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    @objc func didTaptoBookAppointmentAction(sender: UIButton) {
        let timeSlotVC = TimeSlotViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        timeSlotVC.doctorName = "Dr \(searchDoctorViewModel.searchDetails?[sender.tag].doctorName ?? "")"
        timeSlotVC.doctorDetail = searchDoctorViewModel.searchDetails?[sender.tag].specialty ?? ""
        timeSlotVC.doctorExprDetail = searchDoctorViewModel.searchDetails?[sender.tag].description ?? ""
        timeSlotVC.doctorId = searchDoctorViewModel.searchDetails?[sender.tag].id ?? 0
        timeSlotVC.uuidId = searchDoctorViewModel.searchDetails?[sender.tag].uuid ?? 0
        self.navigationController?.pushViewController(timeSlotVC, animated: false)
    }
}

extension FindDoctorViewController{
    func showSearchBar() {
        searchBgView.isHidden = false
        searchBgView.alpha = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.searchBgView.alpha = 1
        }, completion: { finished in
            self.searchTextField.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        self.searchTextField.resignFirstResponder()
        searchBgView.isHidden =  true
        navigationItem.setLeftBarButton(searchBarButtonItem, animated: true)
        searchBgView.alpha = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.searchBgView.alpha = 1
        }, completion: { finished in
        })
    }
    
    func loadSetup(){
        searchDoctorViewModel.vc = self
        searchDoctorViewModel.postRequest(getviewController: self)
    }
    
    //MARK: TableView Setup
    func tableViewSetup()  {
        searchTableView.register(FindDoctorCell.nib, forCellReuseIdentifier: FindDoctorCell.identifier)
        searchTableView.delegate = self
        searchTableView.dataSource = self
        searchTableView.tableFooterView = UIView()
        searchTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        searchTableView.showsHorizontalScrollIndicator = false
        searchTableView.showsVerticalScrollIndicator = false
        searchTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        searchTableView.backgroundColor = .white
        searchTableView.contentInsetAdjustmentBehavior = .never
    }
    
    func setGradient(){
        doOnMain {
            let colorTop =  UIColor.convertRGB(hexString: "#1286A0")
            let colorBottom = UIColor.convertRGB(hexString: "#1286A0")
            self.setStatusBar(color: colorTop)
            let gradientLayer:CAGradientLayer = CAGradientLayer()
//            gradientLayer.frame.size = self.bgView.frame.size
            gradientLayer.colors = [colorTop.cgColor,colorBottom.cgColor] //Use diffrent colors
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
//            self.bgView.layer.addSublayer(gradientLayer)
        }
    }
}
