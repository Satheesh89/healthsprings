//
//  FAQViewController.swift
//  Healthsprings
//
//  Created by Personal on 19/06/22.
//
import UIKit
import Alamofire

class FAQViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var leftButton: UIButton!
    var datasource = [ExpandingTableViewCellContent]()
    var datasource1 = [ExpandingTableViewCellContent]()
    
    var faqmodel = [Faqmodel]()
    var category: [Category]?
    
    var finalDict : NSMutableDictionary = NSMutableDictionary()

    
    override func viewDidLoad() {
        super.viewDidLoad()
//        tableViewSetup()
//        datasource = [ExpandingTableViewCellContent(title: "Payment Settings",
//                                                    subtitle: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ‘Content here, content here’, making it look like readable English."),
//                      ExpandingTableViewCellContent(title: "Why do we use it?",
//                                                    subtitle: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ‘Content here, content here’, making it look like readable English."),
//                      ExpandingTableViewCellContent(title: "Where can I get ?",
//                                                    subtitle: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ‘Content here, content here’, making it look like readable English."),
//                      ExpandingTableViewCellContent(title: "Why do we use it?",
//                                                    subtitle: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ‘Content here, content here’, making it look like readable English."),
//                      ExpandingTableViewCellContent(title: "Where can I get ?",
//                                                    subtitle: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ‘Content here, content here’, making it look like readable English.")
//        ]
//        datasource1 = [ExpandingTableViewCellContent(title: "Payment Settings",
//                                                     subtitle: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ‘Content here, content here’, making it look like readable English."),
//                       ExpandingTableViewCellContent(title: "Why do we use it?",
//                                                     subtitle: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ‘Content here, content here’, making it look like readable English."),
//                       ExpandingTableViewCellContent(title: "Where can I get ?",
//                                                     subtitle: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ‘Content here, content here’, making it look like readable English."),
//                       ExpandingTableViewCellContent(title: "Why do we use it?",
//                                                     subtitle: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ‘Content here, content here’, making it look like readable English."),
//                       ExpandingTableViewCellContent(title: "Where can I get ?",
//                                                     subtitle: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ‘Content here, content here’, making it look like readable English.")
//        ]
        loadViewModel()
    }
    
    
    func  loadViewModel()  {
        let historyDetailViewModel = FAQViewModel()
        historyDetailViewModel.getFAQDetails()
        historyDetailViewModel.completionHandler{[weak self] (details, status, message)in
            if status {
                guard let self = self else {return}
                guard let itemList = details else {return}
                self.category = itemList.category
                self.tableViewSetup()
            }
        }
    }
    
//    func getFAQApi( apitokenStr: String,loadview: UIViewController) {
//        LoadingIndicator.shared.show(forView: loadview.view)
//        AF.request( "\(URL.getFaqs)").response { response in
//            if let data = response.data {
//                do{
//                    let userResponse = try JSONDecoder().decode([Faqmodel].self, from: data)
//                    self.faqmodel.append(contentsOf: userResponse)
//                    print( self.faqmodel.count)
//                    doOnMain {
//                        self.tableView.reloadData()
//                        LoadingIndicator.shared.hide()
//                    }
//                }catch let err{
//                    print(err.localizedDescription)
//                }
//            }
//        }
//    }
    
    //MARK: TableView Setup
    func tableViewSetup()  {
        setStatusBar(color: UIColor.themeColor)
        tableView.register(SettingHeaderTableViewCell.nib, forCellReuseIdentifier: SettingHeaderTableViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = true
        //serviceTableView.setContentOffset(.zero, animated: true)
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.backgroundColor = .clear
        tableView.contentInset.top = 5
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableView.automaticDimension
        tableView.contentInsetAdjustmentBehavior = .never
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    // MARK: - Left Action
    @IBAction func didTapleftAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}
extension FAQViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return category?.count ?? 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        let label = UILabel()
        if ( UIDevice.current.model.range(of: "iPad") != nil){
            label.frame = CGRect.init(x: 70, y: 5, width: headerView.frame.width-50, height: headerView.frame.height-10)
            label.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(10.0))
        } else {
            label.frame = CGRect.init(x: 25, y: 5, width: headerView.frame.width-50, height: headerView.frame.height-10)
            label.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(16.0))
        }
        label.text = "\(category?[section].categoryName ?? "")"
        label.textColor = .black
        headerView.addSubview(label)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return category?[section].faq?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: String(describing: ExpandingTableViewCell.self), for: indexPath) as! ExpandingTableViewCell
        
        cell.titleLabel.text = category?[indexPath.section].faq?[indexPath.row].question
        
        if let array : NSMutableArray = finalDict["\(indexPath.section)"] as? NSMutableArray{
          if array.contains(indexPath.row) {
              cell.checkImageView.image = UIImage.init(named: "dropdown")
              cell.subtitleLabel.text = category?[indexPath.section].faq?[indexPath.row].answer
          }else{
              cell.checkImageView.image = UIImage.init(named: "rightarrowgrey")
              cell.subtitleLabel.text  = ""
          }
        }else{
            cell.subtitleLabel.text  = ""
            cell.checkImageView.image = UIImage.init(named: "rightarrowgrey")
        }
        
//        if indexPath.section == 0{
//            let content = datasource[indexPath.row]
//            cell.set(content: datasource[indexPath.row])
//            if content.expanded {
//                cell.checkImageView.image = UIImage.init(named: "dropdown")
//            }else{
//                cell.checkImageView.image = UIImage.init(named: "rightarrowgrey")
//            }
//        }else{
//            cell.set(content: datasource1[indexPath.row])
//            let content = datasource1[indexPath.row]
//            if content.expanded {
//                cell.checkImageView.image = UIImage.init(named: "dropdown")
//            }else{
//                cell.checkImageView.image = UIImage.init(named: "rightarrowgrey")
//            }
//        }
//        cell.set(content: faqmodel[indexPath.row])
//        let content = faqmodel[indexPath.row]
//        if content.expanded {
//            cell.checkImageView.image = UIImage.init(named: "dropdown")
//        }else{
//            cell.checkImageView.image = UIImage.init(named: "rightarrowgrey")
//        }

//            let content = datasource[indexPath.row]
//            cell.set(content: datasource[indexPath.row])
//            if content.expanded {
//                cell.checkImageView.image = UIImage.init(named: "dropdown")
//            }else{
//                cell.checkImageView.image = UIImage.init(named: "rightarrowgrey")
//            }

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if var array : NSMutableArray = finalDict["\((indexPath.section))"] as? NSMutableArray {
            if array.contains((indexPath.row)){
                array.remove((indexPath.row))
            }else{
                array = NSMutableArray.init()
                array.add((indexPath.row))
            }
            finalDict = NSMutableDictionary.init()
            finalDict.setValue(array, forKey: "\((indexPath.section))")
        }else{
            let array = NSMutableArray.init()
            array.add((indexPath.row))
            finalDict = NSMutableDictionary.init()
            finalDict.setValue(array, forKey: "\((indexPath.section))")
        }
        print("select Final Dict = ",finalDict)
        tableView.reloadData()
        
//        if indexPath.section == 0{
//            var content = faqmodel[indexPath.row]
//            content.expanded = !content.expanded
//            print(content.expanded)
//            tableView.reloadRows(at: [indexPath], with: .none)
//        }else{
//            let content = datasource1[indexPath.row]
//            content.expanded = !content.expanded
//            tableView.reloadRows(at: [indexPath], with: .none)
//        }
        
//        faqmodel = faqmodel.map{
//            var mutableBook = $0
//            if indexPath.row =
            
//            if $0.category_name == faqmodel[indexPath.section].category_name {
//                 if  faqmodel[indexPath.row].expanded == true{
//                    mutableBook.expanded = false
//                }else{
//                    mutableBook.expanded = true
//                }
//            }
//            return mutableBook
//        }
//        tableView.reloadRows(at: [indexPath], with: .none)
    }
}
