//
//  AccountSuccessfullyViewController.swift
//  Healthsprings
//
//  Created by Personal on 17/06/22.
//

import UIKit

class AccountSuccessfullyViewController: UIViewController {
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    var getName: String = ""
    var getMail: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        usernameLabel.text = getName
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func didTapNextAction(_ sender: Any) {
        //        setFrontLoginView()
        //        self.setFrontLoginVC()
        DispatchQueue.main.async {
            let mainStoryBoard = UIStoryboard(name: "Menu", bundle: nil)
            let loginVC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            loginVC.getEmail = self.getMail
            loginVC.cursorBool = true
            UIApplication.shared.windows.first?.rootViewController = loginVC
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }
    
    // MARK: - Tabbar Action
    func setTabPage(){
        let tabStoryboard = UIStoryboard(name: "Tabbar", bundle: nil)
        let Permissionvc = tabStoryboard.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
        UIApplication.shared.windows.first?.rootViewController = Permissionvc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
