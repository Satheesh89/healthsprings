//
//  DashBoard.swift
//

import UIKit

class DashBoard: BaseViewController {
    
    @IBOutlet weak var backScrollView: UIScrollView!
    @IBOutlet weak var popularCollectionView: UICollectionView!
    @IBOutlet weak var specialsCollectionView: UICollectionView!
    @IBOutlet weak var healthExpertsCollectionView: UICollectionView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var profileLabel: UILabel!
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var upcomingView: UIView!
    @IBOutlet weak var upComingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var upComingTop: NSLayoutConstraint!
    @IBOutlet weak var upcomingBottom: NSLayoutConstraint!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var readMoreView: UIView!
    @IBOutlet weak var notificationButton: UIButton!
    
    var getCardModel = GetCardViewModel()
    var upcomingViewModel = UpcomingViewModel()
    var healthBlog: [HealthBlog]?
    var upcompingCount : Int = 0
    var covid19UlrCount : String = ""
    var readMoreUrl : String = ""

    private var categoryDict: [NSDictionary] = [
        ["text": "Instant Video Consultation","details": "Consult your doctor anywhere, anytime","image": "pexelskarolinagrabowska"],["text": "Find Doctors Near You","details": "Instant appointment with doctors.","image": "pexelsthirdman"]
    ]
    
    private var healthExpertsDict: [NSDictionary] = [
        ["title": "General Health","details": "Health is a state of complete physical, mental, and social well-being and not merely the absence of disease or infirmity.","image": "why-healthcheck"],
        ["title": "Yoga & meditation","details": "Meditation is a part of yoga, which deals with mental relaxation and concentration","image": "yoga"]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewSetup()
        registerCells()
        layoutDesign()
        doOnMain {
            self.searchView.viewBorderShadow()
            self.filterView.viewBorderShadow()
            self.bannerImage.layer.cornerRadius = 5
            self.bannerImage.layer.borderWidth = 0.5
            self.bannerImage.layer.borderColor = UIColor.themeColor.cgColor
            self.bannerImage.backgroundColor = .white
            self.upcomingView.layer.borderColor = UIColor.themeColor.cgColor
            self.upcomingView.layer.borderWidth = 0.5
            self.upcomingView.layer.cornerRadius = 5
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(DashBoard.bannerToggleAction))
        bannerImage.addGestureRecognizer(tap)
        bannerImage.isUserInteractionEnabled = true
        self.backScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.readMoreView.frame.size.height + 1000)
        
        let badgeSize: CGFloat = 10
        let badgeView = UIView()
        badgeView.backgroundColor = UIColor.convertRGB(hexString: "#FF0000")
        badgeView.layer.cornerRadius = badgeSize / 2
        notificationButton.addSubview(badgeView)
        
        badgeView.translatesAutoresizingMaskIntoConstraints = false
        let constraints: [NSLayoutConstraint] = [
            badgeView.rightAnchor.constraint(equalTo: notificationButton.rightAnchor, constant: 0),
            badgeView.topAnchor.constraint(equalTo: notificationButton.topAnchor, constant: -2),
            badgeView.widthAnchor.constraint(equalToConstant: badgeSize),
            badgeView.heightAnchor.constraint(equalToConstant: badgeSize)
        ]
        NSLayoutConstraint.activate(constraints)
        
//                let findDoctorVC = VideoConsulationViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
//                findDoctorVC.hidesBottomBarWhenPushed = true
//                navigationController?.pushViewController(findDoctorVC, animated: false)
        
        //        let findDoctorVC = CheckOutViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        //        findDoctorVC.hidesBottomBarWhenPushed = true
        //        navigationController?.pushViewController(findDoctorVC, animated: false)
        
        
        //        let types: [String] = [
        //            kUTTypeJPEG as String,
        //            kUTTypePNG as String,
        //            "com.microsoft.word.doc",
        //            "org.openxmlformats.wordprocessingml.document",
        //            kUTTypeRTF as String,
        //            "com.microsoft.powerpoint.​ppt",
        //            "org.openxmlformats.presentationml.presentation",
        //            kUTTypePlainText as String,
        //            "com.microsoft.excel.xls",
        //            "org.openxmlformats.spreadsheetml.sheet",
        //            kUTTypePDF as String,
        //            kUTTypeMP3 as String
        //        ]
        //        let documentPicker = UIDocumentPickerViewController(documentTypes: types, in: .import)
        //        documentPicker.delegate = self
        //        documentPicker.modalPresentationStyle = .formSheet
        //        self.present(documentPicker, animated: true, completion: nil)
        
//                let findDoctorVC = WaitingViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
//                findDoctorVC.hidesBottomBarWhenPushed = true
//                navigationController?.pushViewController(findDoctorVC, animated: false)

//        let findDoctorVC = RequestSummaryViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
//        findDoctorVC.hidesBottomBarWhenPushed = true
//        navigationController?.pushViewController(findDoctorVC, animated: false)

//        
//        let findDoctorVC = JoinCallViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
//        findDoctorVC.hidesBottomBarWhenPushed = true
//        navigationController?.pushViewController(findDoctorVC, animated: false)

//                let findDoctorVC = VideoConsulati`onViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
//                findDoctorVC.hidesBottomBarWhenPushed = true
//                navigationController?.pushViewController(findDoctorVC, animated: false)

//        let findDoctorVC = IDVerificationViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
//        findDoctorVC.hidesBottomBarWhenPushed = true
//        navigationController?.pushViewController(findDoctorVC, animated: false)
    }
 
    @objc func bannerToggleAction(){
        let articlesVC = ArticlesWebViewController.instantiateFromAppStoryboard(appStoryboard: .Menu)
        articlesVC.hidesBottomBarWhenPushed = true
        articlesVC.titleStr = "COVID-19 Testing"
        articlesVC.webViewUrl = "\(covid19UlrCount)"
        navigationController?.pushViewController(articlesVC, animated: false)
    }
    
    func  loadHealthViewModel()  {
        let healthBlogViewModel = HealthBlogModelView()
        healthBlogViewModel.getHealthBlogDetails()
        healthBlogViewModel.completionHandler{[weak self] (details, status, message)in
            if status {
                guard let self = self else {return}
                guard let itemList = details else {return}
                self.healthBlog = itemList.healthBlog
                self.upcompingCount = itemList.upcomingAppointmentCount ?? 0
                self.covid19UlrCount = itemList.covid19url ?? ""
                self.readMoreUrl = itemList.readmoreblogs ?? ""
                //self.UpcomingAppointTableView.reloadData()
                self.healthExpertsCollectionView.reloadData()
                self.upcomingSetupFrame()
            }
        }
    }
        
    @IBAction func didTapProfileAction(_ sender: Any) {
        let profileVC = ProfileSetttingViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        profileVC.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(profileVC, animated: false)
    }
    
    func loadSetup(){
        getCardModel.vcDashBoard = self
        getCardModel.getCardListApi( apitokenStr: "apiToken=\(UserDetails.shared.getApiToken())",loadview: self)
    }
    
    
//    func upcomigLoadSetup(){
//        print(["apiToken": UserDetails.shared.getApiToken()])
//        upcomingViewModel.vcDashBoard = self
//        upcomingViewModel.getUpcomingListApi(getviewController: self.tabBarController!, viewC: self)
//        upcomingSetupFrame()
//    }
    
    func upcomingSetupFrame(){
        doOnMain {
            if self.upcompingCount == 0 {
                self.upComingViewHeight.constant = 0
                self.upComingTop.constant = 0
                self.upcomingBottom.constant = 17
                self.upcomingView.isHidden = true
            }else{
                self.upComingViewHeight.constant = 54
                self.upComingTop.constant = 17
                self.upcomingBottom.constant = 17
                self.upcomingView.isHidden = false
            }
            self.upComingViewHeight.isActive = true
            self.upComingTop.isActive = true
            self.upcomingBottom.isActive = true
            self.countLabel.layer.cornerRadius = self.countLabel.frame.size.width / 2
            self.countLabel.clipsToBounds = true
            self.countLabel.text = "\(self.upcompingCount)"
        }
    }
    
    func customStringFormatting(of str: String) -> String {
        return "\(str.count)".chunk(n: 4)
            .map{ String($0) }.joined(separator: "-")
    }
    
    private func collectionViewSetup() {
        profileLabel.text = UserDetails.shared.userName ?? ""
        specialsCollectionView.dataSource = self
        specialsCollectionView.delegate = self
        healthExpertsCollectionView.dataSource = self
        healthExpertsCollectionView.delegate = self
        healthExpertsCollectionView.reloadData()
    }
    
    private func registerCells() {
        //        setStatusBar(color: UIColor.themeColor)
        popularCollectionView.register(UINib(nibName: CategoryCell.identifier, bundle: nil), forCellWithReuseIdentifier: CategoryCell.identifier)
        specialsCollectionView.register(UINib(nibName: SpecialCategoryCell.identifier, bundle: nil), forCellWithReuseIdentifier: SpecialCategoryCell.identifier)
        healthExpertsCollectionView.register(UINib(nibName: HealthExpertsReadTopCell.identifier, bundle: nil), forCellWithReuseIdentifier: HealthExpertsReadTopCell.identifier)
    }
    
    private func layoutDesign() {
        //        let cellSize:CGFloat = (popularCollectionView.frame.size.width) / 2
        //   let cellSize = CGSize(width: 150 , height: 182)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        //        layout.itemSize = CGSize(width: cellSize, height: 182)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 21
        layout.minimumInteritemSpacing = 21
        
        popularCollectionView.setCollectionViewLayout(layout, animated: false)
        
        popularCollectionView.isScrollEnabled = false
        specialsCollectionView.isScrollEnabled = false
        
        let cellSize1 = CGSize(width: 218 , height: 217)
        let layout1 = UICollectionViewFlowLayout()
        layout1.scrollDirection = .horizontal
        layout1.itemSize = cellSize1
        layout1.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout1.minimumLineSpacing = 22.0
        layout1.minimumInteritemSpacing = 22.0
        healthExpertsCollectionView.setCollectionViewLayout(layout1, animated: false)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        loadSetup()
        loadHealthViewModel()
//        upcomigLoadSetup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func didTapUpcomingAction(_ sender: Any) {
        doOnMain {
            self.tabBarController?.selectedIndex = 2
        }
    }
    
    @IBAction func didTapNotificationAction(_ sender: Any) {
        let notificationVC = NotificationViewController.instantiateFromAppStoryboard(appStoryboard: .Setting)
        notificationVC.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(notificationVC, animated: false)
    }
    
    @IBAction func didTapReadMoreAction(_ sender: Any) {
        let articlesVC = ArticlesWebViewController.instantiateFromAppStoryboard(appStoryboard: .Menu)
        articlesVC.hidesBottomBarWhenPushed = true
        articlesVC.titleStr = "Blogs"
        articlesVC.webViewUrl = readMoreUrl
         navigationController?.pushViewController(articlesVC, animated: false)
    }
}

extension DashBoard: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case popularCollectionView:
            return categoryDict.count
        case specialsCollectionView:
            return 1
        case healthExpertsCollectionView:
            //return healthExpertsDict.count
            return healthBlog?.count ?? 0
        default: return 0
        }
    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    //        return UIEdgeInsets(top: 1.0, left: 1.0, bottom: 1.0, right: 1.0)//here your custom value for spacing
    //    }
    //
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == popularCollectionView{
            let size = (collectionView.frame.size.width - 23) / 2
            return CGSize(width: size, height: 185)
        }else if collectionView == specialsCollectionView{
            return CGSize(width: specialsCollectionView.frame.size.width, height: 126)
        }
        return CGSize(width: 217, height: 217)
    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //        if popularCollectionView == collectionView{
    //            let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
    //            let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
    //            let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
    //            return CGSize(width: size, height: size)
    //        }
    //        return CGSize(width: 0, height: 0)
    //    }
    
    //    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    //        return 50
    //    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case popularCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCell.identifier, for: indexPath) as! CategoryCell
            cell.setup(catDict: categoryDict, selectIndexPath: indexPath)
            return cell
        case specialsCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SpecialCategoryCell.identifier, for: indexPath) as! SpecialCategoryCell
            //            doOnMain {
            //                cell.bgView.layer.borderWidth = 0.5
            //                cell.bgView.layer.borderColor = UIColor.themeColor.cgColor
            //                cell.bgView.backgroundColor = .white
            //                cell.bgView.backgroundColor = .red
            //            }
            //            cell.bgView.backgroundColor = .green
            //            cell.bgView.frame = CGRect(x: 0, y: 0, width: cell.frame.size.width, height: cell.frame.size.height)
            return cell
        case healthExpertsCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HealthExpertsReadTopCell.identifier, for: indexPath) as! HealthExpertsReadTopCell
            cell.setup(catDict: healthBlog ?? [], selectIndexPath: indexPath)
            return cell
        default: return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == healthExpertsCollectionView {
            let articlesVC = ArticlesWebViewController.instantiateFromAppStoryboard(appStoryboard: .Menu)
            articlesVC.hidesBottomBarWhenPushed = true
            articlesVC.titleStr = "\(healthBlog?[indexPath.row].title ?? "")"
            articlesVC.webViewUrl = "\( healthBlog?[indexPath.row].url ?? "")"
             navigationController?.pushViewController(articlesVC, animated: false)
        }else{
            switch collectionView {
            case popularCollectionView:
                switch indexPath.row {
                case 0:
                    didTaptoVideoButton()
                case 1:
                    didTaptoSearchButton()
                default:
                    break
                }
            default: break
            }
        }
    }
    
    
    
    //MARK:- didTaptoSearchButton
    @objc func didTaptoSearchButton() {
        let findDoctorVC = FindDoctorViewController.instantiateFromAppStoryboard(appStoryboard: .Tabbar)
        findDoctorVC.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(findDoctorVC, animated: false)
    }
    //MARK:- didTaptoVideoButton
    @objc func didTaptoVideoButton() {
        if getCardModel.cardList.count > 0{
            redirectPaymentView()
        }else{
            redirectMakeInstantView()
        }
    }
    
    func redirectPaymentView(){
        let findDoctorVC = PaymentListViewController.instantiateFromAppStoryboard(appStoryboard: .Tabbar)
        findDoctorVC.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(findDoctorVC, animated: false)
    }
    func redirectMakeInstantView(){
        let findDoctorVC = InstantVideoAddCardViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        findDoctorVC.hidesBottomBarWhenPushed = true
        findDoctorVC.cardBool = true
        navigationController?.pushViewController(findDoctorVC, animated: false)
    }
}

extension Collection {
    public func chunk(n: IndexDistance) -> [SubSequence] {
        var res: [SubSequence] = []
        var i = startIndex
        var j: Index
        while i != endIndex {
            j = index(i, offsetBy: n, limitedBy: endIndex) ?? endIndex
            res.append(self[i..<j])
            i = j
        }
        return res
    }
}
extension Data{
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
}


