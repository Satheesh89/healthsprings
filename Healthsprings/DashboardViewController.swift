//
//  DashboardViewController.swift
//  Healthsprings
//
//  Created by Personal on 18/06/22.
//

import UIKit
import Alamofire
import CRNotifications

class DashboardViewController: UIViewController {
    
    var Token = String()
    var profile_id = String()
    var profile_image = String()
    var clinical_id = String()
    var doctor_id = String()
    var id = Int()
    var login_time = String()
    var patientid = String()
    var patient_name = String()
    
    @IBOutlet weak var dashBoardTableView: UITableView!
    @IBOutlet weak var alertBgView: UIView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var popupTitleLabe: UILabel!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var yesButton: UIButton!
    
    
    // ImageScrollView
    var imgArr = [  UIImage(named:"why-healthcheck"),
                    UIImage(named:"yoga")]
    
    var textArr = ["General Health\nBeing healthy is about more than diet and exercise.  It is also about understanding how your body works and what it needs to stay healthy.  You can start by learning these general health terms., print\n12 Mar 2022","Yoga & Meditation\nYoga is a mind and body practice. Various styles of yoga combine physical postures, breathing techniques, and meditation or relaxation. Yoga is an ancient practice that may have originated in India. It involves movement, meditation, and breathing techniques to promote mental and physical well-being., print\n12 Mar 2022"]
    let dataArray = [["item_name": "General Health", "detail_desc": "Being healthy is about more than diet and exercise.  It is also about understanding how your body works and what it needs to stay healthy.  You can start by learning these general health terms.", "date": "12 Mar 2022"], ["item_name": "Yoga & Meditation", "detail_desc": "Yoga is a mind and body practice. Various styles of yoga combine physical postures, breathing techniques, and meditation or relaxation. Yoga is an ancient practice that may have originated in India. It involves movement, meditation, and breathing techniques to promote mental and physical well-being.", "date": "12 Mar 2022"]]
    var symptomsArray = ["Abdominal discomfort",
                         "Anuria/oliguria",
                         "Bloody cough",
                         "Bloody diarrhea",
                         "Bone/muscle/joint pain",
                         "Bubo-lymphadenitis",
                         "Chills","Common cold",
                         "Conjunctivitis",
                         "Cough",
                         "Cutaneous bleeding",
                         "Dark urine",
                         "Dehydration",
                         "Diarrhea",
                         "Difficult breathing",
                         "Signs/Symptoms",
                         "Fever",
                         "Headache",
                         "Hematemesis/melena",
                         "Jaundice",
                         "Malaise",
                         "Mental status disturbances",
                         "Nausea",
                         "Paralysis",
                         "Rash",
                         "Seizures",
                         "Sore throat",
                         "Stiff neck",
                         "Vesicle/bullae",
                         "Vomiting"]
    var timer = Timer()
    var counter = 0
    //    var selectIndexPath = IndexPath()
    var selectIndexPath: Int = 0
    private var authViewModel = AuthViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        initializeHideKeyboard()
        tableViewSetup()
//        let findDoctorVC = MakeRequestViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
//        findDoctorVC.hidesBottomBarWhenPushed = true
//        self.navigationController?.pushViewController(findDoctorVC, animated: false)
//        let findDoctorVC = AddProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
//        findDoctorVC.hidesBottomBarWhenPushed = true
//        self.navigationController?.pushViewController(findDoctorVC, animated: false)
   }
    
    //MARK: TableView Setup
    func tableViewSetup()  {
        dashBoardTableView.register(HeaderCell.nib, forCellReuseIdentifier: HeaderCell.identifier)
        dashBoardTableView.register(DashBoardSearchDoctorCell.nib, forCellReuseIdentifier: DashBoardSearchDoctorCell.identifier)
        dashBoardTableView.register(MedicalServicesCell.nib, forCellReuseIdentifier: MedicalServicesCell.identifier)
        dashBoardTableView.register(HealthExpertsCell.nib, forCellReuseIdentifier: HealthExpertsCell.identifier)
        dashBoardTableView.delegate = self
        dashBoardTableView.dataSource = self
        dashBoardTableView.tableFooterView = UIView()
        dashBoardTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        dashBoardTableView.showsHorizontalScrollIndicator = false
        dashBoardTableView.showsVerticalScrollIndicator = true
        //serviceTableView.setContentOffset(.zero, animated: true)
        dashBoardTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        dashBoardTableView.backgroundColor = .clear
        dashBoardTableView.contentInset.top = -10
        dashBoardTableView.contentInsetAdjustmentBehavior = .never
        //        selectIndexPath = IndexPath(row: 0, section: 0)
    }
    
    //MARK: Load Setup
    func alertSetup()  {
        alertBgView.backgroundColor = .alphaColor
        alertBgView.isHidden = true
        popupView.layer.cornerRadius = 5
        yesButton.layer.cornerRadius = 5
        noButton.layer.cornerRadius = 5
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        // Hide the Navigation Back button
        self.navigationItem.hidesBackButton = true
    }
    
    //MARK:- didTaptoFindDoctorButton
    @objc func didTaptoFindDoctorButton() {
//        let findDoctorVC = FindDoctorViewController.instantiateFromAppStoryboard(appStoryboard: .Tabbar)
//        findDoctorVC.hidesBottomBarWhenPushed = true
//        self.navigationController?.pushViewController(findDoctorVC, animated: false)
    }
    //MARK:- didTaptoSearchButton
    @objc func didTaptoSearchButton() {
        let findDoctorVC = FindDoctorViewController.instantiateFromAppStoryboard(appStoryboard: .Tabbar)
        findDoctorVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(findDoctorVC, animated: false)
    }
    //MARK:- didTaptoVideoButton
    @objc func didTaptoVideoButton() {
        let findDoctorVC = MakeInstantAddCardViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        findDoctorVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(findDoctorVC, animated: false)
    }
}

extension DashboardViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            return getHeaderTableViewCell(tableView: tableView, indexPath: indexPath)
        }else if indexPath.section == 1 {
            return getDoctorTableViewCell(tableView: tableView, indexPath: indexPath)
        }else if indexPath.section == 2 {
            return getMedicalServicesTableViewCell(tableView: tableView, indexPath: indexPath)
        }else{
            return getHealthExpertCell(tableView: tableView, indexPath: indexPath)
        }
    }
    
    func getHeaderTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HeaderCell.identifier) as? HeaderCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        //        Token = UserInformation().getUserDetails().token
        //        patient_name = UserInformation().getUserDetails().patientName
        //        cell.profileNameLabel.text = "Hello!"
        cell.cellCofig(ttitle: UserDetails.shared.userName ?? "",imageUrl: UserInformation().getUserDetails().profileimage)
        let profileImage = cell.profileImageView.image?.resizeImage(targetSize: CGSize(width: cell.profileImageView.frame.size.width, height: cell.profileImageView.frame.size.height))
        cell.profileImageView.image = profileImage
        return cell
    }
    
    func getDoctorTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DashBoardSearchDoctorCell.identifier) as? DashBoardSearchDoctorCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.searchButton.addTarget(self, action: #selector(didTaptoFindDoctorButton), for: .touchUpInside)
        return cell
    }
    
    func getMedicalServicesTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MedicalServicesCell.identifier) as? MedicalServicesCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
        cell.findDoctorBtn.addTarget(self, action: #selector(didTaptoSearchButton), for: .touchUpInside)
        cell.videoButton.addTarget(self, action: #selector(didTaptoVideoButton), for: .touchUpInside)
        return cell
    }
    
    func getHealthExpertCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HealthExpertsCell.identifier) as? HealthExpertsCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: cell.collView.frame.width, height: cell.collView.frame.height)
        layout.minimumInteritemSpacing = 24
        layout.minimumLineSpacing = 24
        layout.scrollDirection = .horizontal
        var sliderCollectionView: UICollectionView!
        sliderCollectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: cell.frame.size.height), collectionViewLayout: layout)
        sliderCollectionView.dataSource = self
        sliderCollectionView.delegate = self
        sliderCollectionView.tag = 1000
        sliderCollectionView.register(ImageScrollViewCell.self, forCellWithReuseIdentifier: "ImageCell")
        cell.collView.addSubview(sliderCollectionView)
        sliderCollectionView.reloadData()
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
class ImageScrollViewCell: UICollectionViewCell {
    let categoryImage: UIImageView = {
        let label = UIImageView()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.contentMode = .scaleToFill
        return label
    }()
    let shadowView: UIView = {
        let backView = UIView()
        backView.translatesAutoresizingMaskIntoConstraints = false
        let color = UIColor.lightGray.withAlphaComponent(0.8)
        backView.backgroundColor = color
        backView.clipsToBounds = true
        backView.layer.cornerRadius = 10
        //        backView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        // Top Left Corner: .layerMinXMinYCorner
        // Top Right Corner: .layerMaxXMinYCorner
        // Bottom Left Corner: .layerMinXMaxYCorner
        // Bottom Right Corner: .layerMaxXMaxYCorner
        return backView
    }()
    
    let categoryLabel: UILabel = {
        let label = UILabel()
        if ( UIDevice.current.model.range(of: "iPad") != nil){
            label.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(10.0))
        } else {
            label.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))
        }
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let detailLabel: UILabel = {
        let label = UILabel()
        if ( UIDevice.current.model.range(of: "iPad") != nil){
            label.font = UIFont(name:Font.FontName.Latoregular.rawValue, size: Utility.dynamicSize(08.0))
        } else {
            label.font = UIFont(name:Font.FontName.Latoregular.rawValue, size: Utility.dynamicSize(12.0))
        }
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        if ( UIDevice.current.model.range(of: "iPad") != nil){
            label.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(08.0))
        } else {
            label.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(11.0))
        }
        label.translatesAutoresizingMaskIntoConstraints = false
        //        label.backgroundColor = .yellow
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addViews()
    }
    
    func addViews(){
        backgroundColor = UIColor.white
        addSubview(categoryImage)
        categoryImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
        categoryImage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true
        categoryImage.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -6).isActive = true
        categoryImage.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        
        addSubview(shadowView)
        shadowView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
        shadowView.topAnchor.constraint(equalTo: centerYAnchor, constant: -20).isActive = true
        shadowView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true
        shadowView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        
        shadowView.addSubview(categoryLabel)
        categoryLabel.leadingAnchor.constraint(equalTo: shadowView.leadingAnchor, constant: 18).isActive = true
        categoryLabel.topAnchor.constraint(equalTo: shadowView.topAnchor, constant: 10).isActive = true
        categoryLabel.trailingAnchor.constraint(equalTo: shadowView.trailingAnchor, constant: -18).isActive = true
        categoryLabel.heightAnchor.constraint(equalToConstant: 21).isActive = true
        
        //        shadowView.addSubview(detailLabel)
        //        detailLabel.leadingAnchor.constraint(equalTo: shadowView.leadingAnchor, constant: 5).isActive = true
        //        detailLabel.trailingAnchor.constraint(equalTo: shadowView.trailingAnchor, constant: -5).isActive = true
        //        detailLabel.topAnchor.constraint(equalTo: categoryLabel.bottomAnchor, constant: 5).isActive = true
        //        detailLabel.bottomAnchor.constraint(equalTo: shadowView.bottomAnchor, constant: -45).isActive = true
        
        shadowView.addSubview(detailLabel)
        detailLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: detailLabel, attribute: .leading, relatedBy: .equal, toItem: shadowView, attribute: .leading, multiplier: 1.0, constant: 18).isActive = true
        NSLayoutConstraint(item: detailLabel, attribute: .top, relatedBy: .equal, toItem: categoryLabel, attribute: .bottom, multiplier: 1.0, constant: 5).isActive = true
        NSLayoutConstraint(item: detailLabel, attribute: .trailing, relatedBy: .equal, toItem: shadowView, attribute: .trailing, multiplier: 1.0, constant: -18).isActive = true
        NSLayoutConstraint(item: detailLabel, attribute: .height, relatedBy: .lessThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50).isActive = true
        
        shadowView.addSubview(dateLabel)
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: dateLabel, attribute: .leading, relatedBy: .equal, toItem: shadowView, attribute: .leading, multiplier: 1.0, constant: 18).isActive = true
        NSLayoutConstraint(item: dateLabel, attribute: .top, relatedBy: .equal, toItem: detailLabel, attribute: .bottom, multiplier: 1.0, constant: 5).isActive = true
        NSLayoutConstraint(item: dateLabel, attribute: .trailing, relatedBy: .equal, toItem: shadowView, attribute: .trailing, multiplier: 1.0, constant: -18).isActive = true
        NSLayoutConstraint(item: dateLabel, attribute: .height, relatedBy: .lessThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50).isActive = true
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class FreelancerCell: UICollectionViewCell {
    let syView: UIView = {
        let sview = UIView()
        sview.translatesAutoresizingMaskIntoConstraints = false
        return sview
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        //        Font Names = [["FuturaBT-Medium", "FuturaBT-MediumItalic", "FuturaBT-Bold", "FuturaBT-BoldItalic"]]
        label.font = UIFont(name: "FuturaBT-Medium", size: 14)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addViews()
    }
    
    func addViews(){
        backgroundColor = UIColor.white
        addSubview(syView)
        syView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
        syView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true
        syView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        syView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        
        syView.addSubview(nameLabel)
        nameLabel.leadingAnchor.constraint(equalTo: syView.leadingAnchor, constant: 0).isActive = true
        nameLabel.topAnchor.constraint(equalTo: syView.topAnchor, constant: 10).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: syView.trailingAnchor, constant: 0).isActive = true
        nameLabel.bottomAnchor.constraint(equalTo: syView.bottomAnchor, constant: -27).isActive = true
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

@available(iOS 13.0, *)
extension DashboardViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageScrollViewCell
        let catImage = imgArr[indexPath.row]
        cell.categoryImage.image = catImage?.resizeImage(targetSize: CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height))
        let dict = dataArray[indexPath.row] as? NSDictionary
        cell.categoryLabel.text = "\(dict!.value(forKey: "item_name") ?? "")"
        cell.detailLabel.text = "\(dict!.value(forKey: "detail_desc") ?? "")"
        cell.dateLabel.text = "\(dict!.value(forKey: "date") ?? "")"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //        if collectionView.tag != 1000 {
        //            selectIndexPath = indexPath
        //            dashBoardTableView.reloadRows(at: [IndexPath(row: 0, section: 1)], with: .automatic)
        //        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing
        return CGSize(width: 250, height: 170)
    }
    
}


