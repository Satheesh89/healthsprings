//
//  AppointmentsDetailsViewController.swift
//  Healthsprings
//
//  Created by Personal on 30/06/22.
//

import UIKit
import iOSDropDown

class AppointmentsDetailsViewController: UIViewController {

    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var appointmentDetailTableview: UITableView!
    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var reasonDropdown: DropDown!
    @IBOutlet weak var resonTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    var getAppointmentid: Int!
    var getProfileid: String!
    var appointmentDetailsViewModel = AppointmentDetailsViewModel()
    
    var historyAppointmentDetail: HistoryAppointmentDetails!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadSetup()
        setupTableview()
        reasonDropDown()
    }
    

    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func loadSetup(){
        appointmentDetailsViewModel.vc = self
        appointmentDetailsViewModel.getAppointmentDetailsListApi(apitokenStr: UserDetails.shared.getApiToken(), loadview: self.tabBarController!, profileid: getProfileid, appointmentid: getAppointmentid, viewC: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupTableview()  {
        alphaView.isHidden = true
        setStatusBar(color: UIColor.themeColor)
        appointmentDetailTableview.register(VideoConsultationCell.nib, forCellReuseIdentifier: VideoConsultationCell.identifier)
        appointmentDetailTableview.register(PatientDetailCell.nib, forCellReuseIdentifier: PatientDetailCell.identifier)
        //        appointmentDetailTableview.delegate = self
        appointmentDetailTableview.dataSource = self
        appointmentDetailTableview.estimatedRowHeight = UITableView.automaticDimension
        appointmentDetailTableview.separatorStyle = .none
        appointmentDetailTableview.tableFooterView = UIView()
        appointmentDetailTableview.showsHorizontalScrollIndicator = false
        appointmentDetailTableview.showsVerticalScrollIndicator = false
    }
    
    func reasonDropDown()  {
        alphaView.backgroundColor = .alphaColor
        centerView.backgroundColor = .white
        centerView.layer.cornerRadius = 10
        submitButton.layer.cornerRadius = 10
        self.reasonDropdown.viewBorderShadow()
        self.resonTextField.viewBorderShadow()
        self.resonTextField.delegate = self
        self.reasonDropdown.text = "    You're sick or feeling unwell."
        OperationQueue.main.addOperation({
            self.reasonDropdown.optionArray = ["    You're sick or feeling unwell.","   You have a personal or family emergency."," Other"]
            self.reasonDropdown.resignFirstResponder()
            self.reasonDropdown.textColor = .lightGray
            self.reasonDropdown.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))
            self.reasonDropdown.didSelect{(selectedText , index ,id) in
                print("Select-index",index)
//                self.personNameTextField.text = selectedText
            }
        })
    }
    
    @IBAction func didTapCloseAction(_ sender: UIButton) {
        alphaView.isHidden = true
    }
    
    @IBAction func didTapSubmitAction(_ sender: UIButton) {
        self.getCancelApi( loadview: self,profileid: getProfileid)
    }

    @IBAction func didTapLeftAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func didTapCancelAction(_ sender: Any) {
        alphaView.isHidden = false
    }
    
    @IBAction func didTapRescheduleAction(_ sender: Any) {
        let timeSlotVC = TimeSlotViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        timeSlotVC.doctorName = "Dr \(appointmentDetailsViewModel.appointmentDetails.doctorName ?? "")"
        timeSlotVC.doctorDetail = appointmentDetailsViewModel.appointmentDetails.speciality ?? ""
        timeSlotVC.doctorExprDetail = appointmentDetailsViewModel.appointmentDetails.speciality ?? ""
        timeSlotVC.doctorId = appointmentDetailsViewModel.appointmentDetails.doctor_id ?? 0
        timeSlotVC.uuidId = 0
        self.navigationController?.pushViewController(timeSlotVC, animated: false)
    }
        
    func getCancelApi( loadview: UIViewController,profileid: String) {
        LoadingIndicator.shared.show(forView: loadview.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()
        paramsDictionary["profile_id"] = profileid
        paramsDictionary["appointment_id"] = getAppointmentid
        
        HttpClientApi.instance().makeAPICall(url: "\(URL.cancelappointment)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                let response = response
                if  response?.statusCode == 200 {
                    doOnMain {
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                            self.showCustomToast(toastTitle: "Info",toastMeassage: "Appointment cancelled successfully",status: 0)
                            self.navigationController?.popViewController(animated: false)
                        }}
                }else{
                    delay(0.1) {
                        self.showCustomToast(toastTitle: "Error",toastMeassage: "error",status: 1)
                        LoadingIndicator.shared.hide()
                    }}}
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
}

extension AppointmentsDetailsViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            return getHeaderTableViewCell(tableView: tableView, indexPath: indexPath)
        }else {
            return getDetailTableViewCell(tableView: tableView, indexPath: indexPath)
        }
    }
    
    func getHeaderTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VideoConsultationCell.identifier) as? VideoConsultationCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        if appointmentDetailsViewModel.appointmentDetails != nil{
            cell.titleLabel.text = appointmentDetailsViewModel.appointmentDetails.doctorName
            cell.detailLabel.text = appointmentDetailsViewModel.appointmentDetails.speciality
            cell.dateLabel.text = appointmentDetailsViewModel.appointmentDetails.appointmentDate
            cell.timeLabel.text = appointmentDetailsViewModel.appointmentDetails.appointmentTime
            cell.statusLabel.text = appointmentDetailsViewModel.appointmentDetails.status
            let status = self.getStatusColor()
            cell.joinView.backgroundColor = status.1
        }
        return cell
    }
    
    
    //MARK: StatusColor
    func getStatusColor() -> (String, UIColor){
        if appointmentDetailsViewModel.appointmentDetails.video_consultation_available == 1{
            return ("COMPLETED", UIColor.themeColor)
        }else{
            return ("NOT COMPLETED", UIColor.joinStatusBgColor)
        }
    }
    
    func getDetailTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PatientDetailCell.identifier) as? PatientDetailCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        if appointmentDetailsViewModel.appointmentDetails != nil{
            cell.patientNameLabel.text = appointmentDetailsViewModel.appointmentDetails.patientName
            cell.symptomsLabel.text = appointmentDetailsViewModel.appointmentDetails.symptom
            cell.addressLabel.text = appointmentDetailsViewModel.appointmentDetails.fulladdress
            
            
            if appointmentDetailsViewModel.appointmentDetails.currency ?? "" == "SGD"{
                cell.priceLabel.text = "S$ \(appointmentDetailsViewModel.appointmentDetails.consultation_fee ?? 0)"
            }else if appointmentDetailsViewModel.appointmentDetails.currency ?? "" == "USD"{
                cell.priceLabel.text = "$ \(appointmentDetailsViewModel.appointmentDetails.consultation_fee ?? 0)"
            }else{
                cell.priceLabel.text = "$ \(appointmentDetailsViewModel.appointmentDetails.consultation_fee ?? 0)"
            }
            
//            cell.addressLabel.text = UserDetails.shared.userName ?? ""
//            if appointmentDetailsViewModel.appointmentDetails.paymentDate == ""{
//                cell.dateandtimeLabel.text = "Date & Time : DD MM YYYY"
//            }else{
//                cell.dateandtimeLabel.text = "Date & Time  :  \(appointmentDetailsViewModel.appointmentDetails.paymentDate ?? "")"
//            }
            cell.dateandtimeLabel.text = "Date & Time  :  \(appointmentDetailsViewModel.appointmentDetails.paymentDate ?? "")"
            cell.cardNoLabel.text = "****       ****        ****        \(appointmentDetailsViewModel.appointmentDetails.cardNo ?? 1234)"
            cell.cancelButton.addTarget(self, action: #selector(didTapCancelAction), for: .touchUpInside)
            cell.rescheduleButton.addTarget(self, action: #selector(didTapRescheduleAction(_:)), for: .touchUpInside)
        }
        return cell
    }
}


