//
//  HistoryViewController.swift
//  Healthsprings
//
//  Created by Personal on 18/06/22.
//

import UIKit

class HistoryViewController: UIViewController {
    
    @IBOutlet weak var historyTableView: UITableView!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    var historyViewModel = HistoryViewModel()
    @IBOutlet weak var emptyView: UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initializeHideKeyboard()
        loadSetup()
        tableViewSetup()
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    func loadSetup()  {
        historyViewModel.vc = self
        historyViewModel.getHistoryList(params: ["apiToken": UserDetails.shared.getApiToken()], loadview: self.tabBarController!)
    }
    //MARK: TableView Setup
    func tableViewSetup()  {
        setStatusBar(color: UIColor.themeColor)
        historyTableView.register(HistoryTableViewCell.nib, forCellReuseIdentifier: HistoryTableViewCell.identifier)
        historyTableView.dataSource = self
        historyTableView.tableFooterView = UIView()
        historyTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        historyTableView.showsHorizontalScrollIndicator = false
        historyTableView.showsVerticalScrollIndicator = true
        historyTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        historyTableView.backgroundColor = .clear
        historyTableView.contentInset.top = 5
        historyTableView.contentInsetAdjustmentBehavior = .never
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
}
extension HistoryViewController: UITableViewDataSource ,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if historyViewModel.history_details?.count == 0 {
//            tableView.setEmptyMessage("no data found!")
            emptyView.isHidden = false
        } else {
            tableView.restore()
            emptyView.isHidden = true
        }
        return historyViewModel.history_details?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getDoctorTableViewCell(tableView: tableView, indexPath: indexPath)
    }
    func getDoctorTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HistoryTableViewCell.identifier) as? HistoryTableViewCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.historyDetails = historyViewModel.history_details?[indexPath.row]
        cell.button.addTarget(self, action: #selector(self.didSelectRowIndex), for: .touchUpInside)
        cell.button.tag = indexPath.row

        return cell
    }
    
    @objc func didSelectRowIndex(sender : UIButton) {
        let findDoctorVC = HistoryDetailsViewController.instantiateFromAppStoryboard(appStoryboard: .Tabbar)
        findDoctorVC.hidesBottomBarWhenPushed = true
        findDoctorVC.getappointmentid = "\(historyViewModel.history_details?[sender.tag].appointment_id ?? 0)"
        findDoctorVC.getprofileid = historyViewModel.history_details?[sender.tag].profile_id  ?? ""
        self.navigationController?.pushViewController(findDoctorVC, animated: false)
    }
}
