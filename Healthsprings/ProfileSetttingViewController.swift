//
//  ProfileSetttingViewController.swift
//  Healthsprings
//
//  Created by Personal on 25/06/22.
//

import UIKit
import SwiftyJSON
import MXSegmentedControl
import AVFoundation
import Photos
import TTSegmentedControl
import iOSDropDown

class ProfileSetttingViewController: UIViewController, UITableViewDelegate {
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var menuSegment: MXSegmentedControl!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameTexField: UITextField!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var dobView: UIView!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var dobTextField: UITextField!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var postalView: UIView!
    @IBOutlet weak var postalLabel: UILabel!
    @IBOutlet weak var postalTextField: UITextField!
    @IBOutlet weak var bloodGroupView: UIView!
    @IBOutlet weak var bloodGroupLabel: UILabel!
    @IBOutlet weak var bloodGroupTextField: DropDown!
    @IBOutlet weak var maritalStatusView: UIView!
    @IBOutlet weak var maritalStatusLabel: UILabel!
    @IBOutlet weak var maritalStatusTextField: UITextField!
    @IBOutlet weak var heightView: UIView!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var heightTextField: UITextField!
    @IBOutlet weak var weightView: UIView!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var econtactView: UIView!
    @IBOutlet weak var econtactLabel: UILabel!
    @IBOutlet weak var econtactTextField: UITextField!
    @IBOutlet weak var medicalTableView: UITableView!
    @IBOutlet weak var profileSegment: TTSegmentedControl!
    @IBOutlet weak var nricTextField: UITextField!

    @IBOutlet weak var nameLine: UILabel!
    @IBOutlet weak var emailLine: UILabel!
    @IBOutlet weak var phoneLine: UILabel!

    @IBOutlet weak var nricLine: UILabel!
    @IBOutlet weak var passwordLine: UILabel!
    @IBOutlet weak var dobLine: UILabel!
    @IBOutlet weak var countryLine: UILabel!
    @IBOutlet weak var postalLine: UILabel!
    @IBOutlet weak var bloodLine: UILabel!
    @IBOutlet weak var maritalLine: UILabel!
    @IBOutlet weak var heightLine: UILabel!
    @IBOutlet weak var weightLine: UILabel!
    @IBOutlet weak var econtactLine: UILabel!
    
    @IBOutlet weak var nameImageview: UIImageView!
    @IBOutlet weak var emailImageview: UIImageView!
    @IBOutlet weak var phoneImageview: UIImageView!
    @IBOutlet weak var nricImageview: UIImageView!
    @IBOutlet weak var passwordImageview: UIImageView!
    @IBOutlet weak var dobImageview: UIImageView!
    @IBOutlet weak var countryImageview: UIImageView!
    @IBOutlet weak var postalImageview: UIImageView!
    @IBOutlet weak var bloodImageview: UIImageView!
    @IBOutlet weak var maritalImageview: UIImageView!
    @IBOutlet weak var heightImageview: UIImageView!
    @IBOutlet weak var weightImageview: UIImageView!
    @IBOutlet weak var econtactImageview: UIImageView!

    let countrycodepicker = UIPickerView()
    var countryCodeArray = NSArray()
    var countryCode: [Code] = []
    var countryModel: CountryModel!
    var viewModelMedical = MedicalViewModel()
    var updateProfileViewModel = UpdateProfileViewModel()
    var getProfileId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setStatusBar(color: UIColor.themeColor)
        loadSetup()
        segmentSetup()
        setupLoad()
        loadJsonData()
        scrollView.isHidden = false
        medicalTableView.isHidden = true
        setupTableview()
        
        self.bloodGroupTextField.text = "Select Blood Group"
        self.bloodGroupTextField.optionArray = ["A+","O+","AB+","A-","O-","B-","AB-"]
        self.bloodGroupTextField.resignFirstResponder()
        self.bloodGroupTextField.textColor = .lightGray
        self.bloodGroupTextField.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))
        self.bloodGroupTextField.didSelect{(selectedText , index ,id) in
            print("Select-index",index)
        }
        self.bloodGroupTextField.backgroundColor = .red
    }
    
//    func nricVal(){
//        var ic = document.getElementByID("nric").value
//        if(!ic.match(/^[STFG]\d{7}[A-Z]$/)){
//           alert("Invalid NRIC")
//           return false
//        }
//    }
    
    func loadSetup(){
        updateProfileViewModel.vc = self
        updateProfileViewModel.getProfileGetRequest(getviewController: self)
    }
    
    func setupTableview()  {
        medicalTableView.register(MedicalCell.nib, forCellReuseIdentifier: MedicalCell.identifier)
        medicalTableView.delegate = self
        medicalTableView.dataSource = self
        medicalTableView.estimatedRowHeight = UITableView.automaticDimension
        medicalTableView.separatorStyle = .none
        medicalTableView.tableFooterView = UIView()
        medicalTableView.showsHorizontalScrollIndicator = false
        medicalTableView.showsVerticalScrollIndicator = false
    }
    
    @IBAction func didTapChangePasswordAction(_ sender: UIButton) {
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: - Segment Setup
    func segmentSetup()  {
        menuSegment.append(title: "Personal")
        menuSegment.append(title: "Medical")
        menuSegment.append(title: "Lifestyle")
        
        if ( UIDevice.current.model.range(of: "iPad") != nil){
            menuSegment.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(09.0))!
        } else {
            menuSegment.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))!
        }

        menuSegment.addTarget(self, action: #selector(changeIndex(segmentedControl:)), for: .valueChanged)
        menuSegment.selectedTextColor = .themeColor
        menuSegment.indicatorColor = .themeColor
        
        profileSegment.itemTitles = ["Personal","Medical","Lifestyle"]
        profileSegment.selectedTextFont = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))!
        profileSegment.defaultTextFont = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))!
        profileSegment.thumbGradientColors = [UIColor.themeColor, UIColor.themeColor]
        profileSegment.didSelectItemWith = { (index, title) -> () in
            print("Selected item \(index)")
            if index == 0 {
                self.scrollView.isHidden = false
                self.medicalTableView.isHidden = true
            }else if  index == 1 {
                self.scrollView.isHidden = true
                self.medicalTableView.isHidden = false
                self.viewModelMedical.vc = self
                self.viewModelMedical.loadJsonData()
            }else{
                self.scrollView.isHidden = true
                self.medicalTableView.isHidden = true
            }
        }

//        let bottomBorder = CALayer()
//        bottomBorder.frame = CGRect(x: 0.0, y: profileSegment.frame.size.height-1, width: self.view.frame.width, height: 1.0)
//        bottomBorder.backgroundColor = #colorLiteral(red: 0.06274509804, green: 0.6117647059, blue: 0.7490196078, alpha: 1)
//        profileSegment.layer.addSublayer(bottomBorder)
//        profileSegment.didSelectItemWith = { (index, title) -> () in
//            print("Selected item \(index)")
//        }

    }
    
    
    func setupLoad(){
        initializeHideKeyboard()
        self.countryTextField.delegate = self
        self.countryTextField.tintColor = .clear
        self.countrycodepicker.dataSource = self
        self.countrycodepicker.delegate = self
        self.countryTextField.inputView = self.countrycodepicker
        self.countrycodepicker.reloadAllComponents()
        
        self.countrycodepicker.backgroundColor = .themeColor
        self.dobTextField.addInputViewDatePicker(target: self, selector: #selector(tapDone)) //1
        updateProfileViewModel.getProfileGetRequest(getviewController: self)
        profileNameLabel.text = "Hi \(UserDetails.shared.userName ?? "")"
        
        nameTexField.delegate = self
        emailTextField.delegate = self
        phoneTextField.delegate = self
        passwordTextField.delegate = self
        dobTextField.delegate = self
        countryTextField.delegate = self
        postalTextField.delegate = self
        bloodGroupTextField.delegate = self
        maritalStatusTextField.delegate = self
        heightTextField.delegate = self
        weightTextField.delegate = self
        econtactTextField.delegate = self
        nricTextField.delegate = self

//        nameTexField.text = UserDetails.shared.userName ?? ""
//        emailTextField.text = UserDetails.shared.emailId ?? ""
//        phoneTextField.text = UserDetails.shared.phoneNumber ?? ""
//        passwordTextField.text = ""
//        dobTextField.text = UserDetails.shared.dateofbirth ?? ""
//        countryTextField.text = UserDetails.shared.country ?? ""
//        postalTextField.text = UserDetails.shared.pincode ?? ""
//        bloodGroupTextField.text = UserDetails.shared.bloodGroup ?? ""
//        maritalStatusTextField.text = UserDetails.shared.maritalstatus ?? ""
//        heightTextField.text = UserDetails.shared.height ?? ""
//        weightTextField.text = UserDetails.shared.weight ?? ""
//        econtactTextField.text = UserDetails.shared.emergencycontact ?? ""
//        profileNameLabel.text = "Hi \(UserDetails.shared.userName ?? "")"
    }
    
    @objc func tapDone() {
        if let datePicker = self.dobTextField.inputView as? UIDatePicker {
            let dateFormatter: DateFormatter = DateFormatter()
            // Set date format
            dateFormatter.dateFormat = "yyyy-MM-dd"
            // Apply date format
            let selectedDate: String = dateFormatter.string(from: datePicker.date)
            self.dobTextField.text = selectedDate
        }
        self.dobTextField.resignFirstResponder() 
    }
    
    func loadJsonData()  {
        if let localData = self.readLocalFile(forName: "Country") {
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: localData, options: .mutableContainers)
                self.countryModel = CountryModel(JSON(jsonResponse))
                self.countryCode = self.countryModel?.countryDetail?.code ?? []
                print(self.countryCode.count)
            }catch let error{
                print(error)
            }
        }
    }
    
    private func readLocalFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name,
                                                 ofType: "json"),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        return nil
    }
    //MARK: - Change Index
    @objc func changeIndex(segmentedControl: MXSegmentedControl) {
        if segmentedControl.selectedIndex == 0 {
            scrollView.isHidden = false
            medicalTableView.isHidden = true
        }else if  segmentedControl.selectedIndex == 1 {
            scrollView.isHidden = true
            medicalTableView.isHidden = false
            viewModelMedical.vc = self
            viewModelMedical.loadJsonData()
        }else{
            scrollView.isHidden = true
            medicalTableView.isHidden = true
        }
    }
    
    //MARK: - Back
    @IBAction func didTapLeftAction(_ sender: Any) {
//        self.navigationController?.popToViewController(ofClass: SettingsViewController.self)
        self.navigationController?.popViewController(animated: false)

    }
    
    @IBAction func didTapImageUploadAction(_ sender: Any) {
        checkCameraPermission()
    }
    
    // MARK: - Camera Permission
    func checkCameraPermission()  {
        let cameraMediaType = AVMediaType.video
        AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
            if granted {
                print("Granted access for camera")
                DispatchQueue.main.async {
                    ImagePickerManager().pickImage(self){ image in
                        doOnMain {
                            self.profileImageView.image = image
                            self.imageUploadB64(uploadImd: self.profileImageView.image!)
                        }
                    }
                }
            } else {
                self.noCameraFound()
                print("Denied access for camera ")
            }
        }
    }
    
    //  MARK: No Camera Found
    func noCameraFound(){
        let alert = UIAlertController(title: "Pingle User", message: "Please allow camera access in phone settings", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {(action:UIAlertAction) in
        }));
        alert.addAction(UIAlertAction(title: "Open setting ", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
            UIApplication.shared.open(NSURL(string:UIApplication.openSettingsURLString)! as URL, options: [:], completionHandler: nil)
        }));
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Image UploadB 64
    func imageUploadB64(uploadImd:UIImage) {
        let sampleImage = image(with: uploadImd, scaledTo: CGSize(width: 216.0, height: 216.0))
        let url = NSURL(string:  URL.getUploaduserimage)
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        var headers = ["":""]
        headers = ["Accept-Language": "en",
                   "apiToken" : UserDetails.shared.getApiToken()]
        request.allHTTPHeaderFields = headers
        let boundary = generateBoundaryString()
        //define the multipart request type
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        if (profileImageView.image == nil){
            return
        }
        let image_data = sampleImage.pngData()
        if(image_data == nil){
            return
        }
        let body = NSMutableData()
        let fname = "image.png"
        let mimetype = "image/png"
        //define the data post parameter
        let parameters = ["apiToken": UserDetails.shared.getApiToken(),
                          "profile_id": getProfileId] as [String : Any]
        if parameters != nil {
            for (key, value) in parameters {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"test\"\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append("hi\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"image\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(image_data!)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        request.httpBody = body as Data
        let session = URLSession.shared
        session.dataTask(with: request as URLRequest) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    doOnMain {
                        self.showCustomToast(toastTitle: "Info",toastMeassage: ((json as? NSDictionary)?.value(forKey: "message") ?? "") as! String,status: 0)
                    }
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    // MARK: - Boundary Value
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func image(with image: UIImage, scaledTo newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        profileImageView.image = newImage
        return newImage ?? UIImage()
    }
    
    @IBAction func didTabProfileUpdateAction(_ sender: Any) {
        
        if nameTexField!.text != "" && emailTextField.text != "" && phoneTextField.text != "" && nricTextField.text != "" && dobTextField.text != "" && countryTextField.text != "" && postalTextField.text != "" && bloodGroupTextField.text != "" && maritalStatusTextField.text != "" && heightTextField.text != "" && weightTextField.text != "" && econtactTextField.text != "" && "\(emailTextField.text ?? "")".isValidEmail() == true && "\(phoneTextField.text ?? "")".isValidContact == true{
            updateProfile()
        }else{
            self.textFieldValidation()
        }
    }
    
    func updateProfile(){
        let get_OutputDateStr = dobTextField.text ?? ""
        //            let get_OutputDateStr = self.convertDateFormatDynamic(inputDate: dobTextField.text ?? "")
        updateProfileViewModel.updatePostRequest(getviewController: self, getPersonName: nameTexField.text ?? "", getEmail: emailTextField.text ?? "", getMobilecode: countryTextField.text ?? "", getMobilenumber: phoneTextField.text ?? "", getStreetname: "", getCountry: countryTextField.text ?? "", getCity: "", getState: "", getPincode: postalTextField.text ?? "", getGender: "", getEmergencycontact: econtactTextField.text ?? "", getMaritalstatus: maritalStatusTextField.text ?? "", getDob: get_OutputDateStr,getHeight: heightTextField.text ?? "",getWeight: weightTextField.text ?? "",getBloodgroup: bloodGroupTextField.text ?? "", getNricid: nricTextField.text ?? "")
//        updateProfileViewModel.getProfileGetRequest(getviewController: self)
//        self.navigationController?.popToViewController(ofClass: SettingsViewController.self)
        loadSetup()        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        self.textFieldValidation()
        if textField == phoneTextField ||  textField == econtactTextField {
            return range.location < 10
        }else if textField == nricTextField{
            return range.location < 9
        }
        return true
    }
    
    func textFieldValidation(){
        if nameTexField.text == ""{
            nameTexField.attributedPlaceholder = NSAttributedString(string:"Enter your name", attributes:[NSAttributedString.Key.foregroundColor: UIColor.textFieldSelectColor])
            nameLine.backgroundColor = UIColor.textFieldSelectColor
            nameTexField.textColor =  UIColor.textFieldSelectColor
//            nameImageview.setImageColor(color: UIColor.textFieldSelectColor)
        }else{
//            nameImageview.setImageColor(color: UIColor.themeColor)
            nameLine.backgroundColor = UIColor.textFieldUnSelectColor
            nameTexField.textColor =  UIColor.blackColor
        }
        if emailTextField.text == "" || "\(emailTextField.text ?? "")".isValidEmail() == false{
            emailTextField.attributedPlaceholder = NSAttributedString(string:"Enter your email", attributes:[NSAttributedString.Key.foregroundColor: UIColor.textFieldSelectColor])
            emailLine.backgroundColor = UIColor.textFieldSelectColor
            emailTextField.textColor =  UIColor.textFieldSelectColor
//            emailImageview.setImageColor(color: UIColor.textFieldSelectColor)
        }else{
            emailTextField.textColor = .white
//            emailImageview.setImageColor(color: UIColor.textFieldUnSelectImageviewColor)
            emailLine.backgroundColor = UIColor.textFieldUnSelectColor
            emailTextField.textColor =  UIColor.blackColor
        }
        if phoneTextField.text == "" || "\(phoneTextField.text ?? "")".isValidContact != true{
            phoneTextField.attributedPlaceholder = NSAttributedString(string:"Enter your phone number", attributes:[NSAttributedString.Key.foregroundColor: UIColor.textFieldSelectColor])
            phoneLine.backgroundColor = UIColor.textFieldSelectColor
            phoneTextField.textColor =  UIColor.textFieldSelectColor
//            phoneImageview.setImageColor(color: UIColor.textFieldSelectColor)
        }else{
//            phoneImageview.setImageColor(color: UIColor.textFieldUnSelectImageviewColor)
            phoneLine.backgroundColor = UIColor.textFieldUnSelectColor
            phoneTextField.textColor =  UIColor.blackColor
        }
        if nricTextField.text == ""{
            nricTextField.attributedPlaceholder = NSAttributedString(string:"Enter your NRIC number", attributes:[NSAttributedString.Key.foregroundColor: UIColor.textFieldSelectColor])
            nricLine.backgroundColor = UIColor.textFieldSelectColor
            nricTextField.textColor =  UIColor.textFieldSelectColor
//            nricImageview.setImageColor(color: UIColor.textFieldSelectColor)
        }else{
//            nricImageview.setImageColor(color: UIColor.textFieldUnSelectImageviewColor)
            nricLine.backgroundColor = UIColor.textFieldUnSelectColor
            nricTextField.textColor =  UIColor.blackColor
        }
        /*if passwordTextField.text == ""{
         passwordTextField.attributedPlaceholder = NSAttributedString(string:"Enter your password", attributes:[NSAttributedString.Key.foregroundColor: UIColor.textFieldSelectColor])
         passwordLine.backgroundColor = UIColor.textFieldSelectColor
         passwordTextField.textColor =  UIColor.textFieldSelectColor
         passwordImageview.setImageColor(color: UIColor.textFieldSelectColor)
         }else{
         passwordImageview.setImageColor(color: UIColor.textFieldUnSelectImageviewColor)
         passwordLine.backgroundColor = UIColor.textFieldUnSelectColor
         passwordTextField.textColor =  UIColor.blackColor
         }*/
        if dobTextField.text == ""{
            dobTextField.attributedPlaceholder = NSAttributedString(string:"Enter your DOB", attributes:[NSAttributedString.Key.foregroundColor: UIColor.textFieldSelectColor])
            dobLine.backgroundColor = UIColor.textFieldSelectColor
            dobTextField.textColor =  UIColor.textFieldSelectColor
//            dobImageview.setImageColor(color: UIColor.textFieldSelectColor)
        }else{
//            dobImageview.setImageColor(color: UIColor.textFieldUnSelectImageviewColor)
            dobLine.backgroundColor = UIColor.textFieldUnSelectColor
            dobTextField.textColor =  UIColor.blackColor
        }
        if countryTextField.text == ""{
            countryTextField.attributedPlaceholder = NSAttributedString(string:"Enter your country", attributes:[NSAttributedString.Key.foregroundColor: UIColor.textFieldSelectColor])
            countryLine.backgroundColor = UIColor.textFieldSelectColor
            countryTextField.textColor =  UIColor.textFieldSelectColor
//            countryImageview.setImageColor(color: UIColor.textFieldSelectColor)
        }else{
//            countryImageview.setImageColor(color: UIColor.textFieldUnSelectImageviewColor)
            countryLine.backgroundColor = UIColor.textFieldUnSelectColor
            countryTextField.textColor =  UIColor.blackColor
        }
        if postalTextField.text == ""{
            postalTextField.attributedPlaceholder = NSAttributedString(string:"Enter your postal code", attributes:[NSAttributedString.Key.foregroundColor: UIColor.textFieldSelectColor])
            postalLine.backgroundColor = UIColor.textFieldSelectColor
            postalTextField.textColor =  UIColor.textFieldSelectColor
//            postalImageview.setImageColor(color: UIColor.textFieldSelectColor)
        }else{
//            postalImageview.setImageColor(color: UIColor.textFieldUnSelectImageviewColor)
            postalLine.backgroundColor = UIColor.textFieldUnSelectColor
            postalTextField.textColor =  UIColor.blackColor
        }
        if bloodGroupTextField.text == ""{
            bloodGroupTextField.attributedPlaceholder = NSAttributedString(string:"Enter your blood group", attributes:[NSAttributedString.Key.foregroundColor: UIColor.textFieldSelectColor])
            bloodLine.backgroundColor = UIColor.textFieldSelectColor
            bloodGroupTextField.textColor =  UIColor.textFieldSelectColor
//            bloodImageview.setImageColor(color: UIColor.textFieldSelectColor)
        }else{
//            bloodImageview.setImageColor(color: UIColor.textFieldUnSelectImageviewColor)
            bloodLine.backgroundColor = UIColor.textFieldUnSelectColor
            bloodGroupTextField.textColor =  UIColor.blackColor
        }
        if maritalStatusTextField.text == ""{
            maritalStatusTextField.attributedPlaceholder = NSAttributedString(string:"Enter your marital status", attributes:[NSAttributedString.Key.foregroundColor: UIColor.textFieldSelectColor])
            maritalLine.backgroundColor = UIColor.textFieldSelectColor
            maritalStatusTextField.textColor =  UIColor.textFieldSelectColor
//            maritalImageview.setImageColor(color: UIColor.textFieldSelectColor)
        }else{
//            maritalImageview.setImageColor(color: UIColor.textFieldUnSelectImageviewColor)
            maritalLine.backgroundColor = UIColor.textFieldUnSelectColor
            maritalStatusTextField.textColor =  UIColor.blackColor
        }
        if heightTextField.text == ""{
            heightTextField.attributedPlaceholder = NSAttributedString(string:"Enter your height", attributes:[NSAttributedString.Key.foregroundColor: UIColor.textFieldSelectColor])
            heightLine.backgroundColor = UIColor.textFieldSelectColor
            heightTextField.textColor =  UIColor.textFieldSelectColor
//            heightImageview.setImageColor(color: UIColor.textFieldSelectColor)
        }else{
//            heightImageview.setImageColor(color: UIColor.textFieldUnSelectImageviewColor)
            heightLine.backgroundColor = UIColor.textFieldUnSelectColor
            heightTextField.textColor =  UIColor.blackColor
        }
        if weightTextField.text == ""{
            weightTextField.attributedPlaceholder = NSAttributedString(string:"Enter your weight", attributes:[NSAttributedString.Key.foregroundColor: UIColor.textFieldSelectColor])
            weightLine.backgroundColor = UIColor.textFieldSelectColor
            weightTextField.textColor =  UIColor.textFieldSelectColor
//            weightImageview.setImageColor(color: UIColor.textFieldSelectColor)
        }else{
//            weightImageview.setImageColor(color: UIColor.textFieldUnSelectImageviewColor)
            weightLine.backgroundColor = UIColor.textFieldUnSelectColor
            weightTextField.textColor =  UIColor.blackColor
        }
        if econtactTextField.text == ""{
            econtactTextField.attributedPlaceholder = NSAttributedString(string:"Enter your emergency contact", attributes:[NSAttributedString.Key.foregroundColor: UIColor.textFieldSelectColor])
            econtactLine.backgroundColor = UIColor.textFieldSelectColor
            econtactTextField.textColor =  UIColor.textFieldSelectColor
//            econtactImageview.setImageColor(color: UIColor.textFieldSelectColor)
        }else{
//            econtactImageview.setImageColor(color: UIColor.textFieldUnSelectImageviewColor)
            econtactLine.backgroundColor = UIColor.textFieldUnSelectColor
            econtactTextField.textColor =  UIColor.blackColor
        }
    }
}

@available(iOS 13.0, *)
extension ProfileSetttingViewController:  UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //return countryCodeArray.count
        return self.countryCode.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            if ( UIDevice.current.model.range(of: "iPad") != nil){
                pickerLabel?.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(10.0))
            } else {
                pickerLabel?.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(20.0))
            }
            pickerLabel?.textAlignment = .center
            pickerLabel?.text = countryCode[row].name
            pickerLabel?.textColor = UIColor.white
        }
        return pickerLabel!
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.countryTextField.text = "\(self.countryCode[row].name ?? "")"
    }
}


extension ProfileSetttingViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModelMedical.symptoms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MedicalCell", for: indexPath) as? MedicalCell
        cell?.medicalModel = viewModelMedical.symptoms[indexPath.row]
        return cell!
    }
}


extension ProfileSetttingViewController :UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let orinalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        //for image rotration
        let image =  orinalImage.upOrientationImage()
        profileImageView.image = image
    }
}


