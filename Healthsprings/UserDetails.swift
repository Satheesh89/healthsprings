//
//  UserDetails.swift
//  Healthsprings
//

import Foundation

struct UserDetails{
    static var shared: UserDetails = UserDetails()

    func setUserLoginData(data: Data){
        UserDefaults.standard.set(data, forKey: "user_details")
        UserDefaults.standard.synchronize()
    }
    
    //MARK:- Get Login Details
    var userLoginData: AuthModel? {
        if let loginData = UserDefaults.standard.data(forKey: "user_details") {
            do {
                let loginDetail = try JSONDecoder().decode(AuthModel.self, from: loginData)
                return loginDetail
            } catch _ {
                return nil
            }
        } else {
            return nil
        }
    }
    
    //MARK:- Get UserID
    var getProfileuuid: String? {
        userLoginData?.success?.user_details?.uuid
    }

    //MARK:- Get UserID
    var userId: Int? {
        userLoginData?.success?.user_details?.id
    }
    
    //MARK:- Get Email
    var emailId: String? {
        userLoginData?.success?.user_details?.patient_email
    }
    
    //MARK:- Get Username
    var userName: String? {
        userLoginData?.success?.user_details?.patient_name
    }
    
    //MARK:- Get Mobile No
    var phoneNumber: String? {
        userLoginData?.success?.user_details?.mobile_no
    }
    
    //MARK:- Get Date of birth
    var dateofbirth: String? {
        userLoginData?.success?.user_details?.date_of_birth
    }
    
    
    //MARK:- Get Blood Group
    var bloodGroup: String? {
        userLoginData?.success?.user_details?.blood_group
    }
    
    
    //MARK:- Get country
    var country: String? {
        userLoginData?.success?.user_details?.country
    }
    
    
    //MARK:- Get pincode
    var pincode: String? {
        userLoginData?.success?.user_details?.pin_code
    }
    
    //MARK:- Get maritalstatus
    var maritalstatus: String? {
        userLoginData?.success?.user_details?.marital_status
    }
    
    //MARK:- Get height
    var height: String? {
        userLoginData?.success?.user_details?.height
    }
    
    //MARK:- Get weight
    var weight: String? {
        userLoginData?.success?.user_details?.weight
    }
    
    //MARK:- Get emergencycontact
    var emergencycontact: String? {
        userLoginData?.success?.user_details?.emergency_contact
    }
    
    //MARK:- Get emergencycontact
    var profile_image: String? {
        userLoginData?.success?.user_details?.profile_image
    }
    
    
    //MARK:- Get street_name
    var getstreet_name: String? {
        userLoginData?.success?.user_details?.street_name
    }
    
    
    //MARK:- Get area_location
    var getarea_location: String? {
        userLoginData?.success?.user_details?.area_location
    }

    //MARK:- Get city
    var getcity: String? {
        userLoginData?.success?.user_details?.city
    }
    

    //MARK:- Get full address
    var getFullAddress: String? {
        userLoginData?.success?.user_details?.full_address
    }
    
    func setUserApiToken(apiTK: String){
        UserDefaults.standard.set(apiTK, forKey: "api_Token")
        UserDefaults.standard.synchronize()
    }

    func getApiToken() -> String {
        return "\(UserDefaults.standard.object(forKey: "api_Token") ?? "")"
    }
    
    var isLoggedIn: Bool {
        userId != nil
    }
    
    mutating func logout() {
        UserDefaults.standard.removeObject(forKey: "user_details")
        clearLocalContent()
    }
    
    func clearLocalContent() {
        let fileManager = FileManager.default
        let myDocuments = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        guard let filePaths = try? fileManager.contentsOfDirectory(at: myDocuments, includingPropertiesForKeys: nil, options: []) else { return }
        print(filePaths)
        for filePath in filePaths {
            try? fileManager.removeItem(at: filePath)
        }
    }
    
    func clearLocalContent(filePath: URL) {
        if filePath.absoluteString.isEmpty == false {
            let fileManager = FileManager.default
            try? fileManager.removeItem(at: filePath)
        }
    }
}
