//
//  GMSignUpViewController.swift
//  Healthsprings
//
//  Created by Personal on 22/06/22.
//

import UIKit
import SwiftyJSON

class GMSignUpViewController: UIViewController {

    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var mobileView: UIView!
    @IBOutlet weak var mobileNoTextField: UITextField!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var signButton: UIButton!
    @IBOutlet weak var signView: UIView!
    
    var passwordiconClick = true
    var confirmPasswordiconClick = true
    
    let countrycodepicker = UIPickerView()
    var countryCodeArray = NSArray()

    var countryModel: CountryModel!
    var countryCode: [Code] = []
    
    var getGmailStr : String = ""
    var getfirstnameStr : String = ""
    var getsocialmediatokenStr : String = ""


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initializeHideKeyboard()
        self.countryCodeTextField.delegate = self
        self.countryCodeTextField.inputView = self.countrycodepicker
        self.countryCodeTextField.tintColor = .clear
        self.countrycodepicker.dataSource = self
        self.countrycodepicker.delegate = self
        self.countrycodepicker.backgroundColor = .themeColor
        emailView.dropShadowWithCornerRaduis()
        nameView.dropShadowWithCornerRaduis()
        mobileView.dropShadowWithCornerRaduis()
        passwordView.dropShadowWithCornerRaduis()
        confirmPasswordView.dropShadowWithCornerRaduis()
        emailTextField.text = getGmailStr
        nameTextField.text = getfirstnameStr
        loadJsonData()
        
        signView.backgroundColor = .themeColor        
//        if "\(self.emailTextField.text ?? "")" != "" && "\(self.nameTextField.text ?? "")" != "" {
//            signButton.tintColor = .red
//            signButton.titleLabel?.textColor = .red
//            signButton.backgroundColor = .themeColor
//        }
//
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        self.navigationItem.hidesBackButton = true
    }
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - SIgnup Api Call
    func signUpApiCall(){
        var paramsDictionary = [String:Any]()
        paramsDictionary["email"] = self.emailTextField.text ?? ""
        paramsDictionary["first_name"] = self.nameTextField.text ?? ""
        paramsDictionary["social_media_token"] = getsocialmediatokenStr
        paramsDictionary["device_token"] = UserDefaults.standard.object(forKey: Key.UserDefaults.deviceToken) ?? "1234"
        paramsDictionary["device_type"] = Key.UserDefaults.devicetype
        paramsDictionary["login_type"] = Key.UserDefaults.logintypeGM
     
        HttpClientApi.instance().makeAPICall(url: URL.socialsignup, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let response = response
                if  response?.statusCode == 200 {
                    let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                    if let responseJSON = responseJSON as? NSDictionary{
//                        UserDefaults.standard.removeObject(forKey: Key.UserDefaults.userDetails)
//                        UserInformation().saveUserData(userDateDict: responseJSON )
                        UserDetails.shared.setUserLoginData(data: try! JSONEncoder().encode(data))

                    }
                    self.setTabPage()
                }else  if response?.statusCode == 404 {
                    let dataDict = json as! NSDictionary
                    print(dataDict.object(forKey:"error") as? String ?? "")
                    DispatchQueue.main.async {
                        self.showToast(message: dataDict.object(forKey:"error") as? String ?? "")
                    }
                }else  if response?.statusCode == 401 {
                    DispatchQueue.main.async {
                        let dataDict = json as! NSDictionary
                        self.showToast(message: dataDict.object(forKey:"error") as? String ?? "")
                    }
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
            DispatchQueue.main.async {
                self.showToast(message: "API call Failure")
            }
        })
    }
    
    func loadJsonData()  {
        if let localData = self.readLocalFile(forName: "Country") {
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: localData, options: .mutableContainers)
                self.countryModel = CountryModel(JSON(jsonResponse))
                self.countryCode = self.countryModel?.countryDetail?.code ?? []
                print(self.countryCode.count)
            }catch let error{
                print(error)
            }
        }
    }
    
    private func readLocalFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name,
                                                 ofType: "json"),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        return nil
    }

}

// MARK: - Action
extension GMSignUpViewController{
    @IBAction func didTapiconAction(sender: UIButton) {
        if sender.tag == 100{
            if(passwordiconClick == true) {
                passwordTextField.isSecureTextEntry = false
            } else {
                passwordTextField.isSecureTextEntry = true
            }
            passwordiconClick = !passwordiconClick
        }else if sender.tag == 101{
            if(confirmPasswordiconClick == true) {
                confirmPasswordTextField.isSecureTextEntry = false
            } else {
                confirmPasswordTextField.isSecureTextEntry = true
            }
            confirmPasswordiconClick = !confirmPasswordiconClick
        }
    }
    
    @IBAction func didTapSignupAction(sender: UIButton) {
        signUpStatus()
    }
    
    func signUpStatus(){
        if "\(self.emailTextField.text ?? "")" == ""{
            self.showToast(message: "please enter your email")
        }else  if !isValidEmail(testStr: "\(self.emailTextField.text ?? "")"){
            self.showToast(message: "Invalid Email Address")
        }else if self.nameTextField.text == "" {
            self.showToast(message: "please enter your name")
        }else{
            self.signUpApiCall()
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
            print("validate emilId: \(testStr)")
            let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
            return result
        }

    
    // MARK: - Tabbar Action
    func setTabPage(){
        DispatchQueue.main.async {
            let tabStoryboard = UIStoryboard(name: "Tabbar", bundle: nil)
            let Permissionvc = tabStoryboard.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
            UIApplication.shared.windows.first?.rootViewController = Permissionvc
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }

    // MARK: - Button Status
    func buttonStatus(){
        if "\(emailTextField.text ?? "")".isValidEmail() {
            signView.backgroundColor = .themeColor
            signButton.tintColor = .white
            signButton.titleLabel?.textColor = .white
        }else{
            signView.backgroundColor = .loginGeryColor
            signButton.tintColor = .loginLightGeryColor
            signButton.titleLabel?.textColor = .loginLightGeryColor
        }
    }

}

extension GMSignUpViewController:  UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.countryCode.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(20.0))
            pickerLabel?.textAlignment = .center
        }
        pickerLabel?.text = countryCode[row].dialCode
        pickerLabel?.textColor = UIColor.white
        return pickerLabel!
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.countryLabel.text = "\(self.countryCode[row].dialCode ?? "")"
    }
}
