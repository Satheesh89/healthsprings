//
//  FontManager.swift
//


import Foundation
import UIKit

//Font Names = [["Lato-Regular", "Lato-Italic", "Lato-Hairline", "Lato-HairlineItalic", "Lato-Light", "Lato-LightItalic", "Lato-Bold", "Lato-BoldItalic", "Lato-Black", "Lato-BlackItalic"]]

struct Font {
    enum FontName : String {
        case Latobold = "Lato-Bold"
        case Latoregular = "Lato-Regular"
        case Latolight = "Lato-Light"
        case LatoMedium = "Lato-Medium"
        case LatoBlack = "Lato-Black"
        case PTSansBold = "PTSans-Bold"
    }
}

class Utility {
    class func logAllAvailableFonts() {
        for family in UIFont.familyNames {
            print("\(family)")
            for name in UIFont.fontNames(forFamilyName: family) {
                print("   \(name)")
            }
        }
    }    
    class func dynamicSize(_ Size : CGFloat) -> CGFloat {
        let screenWidth = UIScreen.main.bounds.size.width
        let calculatedFontSize = screenWidth / 414 * Size   // 414 -> 8 plus points
        return calculatedFontSize
    }
}
