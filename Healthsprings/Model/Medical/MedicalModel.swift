//
//  MedicalModel.swift
//  Healthsprings
//


import Foundation
import SwiftyJSON

struct MedicalModel {
    let medicalDetail: MedicalDetail?
    init(_ json: JSON) {
        medicalDetail = MedicalDetail(json["Medical_detail"])
    }
}

struct MedicalDetail {
    let symptoms: [Symptoms]?
    init(_ json: JSON) {
        symptoms = json["Symptoms"].arrayValue.map { Symptoms($0) }
    }
}

struct Symptoms {
    let name: String?
    let details: String?
    init(_ json: JSON) {
        name = json["name"].stringValue
        details = json["details"].stringValue
    }
}
