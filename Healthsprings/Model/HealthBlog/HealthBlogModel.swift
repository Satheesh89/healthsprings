//
//  HealthBlogModel.swift
//

import Foundation
import SwiftyJSON

struct HealthBlogModel {
    let covid19url: String?
    let readmoreblogs: String?
	let upcomingAppointmentCount: Int?
	let healthBlog: [HealthBlog]?
	init(_ json: JSON) {
		upcomingAppointmentCount = json["upcoming_appointment_count"].intValue
        covid19url = json["covid19_url"].stringValue
        readmoreblogs = json["read_more_blogs"].stringValue
        healthBlog = json["health_blog"].arrayValue.map { HealthBlog($0) }
	}
}
