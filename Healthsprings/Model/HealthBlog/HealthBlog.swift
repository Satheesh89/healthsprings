//
//  HealthBlog.swift
//

import Foundation
import SwiftyJSON

struct HealthBlog {
	let id: Int?
	let title: String?
	let icon: String?
	let description: String?
	let url: String?
	init(_ json: JSON) {
		id = json["id"].intValue
		title = json["title"].stringValue
		icon = json["icon"].stringValue
		description = json["description"].stringValue
		url = json["url"].stringValue
	}
}
