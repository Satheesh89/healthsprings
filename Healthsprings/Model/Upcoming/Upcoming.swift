//
//  Upcoming.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 21, 2022
//
import Foundation
import SwiftyJSON

class Upcoming {

	let upcomingDetails: [UpcomingDetails]?

	init(_ json: JSON) {
		upcomingDetails = json["upcoming_details"].arrayValue.map { UpcomingDetails($0) }
	}

}