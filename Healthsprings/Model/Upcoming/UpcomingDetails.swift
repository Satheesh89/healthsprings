//
//  UpcomingDetails.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 21, 2022
//
import Foundation
import SwiftyJSON

class UpcomingDetails {
    
    let patientId: Int?
    let patientName: String?
    let profileId: String?
    let avatar: String?
    let doctorName: String?
    let doctorEmail: String?
    let doctorDepartment: String?
    let doctorPhone: Any?
    let doctorCountry: Any?
    let doctorGender: Any?
    let doctorAvaliabilityStatus: Any?
    let doctorDescription: Any?
    let doctorState: Any?
    let appointmentId: Int?
    let appointmentToTime: String?
    let appointmentFromTime: String?
    let appointmentStatus: String?
    let appointmentRescheduleStatus: Any?
    
    let video_consultation_available: Int?
    
    init(_ json: JSON) {
        patientId = json["patient_id"].intValue
        patientName = json["patient_name"].stringValue
        profileId = json["profile_id"].stringValue
        avatar = json["avatar"].stringValue
        doctorName = json["doctor_name"].stringValue
        doctorEmail = json["doctor_email"].stringValue
        doctorDepartment = json["doctor_department"].stringValue
        doctorPhone = json["doctor_phone"]
        doctorCountry = json["doctor_country"]
        doctorGender = json["doctor_gender"]
        doctorAvaliabilityStatus = json["doctor_avaliability_status"]
        doctorDescription = json["doctor_description"]
        doctorState = json["doctor_state"]
        appointmentId = json["appointment_id"].intValue
        appointmentToTime = json["appointment_to_time"].stringValue
        appointmentFromTime = json["appointment_from_time"].stringValue
        appointmentStatus = json["appointment_status"].stringValue
        appointmentRescheduleStatus = json["appointment_reschedule_status"]
        video_consultation_available = json["video_consultation_available"].intValue
    }
    
    
}
