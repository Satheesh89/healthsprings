//
//  UpcomingModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 21, 2022
//
import Foundation
import SwiftyJSON

class UpcomingModel {
	let upcoming: Upcoming?
	init(_ json: JSON) {
        upcoming = Upcoming(json["Upcoming"])
	}
}
