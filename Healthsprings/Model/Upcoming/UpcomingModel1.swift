////
////  UpcomingModel.swift
////  Healthsprings
////
////  Created by Personal on 30/06/22.
////
//
//import Foundation
//import SwiftyJSON
//
//struct UpcomingModel : Codable {
//    let upcoming : Upcoming?
//    enum CodingKeys: String, CodingKey {
//        case upcoming = "Upcoming"
//    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        upcoming = try values.decodeIfPresent(Upcoming.self, forKey: .upcoming)
//    }
//}
//
//struct Upcoming_details : Codable {
//    let patient_name : String?
//    let doctor_name : String?
//    let doctor_email : String?
//    let doctor_department : String?
//    let doctor_phone : String?
//    let doctor_country : String?
//    let doctor_gender : String?
//    let doctor_avaliability_status : String?
//    let doctor_description : String?
//    let doctor_state : String?
//    let appointment_to_time : String?
//    let appointment_from_time : String?
//    let appointment_status : String?
//    let appointment_reschedule_status : String?
//
//    enum CodingKeys: String, CodingKey {
//        case patient_name = "patient_name"
//        case doctor_name = "doctor_name"
//        case doctor_email = "doctor_email"
//        case doctor_department = "doctor_department"
//        case doctor_phone = "doctor_phone"
//        case doctor_country = "doctor_country"
//        case doctor_gender = "doctor_gender"
//        case doctor_avaliability_status = "doctor_avaliability_status"
//        case doctor_description = "doctor_description"
//        case doctor_state = "doctor_state"
//        case appointment_to_time = "appointment_to_time"
//        case appointment_from_time = "appointment_from_time"
//        case appointment_status = "appointment_status"
//        case appointment_reschedule_status = "appointment_reschedule_status"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        patient_name = try values.decodeIfPresent(String.self, forKey: .patient_name)
//        doctor_name = try values.decodeIfPresent(String.self, forKey: .doctor_name)
//        doctor_email = try values.decodeIfPresent(String.self, forKey: .doctor_email)
//        doctor_department = try values.decodeIfPresent(String.self, forKey: .doctor_department)
//        doctor_phone = try values.decodeIfPresent(String.self, forKey: .doctor_phone)
//        doctor_country = try values.decodeIfPresent(String.self, forKey: .doctor_country)
//        doctor_gender = try values.decodeIfPresent(String.self, forKey: .doctor_gender)
//        doctor_avaliability_status = try values.decodeIfPresent(String.self, forKey: .doctor_avaliability_status)
//        doctor_description = try values.decodeIfPresent(String.self, forKey: .doctor_description)
//        doctor_state = try values.decodeIfPresent(String.self, forKey: .doctor_state)
//        appointment_to_time = try values.decodeIfPresent(String.self, forKey: .appointment_to_time)
//        appointment_from_time = try values.decodeIfPresent(String.self, forKey: .appointment_from_time)
//        appointment_status = try values.decodeIfPresent(String.self, forKey: .appointment_status)
//        appointment_reschedule_status = try values.decodeIfPresent(String.self, forKey: .appointment_reschedule_status)
//    }
//}
//
//struct Upcoming : Codable {
//    let upcoming_details : [Upcoming_details]?
//    enum CodingKeys: String, CodingKey {
//        case upcoming_details = "upcoming_details"
//    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        upcoming_details = try values.decodeIfPresent([Upcoming_details].self, forKey: .upcoming_details)
//    }
//}
