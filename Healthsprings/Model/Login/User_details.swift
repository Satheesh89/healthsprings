/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct User_details : Codable {
	let id : Int?
	let doctor_id : String?
	let patientid : String?
	let clinical_id : String?
	let patient_name : String?
	let patient_lastname : String?
	let patient_email : String?
	let gender : String?
	let mobile_no : String?
	let patient_complent : String?
	let date_of_birth : String?
	let blood_group : String?
	let blood_pressure : String?
	let profile_image : String?
	let patient_plan_id : String?
	let pulse_rate : String?
	let patient_profile_id : String?
	let health_reward : String?
	let health_score : String?
	let health_grade : String?
    let country : String?
    let pin_code : String?
    let marital_status : String?
    let height : String?
    let weight : String?
    let emergency_contact : String?
    let uuid : String?
    let street_name : String?
    let area_location : String?
    let city : String?
    let state : String?
    let full_address : String?
    
	enum CodingKeys: String, CodingKey {
		case id = "id"
		case doctor_id = "doctor_id"
		case patientid = "patientid"
		case clinical_id = "clinical_id"
		case patient_name = "patient_name"
		case patient_lastname = "patient_lastname"
		case patient_email = "patient_email"
		case gender = "gender"
		case mobile_no = "mobile_no"
		case patient_complent = "patient_complent"
		case date_of_birth = "date_of_birth"
		case blood_group = "blood_group"
		case blood_pressure = "blood_pressure"
		case profile_image = "profile_image"
		case patient_plan_id = "patient_plan_id"
		case pulse_rate = "pulse_rate"
		case patient_profile_id = "patient_profile_id"
		case health_reward = "health_reward"
		case health_score = "health_score"
		case health_grade = "health_grade"
        case country = "country"
        case pin_code = "pin_code"
        case marital_status = "marital_status"
        case height = "height"
        case weight = "weight"
        case emergency_contact = "emergency_contact"
        case uuid = "uuid"

        case street_name = "street_name"
        case area_location = "area_location"
        case city = "city"
        case state = "state"
        case full_address = "full_address"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		doctor_id = try values.decodeIfPresent(String.self, forKey: .doctor_id)
		patientid = try values.decodeIfPresent(String.self, forKey: .patientid)
		clinical_id = try values.decodeIfPresent(String.self, forKey: .clinical_id)
		patient_name = try values.decodeIfPresent(String.self, forKey: .patient_name)
		patient_lastname = try values.decodeIfPresent(String.self, forKey: .patient_lastname)
		patient_email = try values.decodeIfPresent(String.self, forKey: .patient_email)
		gender = try values.decodeIfPresent(String.self, forKey: .gender)
		mobile_no = try values.decodeIfPresent(String.self, forKey: .mobile_no)
		patient_complent = try values.decodeIfPresent(String.self, forKey: .patient_complent)
		date_of_birth = try values.decodeIfPresent(String.self, forKey: .date_of_birth)
		blood_group = try values.decodeIfPresent(String.self, forKey: .blood_group)
		blood_pressure = try values.decodeIfPresent(String.self, forKey: .blood_pressure)
		profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
		patient_plan_id = try values.decodeIfPresent(String.self, forKey: .patient_plan_id)
		pulse_rate = try values.decodeIfPresent(String.self, forKey: .pulse_rate)
		patient_profile_id = try values.decodeIfPresent(String.self, forKey: .patient_profile_id)
		health_reward = try values.decodeIfPresent(String.self, forKey: .health_reward)
		health_score = try values.decodeIfPresent(String.self, forKey: .health_score)
		health_grade = try values.decodeIfPresent(String.self, forKey: .health_grade)
        
        country = try values.decodeIfPresent(String.self, forKey: .country)
        pin_code = try values.decodeIfPresent(String.self, forKey: .pin_code)
        marital_status = try values.decodeIfPresent(String.self, forKey: .marital_status)
        height = try values.decodeIfPresent(String.self, forKey: .height)
        weight = try values.decodeIfPresent(String.self, forKey: .weight)
        emergency_contact = try values.decodeIfPresent(String.self, forKey: .emergency_contact)
        uuid = try values.decodeIfPresent(String.self, forKey: .uuid)
        street_name = try values.decodeIfPresent(String.self, forKey: .street_name)
        area_location = try values.decodeIfPresent(String.self, forKey: .area_location)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        full_address = try values.decodeIfPresent(String.self, forKey: .full_address)
	}
}
