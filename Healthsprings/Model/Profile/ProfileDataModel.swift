//
//  ProfileDataModel.swift
//
import Foundation
import SwiftyJSON
class ProfileDataModel {
	let profileData: ProfileData?
	init(_ json: JSON) {
		profileData = ProfileData(json["profileData"])
	}
}
