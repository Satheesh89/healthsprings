//
//  ProfileData.swift
//

import Foundation
import SwiftyJSON
class ProfileData {
	let id: Int?
	let password: String?
	let doctorId: String?
	let patientid: String?
	let clinicalId: String?
	let patientName: String?
	let patientLastname: String?
	let patientEmail: String?
	let gender: String?
	let mobileNo: String?
	let patientComplent: String?
	let dateOfBirth: String?
	let bloodGroup: String?
	let bloodPressure: String?
	let profileImage: String?
	let patientPlanId: String?
	let pulseRate: String?
	let patientProfileId: String?
	let healthReward: String?
	let healthScore: String?
	let healthGrade: String?
	let country: String?
	let pinCode: String?
	let maritalStatus: String?
	let height: String?
	let weight: String?
	let emergencyContact: String?
    let nric_id: String?
    let profile_id: String?
    
	init(_ json: JSON) {
		id = json["id"].intValue
		password = json["password"].stringValue
		doctorId = json["doctor_id"].stringValue
		patientid = json["patientid"].stringValue
		clinicalId = json["clinical_id"].stringValue
		patientName = json["patient_name"].stringValue
		patientLastname = json["patient_lastname"].stringValue
		patientEmail = json["patient_email"].stringValue
		gender = json["gender"].stringValue
		mobileNo = json["mobile_no"].stringValue
		patientComplent = json["patient_complent"].stringValue
		dateOfBirth = json["dob"].stringValue
		bloodGroup = json["blood_group"].stringValue
		bloodPressure = json["blood_pressure"].stringValue
		profileImage = json["profile_image"].stringValue
		patientPlanId = json["patient_plan_id"].stringValue
		pulseRate = json["pulse_rate"].stringValue
		patientProfileId = json["patient_profile_id"].stringValue
		healthReward = json["health_reward"].stringValue
		healthScore = json["health_score"].stringValue
		healthGrade = json["health_grade"].stringValue
		country = json["country"].stringValue
		pinCode = json["pin_code"].stringValue
		maritalStatus = json["marital_status"].stringValue
		height = json["height"].stringValue
		weight = json["weight"].stringValue
		emergencyContact = json["emergency_contact"].stringValue
        nric_id = json["nric_id"].stringValue
        profile_id = json["profile_id"].stringValue
	}
}
