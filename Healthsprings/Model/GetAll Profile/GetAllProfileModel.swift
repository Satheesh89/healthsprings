//
//  GetAllProfileModel.swift
//

import Foundation
import SwiftyJSON

struct GetAllProfileModel {

    let profileData: [GetProfileData]?

    init(_ json: JSON) {
        profileData = json["profileData"].arrayValue.map { GetProfileData($0) }
    }

}
