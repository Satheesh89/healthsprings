//
//  GetProfileData.swift
//

import Foundation
import SwiftyJSON

struct GetProfileData {

    let profileId: String?
    let name: String?
    let email: String?
    let profilePerson: String?

    init(_ json: JSON) {
        profileId = json["profile_id"].stringValue
        name = json["name"].stringValue
        email = json["email"].stringValue
        profilePerson = json["profile_person"].stringValue
    }

}
