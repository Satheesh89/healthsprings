//
//  RootClass.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 21, 2022
//
import Foundation
import SwiftyJSON

class AppointmentDetailsModel {
	let appointmentDetails: AppointmentDetails?
	init(_ json: JSON) {
		appointmentDetails = AppointmentDetails(json["appointment_details"])
	}
}
