//
//  AppointmentDetails.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 21, 2022
//
import Foundation
import SwiftyJSON

class AppointmentDetails {

	let doctorName: String?
	let speciality: String?
	let appointmentDate: String?
	let appointmentTime: String?
	let status: String?
	let patientName: String?
	let symptom: String?
	let patientLocation: String?
	let totalPaid: String?
	let paymentDate: String?
	let cardNo: Int?
    let doctor_id: Int?
    let uuid: String?
    let fulladdress: String?
    let video_consultation_available: Int?
    
    let consultation_fee: Int?
    let currency: String?
    
	init(_ json: JSON) {
		doctorName = json["doctor_name"].stringValue
		speciality = json["speciality"].stringValue
		appointmentDate = json["appointment_date"].stringValue
		appointmentTime = json["appointment_time"].stringValue
		status = json["status"].stringValue
		patientName = json["patient_name"].stringValue
		symptom = json["symptom"].stringValue
		patientLocation = json["patient_location"].stringValue
		totalPaid = json["total_paid"].stringValue
		paymentDate = json["payment_date"].stringValue
		cardNo = json["card_no"].intValue
        doctor_id = json["doctor_id"].intValue
        uuid = json["uuid"].stringValue
        fulladdress = json["full_address"].stringValue
        video_consultation_available = json["video_consultation_available"].intValue
        consultation_fee = json["consultation_fee"].intValue
        currency = json["currency"].stringValue
	}
}
