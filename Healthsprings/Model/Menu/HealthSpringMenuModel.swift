//
//  HealthSpringMenuModel.swift
//  Healthsprings
//
//  Created by Personal on 15/06/22.
//

import Foundation

class HealthSpringMenuModel: NSObject {
    
    let title : String
    
    init(title:String) {
        self.title = title
    }
    
}
