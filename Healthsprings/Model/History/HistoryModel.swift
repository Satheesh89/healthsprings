import Foundation

struct HistoryModel : Codable {
	let history : History?
	enum CodingKeys: String, CodingKey {
		case history = "history"
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		history = try values.decodeIfPresent(History.self, forKey: .history)
	}
}

struct History : Codable {
    let history_details : [History_details]?
    enum CodingKeys: String, CodingKey {
        case history_details = "history_details"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        history_details = try values.decodeIfPresent([History_details].self, forKey: .history_details)
    }
}

struct History_details : Codable {
    let disease : String?
    let doctor : String?
    let patient : String?
    let appointment_date : String?
    let appointment_time : String?
    let duration : String?
    
    let appointment_id : Int?
    let profile_id : String?

    enum CodingKeys: String, CodingKey {
        case disease = "disease"
        case doctor = "doctor"
        case patient = "patient"
        case appointment_date = "appointment_date"
        case appointment_time = "appointment_time"
        case duration = "duration"
        case appointment_id = "appointment_id"
        case profile_id = "profile_id"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        disease = try values.decodeIfPresent(String.self, forKey: .disease)
        doctor = try values.decodeIfPresent(String.self, forKey: .doctor)
        patient = try values.decodeIfPresent(String.self, forKey: .patient)
        appointment_date = try values.decodeIfPresent(String.self, forKey: .appointment_date)
        appointment_time = try values.decodeIfPresent(String.self, forKey: .appointment_time)
        duration = try values.decodeIfPresent(String.self, forKey: .duration)
        
        appointment_id = try values.decodeIfPresent(Int.self, forKey: .appointment_id)
        profile_id = try values.decodeIfPresent(String.self, forKey: .profile_id)
    }
}
