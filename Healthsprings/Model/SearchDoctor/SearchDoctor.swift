/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct SearchDoctor : Codable {
	let id : Int?
	let doctor_name : String?
	let room_name : String?
	let email : String?
	let gender : String?
	let profile_link : String?
	let phone : String?
	let country : String?
	let avaliability_status : String?
	let description : String?
	let avatar : String?
	let uuid : Int?
	let specialty : String?
	let next_avail_date : String?
	let next_avail_time : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case doctor_name = "doctor_name"
		case room_name = "room_name"
		case email = "email"
		case gender = "gender"
		case profile_link = "profile_link"
		case phone = "phone"
		case country = "country"
		case avaliability_status = "avaliability_status"
		case description = "description"
		case avatar = "avatar"
		case uuid = "uuid"
		case specialty = "specialty"
		case next_avail_date = "next_avail_date"
		case next_avail_time = "next_avail_time"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		doctor_name = try values.decodeIfPresent(String.self, forKey: .doctor_name)
		room_name = try values.decodeIfPresent(String.self, forKey: .room_name)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		gender = try values.decodeIfPresent(String.self, forKey: .gender)
		profile_link = try values.decodeIfPresent(String.self, forKey: .profile_link)
		phone = try values.decodeIfPresent(String.self, forKey: .phone)
		country = try values.decodeIfPresent(String.self, forKey: .country)
		avaliability_status = try values.decodeIfPresent(String.self, forKey: .avaliability_status)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		avatar = try values.decodeIfPresent(String.self, forKey: .avatar)
		uuid = try values.decodeIfPresent(Int.self, forKey: .uuid)
		specialty = try values.decodeIfPresent(String.self, forKey: .specialty)
		next_avail_date = try values.decodeIfPresent(String.self, forKey: .next_avail_date)
		next_avail_time = try values.decodeIfPresent(String.self, forKey: .next_avail_time)
	}

}
