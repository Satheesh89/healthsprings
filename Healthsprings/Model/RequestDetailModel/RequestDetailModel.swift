//
//  RequestDetailModel.swift
//

import Foundation
import SwiftyJSON

struct RequestDetailModel {
    let requestdetail: [Requestdetail]?
    let message: String?
    init(_ json: JSON) {
        requestdetail = json["requestdetail"].arrayValue.map { Requestdetail($0) }
        message = json["message"].stringValue
    }
}
