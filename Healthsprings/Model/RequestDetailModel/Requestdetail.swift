//
//  Requestdetail.swift
//

import Foundation
import SwiftyJSON

struct Requestdetail {
    let profileId: String?
    let appointmentTime: String?
    let appointmentDate: String?
    let uuid: String?
    init(_ json: JSON) {
        profileId = json["profile_id"].stringValue
        appointmentTime = json["appointment_time"].stringValue
        appointmentDate = json["appointment_date"].stringValue
        uuid = json["uuid"].stringValue
    }
}
