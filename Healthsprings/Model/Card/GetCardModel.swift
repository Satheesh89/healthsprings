//
//  GetCardModel.swift
//  Healthsprings
//

import Foundation
import SwiftyJSON

struct GetCardModel : Codable {
    let id : Int?
    let cardholder_name : String?
    let card_number : Int?
    let expiration_year : Int?
    let expiration_month : Int?
    let card_id : String?
    let patient_id : Int?
    
    let brand : String?
    let fingerprint : String?
    let branch : String?
    let default_card : Int?


    enum CodingKeys: String, CodingKey {
        case id = "id"
        case cardholder_name = "cardholder_name"
        case card_number = "card_number"
        case expiration_year = "expiration_year"
        case expiration_month = "expiration_month"
        case card_id = "card_id"
        case patient_id = "patient_id"
        case brand = "brand"
        case fingerprint = "fingerprint"
        case branch = "branch"
        case default_card = "default_card"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        cardholder_name = try values.decodeIfPresent(String.self, forKey: .cardholder_name)
        card_number = try values.decodeIfPresent(Int.self, forKey: .card_number)
        expiration_year = try values.decodeIfPresent(Int.self, forKey: .expiration_year)
        expiration_month = try values.decodeIfPresent(Int.self, forKey: .expiration_month)
        card_id = try values.decodeIfPresent(String.self, forKey: .card_id)
        patient_id = try values.decodeIfPresent(Int.self, forKey: .patient_id)
        brand = try values.decodeIfPresent(String.self, forKey: .brand)
        fingerprint = try values.decodeIfPresent(String.self, forKey: .fingerprint)
        branch = try values.decodeIfPresent(String.self, forKey: .branch)
        default_card = try values.decodeIfPresent(Int.self, forKey: .default_card)
    }
}
