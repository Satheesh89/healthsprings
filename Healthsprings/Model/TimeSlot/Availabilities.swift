//
//  Availabilities.swift
//

import Foundation
import SwiftyJSON

class Availabilities {
    let date: String?
	let availDate: String?
	let availFromTime: String?
	let availToTime: String?
	let slots: Int?

	init(_ json: JSON) {
        date = json["date"].stringValue
		availDate = json["avail_date"].stringValue
		availFromTime = json["avail_from_time"].stringValue
		availToTime = json["avail_to_time"].stringValue
		slots = json["slots"].intValue
	}

}
