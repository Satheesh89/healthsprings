//
//  MorningSection.swift
//

import Foundation
import SwiftyJSON

class MorningSection {

	let morningTime: String?

	init(_ json: JSON) {
		morningTime = json["morning_time"].stringValue
	}

}
