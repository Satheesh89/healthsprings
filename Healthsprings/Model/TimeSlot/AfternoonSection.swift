//
//  AfternoonSection.swift
//
import Foundation
import SwiftyJSON

class AfternoonSection {

	let afternoonTime: String?

	init(_ json: JSON) {
		afternoonTime = json["afternoon_time"].stringValue
	}

}
