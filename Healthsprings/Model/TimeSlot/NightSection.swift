//
//  NightSection.swift
//

import Foundation
import SwiftyJSON

class NightSection {

	let nightTime: String?

	init(_ json: JSON) {
		nightTime = json["night_time"].stringValue
	}

}
