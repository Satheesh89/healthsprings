//
//  EveningSection.swift
//

import Foundation
import SwiftyJSON

class EveningSection {

	let eveningTime: String?

	init(_ json: JSON) {
		eveningTime = json["evening_time"].stringValue
	}

}
