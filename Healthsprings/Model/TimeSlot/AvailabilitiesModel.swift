//
//  AvailabilitiesModel.swift
//
import Foundation
import SwiftyJSON

class AvailabilitiesModel {

	let availabilities: [Availabilities]?
	let selectDate: String?
	let morningSection: [MorningSection]?
	let afternoonSection: [AfternoonSection]?
	let eveningSection: [EveningSection]?
	let nightSection: [NightSection]?

	init(_ json: JSON) {
		availabilities = json["availabilities"].arrayValue.map { Availabilities($0) }
		selectDate = json["select_date"].stringValue
		morningSection = json["morning_section"].arrayValue.map { MorningSection($0) }
		afternoonSection = json["afternoon_section"].arrayValue.map { AfternoonSection($0) }
		eveningSection = json["evening_section"].arrayValue.map { EveningSection($0) }
		nightSection = json["night_section"].arrayValue.map { NightSection($0) }
	}

}
