//
//  Code.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on June 16, 2022
//
import Foundation
import SwiftyJSON

struct Code {

	let name: String?
	let dialCode: String?
	let code: String?

	init(_ json: JSON) {
		name = json["name"].stringValue
		dialCode = json["dial_code"].stringValue
		code = json["code"].stringValue
	}

}