//
//  CountryModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on June 16, 2022
//
import Foundation
import SwiftyJSON

struct CountryModel {
	let countryDetail: CountryDetail?
	init(_ json: JSON) {
		countryDetail = CountryDetail(json["country_detail"])
	}
}
