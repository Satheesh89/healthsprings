//
//  CountryDetail.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on June 16, 2022
//
import Foundation
import SwiftyJSON

struct CountryDetail {

	let code: [Code]?

	init(_ json: JSON) {
		code = json["code"].arrayValue.map { Code($0) }
	}

}