//
//  OnboardingSlide.swift
//  Healthsprings
//
//  Created by Personal on 07/09/22.
//

import UIKit

struct OnboardingSlide {
    let title: String
    let description: String
    let image: UIImage
}
