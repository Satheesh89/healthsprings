//
//  AppDelegate.swift
//  Healthsprings
//
//  Created by Personal on 14/06/22.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import IQKeyboardManagerSwift
//import Stripe

public typealias SimpleClosure = (() -> ())

@available(iOS 13.0, *)
@available(iOS 13.0, *)
@available(iOS 13.0, *)
@main
class AppDelegate: UIResponder, UIApplicationDelegate ,UNUserNotificationCenterDelegate{
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        setLaunchScreen()
//        preconditionFailure()
//        exit(0)

//        StripeAPI.defaultPublishableKey = "pk_test_51LTKLmSHgvLhFxVox8...H6ANNnGix00zDemJMAD"
        
        //Set default Development Api
        IQKeyboardManager.shared.enable = true
        UserDefaults.standard.set(Environment.Production.rawValue, forKey:Key.UserDefaults.stagingURL)
        
        //UNUserNotificationCenter
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        
        // check for user permission first
        center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, _) in
            NSLog("PUSH NOTIFICATION PERMISSION GRANTED: \(granted)")
            guard granted else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
        
        // Fackbook
        ApplicationDelegate.shared.application(application,didFinishLaunchingWithOptions: launchOptions
        )
        UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil);
        return true
    }
    
    // MARK: - Tabbar Action
    func moveLoginPage(){
        let story = UIStoryboard(name: "Menu", bundle:nil)
        let vc = story.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    // MARK: - Tabbar Action
    func setTabPage(){
        let story = UIStoryboard(name: "Tabbar", bundle:nil)
        let vc = story.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        UserDefaults.standard.set(token, forKey: Key.UserDefaults.deviceToken)
        UserDefaults.standard.synchronize()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for remote notifications with error: \(error)")
    }
    
    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    //MARK:- setLaunchScreen
    func setLaunchScreen()  {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UIStoryboard(name: "LaunchScreen", bundle: nil).instantiateInitialViewController()
        window?.makeKeyAndVisible()
    }
    
    // Fackbook
    func application(
        _ app: UIApplication,open url: URL,options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
            ApplicationDelegate.shared.application(app,open: url,sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                   annotation: options[UIApplication.OpenURLOptionsKey.annotation]
            )
        }
}


