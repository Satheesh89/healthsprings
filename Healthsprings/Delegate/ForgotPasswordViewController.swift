//
//  ForgotPasswordViewController.swift
//  Healthsprings
//

import UIKit
import CRNotifications

class ForgotPasswordViewController: UIViewController {
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailImageView: UIImageView!
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var sendOtpBUtton: UIButton!
    var emailStr: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        emailTextField.text = emailStr
        emailTextField.delegate = self
        emailView.viewBorderShadow()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func didTapSendOtpAction(_ sender: Any) {
        if emailTextField.text == ""{
            self.showCRNotificationsToast(toastType: CRNotifications.info, toastTitle: "Error", toastMeassage: "Please enter your email")
        }else{
            sendForgotOtp(emailStr: emailTextField.text ?? "")
        }
    }
    
    @IBAction func didTapBacktoLoginAction(_ sender: Any) {
        self.navigationController?.popToViewController(ofClass: LoginVC.self)
    }
    
    // MARK: Forgot Password
    func sendForgotOtp(emailStr: String){
        LoadingIndicator.shared.show(forView: self.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["email"] = emailStr
        print(paramsDictionary)
        
        HttpClientApi.instance().makeAPICall(url: "\(URL.forgotpassword)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("dataDict",dataDict)
                let response = response
                if  response?.statusCode == 200 {
                    doOnMain {
                        self.showCRNotificationsToast(toastType: CRNotifications.success, toastTitle: "Success", toastMeassage: "\(dataDict.value(forKeyPath: "success") ?? "")")
                        LoadingIndicator.shared.hide()
                        self.directToOtpPage()
                    }
                }else{
                    doOnMain {
                        self.showCRNotificationsToast(toastType: CRNotifications.info, toastTitle: "Success", toastMeassage: "\(dataDict.value(forKeyPath: "message") ?? "")")
                        LoadingIndicator.shared.hide()
                    }
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
            doOnMain {
                self.showCustomToast(toastTitle: "Error",toastMeassage: "API call Failure",status: 1)
                LoadingIndicator.shared.hide()
            }
        })
    }
    
    func directToOtpPage(){
        let otpVC = ForgotOtpPasswordViewController.instantiateFromAppStoryboard(appStoryboard: .Menu)
        otpVC.hidesBottomBarWhenPushed = true
        otpVC.emailStr = emailTextField.text ?? ""
        self.navigationController?.pushViewController(otpVC, animated: false)
    }
}
