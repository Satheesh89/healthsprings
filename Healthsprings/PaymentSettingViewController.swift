//
//  PaymentSettingViewController.swift
//  Healthsprings
//
//  Created by Personal on 30/06/22.
//

import UIKit

class PaymentSettingViewController: UIViewController {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var paymentSettingTableView: UITableView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!

    var selectIndexpath : IndexPath = IndexPath()
    var getCardModel = GetCardViewModel()
    var cardIdStr: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func loadSetup(){
        getCardModel.vc = self
        getCardModel.getCardListApi( apitokenStr: "apiToken=\(UserDetails.shared.getApiToken())",loadview: self)
    }
        
    //MARK: Load Setup
    func alertViewLoadSetup()  {
        alertView.backgroundColor = .alphaColor
        alertView.layer.cornerRadius = 5
        alertView.isHidden = false
        cancelButton.layer.cornerRadius = 5
        okButton.layer.cornerRadius = 5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        loadSetup()
        tableViewSetup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: TableView Setup
    func tableViewSetup()  {
        setStatusBar(color: UIColor.themeColor)
        paymentSettingTableView.register(PaymentSettingCell.nib, forCellReuseIdentifier: PaymentSettingCell.identifier)
        paymentSettingTableView.delegate = self
        paymentSettingTableView.dataSource = self
        paymentSettingTableView.tableFooterView = UIView()
        paymentSettingTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        paymentSettingTableView.showsHorizontalScrollIndicator = false
        paymentSettingTableView.showsVerticalScrollIndicator = false
        paymentSettingTableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        paymentSettingTableView.backgroundColor = .clear
        paymentSettingTableView.contentInsetAdjustmentBehavior = .never
        selectIndexpath = IndexPath(item:0, section:0)
    }
    
    @IBAction func didTabLeftAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func didTapRemoveAction(_ sender: UIButton) {
        cardIdStr = getCardModel.cardList[sender.tag].id ?? 0
        alertViewLoadSetup()
    }
    
    @IBAction func didTapProceedAction(_ sender: UIButton) {
        cardIdStr = getCardModel.cardList[sender.tag].id ?? 0
        self.redirectMakeRequestView ()
    }
    
    @IBAction func didTapYesAction(_ sender: Any) {
        self.getCardRemoveListApi( apitokenStr: "apiToken=\(UserDetails.shared.getApiToken())",loadview: self, cardId: cardIdStr)
    }
    
    func getDefualtApi( loadview: UIViewController,cardId: Int) {
        LoadingIndicator.shared.show(forView: loadview.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()
        paramsDictionary["card_id"] = cardId
        HttpClientApi.instance().makeAPICall(url: "\(URL.setDefaultCard)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("dataDict",dataDict)
                let response = response
                if  response?.statusCode == 200 {
                    doOnMain {
                        LoadingIndicator.shared.hide()
                        self.alertView.isHidden = true
                        self.loadSetup()
                        self.showCustomToast(toastTitle: "Info",toastMeassage: "\(dataDict.value(forKeyPath: "message") ?? "")",status: 0)
                    }
                }else{
                    self.showCustomToast(toastTitle: "Error",toastMeassage: "error",status: 1)
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
    
    
    private func redirectMakeRequestView (){
        let findDoctorVC = MakeRequestViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        findDoctorVC.hidesBottomBarWhenPushed = true
        findDoctorVC.getCardId = cardIdStr
        findDoctorVC.getCartBool = true
        self.navigationController?.pushViewController(findDoctorVC, animated: false)
    }
    
    func getCardRemoveListApi( apitokenStr: String,loadview: UIViewController,cardId: Int) {
        LoadingIndicator.shared.show(forView: loadview.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()
        paramsDictionary["card_id"] = cardId
        HttpClientApi.instance().makeAPICall(url: "\(URL.getRemovecard)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                print("dataDict",dataDict)
                let response = response
                if  response?.statusCode == 200 {
                    doOnMain {
                        LoadingIndicator.shared.hide()
                        self.alertView.isHidden = true
                        self.loadSetup()
                    }
                }else{
                    self.showCustomToast(toastTitle: "Error",toastMeassage: "error",status: 1)
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
    
    @IBAction func didTapNoAction(_ sender: Any) {
        alertView.isHidden = true
    }
    
}

extension PaymentSettingViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getCardModel.cardList.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        let label = UILabel()
        label.frame = CGRect.init(x: 24, y: 0, width: headerView.frame.width-10, height: headerView.frame.height)
        headerView.addSubview(label)
        label.text = "Setup your card for payment"
        if ( UIDevice.current.model.range(of: "iPad") != nil){
            label.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(10.0))
        } else {
            label.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(16.0))
        }
        label.textColor = .blackColor
        
        if getCardModel.cardList.count > 0{
            headerView.frame = CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50)
        }else{
            headerView.frame = CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 0)
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 44))
        let label = UILabel()
        label.frame = CGRect.init(x: 24, y: (footerView.frame.height/2)-15, width: 20, height: 30)
        label.text = "+"
        label.textColor = .themeColor
        footerView.addSubview(label)
        let addButton = UIButton()
        addButton.frame = CGRect.init(x: label.frame.origin.x+label.frame.size.width+15, y: 0, width: (footerView.frame.width-10)-(label.frame.origin.x+label.frame.size.width+30), height: footerView.frame.height)
        if ( UIDevice.current.model.range(of: "iPad") != nil){
            addButton.titleLabel?.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(10.0))
            label.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(13.0))
        } else {
            addButton.titleLabel?.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(16.0))
            label.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(30.0))
        }
        addButton.contentHorizontalAlignment = .left
        footerView.addSubview(addButton)
        addButton.setTitle("Add New Bank Account", for: .normal)
        addButton.setTitleColor(.themeColor, for: .normal)
        addButton.addTarget(self, action: #selector(self.didSelectRowIndex), for: .touchUpInside)
        
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return availableTableViewCell(tableView: tableView, indexPath: indexPath)
    }
    
    func availableTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentSettingCell.identifier) as? PaymentSettingCell else {
            return UITableViewCell()
        }
        cell.selectRadioImage(getIndexpath: selectIndexpath,normalIndex: indexPath)
        cell.getCardModel =  getCardModel.cardList[indexPath.row]
        cell.removeButton.addTarget(self, action: #selector(self.didTapRemoveAction), for: .touchUpInside)
        cell.proceedButton.addTarget(self, action: #selector(self.didTapProceedAction), for: .touchUpInside)
        cell.removeButton.tag = indexPath.row
        cell.proceedButton.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectIndexpath = indexPath
        cardIdStr = getCardModel.cardList[indexPath.row].id ?? 0
        getDefualtApi( loadview: self,cardId: cardIdStr)
        paymentSettingTableView.reloadData()
    }
    
    @objc func didSelectRowIndex(sender : UIButton) {
        //let profileVC = AddCardViewController.instantiateFromAppStoryboard(appStoryboard: .Tabbar)
        let profileVC = MakeInstantAddCardViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        profileVC.hidesBottomBarWhenPushed = true
        profileVC.cardBool = true
        self.navigationController?.pushViewController(profileVC, animated: false)
    }
}
