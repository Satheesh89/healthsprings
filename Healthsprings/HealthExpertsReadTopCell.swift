//
//  HealthExpertsCell.swift
//  Yummie
//
//  Created by Personal on 08/09/22.
//

import UIKit

class HealthExpertsReadTopCell: UICollectionViewCell {
    
    static let identifier = String(describing: HealthExpertsReadTopCell.self)
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var blurView: UIView!
    
    
    func setup(catDict: [HealthBlog], selectIndexPath: IndexPath) {
        DispatchQueue.main.async {
            self.categoryImage.sd_setImage(with: URL(string: catDict[selectIndexPath.row].icon ?? ""), placeholderImage: UIImage(named: "why-healthcheck"))
        }
//           categoryImage.image = UIImage(named: catDict[selectIndexPath.row].value(forKey: "image") as! String)
        titleLabel.text = catDict[selectIndexPath.row].title
        detailsLabel.text = catDict[selectIndexPath.row].description
//        dateLabel.text = "\(catDict[selectIndexPath.row].value(forKey: "details") ?? "")"
    }
}
