//
//  LoadingIndicator.swift
//  Caravan
//
//  Created by Premkumar on 31/01/21.
//

import UIKit
import SwiftGifOrigin

class LoadingIndicator: UIView {
    private let loader: UIImageView = {
        // A UIImageView with async loading
        let activityIndicator = UIImageView()
        //activityIndicator.loadGif(name: "loader-healthSprings")
        //activityIndicator.loadGif(name: "LoaderImage")
        activityIndicator.image = UIImage(named: "LoaderImage")
        activityIndicator.contentMode = .scaleAspectFit
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.backgroundColor = .clear
        
        let rotateView = CABasicAnimation()
        rotateView.fromValue = 0.degreesToRadian
        rotateView.toValue = 360.degreesToRadian
        rotateView.duration = 1
        rotateView.repeatCount = Float.infinity
        rotateView.isRemovedOnCompletion = false
        rotateView.fillMode = CAMediaTimingFillMode.forwards
        rotateView.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        activityIndicator.layer.add(rotateView, forKey: "transform.rotation.z")
        return  activityIndicator
    }()
    /// Shared Instance
    static let shared: LoadingIndicator = LoadingIndicator()
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepared()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    /// Design Indicator and Adding subView to Window
    func prepared() {
        tag = 101011
        self.addSubview(loader)
    }
    /// Show Indicator View with animation
    func show(forView: UIView, useAutoLayout: Bool = false) {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        if let previousLoader = forView.subviews.filter({$0.tag == 101011 }).first as? LoadingIndicator {
            previousLoader.hide()
        }
        self.frame = forView.frame
        loader.center = self.center
        forView.addSubview(self)
        //if useAutoLayout {
        //forView.translatesAutoresizingMaskIntoConstraints = false
        self.fillSuperview()
        //        loader.centerInSuperview()
        //        }
        //        loader.startAnimating()
        loader.bringSubviewToFront(forView)
    }
    /// Hide Indicator View with animation
    func hide() {
        self.removeFromSuperview()
        //        self.loader.layer.removeAllAnimations()
        //        loader.stopAnimating()
    }
}
extension Int {
    var degreesToRadian : CGFloat {
        return CGFloat(self) * CGFloat(M_PI) / 180.0
    }
}
