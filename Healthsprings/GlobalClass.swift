//
//  GlobalClass.swift
//  Healthsprings
//
//  Created by Personal on 16/06/22.
//

import UIKit
import CRNotifications

class GlobalClass: NSObject {
    static let sharedInstance: GlobalClass = GlobalClass()
    
    var activityBackgroundView : UIView!
    var msgFrame = UIView()
    let loading = InstagramActivityIndicator()
    
    // MARK: hasConnectivity
    func hasConnectivity() -> Bool {
        do{
            let reachability = Reachability()
            if  (reachability?.isReachable)!{
                return true
            }else{
                return false
            }
        }
    }
    
    func activity(){
        DispatchQueue.main.async{
            let view = UIApplication.shared.keyWindow!
            if self.activityBackgroundView != nil {
                self.activityBackgroundView.removeFromSuperview()
            }
            //  set fullview bacground
            let screenSize: CGRect = UIScreen.main.bounds
            self.activityBackgroundView = UIView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height))
            //Set background color
            self.activityBackgroundView.backgroundColor = UIColor.init(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.1)
            self.activityBackgroundView.tag = 103
            
            // set center gray color bacground view
            self.msgFrame = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 40 , width: 100, height: 100))
            self.msgFrame.center = self.activityBackgroundView.center
            self.msgFrame.layer.cornerRadius = 15
            //            self.msgFrame.backgroundColor = UIColor(white: 0, alpha: 0.7)
            //  set activity view controller in center gray view
            
            self.loading.frame = CGRect(x:(self.msgFrame.frame.width/2)-40 , y:(self.msgFrame.frame.height/2)-40, width: 80, height: 80)
            //loading.animationDuration = 0
            self.loading.rotationDuration = 3
            self.loading.numSegments = 14
            self.loading.strokeColor = UIColor.themeColor
            self.loading.lineWidth = 4
            self.loading.startAnimating()
            self.msgFrame.addSubview(self.loading)
            self.activityBackgroundView.addSubview(self.msgFrame)
            view.addSubview(self.activityBackgroundView)
        }
    }
    
    // MARK: removeActivity
    func removeActivity(){
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.activityBackgroundView.removeFromSuperview()
        }
    }
    
    func popUpView()  {
        let view = UIApplication.shared.keyWindow!
        let screenSize: CGRect = UIScreen.main.bounds
        
        popView.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        popView.backgroundColor = UIColor.appBgViewLightColor
        view.addSubview(popView)
        
        let bgView = UIView(frame: CGRect(x: 30, y: 0, width: screenSize.width - 90, height: 150))
        bgView.center = CGPoint(x: popView.frame.width / 2, y: popView.frame.height / 2)
        bgView.backgroundColor = UIColor.white
        bgView.layer.cornerRadius = 10
        bgView.clipsToBounds = true
        popView.addSubview(bgView)
        
        let popupLbl = UILabel()
        popupLbl.frame = CGRect(x: 0, y: 30, width: bgView.frame.width, height: 21)
        popupLbl.text = "no internet availble"
        popupLbl.font = UIFont(name:Font.FontName.LatoBlack.rawValue, size: Utility.dynamicSize(14.0))
        popupLbl.textAlignment = .center
        bgView.addSubview(popupLbl)
        
        let cancelBtn = UIButton(frame: CGRect(x: 30, y: popupLbl.frame.origin.y+popupLbl.frame.size.height+30, width: (bgView.frame.size.width/2)-30, height: 40))
        cancelBtn.setTitle("Okay".uppercased(), for: .normal)
        cancelBtn.backgroundColor = .themeColor
        cancelBtn.setTitleColor(UIColor.white, for: .normal)
        cancelBtn.layer.cornerRadius = 5
        cancelBtn.titleLabel?.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))
        cancelBtn.addTarget(self, action: #selector(self.okBtnTapped), for: .touchUpInside)
        bgView.addSubview(cancelBtn)
        
        let okBtn = UIButton(frame: CGRect(x: cancelBtn.frame.origin.x+cancelBtn.frame.size.width+10, y: popupLbl.frame.origin.y+popupLbl.frame.size.height+30, width: (bgView.frame.size.width/2)-30, height: 40))
        okBtn.setTitle("Cancel".uppercased(), for: .normal)
        okBtn.backgroundColor = .appBgViewLightColor
        okBtn.setTitleColor(UIColor.white, for: .normal)
        okBtn.titleLabel?.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))
        okBtn.addTarget(self, action: #selector(self.cancelBtnTapped), for: .touchUpInside)
        okBtn.layer.cornerRadius = 5
        bgView.addSubview(okBtn)
    }
    
    @objc func cancelBtnTapped(sender : UIButton) {
        popView.removeFromSuperview()
    }
    
    @objc func okBtnTapped(sender : UIButton) {
        DispatchQueue.main.async {
            popView.removeFromSuperview()
            exit(0)
        }
    }
}

//MARK: Loader Show and Hide
var vSpinner : UIView?
var popView = UIView()

extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = .alphaColor
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.color = .themeColor
        ai.center = spinnerView.center
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        vSpinner = spinnerView
    }
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
    
    //Method for Alert iew Controller
    func showAlertGlobalMessage(title: String, message: String, completion: @escaping () -> Void) {
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            alertController.dismiss(animated: true, completion: {
            })
            completion()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- Toast view
    func showToast(message : String) {
        let lblHeight = self.height(constraintedWidth:UIScreen.main.bounds.size.width-40 , font:UIFont.systemFont(ofSize: 14),text:message)
        let toastLabel = UILabel(frame: CGRect(x: 20, y: UIScreen.main.bounds.size.height-100, width: UIScreen.main.bounds.size.width-40, height:lblHeight+30))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        
        if ( UIDevice.current.model.range(of: "iPad") != nil){
            toastLabel.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(10.0))
        } else {
            toastLabel.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))
        }
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        toastLabel.numberOfLines = 0
        let view = UIApplication.shared.keyWindow!
        view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    //MARK:- label height calculate
    func height(constraintedWidth width: CGFloat, font: UIFont, text:String) -> CGFloat {
        
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = text
        label.font = font
        label.sizeToFit()
        return label.frame.height
    }
    
    //MARK:- String Replacing
    func stringReplacingToast(string:String, replacingStr:String) -> String {
        let result = string.replacingOccurrences(of:"{0}", with:replacingStr)
        return result;
    }
    
    //MARK:- String Convert Date
    func convertDateFormatDynamic(inputDate: String) -> String {
        let olDateFormatter = DateFormatter()
        olDateFormatter.dateFormat = "MMM dd,yyyy"
        let oldDate = olDateFormatter.date(from: inputDate)
        let convertDateFormatter = DateFormatter()
        convertDateFormatter.dateFormat = "yyyy-MM-dd"
        return convertDateFormatter.string(from: oldDate!)
    }
}

// MARK:- App version and build number
extension Bundle {
    var appName: String {
        return infoDictionary?["CFBundleName"] as! String
    }
    var bundleId: String {
        return bundleIdentifier!
    }
    var versionNumber: String {
        return infoDictionary?["CFBundleShortVersionString"] as! String
    }
    var buildNumber: String {
        return infoDictionary?["CFBundleVersion"] as! String
    }
    var displayName: String? {
        return object(forInfoDictionaryKey: "CFBundleDisplayName") as? String
    }
}

// MARK:- UIImageView
extension UIImageView {
    func setRounded() {
        let radius = self.frame.width / 2
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    func setRoundedColor() {
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.themeColor.cgColor
    }
}

// MARK:- UIButton
extension UIButton {
    func setRoundedButtonColor() {
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.themeColor.cgColor
    }
}

// MARK:- UIView
extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    func borderLineWithColor(selectColor: UIColor,width: Int,radius: Int) {
        layer.cornerRadius = CGFloat(radius)
        layer.borderWidth = CGFloat(width)
        layer.borderColor = selectColor.cgColor
    }
    func applyGradient(isVertical: Bool, colorArray: [UIColor]) {
        layer.sublayers?.filter({ $0 is CAGradientLayer }).forEach({ $0.removeFromSuperlayer() })
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colorArray.map({ $0.cgColor })
        if isVertical {
            //top to bottom
            gradientLayer.locations = [0.0, 1.0]
        } else {
            //left to right
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        }
        backgroundColor = .clear
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func addShadow() {
        self.layer.shadowColor = UIColor.viewBorderColor.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowOpacity = 4
        self.layer.cornerRadius = 5
    }
    
    public func addViewBorder(borderColor:CGColor,borderWith:CGFloat,borderCornerRadius:CGFloat){
        self.layer.borderWidth = borderWith
        self.layer.borderColor = borderColor
        self.layer.cornerRadius = borderCornerRadius
    }
    
    func dropShadowWithCornerRaduis() {
        doOnMain {
            self.layer.masksToBounds = false
            self.layer.shadowRadius = 4
            self.layer.shadowOpacity = 0.5
            self.layer.shadowColor = UIColor.grayColor.cgColor
            self.layer.shadowOffset = CGSize(width: 0 , height:0.9)
            self.layer.cornerRadius = 5
        }
    }
    
    func viewBorderShadow() {
        doOnMain {
            self.layer.cornerRadius = 5
            self.layer.shadowColor = UIColor.convertRGB(hexString: "#DBDBDB").cgColor
            self.layer.shadowOffset = .zero
            self.layer.shadowOpacity = 0.9
            self.layer.shadowRadius = 10
            self.layer.borderWidth = 1
            self.layer.borderColor = UIColor.init(red: 219/255, green: 219/255, blue: 219/255, alpha: 1.0).cgColor
        }
    }
    
    
    
    
    
    func dropShadowWithCornerRaduisWithColor(setColor: UIColor) {
        doOnMain {
            self.layer.masksToBounds = false
            self.layer.shadowRadius = 4
            self.layer.shadowOpacity = 0.5
            self.layer.shadowColor = setColor.cgColor
            self.layer.shadowOffset = CGSize(width: 0 , height:1)
            self.layer.cornerRadius = 5
        }
    }
    
    
    /// Fill superview
    /// - Parameter padding: padding
    func fillSuperview(padding: UIEdgeInsets = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        if let superviewTopAnchor = superview?.topAnchor {
            topAnchor.constraint(equalTo: superviewTopAnchor, constant: padding.top).isActive = true
        }
        if let superviewBottomAnchor = superview?.bottomAnchor {
            bottomAnchor.constraint(equalTo: superviewBottomAnchor, constant: -padding.bottom).isActive = true
        }
        if let superviewLeadingAnchor = superview?.leadingAnchor {
            leadingAnchor.constraint(equalTo: superviewLeadingAnchor, constant: padding.left).isActive = true
        }
        if let superviewTrailingAnchor = superview?.trailingAnchor {
            trailingAnchor.constraint(equalTo: superviewTrailingAnchor, constant: -padding.right).isActive = true
        }
    }
    
    func centerInSuperview(size: CGSize = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        if let superviewCenterXAnchor = superview?.centerXAnchor {
            centerXAnchor.constraint(equalTo: superviewCenterXAnchor).isActive = true
        }
        if let superviewCenterYAnchor = superview?.centerYAnchor {
            centerYAnchor.constraint(equalTo: superviewCenterYAnchor).isActive = true
        }
        if size.width != 0 {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
        if size.height != 0 {
            heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
    }
}

extension UIImage {
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        let newSize = widthRatio > heightRatio ?  CGSize(width: size.width * heightRatio, height: size.height * heightRatio) : CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    func upOrientationImage() -> UIImage? {
        switch imageOrientation {
        case .up:
            return self
        default:
            UIGraphicsBeginImageContextWithOptions(size, false, scale)
            draw(in: CGRect(origin: .zero, size: size))
            let result = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return result
        }
    }
    
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat,yOrginSize: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        //        UIRectFill(CGRect(x: 0, y: size.height - lineWidth, width: size.width, height: lineWidth))
        UIRectFill(CGRect(x: 0, y: 5, width: size.width, height: lineWidth))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func imageWithColor(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color.setFill()
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)
        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
}

// MARK:- UITextFieldDelegate
extension UIViewController: UITextFieldDelegate{
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    // MARK: Hide Keyboard
    func initializeHideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissMyKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissMyKeyboard(){
        view.endEditing(true)
    }
}

// MARK:- UITextFieldDelegate
extension UINavigationController {
    func popToViewController(ofClass: AnyClass, animated: Bool = false) {
        DispatchQueue.main.async { [self] in
            if let vc = self.viewControllers.last(where: { $0.isKind(of: ofClass) }) {
                self.popToViewController(vc, animated: animated)
            }
        }
    }
}

// MARK:- SVGKImage URL Convert
//extension UIImageView {
//    func downloadedsvg(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
//        contentMode = mode
//        URLSession.shared.dataTask(with: url) { data, response, error in
//            guard
//                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
//                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
//                let data = data, error == nil,
//                let receivedicon: SVGKImage = SVGKImage(data: data),
//                let image = receivedicon.uiImage
//            else { return }
//            DispatchQueue.main.async() {
//                self.image = image
//            }
//        }.resume()
//    }
//}

extension UIImage {
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
}

extension UIColor {
    //    static let chartBarColour = #colorLiteral(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
    //    static let chartLineColour = #colorLiteral(red: 0.1764705882, green: 0.2509803922, blue: 0.3490196078, alpha: 1)
    static let chartBarColour = #colorLiteral(red: 0.9411764706, green: 0.4823529412, blue: 0.2470588235, alpha: 1)
    //    static let chartLineColour = #colorLiteral(red: 0.9411764706, green: 0.4823529412, blue: 0.2470588235, alpha: 1)
    static let chartLineColour = #colorLiteral(red: 0, green: 0.5912256837, blue: 0.6998883486, alpha: 1)
    static let chartReplacementColour = #colorLiteral(red: 0, green: 0.5912256837, blue: 0.6998883486, alpha: 1)
    static let chartAverageColour = #colorLiteral(red: 0, green: 0.5912256837, blue: 0.6998883486, alpha: 1)
    //    static let chartBarValueColour = #colorLiteral(red: 0.9411764706, green: 0.4823529412, blue: 0.2470588235, alpha: 1)
    //    static let chartHightlightColour = #colorLiteral(red: 0.9411764706, green: 0.4823529412, blue: 0.2470588235, alpha: 1)
    static let chartBarValueColour = #colorLiteral(red: 0, green: 0.5912256837, blue: 0.6998883486, alpha: 1)
    static let chartHightlightColour = #colorLiteral(red: 0, green: 0.5912256837, blue: 0.6998883486, alpha: 1)
}

extension Collection where Iterator.Element == String {
    var convertToDouble: [Double] {
        return compactMap{ Double($0) }
    }
    var convertToFloat: [Float] {
        return compactMap{ Float($0) }
    }
}

extension String {
    func insertSeparator(_ separatorString: String, atEvery n: Int) -> String {
        guard 0 < n else { return self }
        return self.enumerated().map({String($0.element) + (($0.offset != self.count - 1 && $0.offset % n ==  n - 1) ? "\(separatorString)" : "")}).joined()
    }
    mutating func insertedSeparator(_ separatorString: String, atEvery n: Int) {
        self = insertSeparator(separatorString, atEvery: n)
    }
    
    // MARK:- UITextField Email Valid
    func isValidEmail() -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    
    
    /*  Minimum 8 characters at least 1 Alphabet and 1 Number:
     "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"
     Minimum 8 characters at least 1 Alphabet, 1 Number and 1 Special Character:
     "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$"
     Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet and 1 Number:
     "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$"
     Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:
     "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[d$@$!%*?&#])[A-Za-z\\dd$@$!%*?&#]{8,}"
     Minimum 8 and Maximum 10 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:
     "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&#]{8,10}"
     */
    //MARK:-  Minimum 8 characters at least 1 Alphabet, 1 Number and 1 Special Character:
    public func isValidPassword() -> Bool {
        let passwordRegex = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
}

extension Double {
    var decimalPlaces: Int {
        let decimals = String(self).split(separator: ".")[1]
        return decimals == "0" ? 0 : decimals.count
    }
}

extension String {
    func replace(string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(string: " ", replacement: "")
    }
    
    func attributedStringWithColor(_ strings: [String], color: UIColor, characterSpacing: UInt? = nil) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        for string in strings {
            let range = (self as NSString).range(of: string)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        }
        guard let characterSpacing = characterSpacing else {return attributedString}
        attributedString.addAttribute(NSAttributedString.Key.kern, value: characterSpacing, range: NSRange(location: 0, length: attributedString.length))
        return attributedString
    }
}

public extension UIViewController {
    func setStatusBar(color: UIColor) {
        let tag = 12321
        if let taggedView = self.view.viewWithTag(tag){
            taggedView.removeFromSuperview()
        }
        let overView = UIView()
        overView.frame = UIApplication.shared.statusBarFrame
        overView.backgroundColor = color
        overView.tag = tag
        self.view.addSubview(overView)
    }
    
    // MARK: - Tabbar Action
    func setFrontTabPage(){
        doOnMain {
            let tabStoryboard = UIStoryboard(name: "Tabbar", bundle: nil)
            let Permissionvc = tabStoryboard.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
            UIApplication.shared.windows.first?.rootViewController = Permissionvc
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }
    
    
    // MARK: -  Login
    func setupOtp(getMobileNo: String,getName: String,getEmail: String){
        doOnMain {
            let mainstoryboard = UIStoryboard(name: "Menu", bundle: nil)
            let otpVC = mainstoryboard.instantiateViewController(withIdentifier: "OtpViewController") as! OtpViewController
            otpVC.getEmail = getEmail
            otpVC.getMobile = getMobileNo
            otpVC.getName = getName
            self.navigationController?.pushViewController(otpVC, animated: false)
        }
    }
    
    
    func setFrontLoginVC() {
        let mainStoryBoard = UIStoryboard(name: "Menu", bundle: nil)
        let loginVC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        UIApplication.shared.windows.first?.rootViewController = loginVC
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
}

extension UINavigationController {
    func backToViewController(viewController: Swift.AnyClass) {
        for element in viewControllers as Array {
            if element.isKind(of: viewController) {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
}
extension UIViewController {
    class var storyboardID : String {
        return "\(self)"
    }
    static func instantiateFromAppStoryboard(appStoryboard : AppStoryBoard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
    
    func showCRNotificationsToast(toastType: CRNotificationType,toastTitle: String,toastMeassage: String)  {
        CRNotifications.showNotification(type: toastType, title: toastTitle, message: toastMeassage, dismissDelay: 3)
    }
    
    func showCustomToast(toastTitle: String,toastMeassage: String,status: Int){
        let frame = CGRect(x: 0, y: 0, width: view.frame.size.width/1.2, height: 49)
        let viewModel: SnackbarViewModel
        var statusImage = UIImage(named: "error")
        if status == 1{
            statusImage = UIImage(named: "error")
        }else if status == 0{
            statusImage = UIImage(named: "info-white")
        }else if status == 2{
            statusImage = UIImage(named: "circletick")
        }
        viewModel = SnackbarViewModel(type: .info, text: toastMeassage, image: statusImage)
        let snackbar = SnackbarView(viewModel: viewModel, frame: frame)
        if status == 1{
            snackbar.label.textColor = .red
        }else if status == 0{
            snackbar.label.textColor = .white
        }else{
            snackbar.label.textColor = .themeColor
        }
        showSnackbar(snackBar: snackbar)
    }
    
    func showSnackbar(snackBar: SnackbarView){
        let width = view.frame.size.width/1.2
        snackBar.frame = CGRect(x: (view.frame.size.width-width)/2, y: view.frame.size.height, width: width, height: 49)
        view.addSubview(snackBar)
        UIView.animate(withDuration: 0.5, animations: {
            snackBar.frame = CGRect(x: (self.view.frame.size.width-width)/2, y: self.view.frame.size.height - 150, width: width, height: 49)
        },completion: { done in
            if done{
                DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                    UIView.animate(withDuration: 0.5, animations: {
                        snackBar.frame = CGRect(x: (self.view.frame.size.width-width)/2, y: self.view.frame.size.height , width: width, height: 49)
                    },completion: { finished in
                        if finished{
                            snackBar.removeFromSuperview()
                        }
                    })
                })
            }
        })
    }
    
}


extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

extension UITextField {
    func addInputViewDatePicker(target: Any, selector: Selector) {
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        //        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -16, to: Date())
        datePicker.maximumDate = Date()
        
        // iOS 14 and above
        if #available(iOS 14, *) {// Added condition for iOS 14
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.sizeToFit()
        }
        self.inputView = datePicker //3
        
        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) //7
        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
        toolBar.barTintColor = UIColor.themeColor
        cancel.tintColor = .white
        barButton.tintColor = .white
        self.inputAccessoryView = toolBar //9
        //        datePicker.setValue(UIColor.red, forKeyPath: "textColor")
        
    }
    
    @objc func tapCancel() {
        self.resignFirstResponder()
    }
}


extension UITableView {
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .lightGray
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))
        messageLabel.sizeToFit()
        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .none
    }
}

//extension String {
//    subscript(_ i: Int) -> String {
//        let idx1 = index(startIndex, offsetBy: i)
//        let idx2 = index(idx1, offsetBy: 1)
//        return String(self[idx1..<idx2])
//    }
//}

extension UIApplication {
    class func isFirstLaunch() -> Bool {
        if !UserDefaults.standard.bool(forKey: "hasBeenLaunchedBeforeFlag") {
            UserDefaults.standard.set(true, forKey: "hasBeenLaunchedBeforeFlag")
            UserDefaults.standard.synchronize()
            return true
        }
        return false
    }
}


extension String {
    var condensedWhitespace: String {
        let components = self.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }
    
    func removeSpecialCharacters() -> String {
        let okayChars = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890 ")
        return String(self.unicodeScalars.filter { okayChars.contains($0) || $0.properties.isEmoji })
    }
    
//    var isValidContact: Bool {
//        let phoneNumberRegex = "^[6-9]\\d{9}$"
//        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
//        let isValidPhone = phoneTest.evaluate(with: self)
//        return isValidPhone
//    }
    
    var isValidContact: Bool {
        let phoneNumberRegex = "^[6-8]\\d{8}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        let isValidPhone = phoneTest.evaluate(with: self)
        return isValidPhone
    }
    
    var nricVal: Bool {
        let phoneNumberRegex = "^[fgmstFGMST]\\d{7}[a-zA-Z]$"
        //let phoneNumberRegex = "^[STFGstfg]\\d{7}[a-zA-Z]$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        let isValidPhone = phoneTest.evaluate(with: self)
        return isValidPhone
    }
}

extension Dictionary {
    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
    func printJson() {
        print(json)
    }
}


extension UIView {
    func setRadiusWithShadow(_ radius: CGFloat? = nil) { // this method adds shadow to right and bottom side of button
        self.layer.cornerRadius = radius ?? self.frame.width / 2
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        self.layer.shadowRadius = 1.0
        self.layer.shadowOpacity = 0.7
        self.layer.masksToBounds = false
    }
    
    func setAllSideShadow(shadowShowSize: CGFloat = 1.0) { // this method adds shadow to allsides
        let shadowSize : CGFloat = shadowShowSize
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: -shadowSize / 2,
                                                   width: self.frame.size.width + shadowSize,
                                                   height: self.frame.size.height + shadowSize))
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.withAlphaComponent(0.8).cgColor
        //    self.layer.shadowColor = UIColor.appDropShadowColor.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowPath = shadowPath.cgPath
    }
}


extension UIView {
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = .zero
        layer.shadowRadius = 3
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}

extension UIView {
    func addshadow(top: Bool,
                   left: Bool,
                   bottom: Bool,
                   right: Bool,
                   shadowRadius: CGFloat = 2.0,shadowOpacity: CGFloat) {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = Float(shadowOpacity)
        self.layer.cornerRadius = 5
        let path = UIBezierPath()
        var x: CGFloat = 0
        var y: CGFloat = 0
        var viewWidth = self.frame.width
        var viewHeight = self.frame.height
        if (!top) {
            y+=(shadowRadius+1)
        }
        if (!bottom) {
            viewHeight-=(shadowRadius+1)
        }
        if (!left) {
            x+=(shadowRadius+1)
        }
        if (!right) {
            viewWidth-=(shadowRadius+1)
        }
        path.move(to: CGPoint(x: x, y: y))
        path.addLine(to: CGPoint(x: x, y: viewHeight))
        path.addLine(to: CGPoint(x: viewWidth, y: viewHeight))
        path.addLine(to: CGPoint(x: viewWidth, y: y))
        path.close()
        self.layer.shadowPath = path.cgPath
    }
}

extension String {
    func isBlankOrEmpty() -> Bool {
        // Check empty string
        if self.isEmpty {
            return true
        }
        // Trim and check empty string
        return (self.trimmingCharacters(in: .whitespaces) == "")
    }
}




extension UITextField {
    
    enum PaddingSide {
        case left(CGFloat)
        case right(CGFloat)
        case both(CGFloat)
    }
    
    func addPadding(_ padding: PaddingSide) {
        
        self.leftViewMode = .always
        self.layer.masksToBounds = true
        
        
        switch padding {
            
        case .left(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.rightViewMode = .always
            
        case .right(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.rightView = paddingView
            self.rightViewMode = .always
            
        case .both(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            // left
            self.leftView = paddingView
            self.leftViewMode = .always
            // right
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }

    //    How to use
    //    1.  To add left padding
    //    yourTextFieldName.addPadding(.left(20))
    //
    //    2.  To add right padding
    //    yourTextFieldName.addPadding(.right(20))
    //
    //   3. To add left & right padding both
    //    yourTextFieldName.addPadding(.both(20))
}
