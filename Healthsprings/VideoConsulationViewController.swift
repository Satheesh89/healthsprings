//
//  VideoConsulationViewController.swift
//  Healthsprings
//
//  Created by Personal on 19/09/22.
//

import UIKit

class VideoConsulationViewController: UIViewController {
    
    @IBOutlet weak var videoTableView: UITableView!

    var getconfirmdetails =  [confirmDetails]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setStatusBar(color: UIColor.videoConsultationbgColor)
        tableViewSetup()
    }
    

    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func didTapSwipeAction(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Tabbar", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ItemisedBillViewController") as! ItemisedBillViewController
        let transition = CATransition()
        transition.duration = 1.0
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromTop
        self.navigationController?.view.layer.add(transition, forKey: nil)
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    @IBAction func didTapHomeAction(_ sender: Any) {
        DispatchQueue.main.async {
            let tabStoryboard = UIStoryboard(name: "Tabbar", bundle: nil)
            let Permissionvc = tabStoryboard.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
            UIApplication.shared.windows.first?.rootViewController = Permissionvc
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }
    
    //MARK: TableView Setup
    func tableViewSetup()  {
        videoTableView.register(VideoConsulationCompletedCell.nib, forCellReuseIdentifier: VideoConsulationCompletedCell.identifier)
        videoTableView.register(PrescriptionMCCell.nib, forCellReuseIdentifier: PrescriptionMCCell.identifier)
        videoTableView.register(ConsultationBillCell.nib, forCellReuseIdentifier: ConsultationBillCell.identifier)
        videoTableView.delegate = self
        videoTableView.dataSource = self
        videoTableView.tableFooterView = UIView()
        videoTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        videoTableView.showsHorizontalScrollIndicator = false
        videoTableView.showsVerticalScrollIndicator = false
        videoTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        videoTableView.backgroundColor = UIColor.videoConsultationbgColor
        videoTableView.contentInsetAdjustmentBehavior = .never
        videoTableView.contentInsetAdjustmentBehavior = .never
    }
}

extension VideoConsulationViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            return videoConsulationTableviewCompletedCell(tableView: tableView, indexPath: indexPath)
        }else if indexPath.row == 1{
            return prescriptionMCtableViewCell(tableView: tableView, indexPath: indexPath)
        }  else {
            return consultationBilltableViewCell(tableView: tableView, indexPath: indexPath)
        }
    }

    func videoConsulationTableviewCompletedCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VideoConsulationCompletedCell.identifier) as? VideoConsulationCompletedCell else {
            return UITableViewCell()
        }
        cell.dateandtimeLabel.text = "Your Appointment : \("\(getconfirmdetails.first?.time ?? "")") \(getconfirmdetails.first?.date ?? "")"
        return cell
    }
    
    func prescriptionMCtableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PrescriptionMCCell.identifier) as? PrescriptionMCCell else {
            return UITableViewCell()
        }
        cell.historyButton.addTarget(self, action: #selector(self.didTapHistoryAction), for: .touchUpInside)
        cell.nameLabel.text = UserDetails.shared.userName ?? ""
        return cell
    }
    
    @objc func didTapHistoryAction(sender : UIButton) {
        doOnMain {
            //            self.tabBarController?.selectedIndex = 2
            DispatchQueue.main.async {
                let tabStoryboard = UIStoryboard(name: "Tabbar", bundle: nil)
                let Permissionvc = tabStoryboard.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
                Permissionvc.selectedIndex = 2
                UIApplication.shared.windows.first?.rootViewController = Permissionvc
                UIApplication.shared.windows.first?.makeKeyAndVisible()
            }
        }
    }
    
    
    func consultationBilltableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ConsultationBillCell.identifier) as? ConsultationBillCell else {
            return UITableViewCell()
        }
        cell.homeButton.addTarget(self, action: #selector(didTapHomeAction), for: .touchUpInside)
        cell.swipeButton.addTarget(self, action: #selector(didTapSwipeAction), for: .touchUpInside)
        
        if getconfirmdetails.first?.currency ?? "" == "SGD"{
            cell.priceLabel.text = "S$ \(getconfirmdetails.first?.consultationfee ?? "")"
        }else if getconfirmdetails.first?.currency ?? "" == "USD"{
            cell.priceLabel.text = "$ \(getconfirmdetails.first?.consultationfee ?? "")"
        }else{
            cell.priceLabel.text = "$ \(getconfirmdetails.first?.consultationfee ?? "")"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
