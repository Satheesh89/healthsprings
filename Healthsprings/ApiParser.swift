//
//  ApiParser.swift
//  Healthsprings
//
//  Created by Personal on 01/07/22.
//

import Foundation
import UIKit


class ApiParser: NSObject {
    
    func apiCallWitoutActivity(url:String, parameter:String, methodType: String, isActivity: Bool, CompletionHandler:@escaping(NSDictionary) ->()) {
        if GlobalClass.sharedInstance.hasConnectivity(){
            let encodeURL : String = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            var headers = NSDictionary()
            if isActivity {
                GlobalClass.sharedInstance.activity()
            }
            headers = ["Accept-Language": "en",
                "apiToken" : UserDetails.shared.getApiToken(),
                "Accept" : "application/json",
                "Content-Type" : "application/x-www-form-urlencoded"]
            //print(headers)
            let sampleCall2 : SSHTTPClient = SSHTTPClient(url: encodeURL , method: methodType, httpBody:parameter, headerFieldsAndValues: headers as? [String : String])
            sampleCall2.getJsonData { (obj, error) -> Void in
                if isActivity {
                    GlobalClass.sharedInstance.removeActivity()
                    //                    GlobalClass.sharedInstance.hideLoading()
                    
                }
                if error != nil {
                    //print(error ?? "no error")
                }else {
                    //print(obj ?? "")
                    let dataDict = obj as! NSDictionary
                    //print(dataDict.object(forKey:"status") ?? "")
                    if dataDict.object(forKey:"status") as! Int == 200 || dataDict.object(forKey:"status") as! Int == 419 {
                        CompletionHandler(dataDict)
                    } else {
                        DispatchQueue.main.async {
//                            self.showToast(message:dataDict.object(forKey:"message") as! String)
                        }
                    }
                }
            }
        } else {
            //            self.showToast(message:"Please check your internet connection")
            GlobalClass.sharedInstance.popUpView()
        }
    }
    
    func apiCall(url:String, parameter:String, methodType: String, ishidden: Bool,viewController: UIViewController, CompletionHandler:@escaping(NSDictionary) ->()) {
        if GlobalClass.sharedInstance.hasConnectivity(){
            //print("Call func")
            let encodeURL : String = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print(encodeURL)
            print(parameter)
            var headers = NSDictionary()
            
            if ishidden == true{
            }else{
                GlobalClass.sharedInstance.activity()
                //                GlobalClass.sharedInstance.showLoading()
            }
            headers = ["Accept-Language": "en",
                       "apiToken" : UserDetails.shared.getApiToken(),
                       "Accept" : "application/json",
                       "Content-Type" : "application/x-www-form-urlencoded"]
            //print(headers)
            
            let sampleCall2 : SSHTTPClient = SSHTTPClient(url: encodeURL , method: methodType, httpBody:parameter, headerFieldsAndValues: headers as? [String : String])
            sampleCall2.getJsonData { (obj, error) -> Void in
                
                if ishidden == true{
                }else{
                    GlobalClass.sharedInstance.removeActivity()
                    //GlobalClass.sharedInstance.hideLoading()
                }
                
                if error != nil {
                    //print(error ?? "no error")
                } else {
                    
                    //print(obj ?? "")
                    let dataDict = obj as? NSDictionary ?? [:]
                    print(dataDict)
                    if dataDict.object(forKey:"status") as? Int ?? 0 == 200 || dataDict.object(forKey:"status") as? Int ?? 0 == 419 {
                        CompletionHandler(dataDict)
                    } else {
                        DispatchQueue.main.async {
//                            self.showToast(message:dataDict.object(forKey:"message") as? String ?? "")
                        }
                    }
                }
            }
        } else {
            //self.showToast(message:"Please check your internet connection")
            GlobalClass.sharedInstance.popUpView()
        }
    }
    func apiCallRaw(url:String, parameter:NSMutableDictionary, methodType: String,ishidden: Bool,viewController: UIViewController, CompletionHandler:@escaping(NSDictionary) ->()) {
        if GlobalClass.sharedInstance.hasConnectivity() {
            //print("Call func")
            let encodeURL : String = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            //print(encodeURL)
            //print(parameter)
            var headers = NSDictionary()
            //            GlobalClass.sharedInstance.showLoading()
            GlobalClass.sharedInstance.activity()
            headers = ["Accept-Language": "en",
                       "apiToken" : UserDetails.shared.getApiToken(),
                       "Accept" : "application/json",
                       "Content-Type" : "application/x-www-form-urlencoded"]
            //print(headers)
            
            let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
            var param: String? = ""
            if let aData = jsonData {
                param = String(data: aData, encoding: .utf8)
                //print(param ?? "")
            }
            
            let sampleCall2 : SSHTTPClient = SSHTTPClient(url: encodeURL , method: methodType, httpBody:param, headerFieldsAndValues: headers as? [String : String])
            sampleCall2.getJsonData { (obj, error) -> Void in
                GlobalClass.sharedInstance.removeActivity()
                //                GlobalClass.sharedInstance.hideLoading()
                if error != nil {
                    //print(error ?? "no error")
                }else {
                    //print(obj ?? "")
                    let dataDict = obj as! NSDictionary
                    //print(dataDict.object(forKey:"status") ?? "")
                    if dataDict.object(forKey:"status") as! Int == 200 || dataDict.object(forKey:"status") as! Int == 419 {
                        CompletionHandler(dataDict)
                    } else {
                        DispatchQueue.main.async {
//                            self.showToast(message:dataDict.object(forKey:"message") as! String)
                        }
                    }
                }
            }
        } else {
            //            self.showToast(message:"Please check your internet connection")
            GlobalClass.sharedInstance.popUpView()
        }
    }
    
    //MARK:- Upload Multipart data
    func apiCallMultipart(url:String, parameter:[String: String]?, methodType: String, image:UIImage, CompletionHandler:@escaping(NSDictionary) ->()) {
//        GlobalClass.sharedInstance.showLoading()
        let encodeURL : String = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        //print(parameter ?? [:])
        //print(encodeURL)
        
        let url = URL(string:encodeURL);
        let request = NSMutableURLRequest(url: url!);
        request.httpMethod = "POST"
        let boundary = "Boundary-\(NSUUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = ["Accept-Language": "en",
                                       "apiToken" : UserDetails.shared.getApiToken(),
                                       "Accept" : "application/json",
                                       "Content-Type" : "application/x-www-form-urlencoded"]
        let imageData = image.jpegData(compressionQuality: 0.50)
        if (imageData == nil) {
            //print("UIImageJPEGRepresentation return nil")
            return
        }
        let body = NSMutableData()
        if parameter!.count > 0 {
            //        if parameter != nil {
            for (key, value) in parameter! {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        
        body.append(NSString(format:"Content-Disposition: form-data; name=\"profile_image\"; filename=\"testfromios.jpg\"\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "Content-Type: application/octet-stream\r\n\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(imageData!)
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        
        request.httpBody = body as Data
        
        let task =  URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            (data, response, error) -> Void in
            //            GlobalClass.sharedInstance.removeActivity() //Remove Activity
//            GlobalClass.sharedInstance.hideLoading()
            if let data = data {
                ////print(response as Any)
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                
                let dataDict = json as! NSDictionary
                //print(dataDict)
                //print(dataDict.object(forKey:"status") ?? "")
                
                if dataDict.object(forKey:"status") as! Int == 200 {
                    CompletionHandler(dataDict)
                } else {
                    DispatchQueue.main.async {
//                        self.showToast(message:dataDict.object(forKey:"message") as! String)
                    }
                }
            } else if let error = error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
}
