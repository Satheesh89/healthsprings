import UIKit
class CategoryCell: UICollectionViewCell {
    static let identifier = "CategoryCell"
    @IBOutlet weak var catImageView: UIImageView!
    @IBOutlet weak var titleLabe: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    func setup(catDict: [NSDictionary], selectIndexPath: IndexPath) {
        catImageView.image = UIImage(named: catDict[selectIndexPath.row].value(forKey: "image") as! String)
        titleLabe.text = "\(catDict[selectIndexPath.row].value(forKey: "text") ?? "")"
        detailLabel.text = "\(catDict[selectIndexPath.row].value(forKey: "details") ?? "")"
    }
}
