//
//  JoinCallViewController.swift
//  Healthsprings
//
//  Created by Personal on 19/09/22.
//

import UIKit

class JoinCallViewController: BaseViewController {
    var getconfirmdetails =  [confirmDetails]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: - Back
    @IBAction func didTapLeftAction(_ sender: Any) {
        self.navigationController?.popToViewController(ofClass: WaitingViewController.self)
    }
    
    @IBAction func didTapVerifyAction(_ sender: Any) {
        DispatchQueue.main.async {
//            let mainstoryboard = UIStoryboard(name: "Profile", bundle: nil)
//            let mainview = mainstoryboard.instantiateViewController(withIdentifier: "VideoConsulationViewController") as! VideoConsulationViewController
//            self.navigationController?.pushViewController(mainview, animated: false)
            let controller = UIStoryboard.init(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "Conference") as! ConferenceViewController
            controller.details = self.getconfirmdetails
            controller.clientUrl = videoSocketURL
            controller.roomId = ""
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }

}
