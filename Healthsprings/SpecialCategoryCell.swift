//
//  DIshLandscapeCollectionViewCell.swift
//  Yummie
//
//  Created by Emmanuel Okwara on 20/02/2021.
//

import UIKit

class SpecialCategoryCell: UICollectionViewCell {
    
    static let identifier = String(describing: SpecialCategoryCell.self)
    
    @IBOutlet weak var bgView: UIView!
    
//    func setup(dish: Dish) {
//        dishImageView.kf.setImage(with: dish.image?.asUrl)
//        titleLbl.text = dish.name
//        descriptionLbl.text = dish.description
//        caloriesLbl.text = dish.formattedCalories
//    }
    
    func setHorizontalGradientColor(view: UIView) {
        let gradientLayer = CAGradientLayer()
        var updatedFrame = view.bounds
        updatedFrame.size.height += 20
        gradientLayer.frame = updatedFrame
        gradientLayer.colors = [UIColor.white.cgColor, UIColor.themeColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.2)
        gradientLayer.endPoint = CGPoint(x: 4.0, y: 0.5)
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
}
