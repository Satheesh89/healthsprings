//
//  SingpassViewController.swift
//  Healthsprings
//
//  Created by Personal on 18/06/22.
//

import UIKit

class SingpassViewController: UIViewController {
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var qrImageView: UIImageView!
    @IBOutlet weak var dontLabel: UILabel!
    @IBOutlet weak var downloadButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Download
    @IBAction func didTapdownloadAction(_ sender: Any) {
    }
    
    // MARK: - Close
    @IBAction func didTapcloseAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}
