//
//  SignupViewController.swift
//  Healthsprings
//
//  Created by Personal on 16/06/22.
//

import UIKit
import SwiftyJSON
import CoreLocation
import CocoaTextField
import Alamofire

let ACCEPTABLE_CHARACTERS = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

class SignupViewController: UIViewController  {
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTextField: CustomTextField!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameTextField: CustomTextField!
    @IBOutlet weak var mobileView: UIView!
    @IBOutlet weak var mobileNoTextField: CustomTextField!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordTextField: CustomTextField!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var confirmPasswordTextField: CustomTextField!
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var signButton: UIButton!
    @IBOutlet weak var signView: UIView!
    @IBOutlet weak var nricView: UIView!
    @IBOutlet weak var nricTextfield: CustomTextField!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var addressTextfield: CustomTextField!
    
    @IBOutlet weak var addressTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var passwordTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var confirmPasswordTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var nricTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var nameBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var mobileTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var mobileBottomConstraint: NSLayoutConstraint!

    
    var getEmail: String = ""
    var getName: String = ""
    var passwordiconClick = true
    var confirmPasswordiconClick = true
    let countrycodepicker = UIPickerView()
    var countryCodeArray = NSArray()
    var countryModel: CountryModel!
    var countryCode: [Code] = []
    var locationManager:CLLocationManager!
    var passwordBool : Bool = true
    var confirmPasswordBool : Bool = true

    var emailRightViewImageView : UIImageView!
    var nameRightViewImageView : UIImageView!
    var mobileRightViewImageView : UIImageView!
    var nricRightViewImageView : UIImageView!
    var passwprdRightViewImageView : UIImageView!
    var confirmpasswprdRightViewImageView : UIImageView!
    var addressRightViewImageView : UIImageView!
    var countryListModel = [GetCountryList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initializeHideKeyboard()
        self.countryCodeTextField.delegate = self
        self.countryCodeTextField.inputView = self.countrycodepicker
        self.countryCodeTextField.tintColor = .clear
        self.countrycodepicker.dataSource = self
        self.countrycodepicker.delegate = self
        self.countrycodepicker.backgroundColor = .themeColor
        
        emailTextField.text = getEmail
//        self.emailTextField.isEditing = false
        nameTextField.text = getName
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        DispatchQueue.global().async {
            if CLLocationManager.locationServicesEnabled() {
                self.locationManager.startUpdatingLocation()
            }
        }
        //loadJsonData()
        mobileView.dropShadowWithCornerRaduis()
        
        getCardListApi( )
        emailSetup()
        usernameSetup()
        mobileSetup()
        nricSetup()
        passwordSetup()
        confirmSetup()
        addressSetup()
//        buttonSuccess()
    }
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    func emailSetup(){
        emailTextField.delegate = self
        emailTextField.placeholder = "ENTER EMAIL ID"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        emailRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 17.34, height: 12.63))
        emailRightViewImageView.image = UIImage(named: "email")
        outerView.addSubview(emailRightViewImageView)
        
        emailTextField.rightView = outerView
        emailTextField.rightViewMode = .always
        emailTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        emailTextField.addPadding(.left(18))
        emailTextField.backgroundColor = .white
        emailTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
    }
    
    func usernameSetup(){
        nameTextField.delegate = self
        nameTextField.placeholder = "Enter your full name"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        nameRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 14.97, height: 19.24))
        nameRightViewImageView.image = UIImage(named: "useraccount")
        outerView.addSubview(nameRightViewImageView)
        
        nameTextField.rightView = outerView
        nameTextField.rightViewMode = .always
        nameTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        nameTextField.addPadding(.left(18))
        nameTextField.backgroundColor = .white
        nameTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
    }
    
    func mobileSetup(){
        mobileNoTextField.delegate = self
        mobileNoTextField.placeholder = "Enter your mobile number"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        mobileRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 19, height: 19.21))
        mobileRightViewImageView.image = UIImage(named: "message")
        outerView.addSubview(mobileRightViewImageView)
        
        mobileNoTextField.rightView = outerView
        mobileNoTextField.rightViewMode = .always
        mobileNoTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        mobileNoTextField.addPadding(.left(18))
        mobileNoTextField.backgroundColor = .white
        mobileNoTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        
        doOnMain {
            self.mobileNoTextField.layer.masksToBounds = false
            self.mobileNoTextField.layer.shadowRadius = 4
            self.mobileNoTextField.layer.shadowOpacity = 0.5
            self.mobileNoTextField.layer.shadowColor = UIColor.whiteColor.cgColor
            //mobileNoTextField.layer.shadowOffset = CGSize(width: 0 , height:0.9)
            self.mobileNoTextField.layer.cornerRadius = 0
        }
    }
    
    
    func nricSetup(){
        nricTextfield.delegate = self
        nricTextfield.placeholder = "Enter your NRIC Number"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        nricRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 19, height: 19.21))
        nricRightViewImageView.image = UIImage(named: "noun-payment-1800088")
        nricRightViewImageView.contentMode = .scaleAspectFit
        outerView.addSubview(nricRightViewImageView)
        
        nricTextfield.rightView = outerView
        nricTextfield.rightViewMode = .always
        nricTextfield.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        nricTextfield.addPadding(.left(18))
        nricTextfield.backgroundColor = .white
        nricTextfield.errorFont = UIFont(name:"Lato-Medium", size: 12)!
        nricTextfield.autocapitalizationType = .allCharacters
    }
    
    func passwordSetup(){
        passwordTextField.delegate = self
        passwordTextField.placeholder = "Password"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        passwprdRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 20.68, height: 17))
        passwprdRightViewImageView.image = UIImage(named: "ic_password_hidden")
        outerView.addSubview(passwprdRightViewImageView)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(SignupViewController.passwordToggleAction))
        passwprdRightViewImageView.addGestureRecognizer(tap)
        passwprdRightViewImageView.isUserInteractionEnabled = true

        passwordTextField.rightView = outerView
        passwordTextField.rightViewMode = .always
        passwordTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        passwordTextField.addPadding(.left(18))
        passwordTextField.backgroundColor = .white
        passwordTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
    }
    
    @objc func passwordToggleAction(){
        if(passwordBool == true) {
            passwordTextField.isSecureTextEntry = false
            passwprdRightViewImageView.image = UIImage(named: "ic_password_hidden")
            passwprdRightViewImageView.setImageColor(color: .red)
        } else {
            passwordTextField.isSecureTextEntry = true
            passwprdRightViewImageView.image = UIImage(named: "ic_password_visible")
            passwprdRightViewImageView.setImageColor(color: .black)
        }
        passwordBool = !passwordBool
    }
    
    func confirmSetup(){
        confirmPasswordTextField.delegate = self
        confirmPasswordTextField.placeholder = "Confirm Password"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        confirmpasswprdRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 20.68, height: 17))
        confirmpasswprdRightViewImageView.image = UIImage(named: "ic_password_hidden")
        outerView.addSubview(confirmpasswprdRightViewImageView)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(SignupViewController.confirmPasswordToggleAction))
        confirmpasswprdRightViewImageView.addGestureRecognizer(tap)
        confirmpasswprdRightViewImageView.isUserInteractionEnabled = true

        
        confirmPasswordTextField.rightView = outerView
        confirmPasswordTextField.rightViewMode = .always
        confirmPasswordTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        confirmPasswordTextField.addPadding(.left(18))
        confirmPasswordTextField.backgroundColor = .white
        confirmPasswordTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
    }
    
    
    @objc func confirmPasswordToggleAction(){
        if(confirmPasswordBool == true) {
            confirmPasswordTextField.isSecureTextEntry = false
            confirmpasswprdRightViewImageView.image = UIImage(named: "ic_password_hidden")
            confirmpasswprdRightViewImageView.setImageColor(color: .red)
        } else {
            confirmPasswordTextField.isSecureTextEntry = true
            confirmpasswprdRightViewImageView.image = UIImage(named: "ic_password_visible")
            confirmpasswprdRightViewImageView.setImageColor(color: .black)
        }
        confirmPasswordBool = !confirmPasswordBool
    }
    
    func addressSetup(){
        addressTextfield.delegate = self
        addressTextfield.placeholder = "Enter your Address"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        addressRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 17.34, height: 12.63))
        addressRightViewImageView.image = UIImage(named: "location_pin")
        addressRightViewImageView.contentMode = .scaleAspectFill
        outerView.addSubview(addressRightViewImageView)
        
        addressTextfield.rightView = outerView
        addressTextfield.rightViewMode = .always
        addressTextfield.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        addressTextfield.addPadding(.left(18))
        addressTextfield.backgroundColor = .white
        addressTextfield.errorFont = UIFont(name:"Lato-Medium", size: 12)!
    }
    
    @objc func textFieldDidChange(sender: UITextField) {
        if sender == nricTextfield{
            nricTextfield.text = nricTextfield.text?.uppercased()
        }
        validateData(statusTextField: sender)
    }
    
    func buttonError() {
        DispatchQueue.main.async {
            self.signView.backgroundColor = .loginGeryColor
            self.signButton.tintColor = UIColor.convertRGB(hexString: "#A0A0A0")
            self.signButton.titleLabel?.textColor = UIColor.convertRGB(hexString: "#A0A0A0")
            self.signButton.setTitleColor(UIColor.convertRGB(hexString: "#A0A0A0"), for: .normal)
        }
    }
    
    func buttonSuccess(){
        self.signView.backgroundColor = .themeColor
        signButton.setTitleColor(.white, for: .normal)
    }
    
    func validateData(statusTextField: UITextField)  {
        if statusTextField == emailTextField {
            if emailTextField.text! == ""{
                buttonError()
                emailTextField.showError(errorString: emptyMessage)
                emailRightViewImageView.setImageColor(color: .red)
                nameBottomConstraint.constant = 30
            }else if  isValidEmail(emailID: emailTextField.text ?? "") == false {
                buttonError()
                emailTextField.showError(errorString: invaildEmailMessage)
                emailRightViewImageView.setImageColor(color: .red)
                nameBottomConstraint.constant = 30
            }else if emailTextField.text! !=  "" &&  isValidEmail(emailID: emailTextField.text ?? "") == true {
                emailRightViewImageView.setImageColor(color: .black)
                emailTextField.hideError()
                nameBottomConstraint.constant = 20
            }
            nameBottomConstraint.isActive = true
        }
        if statusTextField == nameTextField {
            if nameTextField.text! == ""{
                buttonError()
                nameTextField.showError(errorString: emptyMessage)
                nameRightViewImageView.setImageColor(color: .red)
                mobileTopConstraint.constant = 30
            }else if nameTextField.text?.count ?? 0 > 0{
                nameRightViewImageView.setImageColor(color: .black)
                nameTextField.hideError()
                mobileTopConstraint.constant = 20
            }
        }
        mobileTopConstraint.isActive = true

        if statusTextField == mobileNoTextField {
            if mobileNoTextField.text! == ""{
                buttonError()
                mobileNoTextField.showMobileError(errorString: phoneNoEmptyMessage)
                mobileRightViewImageView.setImageColor(color: .red)
                mobileView.layer.borderWidth = 1
                mobileView.layer.borderColor = UIColor.red.cgColor
                mobileBottomConstraint.constant = 30
            }   else if mobileNoTextField.text?.count ?? 0 < 8  {
                buttonError()
                mobileNoTextField.showMobileError(errorString: invaildPhoneNoMessage)
                mobileRightViewImageView.setImageColor(color: .red)
                mobileView.layer.borderWidth = 1
                mobileView.layer.borderColor = UIColor.red.cgColor
                mobileBottomConstraint.constant = 30
            }
            //else if "\(mobileNoTextField.text ?? "")".isValidContact == false  {
            //buttonError()
            //mobileNoTextField.showMobileError(errorString: invaildPhoneNoMessage)
            //mobileRightViewImageView.setImageColor(color: .red)
            //mobileView.layer.borderWidth = 1
            //mobileView.layer.borderColor = UIColor.red.cgColor
            //}
            else{
                if mobileNoTextField.text! !=  "" &&  mobileNoTextField.text?.count ?? 0 == 8 {
                    mobileRightViewImageView.setImageColor(color: .black)
                    mobileNoTextField.hideError()
                    mobileView.layer.borderWidth = 0
                    mobileView.layer.borderColor = UIColor.white.cgColor
                    mobileBottomConstraint.constant = 20
                }
                //if mobileNoTextField.text! !=  "" &&  "\(mobileNoTextField.text ?? "")".isValidContact == true {
                //mobileRightViewImageView.setImageColor(color: .black)
                //mobileNoTextField.hideError()
                //mobileView.layer.borderWidth = 0
                //mobileView.layer.borderColor = UIColor.white.cgColor
                //}
            }
            mobileBottomConstraint.isActive = true
        }
        if statusTextField == nricTextfield {
            if  nricTextfield.text! == "" {
                buttonError()
                nricTextfield.showError(errorString: emptyMessage)
                nricRightViewImageView.setImageColor(color: .red)
                passwordTopConstraint.constant = 30
            }else if "\(nricTextfield.text ?? "")".nricVal == false {
                buttonError()
                nricTextfield.showError(errorString: nricMessage)
                nricRightViewImageView.setImageColor(color: .red)
                passwordTopConstraint.constant = 60
            }else{
                if nricTextfield.text! !=  "" &&  "\(nricTextfield.text ?? "")".nricVal == true {
                    nricRightViewImageView.setImageColor(color: .black)
                    nricTextfield.hideError()
                    passwordTopConstraint.constant = 20
                }
            }
            passwordTopConstraint.isActive = true
        }
        if statusTextField == passwordTextField {
            if passwordTextField.text == ""{
                buttonError()
                passwordTextField.showError(errorString: emptyMessage)
                passwprdRightViewImageView.setImageColor(color: .red)
                confirmPasswordTopConstraint.constant = 30
            }else if "\(passwordTextField.text ?? "")".isValidPassword() == false  {
                buttonError()
                passwordTextField.showError(errorString: InvalidPasswordMessage)
                passwprdRightViewImageView.setImageColor(color: .red)
                confirmPasswordTopConstraint.constant = 60
            }
            else{
                if passwordTextField.text! !=  "" &&  "\(passwordTextField.text ?? "")".isValidPassword() == true  {
                    passwprdRightViewImageView.setImageColor(color: .black)
                    passwordTextField.hideError()
                    confirmPasswordTopConstraint.constant = 20
                }
            }
            confirmPasswordTopConstraint.isActive = true
        }
        if statusTextField == confirmPasswordTextField {
            if confirmPasswordTextField.text! == "" {
                buttonError()
                confirmPasswordTextField.showError(errorString: emptyMessage)
                confirmpasswprdRightViewImageView.setImageColor(color: .red)
                addressTopConstraint.constant = 20
            }
//            else if "\(confirmPasswordTextField.text ?? "")".isValidPassword() == false  {
//                buttonError()
//                confirmPasswordTextField.showError(errorString: InvalidPasswordMessage)
//                confirmpasswprdRightViewImageView.setImageColor(color: .red)
//                addressTopConstraint.constant = 40
//            }
            else{
                let password = self.confirmPasswordTextField.text == self.passwordTextField.text
                if confirmPasswordTextField.text! !=  "" &&  password == true  {
                    confirmpasswprdRightViewImageView.setImageColor(color: .black)
                    confirmPasswordTextField.hideError()
                    addressTopConstraint.constant = 30
                }else{
                    buttonError()
                    confirmPasswordTextField.showError(errorString: InvalidConfirmPasswordMessage)
                    confirmpasswprdRightViewImageView.setImageColor(color: .red)
                    addressTopConstraint.constant = 60
                }
            }
            addressTopConstraint.isActive = true
        }
        if statusTextField == addressTextfield {
            if addressTextfield.text! == "" {
                buttonError()
                addressTextfield.showError(errorString: emptyMessage)
                addressRightViewImageView.setImageColor(color: .red)
            }else{
                addressRightViewImageView.setImageColor(color: .black)
                addressTextfield.hideError()
            }
        }
        let password = self.confirmPasswordTextField.text == self.passwordTextField.text
        if emailTextField.text! != "" && isValidEmail(emailID: emailTextField.text ?? "") == true && nameTextField.text! != "" && mobileNoTextField.text! != "" && mobileNoTextField.text?.count ?? 0 == 8  && nricTextfield.text! != "" && "\(nricTextfield.text ?? "")".nricVal == true && passwordTextField.text != "" && "\(passwordTextField.text ?? "")".isValidPassword() == true && confirmPasswordTextField.text! != "" && password == true && addressTextfield.text! != "" {
            buttonSuccess()
        }else{
            buttonError()
        }
    }
    //MARK:- Email Check
    func isValidEmail(emailID:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailID)
    }
    
    func getCardListApi( ) {
        LoadingIndicator.shared.show(forView: view)
        AF.request( "\(URL.getCountrylist)").response { response in
            if let data = response.data {
                do{
                    let userResponse = try JSONDecoder().decode([GetCountryList].self, from: data)
                    self.countryListModel.removeAll()
                    self.countryListModel.append(contentsOf: userResponse)
                    doOnMain {
                        LoadingIndicator.shared.hide()
                    }
                }catch let err{
                    print(err.localizedDescription)
                }
            }
        }
    }
}

// MARK: - Action
extension SignupViewController{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileNoTextField {
            return range.location < 8
        }else if textField == nricTextfield{
            return range.location < 9
        }else if textField == nameTextField{
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let Regex = "[a-z A-Z ]+"
                let predicate = NSPredicate.init(format: "SELF MATCHES %@", Regex)
                if predicate.evaluate(with: text) || string == ""{
                    //textField.text = textField.text?.replacingOccurrences(of: " ", with: "")
                    return true
                }
                else{
                    return false
                }
        }
        return true
    }
    
    //    func textFieldDidEndEditing(_ textField: UITextField) {
    //        buttonStatus()
    //    }
    
    @IBAction func didTapiconAction(sender: UIButton) {
        if sender.tag == 100{
            if(passwordiconClick == true) {
                passwordTextField.isSecureTextEntry = false
            } else {
                passwordTextField.isSecureTextEntry = true
            }
            passwordiconClick = !passwordiconClick
        }else if sender.tag == 101{
            if(confirmPasswordiconClick == true) {
                confirmPasswordTextField.isSecureTextEntry = false
            } else {
                confirmPasswordTextField.isSecureTextEntry = true
            }
            confirmPasswordiconClick = !confirmPasswordiconClick
        }
    }
    
    @IBAction func didTapSignupAction(sender: UIButton) {
//        validateData()
        signUpApiCall()
    }
    
    // MARK: - SIgnup Api Call
    func signUpApiCall(){
        LoadingIndicator.shared.show(forView: self.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["patient_name"] = self.nameTextField.text ?? ""
        paramsDictionary["patient_email"] = self.emailTextField.text ?? ""
        paramsDictionary["mobile_code"] = self.countryLabel.text ?? ""
        paramsDictionary["mobile_number"] = self.mobileNoTextField.text ?? ""
        paramsDictionary["nric_id"] = self.nricTextfield.text ?? ""
        paramsDictionary["password"] = self.passwordTextField.text ?? ""
        paramsDictionary["password_confirmation"] = self.confirmPasswordTextField.text ?? ""
        paramsDictionary["full_address"] = self.addressTextfield.text ?? ""
        
        HttpClientApi.instance().makeAPICall(url: URL.registerpatient, params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let response = response
                if  response?.statusCode == 200 {
                    doOnMain {
                        //self.setFrontLoginView()
//                        self.setupOtp(getMobileNo: self.mobileNoTextField.text ?? "", getName: self.nameTextField.text ?? "", getEmail: self.emailTextField.text ?? "")
                        delay(0.1) {
                            LoadingIndicator.shared.hide()
                            self.setFrontAccountView()
                        }
                    }
                }else  if response?.statusCode == 404 {
                    self.delayWithHide()
                    let dataDict = json as! NSDictionary
                    print(dataDict.object(forKey:"error") as? String ?? "")
                    DispatchQueue.main.async {
                        self.showCustomToast(toastTitle: "error",toastMeassage: "error",status: 1)
                    }
                }else if  response?.statusCode == 401 {
                    self.delayWithHide()
                    let tempArray = NSMutableArray()
                    let dataDict = json as! NSDictionary
                    for  i in 0..<((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.count ?? 0) {
                        let tArray  = ((dataDict.value(forKeyPath: "field_error")as? NSDictionary)?.allValues[i]) as? Array<Any>
                        tempArray.add(tArray?.first ?? "")
                    }
                    let errorStr = tempArray.componentsJoined(by: ",")
                    CustomToast.show(message: errorStr,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }
                else{
                    self.delayWithHide()
                    let dataDict = json as! NSDictionary
                    CustomToast.show(message: dataDict["error"] as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
            self.delayWithHide()
            CustomToast.show(message: error as! String,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)

        })
    }
    
    func delayWithHide(){
        delay(0.1) {
            LoadingIndicator.shared.hide()
        }
    }
    
    func setFrontAccountView() {
        DispatchQueue.main.async {
            let mainstoryboard = UIStoryboard(name: "Menu", bundle: nil)
            let mainview = mainstoryboard.instantiateViewController(withIdentifier: "AccountSuccessfullyViewController") as! AccountSuccessfullyViewController
            mainview.getName = "\(self.nameTextField.text ?? "")"
            mainview.getMail = "\(self.emailTextField.text ?? "")"
            self.navigationController?.pushViewController(mainview, animated: false)
        }
    }
    
    
    @IBAction func didTapPasswordHideShowAction(sender: UIButton) {
        if(passwordBool == true) {
            passwordTextField.isSecureTextEntry = false
        } else {
            passwordTextField.isSecureTextEntry = true
        }
        passwordBool = !passwordBool
    }
    
    
    @IBAction func didTapConfirPasswordHideShowAction(sender: UIButton) {
        if(confirmPasswordBool == true) {
            confirmPasswordTextField.isSecureTextEntry = false
        } else {
            confirmPasswordTextField.isSecureTextEntry = true
        }
        confirmPasswordBool = !confirmPasswordBool
    }
    
    func setFrontLoginView() {
        let mainStoryBoard = UIStoryboard(name: "Menu", bundle: nil)
        let loginVC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        UIApplication.shared.windows.first?.rootViewController = loginVC
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    
    // MARK: - Tabbar Action
    func setTabPage(){
        let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
        let registerPage = menuStoryboard.instantiateViewController(withIdentifier: "OtpViewController") as! OtpViewController
        registerPage.getMobile = emailTextField.text ?? ""
        self.navigationController?.pushViewController(registerPage, animated: false)
    }
    
    // MARK: - Button Status
    func buttonStatus(){
        if "\(emailTextField.text ?? "")".isValidEmail() && "\(self.emailTextField.text ?? "")" != "" && "\(self.nameTextField.text ?? "")" != "" && "\(self.mobileNoTextField.text ?? "")" != "" && "\(self.passwordTextField.text ?? "")" != "" && "\(self.passwordTextField.text ?? "")" == "\(self.confirmPasswordTextField.text ?? "")"{
            signView.backgroundColor = .themeColor
            signButton.tintColor = .white
            signButton.titleLabel?.textColor = .white
            signButton.setTitleColor(.white, for: .normal)
        }else{
            signView.backgroundColor = .loginGeryColor
            signButton.tintColor = .loginLightGeryColor
            signButton.titleLabel?.textColor = .loginLightGeryColor
            signButton.setTitleColor(.loginLightGeryColor, for: .normal)
        }
    }
    
    
    //MARK: - location delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0] as CLLocation
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count>0{
                let placemark = placemarks![0]
                self.addressTextfield.text = "\(placemark.locality!), \(placemark.administrativeArea!), \(placemark.country!)"
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
}

extension SignupViewController:  UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.countryListModel.count
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(20.0))
            pickerLabel?.textAlignment = .center
        }
        pickerLabel?.text = "(+ \(countryListModel[row].country_phone_code ?? 0)) \(countryListModel[row].country ?? "")"
        pickerLabel?.textColor = UIColor.white
        return pickerLabel!
    }
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.countryLabel.text = "+ \(self.countryListModel[row].country_phone_code ?? 0)"
    }
}
extension SignupViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            print("not determined - hence ask for Permission")
            manager.requestWhenInUseAuthorization()
        case .restricted, .denied:
            print("permission denied")
        case .authorizedAlways, .authorizedWhenInUse:
            print("Apple delegate gives the call back here once user taps Allow option, Make sure delegate is set to self")
        }
    }
}


extension SignupViewController {
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == confirmPasswordTextField {
            let password = self.confirmPasswordTextField.text == self.passwordTextField.text
            if password == false {
                buttonError()
                confirmPasswordTextField.showError(errorString: InvalidConfirmPasswordMessage)
                confirmpasswprdRightViewImageView.setImageColor(color: .red)
            }else{
                confirmpasswprdRightViewImageView.setImageColor(color: .black)
                confirmPasswordTextField.hideError()
            }
        }
    }
    
}
