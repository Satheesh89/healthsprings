//
//  SearchDoctorModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 02, 2022
//
import Foundation
import SwiftyJSON

struct SearchDoctorModel {

	let currentPage: Int?
	var searchData: [SearchData]?
	let firstPageUrl: String?
	let from: Int?
	let lastPage: Int?
	let lastPageUrl: String?
	let nextPageUrl: Any?
	let path: String?
	let perPage: String?
	let prevPageUrl: Any?
	let to: Int?
	let total: Int?

	init(_ json: JSON) {
		currentPage = json["current_page"].intValue
        searchData = json["data"].arrayValue.map { SearchData($0) }
		firstPageUrl = json["first_page_url"].stringValue
		from = json["from"].intValue
		lastPage = json["last_page"].intValue
		lastPageUrl = json["last_page_url"].stringValue
		nextPageUrl = json["next_page_url"]
		path = json["path"].stringValue
		perPage = json["per_page"].stringValue
		prevPageUrl = json["prev_page_url"]
		to = json["to"].intValue
		total = json["total"].intValue
	}

}
