//
//  LoginVC.swift
//  Healthsprings
//
//  Created by Personal on 19/06/22.
//

import UIKit
import Alamofire
import CocoaTextField

class LoginVC: UIViewController {
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTextField: CustomTextField!
    @IBOutlet weak var passwordTextField: CustomTextField!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    private var authViewModel = AuthViewModel()

    var getEmail: String = ""
    var cursorBool: Bool = false
    
    @IBOutlet weak var emailBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var passwordBottomConstraint: NSLayoutConstraint!
    
    var emailRightViewImageView : UIImageView!
    var passwprdRightViewImageView : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initializeHideKeyboard()
        emailSetup()
        passwordSetup()
        buttonError()
        emailTextField.text = getEmail
        passwordTextField.becomeFirstResponder()
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        // Hide the Navigation Back button
        self.navigationItem.hidesBackButton = true
    }
    
    // MARK: - Forgot
    @IBAction func didTapforgotAction(_ sender: Any) {
        let forgotPasswardVC = ForgotPasswordViewController.instantiateFromAppStoryboard(appStoryboard: .Menu)
        forgotPasswardVC.hidesBottomBarWhenPushed = true
        forgotPasswardVC.emailStr = emailTextField.text ?? ""
        self.navigationController?.pushViewController(forgotPasswardVC, animated: false)
    }
    
    // MARK: - Login
    @IBAction func didTapLoginAction(_ sender: Any) {
        if emailTextField.text! != "" && isValidEmail(emailID: emailTextField.text ?? "") == true && passwordTextField.text != ""{
            buttonSuccess()
            loginApiCall()
        }else{
            buttonError()
        }
    }
    
    // MARK: - Login Api Call
    func loginApiCall(){
        let params: Parameters = [
            "email": self.emailTextField.text ?? "",
            "password": self.passwordTextField.text ?? ""]
        authViewModel.requestNormalLogin(input: params) { (result: AuthModel?, alert: AlertMessage?) in
            if let result = result {
                if result.success != nil {
                    print("refresh_token",result.success?.refresh_token ?? "")
                    UserDetails.shared.setUserLoginData(data: try! JSONEncoder().encode(result))
                    UserDetails.shared.setUserApiToken(apiTK: result.success?.token ?? "")
                    self.setTabPage()
                }else{
                }
            } else if let alert = alert {
                print(alert.statusCode)
                print(alert.errorMessage)
                DispatchQueue.main.async {
                    CustomToast.show(message: alert.errorMessage,bgColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.6),showIn: .bottom, controller: self,tarbarHeight: 0.0)
                }
            }
        }
        
        //        var paramsDictionary = [String:Any]()
        //        paramsDictionary["email"] = self.emailTextField.text ?? ""
        //        paramsDictionary["password"] = self.passwordTextField.text ?? ""
        //        HttpClientApi.instance().makeAPICall(url: URL.login, params:paramsDictionary, method: .POST, success: { (data, response, error) in
        //            if let data = data {
        //                let json = try? JSONSerialization.jsonObject(with: data, options: [])
        //                let response = response
        //                if  response?.statusCode == 200 {
        //                    let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
        //                    if let responseJSON = responseJSON as? NSDictionary{
        ////                        UserDefaults.standard.removeObject(forKey: Key.UserDefaults.userDetails)
        ////                        UserInformation().saveUserData(userDateDict: responseJSON )
        //                        UserDetails.shared.setUserLoginData(data: try! JSONEncoder().encode(data))
        //
        //                    }
        //                    self.setTabPage()
        //                }else  if response?.statusCode == 404 {
        //                    let dataDict = json as! NSDictionary
        //                    print(dataDict.object(forKey:"error") as? String ?? "")
        //                    DispatchQueue.main.async {
        //                        self.showToast(message: dataDict.object(forKey:"error") as? String ?? "")
        //                    }
        //                }else  if response?.statusCode == 401 {
        //                    DispatchQueue.main.async {
        //                        self.showToast(message: "The password field is required.")
        //                    }
        //                }
        //            }
        //        }, failure: { (data, response, error) in
        //            // API call Failure
        //            DispatchQueue.main.async {
        //                self.showToast(message: "API call Failure")
        //            }
        //        })
    }
    
    // MARK: - Tabbar Action
    func setTabPage(){
        DispatchQueue.main.async {
            let tabStoryboard = UIStoryboard(name: "Tabbar", bundle: nil)
            let Permissionvc = tabStoryboard.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
            UIApplication.shared.windows.first?.rootViewController = Permissionvc
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }
}



extension LoginVC{
    
    func emailSetup(){
        emailTextField.delegate = self
        emailTextField.placeholder = "ENTER EMAIL ID"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        emailRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 17.34, height: 12.63))
        emailRightViewImageView.image = UIImage(named: "email")
        outerView.addSubview(emailRightViewImageView)
        
        emailTextField.rightView = outerView
        emailTextField.rightViewMode = .always
        emailTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        emailTextField.addPadding(.left(18))
        emailTextField.backgroundColor = .white
        emailTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
    }
    
    func passwordSetup(){
        passwordTextField.delegate = self
        passwordTextField.placeholder = "Password"
        let padding: CGFloat = 18
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: padding * 2 + 23, height: 11.5))
        passwprdRightViewImageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 19, height: 17))
        passwprdRightViewImageView.image = UIImage(named: "key")
        passwprdRightViewImageView.contentMode = .scaleAspectFit
        outerView.addSubview(passwprdRightViewImageView)
        
        passwordTextField.rightView = outerView
        passwordTextField.rightViewMode = .always
        passwordTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        passwordTextField.addPadding(.left(18))
        passwordTextField.backgroundColor = .white
        passwordTextField.errorFont = UIFont(name:"Lato-Medium", size: 12)!
    }
    
    @objc func textFieldDidChange(sender: UITextField) {
        validateData(statusTextField: sender)
    }
    
    func buttonError(){
        loginView.backgroundColor = .loginGeryColor
        loginButton.tintColor = UIColor.convertRGB(hexString: "#A0A0A0")
        loginButton.titleLabel?.textColor = UIColor.convertRGB(hexString: "#A0A0A0")
    }
    func buttonSuccess(){
        loginView.backgroundColor = .themeColor
        loginButton.tintColor = .white
        loginButton.titleLabel?.textColor = .white
        loginButton.setTitleColor(.white, for: .normal)
    }
    
    func validateData(statusTextField: UITextField) {
        if statusTextField == emailTextField {
            if emailTextField.text! == ""{
                buttonError()
                emailTextField.showError(errorString: emptyMessage)
                emailRightViewImageView.setImageColor(color: .red)
                emailBottomConstraint.constant = 30
            }else if  isValidEmail(emailID: emailTextField.text ?? "") == false {
                buttonError()
                emailTextField.showError(errorString: invaildEmailMessage)
                emailRightViewImageView.setImageColor(color: .red)
                emailBottomConstraint.constant = 30
            }else if emailTextField.text! !=  "" &&  isValidEmail(emailID: emailTextField.text ?? "") == true {
                emailRightViewImageView.setImageColor(color: .black)
                emailTextField.hideError()
                emailBottomConstraint.constant = 20
            }
            emailBottomConstraint.isActive = true
        }
        
        if statusTextField == passwordTextField {
            if passwordTextField.text == ""{
                buttonError()
                passwordTextField.showError(errorString: emptyMessage)
                passwprdRightViewImageView.setImageColor(color: .red)
                passwordBottomConstraint.constant = 30
            }
            else{
                passwprdRightViewImageView.setImageColor(color: .black)
                passwordTextField.hideError()
                passwordBottomConstraint.constant = 20
            }
            passwordBottomConstraint.isActive = true
        }
        
        if emailTextField.text! != "" && isValidEmail(emailID: emailTextField.text ?? "") == true && passwordTextField.text != ""{
            buttonSuccess()
        }else{
            buttonError()
        }
    }
  
    
    //MARK:- Email Check
    func isValidEmail(emailID:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailID)
    }
}
