//
//  RequestSummary.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on August 23, 2022
//
import Foundation
import SwiftyJSON

struct RequestSummary {

	let profileId: String?
	let Uuid: String?
	let bookingType: String?
	let type: String?
	let personName: String?
	let symptoms: String?
	let country: String?
	let streetName: String?
	let areaLocation: String?
	let cardNumber: Int?
	let expirationYear: Int?
	let expirationMonth: Int?
	let currency: String?
	let consultationFee: Int?

	init(_ json: JSON) {
		profileId = json["profile_id"].stringValue
		Uuid = json["Uuid"].stringValue
		bookingType = json["booking_type"].stringValue
		type = json["type"].stringValue
		personName = json["person_name"].stringValue
		symptoms = json["symptoms"].stringValue
		country = json["country"].stringValue
		streetName = json["street_name"].stringValue
		areaLocation = json["area_location"].stringValue
		cardNumber = json["card_number"].intValue
		expirationYear = json["expiration_year"].intValue
		expirationMonth = json["expiration_month"].intValue
		currency = json["currency"].stringValue
		consultationFee = json["consultation_fee"].intValue
	}

}