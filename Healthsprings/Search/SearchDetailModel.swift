//
//  SearchDetailModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 26, 2022
//
import Foundation
import SwiftyJSON

class SearchDetailModel {
  
	let search: Search?

	init(_ json: JSON) {
		search = Search(json["search"])
	}

}
