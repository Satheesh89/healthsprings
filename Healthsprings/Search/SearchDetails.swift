//
//  SearchDetails.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 26, 2022
//
import Foundation
import SwiftyJSON

class SearchDetails {

	let doctorName: String?
	let roomName: String?
	let email: String?
	let gender: String?
	let profileLink: String?
	let phone: String?
	let country: String?
	let avaliabilityStatus: Int?
	let description: String?
	let avatar: String?
	let uuid: Int?
	let specialty: String?
	let nextAvailDate: String?
	let nextAvailTime: String?
    let id: Int?

	init(_ json: JSON) {
		doctorName = json["doctor_name"].stringValue
		roomName = json["room_name"].stringValue
		email = json["email"].stringValue
		gender = json["gender"].stringValue
		profileLink = json["profile_link"].stringValue
		phone = json["phone"].stringValue
		country = json["country"].stringValue
		avaliabilityStatus = json["avaliability_status"].intValue
		description = json["description"].stringValue
		avatar = json["avatar"].stringValue
		uuid = json["uuid"].intValue
		specialty = json["specialty"].stringValue
		nextAvailDate = json["next_avail_date"].stringValue
		nextAvailTime = json["next_avail_time"].stringValue
        id = json["id"].intValue
	}

}
