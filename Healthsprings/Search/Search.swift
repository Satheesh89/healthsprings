//
//  Search.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 26, 2022
//
import Foundation
import SwiftyJSON

class Search {

	let searchDetails: [SearchDetails]?

	init(_ json: JSON) {
		searchDetails = json["search_details"].arrayValue.map { SearchDetails($0) }
	}

}