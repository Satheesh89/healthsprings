//
//  Data.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 02, 2022
//
import Foundation
import SwiftyJSON

struct SearchData {

	let id: Int?
	let doctorName: String?
	let roomName: String?
	let email: String?
	let gender: Any?
	let profileLink: String?
	let phone: Any?
	let country: Any?
	let avaliabilityStatus: Any?
	let description: Any?
	let avatar: String?
	let uuid: Int?
	let specialty: String?
	let nextAvailDate: String?
	let nextAvailTime: String?

	init(_ json: JSON) {
		id = json["id"].intValue
		doctorName = json["doctor_name"].stringValue
		roomName = json["room_name"].stringValue
		email = json["email"].stringValue
		gender = json["gender"]
		profileLink = json["profile_link"].stringValue
		phone = json["phone"]
		country = json["country"]
		avaliabilityStatus = json["avaliability_status"]
		description = json["description"]
		avatar = json["avatar"].stringValue
		uuid = json["uuid"].intValue
		specialty = json["specialty"].stringValue
		nextAvailDate = json["next_avail_date"].stringValue
		nextAvailTime = json["next_avail_time"].stringValue
	}

}
