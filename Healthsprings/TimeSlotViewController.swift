//
//  TimeSlotViewController.swift
//  Healthsprings
//
//  Created by Personal on 12/07/22.
//
import UIKit


class TimeSlotViewController: UIViewController {
    @IBOutlet weak var timeSlotTbleView: UITableView!
    var slotArray = NSArray()
    var selectSlotStr = String()

    var timeSlotViewModel = TimeSlotViewModel()
    var selectTimeSlotIndexpath : IndexPath = IndexPath()
    var selectSessionIndexpath : IndexPath = IndexPath()
    
    var sessionMorningBool: Bool = false
    var sessionAfternoonBool: Bool = false
    var sessionEveningBool: Bool = false
    var sessionNightBool: Bool = false

    var doctorName: String = ""
    var doctorDetail: String = ""
    var doctorExprDetail: String = ""
    var doctorId: Int = 0
    var uuidId: Int = 0

    var selectDate: String = ""
    var selectTIme: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        slotArray = ["6:00 PM","6:00 PM","6:00 PM","6:00 PM","6:00 PM","6:00 PM","6:00 PM","6:00 PM","6:00 PM","6:00 PM","6:00 PM","6:00 PM","6:00 PM","6:00 PM","6:00 PM","6:00 PM"]
        loadSetup()
        tableViewSetup()
    }
    
    func loadSetup(){
        timeSlotViewModel.vc = self
        timeSlotViewModel.getTimeSlotListApi(loadview: self, doctorid: doctorId, dateStr: "2022-12-07")
    }
    
    func tableViewSetup()  {
        setStatusBar(color: UIColor.themeColor)
        timeSlotTbleView.register(SettingHeaderTableViewCell.nib, forCellReuseIdentifier: SettingHeaderTableViewCell.identifier)
        timeSlotTbleView.register(TimeSlotHeaderCell.nib, forCellReuseIdentifier: TimeSlotHeaderCell.identifier)
        timeSlotTbleView.delegate = self
        timeSlotTbleView.dataSource = self
        timeSlotTbleView.tableFooterView = UIView()
        timeSlotTbleView.separatorStyle = UITableViewCell.SeparatorStyle.none
        timeSlotTbleView.showsHorizontalScrollIndicator = false
        timeSlotTbleView.showsVerticalScrollIndicator = true
        //serviceTableView.setContentOffset(.zero, animated: true)
        timeSlotTbleView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        timeSlotTbleView.contentInsetAdjustmentBehavior = .never
        timeSlotTbleView.backgroundColor = .clear
        timeSlotTbleView.contentInset.top = 0
        selectSlotStr = "Select date and time"
//        if timeSlotViewModel.availabilities?.count ?? 0 > 0{
//            selectSlotStr = "\(timeSlotViewModel.availabilities?[0].availDate ?? "") \(timeSlotViewModel.availabilities?[0].availToTime ?? "")"
//            selectTimeSlotIndexpath = IndexPath(item:0, section:0)
//        }else{
//
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //MARK: - Back
    @IBAction func didTapLeftAction(_ sender: Any) {
        self.navigationController?.popToViewController(ofClass: FindDoctorViewController.self)
    }
    
    
    @IBAction func didTapNextAction(_ sender: Any) {
        let makeRequestVC = MakeRequestViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        makeRequestVC.getDoctorUuid = "\(uuidId)"
        makeRequestVC.getDoctorName = doctorName
        makeRequestVC.getDoctorId = "\(doctorId)"
//        makeRequestVC.getBookingType = "video consultation"
        makeRequestVC.getDate = selectDate
        makeRequestVC.getTIme = selectTIme
        makeRequestVC.getCardId = 0
        makeRequestVC.getCartBool = false
        self.navigationController?.pushViewController(makeRequestVC, animated: false)
    }
}


class WorkSlotCell : UITableViewCell {
    @IBOutlet weak var TimeSlotBgview: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var lineLbl: UILabel!
    @IBOutlet weak var timeSlotCollectionView: UICollectionView!
    @IBOutlet weak var sortImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleLbl.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(11.0))
        titleLbl.textColor = .themeColor
        //lineLbl.backgroundColor = .themeColor
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

class WeekSlotCell : UITableViewCell {
    @IBOutlet weak var TimeSlotBgview: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var lineLbl: UILabel!
    @IBOutlet weak var timeSlotCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleLbl.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(11.0))
        titleLbl.textColor = .themeColor
        lineLbl.backgroundColor = .themeColor
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

extension TimeSlotViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 210
        }else{
            if indexPath.row == 1{
                return CGFloat((timeSlotViewModel.morningSection?.count ?? 0) / 3 * 41) + CGFloat((timeSlotViewModel.morningSection?.count ?? 0) % 3 == 0 ? 0 : 41)
            }else  if indexPath.row == 2{
                return CGFloat((timeSlotViewModel.afternoonSection?.count ?? 0) / 3 * 41) + CGFloat((timeSlotViewModel.afternoonSection?.count ?? 0) % 3 == 0 ? 0 : 41)
            }else  if indexPath.row == 3{
                return CGFloat((timeSlotViewModel.eveningSection?.count ?? 0) / 3 * 41) + CGFloat((timeSlotViewModel.eveningSection?.count ?? 0) % 3 == 0 ? 0 : 41)
            }else  if indexPath.row == 4{
                return CGFloat((timeSlotViewModel.nightSection?.count ?? 0) / 3 * 41) + CGFloat((timeSlotViewModel.nightSection?.count ?? 0) % 3 == 0 ? 0 : 41)
            }
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let workSlotCell = tableView.dequeueReusableCell(withIdentifier: "TimeSlotHeaderCell") as! TimeSlotHeaderCell
            workSlotCell.selectionStyle = UITableViewCell.SelectionStyle.none
            workSlotCell.slotCollectionView.register(UINib(nibName: "TimeSlotCollectionCell", bundle: .main), forCellWithReuseIdentifier: "TimeSlotCollectionCell")
            workSlotCell.selectTimeLabel.text = selectSlotStr
            workSlotCell.slotCollectionView.delegate = self
            workSlotCell.slotCollectionView.dataSource = self
            workSlotCell.slotCollectionView.tag = 100
            workSlotCell.slotCollectionView.reloadData()
            workSlotCell.doctorNameLabel.text = doctorName
            workSlotCell.specialtyLabel.text = doctorDetail
            workSlotCell.yearLabel.text = doctorExprDetail
            return workSlotCell
        }else{
            let workSlotCell = tableView.dequeueReusableCell(withIdentifier: "WorkSlotCell") as! WorkSlotCell
            workSlotCell.selectionStyle = UITableViewCell.SelectionStyle.none
            workSlotCell.TimeSlotBgview.backgroundColor = .white
            workSlotCell.tag = 10020
            workSlotCell.timeSlotCollectionView.backgroundColor = .white
            workSlotCell.timeSlotCollectionView.register(UINib(nibName: "TimeCell", bundle: .main), forCellWithReuseIdentifier: "MyTimeCell")
            if #available(iOS 11.0, *) { workSlotCell.timeSlotCollectionView.contentInsetAdjustmentBehavior = .never }
            else { automaticallyAdjustsScrollViewInsets = false }
            if indexPath.row == 1 {
                workSlotCell.titleLbl.text = "Morning"
                workSlotCell.sortImageView.image = UIImage(named: "morning")
                if  Int(slotArray.count) == 0 {
                    workSlotCell.lineLbl.isHidden = false
                }else{
                    workSlotCell.lineLbl.isHidden = true
                }
                workSlotCell.timeSlotCollectionView.tag = 1
            }else if indexPath.row == 2 {
                workSlotCell.titleLbl.text = "Afternoon"
                workSlotCell.sortImageView.image = UIImage(named: "afternoon")
                if  Int(slotArray.count) == 0 {
                    workSlotCell.lineLbl.isHidden = false
                }else{
                    workSlotCell.lineLbl.isHidden = true
                }
                workSlotCell.timeSlotCollectionView.tag = 2
            }else if indexPath.row == 3 {
                workSlotCell.titleLbl.text = "Evening"
                workSlotCell.sortImageView.image = UIImage(named: "moon")
                if  Int(slotArray.count) == 0 {
                    workSlotCell.lineLbl.isHidden = false
                }else{
                    workSlotCell.lineLbl.isHidden = true
                }
                workSlotCell.timeSlotCollectionView.tag = 3
            }else if indexPath.row == 4 {
                workSlotCell.titleLbl.text = "Night"
                workSlotCell.sortImageView.image = UIImage(named: "nightmoon")
                if  Int(slotArray.count) == 0 {
                    workSlotCell.lineLbl.isHidden = false
                }else{
                    workSlotCell.lineLbl.isHidden = true
                }
                workSlotCell.timeSlotCollectionView.tag = 4
            }
            workSlotCell.titleLbl.textColor = UIColor.convertRGB(hexString: "#000000")
            if ( UIDevice.current.model.range(of: "iPad") != nil){
                workSlotCell.titleLbl.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(12.0))
            } else {
                workSlotCell.titleLbl.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(14.0))
            }
            workSlotCell.timeSlotCollectionView.dataSource = self
            workSlotCell.timeSlotCollectionView.delegate = self
            workSlotCell.timeSlotCollectionView.reloadData()
            return workSlotCell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

extension TimeSlotViewController: UICollectionViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 100{
            return timeSlotViewModel.availabilities?.count ?? 0
        }else{
            if collectionView.tag == 1{
                return timeSlotViewModel.morningSection?.count ?? 0
            }else if collectionView.tag == 2{
                return timeSlotViewModel.afternoonSection?.count ?? 0
            }else if collectionView.tag == 3{
                return timeSlotViewModel.eveningSection?.count ?? 0
            }else if collectionView.tag == 4{
                return timeSlotViewModel.nightSection?.count ?? 0
            }
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 100{
            let cell : TimeSlotCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeSlotCollectionCell", for: indexPath as IndexPath) as! TimeSlotCollectionCell
            cell.selectIndexImage(getIndexpath: selectTimeSlotIndexpath,normalIndex: indexPath)
            cell.titleLbl.text = timeSlotViewModel.availabilities?[indexPath.row].availDate
            cell.detailLbl.text = "\(timeSlotViewModel.availabilities?[indexPath.row].availToTime ?? "")"
            
//            let slotMCount: Int = (timeSlotViewModel.morningSection?.count ?? 0)
//            let slotACount: Int = (timeSlotViewModel.afternoonSection?.count ?? 0)
//            let slotEAvilbleCount: Int = (timeSlotViewModel.eveningSection?.count ?? 0)
//            let slotNAvilbleCount: Int = (timeSlotViewModel.nightSection?.count ?? 0)

//            let slotAvilbleCount: Int = (timeSlotViewModel.morningSection?.count ?? 0) + (timeSlotViewModel.afternoonSection?.count ?? 0) + (timeSlotViewModel.eveningSection?.count ?? 0) + (timeSlotViewModel.nightSection?.count ?? 0)
//            cell.detailLbl.text = "\(slotAvilbleCount) slots avilable"

            return cell
        }else{
            let cell : TimeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyTimeCell", for: indexPath as IndexPath) as! TimeCell
            cell.bgView.layer.cornerRadius = 5
            cell.bgView.backgroundColor = .white
            cell.bgView.layer.borderWidth = 0.5
            cell.bgView.layer.borderColor = UIColor.lightGray.cgColor
            
            cell.timeNameLbl.textColor = UIColor.convertRGB(hexString: "#1e2933")
            cell.viewWidth.constant = 75
            cell.viewWidth.isActive = true
            cell.viewHieght.constant = 30
            cell.viewHieght.isActive = true
            
            if collectionView.tag == 1 {
                cell.timeNameLbl.text = timeSlotViewModel.morningSection?[indexPath.row].morningTime
                if sessionMorningBool == true{
                    if selectSessionIndexpath == indexPath {
                        cell.bgView.backgroundColor = UIColor.convertRGB(hexString: "#E6FBFF")
                        cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#109CBF").cgColor
                    }else{
                        cell.bgView.backgroundColor = UIColor.white
                        cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#D0D0D0").cgColor
                    }
                }else{
                    cell.bgView.backgroundColor = UIColor.white
                    cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#D0D0D0").cgColor
                }
            }else if collectionView.tag == 2 {
                cell.timeNameLbl.text = timeSlotViewModel.afternoonSection?[indexPath.row].afternoonTime
                if sessionAfternoonBool == true{
                    if selectSessionIndexpath == indexPath {
                        cell.bgView.backgroundColor = UIColor.convertRGB(hexString: "#E6FBFF")
                        cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#109CBF").cgColor
                    }else{
                        cell.bgView.backgroundColor = UIColor.white
                        cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#D0D0D0").cgColor
                    }
                }else{
                    cell.bgView.backgroundColor = UIColor.white
                    cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#D0D0D0").cgColor
                }
            }else if collectionView.tag == 3 {
                cell.timeNameLbl.text = timeSlotViewModel.eveningSection?[indexPath.row].eveningTime
                if sessionEveningBool == true{
                    if selectSessionIndexpath == indexPath {
                        cell.bgView.backgroundColor = UIColor.convertRGB(hexString: "#E6FBFF")
                        cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#109CBF").cgColor
                    }else{
                        cell.bgView.backgroundColor = UIColor.white
                        cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#D0D0D0").cgColor
                    }
                }else{
                    cell.bgView.backgroundColor = UIColor.white
                    cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#D0D0D0").cgColor
                }
            }else if collectionView.tag == 4 {
                cell.timeNameLbl.text = timeSlotViewModel.nightSection?[indexPath.row].nightTime
                if sessionNightBool == true{
                    if selectSessionIndexpath == indexPath {
                        cell.bgView.backgroundColor = UIColor.convertRGB(hexString: "#E6FBFF")
                        cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#109CBF").cgColor
                    }else{
                        cell.bgView.backgroundColor = UIColor.white
                        cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#D0D0D0").cgColor
                    }
                }else{
                    cell.bgView.backgroundColor = UIColor.white
                    cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#D0D0D0").cgColor
                }
            }
            cell.bgView.backgroundColor = UIColor.convertRGB(hexString: "#ffffff")
            cell.timeNameLbl.textColor = UIColor.convertRGB(hexString: "#1e2933")
            cell.timeNameLbl.textColor = UIColor.convertRGB(hexString: "#000000")
            
            if ( UIDevice.current.model.range(of: "iPad") != nil){
                cell.timeNameLbl.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(06.0))
            } else {
                cell.timeNameLbl.font = UIFont(name:Font.FontName.Latobold.rawValue, size: Utility.dynamicSize(11.0))
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView.tag == 100{
            return 10
        }else{
            return 10
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView.tag == 100{
            return 10
        }else{
            return 10
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 100 {
            selectTimeSlotIndexpath = indexPath
            selectSlotStr = "\(timeSlotViewModel.availabilities?[indexPath.row].availDate ?? "") \(timeSlotViewModel.availabilities?[indexPath.row].availToTime ?? "")"
            selectDate = timeSlotViewModel.availabilities?[indexPath.row].date ?? ""
        }else{
//            selectSessionIndexpath = indexPath
//            if collectionView.tag == 1{
//                selectTIme = timeSlotViewModel.morningSection?[indexPath.row].morningTime ?? ""
//                sessionMorningBool = true
//                sessionAfternoonBool = false
//                sessionEveningBool = false
//                sessionNightBool = false
//            }else if collectionView.tag == 2{
//                sessionMorningBool = false
//                selectTIme = timeSlotViewModel.afternoonSection?[indexPath.row].afternoonTime ?? ""
//                sessionAfternoonBool = true
//                sessionEveningBool = false
//                sessionNightBool = false
//            }else if collectionView.tag == 3{
//                selectTIme = timeSlotViewModel.eveningSection?[indexPath.row].eveningTime ?? ""
//
//                sessionMorningBool = false
//                sessionAfternoonBool = false
//                sessionEveningBool = true
//                sessionNightBool = false
//            }else if collectionView.tag == 4{
//                selectTIme = timeSlotViewModel.nightSection?[indexPath.row].nightTime ?? ""
//                sessionMorningBool = false
//                sessionAfternoonBool = false
//                sessionEveningBool = false
//                sessionNightBool = true
//            }
        }
        timeSlotTbleView.reloadData()
    }
}

class TimeCell: UICollectionViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var timeNameLbl: UILabel!
    @IBOutlet weak var viewHieght: NSLayoutConstraint!
    @IBOutlet weak var viewWidth: NSLayoutConstraint!
}

class TimeSlotCollectionCell: UICollectionViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var viewHieght: NSLayoutConstraint!
    @IBOutlet weak var viewWidth: NSLayoutConstraint!
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    //MARK: Select Radio Image
    func selectIndexImage(getIndexpath: IndexPath,normalIndex: IndexPath)  {
        doOnMain {
            self.bgView.layer.borderWidth = 1
            self.bgView.layer.cornerRadius = 5
            if getIndexpath == normalIndex {
                self.bgView.backgroundColor = UIColor.convertRGB(hexString: "#E6FBFF")
                self.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#109CBF").cgColor
            }else{
                self.bgView.backgroundColor = UIColor.white
                self.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#D0D0D0").cgColor
            }
        }
    }
}


