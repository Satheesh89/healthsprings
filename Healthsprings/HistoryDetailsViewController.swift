//
//  HistoryDetailsViewController.swift
//  Healthsprings
//

import UIKit
import SDWebImage
import CRNotifications

class HistoryDetailsViewController: UIViewController {
    
    @IBOutlet weak var historyDetailTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var appointmentDetailsViewModel = AppointmentDetailsViewModel()
    var historyAppointmentDetail: HistoryAppointmentDetails!
    var getappointmentid: String = ""
    var getprofileid: String = ""
    var historyModelDetails: HistoryModelDetails!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setStatusBar(color: UIColor.themeColor)
        loadViewModel()
    }
    
    //MARK:- loadView Model
    func  loadViewModel()  {
        let historyDetailViewModel = HistoryDetailViewModel()
        historyDetailViewModel.getAppoinmentDetails(appointmentId: getappointmentid, profileId: getprofileid)
        historyDetailViewModel.completionHandler{[weak self] (details, status, message)in
            if status {
                guard let self = self else {return}
                guard let _countries = details else {return}
                self.historyModelDetails = _countries
                self.titleLabel.text = "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.symptom ?? "")"
                //self.historyDetailTableView.reloadData()
                self.tableViewSetup()
            }
        }
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK: TableView Setup
    func tableViewSetup()  {
        historyDetailTableView.register(HistoryDetailHeaderCell.nib, forCellReuseIdentifier: HistoryDetailHeaderCell.identifier)
        historyDetailTableView.register(PaymentHeaderCell.nib, forCellReuseIdentifier: PaymentHeaderCell.identifier)
        historyDetailTableView.register(PaymentPriceDetailsCell.nib, forCellReuseIdentifier: PaymentPriceDetailsCell.identifier)
        historyDetailTableView.register(PaymentDetailsCell.nib, forCellReuseIdentifier: PaymentDetailsCell.identifier)
        
        historyDetailTableView.delegate = self
        historyDetailTableView.dataSource = self
        historyDetailTableView.tableFooterView = UIView()
        historyDetailTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        historyDetailTableView.showsHorizontalScrollIndicator = false
        historyDetailTableView.showsVerticalScrollIndicator = false
        historyDetailTableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        historyDetailTableView.backgroundColor = .clear
        historyDetailTableView.contentInsetAdjustmentBehavior = .never
    }
    
    //MARK: - Back
    @IBAction func didTapLeftAction(_ sender: Any) {
        self.navigationController?.popToViewController(ofClass: HistoryViewController.self)
    }
    
    //MARK:- Show Saved Pdf
    func savePdf(urlString:String, fileName:String) {
        DispatchQueue.main.async {
            let url = URL(string: urlString)
            let pdfData = try? Data.init(contentsOf: url!)
            let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            let pdfNameFromUrl = "Healthspring-\(fileName).pdf"
            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
            print("actualPath =",actualPath,"actualPath")
            do {
                try pdfData?.write(to: actualPath, options: .atomic)
                print("pdf successfully saved!")
                self.showCRNotificationsToast(toastType: CRNotifications.success, toastTitle: "success", toastMeassage: "Healthspring - \(fileName) successfully saved!")
            } catch {
                print("Pdf could not be saved")
                self.showCRNotificationsToast(toastType: CRNotifications.error, toastTitle: "Error", toastMeassage: "Healthspring - \(fileName) could not be saved")
            }
        }
    }
    
    //MARK:- Show Saved Pdf
    func showSavedPdf(url:String, fileName:String) {
        if #available(iOS 10.0, *) {
            do {
                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                for url in contents {
                    if url.description.contains("\(fileName).pdf") {
                        // its your file! do what you want with it!
                    }
                }
            } catch {
                print("could not locate pdf file !!!!!!!")
            }
        }
    }
    
    // check to avoid saving a file multiple times
    func pdfFileAlreadySaved(url:String, fileName:String)-> Bool {
        var status = false
        if #available(iOS 10.0, *) {
            do {
                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                for url in contents {
                    if url.description.contains("Healthspring-\(fileName).pdf") {
                        status = true
                    }
                }
            } catch {
                print("could not locate pdf file !!!!!!!")
            }
        }
        return status
    }
    
    //MARK:- Save Invoice To Device
    func saveInvoiceToDevice(filePath : String) {
        let fileURL = URL(string: filePath)
        if FileManager.default.fileExists(atPath: fileURL!.path){
            let url = URL(fileURLWithPath: fileURL!.path)
            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView=self.view
            //If user on iPad
            if UIDevice.current.userInterfaceIdiom == .pad {
                if activityViewController.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
                }
            }
            self.present(activityViewController, animated: true, completion: nil)
        }
        else {
            debugPrint("document was not found")
        }
    }
    
    //MARK:- Download Pdf File
    func  downloadPdfFile(downloadPdfUrl: String){
        DispatchQueue.main.async {
            let pdfUrl = downloadPdfUrl
            guard let fileURL = URL(string: pdfUrl) else { return }
            var originalUrlStr : String = ""
            print(fileURL.pathExtension)
            if fileURL.pathExtension == ""{
                //originalUrlStr = pdfUrl + ".pdf"
                originalUrlStr = pdfUrl
            } else {
                originalUrlStr = pdfUrl
            }
            guard let originalUrl = URL(string: originalUrlStr) else { return }
            let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
            let downloadTask = urlSession.downloadTask(with: originalUrl)
            downloadTask.resume()
        }
    }
}

extension HistoryDetailsViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2{
            return self.historyModelDetails.historyModelDetailsAppointmentDetails?.historyModelDetailsItems?.count ?? 0
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            return availableTableViewCell(tableView: tableView, indexPath: indexPath)
        }else  if indexPath.section == 1{
            return paymentHeaderCell(tableView: tableView, indexPath: indexPath)
        }else  if indexPath.section == 2{
            return paymentPriceCell(tableView: tableView, indexPath: indexPath)
        }else{
            return paymentDetailCell(tableView: tableView, indexPath: indexPath)
        }
    }
    
    func availableTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HistoryDetailHeaderCell.identifier) as? HistoryDetailHeaderCell else {
            return UITableViewCell()
        }
        cell.doctorName.text = "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.doctorName ?? "")"
        cell.doctorDetail.text = "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.speciality ?? "")"
        cell.detailName.text = "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.patientName ?? "")"
        cell.dateLabel.text = "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.appointmentDate ?? "")"
        cell.timeLabel.text = "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.appointmentTime ?? "")"
        cell.videoCodeLabel.text = "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.videoId ?? "")"
        cell.symptomsLabel.text = "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.symptom ?? "")"
        cell.doctorImageView.sd_setImage(with: URL(string: "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.avatar ?? "")"), placeholderImage: UIImage(named: "accountplaceholder"))
        return cell
    }
    
    func paymentHeaderCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentHeaderCell.identifier) as? PaymentHeaderCell else {
            return UITableViewCell()
        }
        return cell
    }
    
    func paymentPriceCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentPriceDetailsCell.identifier) as? PaymentPriceDetailsCell else {
            return UITableViewCell()
        }
        
        if "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.currency ?? "")" == "SGD"{
            cell.priceLabel.text = "S$ \(self.historyModelDetails.historyModelDetailsAppointmentDetails?.totalPaid ?? 0.0)"
        }else if "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.currency ?? "")" == "USD"{
            cell.priceLabel.text = "$ \(self.historyModelDetails.historyModelDetailsAppointmentDetails?.totalPaid ?? 0.0)"
        }else{
            cell.priceLabel.text = "$ \(self.historyModelDetails.historyModelDetailsAppointmentDetails?.totalPaid ?? 0.0)"
        }
        
        let itemList = self.historyModelDetails.historyModelDetailsAppointmentDetails?.historyModelDetailsItems
        cell.titleLabel.text = "\(itemList?[indexPath.row].name ?? "")"
        cell.qtyLabel.text = "\(itemList?[indexPath.row].qty ?? "")"
        if "\(itemList?[indexPath.row].currency ?? "")" == "SGD"{
            cell.priceLabel.text = "S$ \(itemList?[indexPath.row].price ?? 0)"
        }else if "\(itemList?[indexPath.row].currency ?? "")" == "USD"{
            cell.priceLabel.text = "$ \(itemList?[indexPath.row].price ?? 0)"
        }else{
            cell.priceLabel.text = "$ \(itemList?[indexPath.row].price ?? 0)"
        }
        return cell
    }
    
    func paymentDetailCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentDetailsCell.identifier) as? PaymentDetailsCell else {
            return UITableViewCell()
        }
        if "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.currency ?? "")" == "SGD"{
            cell.totalPriceLabel.text = "S$ \(self.historyModelDetails.historyModelDetailsAppointmentDetails?.totalPaid ?? 0.0)"
            cell.subTotalPriceLabel.text = "S$ \(self.historyModelDetails.historyModelDetailsAppointmentDetails?.subTotal ?? 0.0)"
            cell.taxPriceLabel.text = "S$ \(self.historyModelDetails.historyModelDetailsAppointmentDetails?.taxService ?? "")"
        }else if "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.currency ?? "")" == "USD"{
            cell.totalPriceLabel.text = "$ \(self.historyModelDetails.historyModelDetailsAppointmentDetails?.totalPaid ?? 0.0)"
            cell.subTotalPriceLabel.text = "$ \(self.historyModelDetails.historyModelDetailsAppointmentDetails?.subTotal ?? 0.0)"
            cell.taxPriceLabel.text = "$ \(self.historyModelDetails.historyModelDetailsAppointmentDetails?.taxService ?? "")"
        }else{
            cell.totalPriceLabel.text = "$ \(self.historyModelDetails.historyModelDetailsAppointmentDetails?.totalPaid ?? 0.0)"
            cell.subTotalPriceLabel.text = "$ \(self.historyModelDetails.historyModelDetailsAppointmentDetails?.subTotal ?? 0.0)"
            cell.taxPriceLabel.text = "$ \(self.historyModelDetails.historyModelDetailsAppointmentDetails?.taxService ?? "")"
        }
        cell.dateandtimeLabel.text = "Date & Time  : \("\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.appointmentDate ?? "")") \("\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.appointmentTime ?? "")")"
        cell.cardNoLabel.text = "****       ****        ****        \(self.historyModelDetails.historyModelDetailsAppointmentDetails?.cardNo ?? 0)"
        cell.downloadButton.addTarget(self, action: #selector(didTaptoDownloadButton), for: .touchUpInside)
        cell.prescriptionButton.addTarget(self, action: #selector(didTaptoPrescriptionButton), for: .touchUpInside)
        cell.medicalCerficateButton.addTarget(self, action: #selector(didTaptomedicalCerficateButton), for: .touchUpInside)

        if self.historyModelDetails.historyModelDetailsAppointmentDetails?.bill == ""{
            cell.downloadBillView.isHidden = true}else{cell.downloadBillView.isHidden = false}
        if self.historyModelDetails.historyModelDetailsAppointmentDetails?.prescription == ""{cell.downloadPrescriptionView.isHidden = true
        }else{cell.downloadPrescriptionView.isHidden = false}
        if self.self.historyModelDetails.historyModelDetailsAppointmentDetails?.medicalCertificate == ""{cell.downloadMedicalCertificateView.isHidden = true
        }else{cell.downloadMedicalCertificateView.isHidden = false}
        return cell
    }
    //    historyDetailTableView.register(HistoryDetailHeaderCell.nib, forCellReuseIdentifier: HistoryDetailHeaderCell.identifier)
    //    historyDetailTableView.register(PaymentDetailsCell.nib, forCellReuseIdentifier: PaymentDetailsCell.identifier)
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    //MARK:- Download Action
    @objc func didTaptoDownloadButton() {
        if "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.bill ?? "")" == ""{
            downloadPdfFile(downloadPdfUrl: "https://www.africau.edu/images/default/sample.pdf")
        }else{
            downloadPdfFile(downloadPdfUrl: "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.bill ?? "")")
        }
    }
    
    //MARK:- Prescription Action
    @objc func didTaptoPrescriptionButton() {
        if "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.prescription ?? "")" == ""{
            downloadPdfFile(downloadPdfUrl: "https://www.africau.edu/images/default/sample.pdf")
        }else{
            downloadPdfFile(downloadPdfUrl: "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.prescription ?? "")")
        }
    }
    
    //MARK:- Medical Cerficate Action
    @objc func didTaptomedicalCerficateButton() {
        if "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.medicalCertificate ?? "")" == ""{
            downloadPdfFile(downloadPdfUrl: "https://www.africau.edu/images/default/sample.pdf")
        }else{
            downloadPdfFile(downloadPdfUrl: "\(self.historyModelDetails.historyModelDetailsAppointmentDetails?.medicalCertificate ?? "")")
        }
    }
}
extension HistoryDetailsViewController:  URLSessionDownloadDelegate {
    //MARK:- File Downloaded Location
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("File Downloaded Location- ",  location)
        guard let url = downloadTask.originalRequest?.url else {
            return
        }
        let docsPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        let destinationPath = docsPath.appendingPathComponent(url.lastPathComponent)
        try? FileManager.default.removeItem(at: destinationPath)
        do{
            try FileManager.default.copyItem(at: location, to: destinationPath)
            print("File Downloaded Location- ",  destinationPath)
            DispatchQueue.main.async {
                let urlString: String = destinationPath.absoluteString
                self.saveInvoiceToDevice(filePath: urlString)
            }
        }catch let error {
            print("Copy Error: \(error.localizedDescription)")
        }
    }
}
