//
//  KeyConstant.swift
//  Healthsprings
//
//  Created by Personal on 20/06/22.
//

import Foundation

struct Key {
    struct UserDefaults {
        static let devicetype = "ios"
        static let deviceToken = "DeviceToken"
        static let logintypeFB = "fb"
        static let logintypeGM = "gm"
        static let logintypeapple = "apple"

        let googleLogin = "com.googleusercontent.apps.1056163698692-80qn6smih4qet6odog92rd9l76fnv3ar"
        let clientID = "1056163698692-80qn6smih4qet6odog92rd9l76fnv3ar.apps.googleusercontent.com"

        static let stagingURL = "StagingURL"
        static let userDetails = "userDetails"
        
        static var baseURLString = "https://www.docis.live/"

    }
}


