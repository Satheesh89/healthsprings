//
//  SettingsViewController.swift
//  Healthsprings
//
//  Created by Personal on 18/06/22.
//

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var settingTableView: UITableView!
    
    var TitleLblRef = NSArray()
    var leftIconArray = NSArray()
    var selectIndexpath : IndexPath = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initializeHideKeyboard()
        TitleLblRef = ["Payment Settings","Payment Settings","Security","Notifications","FAQs","Feedbacks","Logout"]
        leftIconArray = ["profile-card","profile-card","profile-passward","profile-notification","profile-faq","profile-message","logout"]
         
        tableViewSetup()
        selectIndexpath = IndexPath(item:7, section:0)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: TableView Setup
    func tableViewSetup()  {
        setStatusBar(color: UIColor.themeColor)
        settingTableView.register(SettingHeaderTableViewCell.nib, forCellReuseIdentifier: SettingHeaderTableViewCell.identifier)
        settingTableView.delegate = self
        settingTableView.dataSource = self
        settingTableView.tableFooterView = UIView()
        settingTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        settingTableView.showsHorizontalScrollIndicator = false
        settingTableView.showsVerticalScrollIndicator = true
        //serviceTableView.setContentOffset(.zero, animated: true)
        settingTableView.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
        settingTableView.backgroundColor = .clear
//        settingTableView.contentInset.top = 5
        settingTableView.contentInsetAdjustmentBehavior = .never
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
}


extension SettingsViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TitleLblRef.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            return getHeaderTableViewCell(tableView: tableView, indexPath: indexPath)
        }else{
            return getSettingTableViewCell(tableView: tableView, indexPath: indexPath)
        }
    }
    
    func getHeaderTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SettingHeaderTableViewCell.identifier) as? SettingHeaderTableViewCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: cell.frame.size.width))
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(self.buttonTapped), for: .touchUpInside)
        button.tag = indexPath.row
        cell.addSubview(button)
        cell.profilenameLabel.text = "Hi \(UserDetails.shared.userName ?? "")"
        //cell..profileImageView.sd_setImage(with: URL(string: UserDetails.shared. ?? ""), placeholderImage: UIImage(named: "accountplaceholder"))
        cell.profileImageView?.sd_setImage(with: URL(string: UserDetails.shared.profile_image ?? ""), placeholderImage: UIImage(named: "accountplaceholder"))

        if selectIndexpath.row == indexPath.row{
            cell.headerView.backgroundColor = .white
            cell.headerView.layer.cornerRadius = 5
            cell.headerView.layer.borderWidth = 1
            cell.headerView.layer.borderColor = UIColor.convertRGB(hexString: "#A2D6E3").cgColor
        }else{
            cell.headerView.viewBorderShadow()
        }
        return cell
    }
    
    func getSettingTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? SettingTableViewCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.separatorInset = UIEdgeInsets(top: 0, left: cell.bounds.size.width, bottom: 0, right: 0)
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        cell.TitleLblRef.text = self.TitleLblRef[indexPath.row] as? String
        cell.leftImgRef.image = UIImage.init(named:leftIconArray[indexPath.row] as! String)
        cell.leftImgRef.setImageColor(color: UIColor.convertRGB(hexString: "#215460"))
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: cell.frame.size.width))
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(self.buttonTapped), for: .touchUpInside)
        button.tag = indexPath.row
        cell.addSubview(button)
        cell.bgView.backgroundColor = .white
        
        cell.bgView.layer.cornerRadius = 5
        cell.bgView.layer.borderWidth = 1
        if selectIndexpath.row == indexPath.row{
            cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#A2D6E3").cgColor
        }else{
            //cell.bgView.viewBorderShadow()
            cell.bgView.layer.borderColor = UIColor.convertRGB(hexString: "#E6E6E6").cgColor
        }
        return cell
    }
    
    @objc func buttonTapped(sender : UIButton) {
        selectIndexpath = IndexPath(item: sender.tag, section:0)
        settingTableView.reloadData()
        delayWithSeconds(0.5) {
            if sender.tag == 6{
                UserDefaults.standard.removeObject(forKey: Key.UserDefaults.userDetails)
                UserDetails.shared.logout()
                doOnMain{
                    let mainstoryboard = UIStoryboard(name: "Menu", bundle: nil)
                    let mainview = mainstoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    UserDefaults.standard.hasOnboarded = true
                    self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.pushViewController(mainview, animated: false)
                }
            }else{
                self.tabBarController?.tabBar.isHidden = true
                var menuStoryboard = UIStoryboard(name: "Setting", bundle: nil)
                var viewName = UIViewController()
                if sender.tag == 0{
                    menuStoryboard = UIStoryboard(name: "Profile", bundle: nil)
                    viewName = menuStoryboard.instantiateViewController(withIdentifier: "ProfileSetttingViewController") as! ProfileSetttingViewController
                }else if sender.tag == 1{
                    menuStoryboard = UIStoryboard(name: "Tabbar", bundle: nil)
                    viewName = menuStoryboard.instantiateViewController(withIdentifier: "PaymentSettingViewController") as! PaymentSettingViewController
//                    viewName.titleLabel.text = "Payment Settings"
                } else if sender.tag == 2{
//                    viewName = menuStoryboard.instantiateViewController(withIdentifier: "PinSettingViewController") as! PinSettingViewController
                    viewName = menuStoryboard.instantiateViewController(withIdentifier: "SecurityListViewController") as! SecurityListViewController
                }
                else if sender.tag == 3{
                    viewName = menuStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
                }else if sender.tag == 4{
                    viewName = menuStoryboard.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
                }else if sender.tag == 5{
                    viewName = menuStoryboard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
                }
                self.navigationController?.pushViewController(viewName, animated: false)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            //            let mainStoryBoard = UIStoryboard(name: "Menu", bundle: nil)
            //            let Permissionvc = mainStoryBoard.instantiateViewController(withIdentifier: "HealthspringMenuViewController") as! HealthspringMenuViewController
            //            UIApplication.shared.windows.first?.rootViewController = Permissionvc
            //            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }
}
