
import Foundation
import SwiftyJSON

struct AllSearchDoctor : Codable {
    let doctor_name : String?
    let room_name : String?
    let email : String?
    let gender : String?
    let profile_link : String?
    let phone : String?
    let country : String?
    let avaliability_status : Int?
    let description : String?
    let avatar : String?
    let uuid : Int?
    let specialty : String?
    let next_avail_date : String?
    let next_avail_time : String?

    enum CodingKeys: String, CodingKey {
        case doctor_name = "doctor_name"
        case room_name = "room_name"
        case email = "email"
        case gender = "gender"
        case profile_link = "profile_link"
        case phone = "phone"
        case country = "country"
        case avaliability_status = "avaliability_status"
        case description = "description"
        case avatar = "avatar"
        case uuid = "uuid"
        case specialty = "specialty"
        case next_avail_date = "next_avail_date"
        case next_avail_time = "next_avail_time"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        doctor_name = try values.decodeIfPresent(String.self, forKey: .doctor_name)
        room_name = try values.decodeIfPresent(String.self, forKey: .room_name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        profile_link = try values.decodeIfPresent(String.self, forKey: .profile_link)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        avaliability_status = try values.decodeIfPresent(Int.self, forKey: .avaliability_status)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        avatar = try values.decodeIfPresent(String.self, forKey: .avatar)
        uuid = try values.decodeIfPresent(Int.self, forKey: .uuid)
        specialty = try values.decodeIfPresent(String.self, forKey: .specialty)
        next_avail_date = try values.decodeIfPresent(String.self, forKey: .next_avail_date)
        next_avail_time = try values.decodeIfPresent(String.self, forKey: .next_avail_time)
    }
}

