//
//  URLStaging.swift
//  Healthsprings
//

import Foundation

extension URL {
    
    
    // MARK:- Login api
    static var checkemail: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "check-email"
    }
    
    // MARK:- Login api
    static var login: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "booking-login?"
    }
    
    // MARK:- social-signup api
    static var socialsignup: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "social-signup"
    }
    
    // MARK:- social-signin api
    static var socialsignin: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "social-signin"
    }
    
    // MARK:- normal-signin api
    static var registerpatient: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "register-new-patient?"
    }
    
    
    // MARK:- get-card Api
    static var getcard: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "get-card?"
    }
    
    
    // MARK:- Add-card Api
    static var addCard: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "add-card?"
    }
    
    // MARK:- searchdoctor Api
    static var searchdoctor: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "search-doctor?"
    }
    
    // MARK:- get-symptom Api
    static var getsymptom: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "get-symptom"
    }
    
    // MARK:- get appointments Api
    static var getappointments: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "appointments?"
    }
    
    // MARK:- get appointment-details Api
    static var getappointmentdetails: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "appointment-details"
    }
    
    
    // MARK:- get get-doctor-availability Api
    static var getdoctoravailability: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "get-doctor-availability"
    }
    
    
    // MARK:- get social-signin Api
    static var getsocialsignin: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "social-signin"
    }
    
    
    // MARK:- get social-signup Api
    static var getsocialsignup: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "social-signup"
    }
    
    // MARK:- get update-profile Api
    static var getupdateprofile: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "update-profile"
    }
    
    // MARK:- get Profile Api
    static var getProfile: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "get-profile?"
    }
    
    // MARK:- get Profile Api
    static var getUploaduserimage: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "uploaduserimage"
    }
    
    // MARK:- get get-doctor-availability Api
    static var getRemovecard: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "remove-card"
    }
    
    
    // MARK:- get get-doctor-availability Api
    static var getaddprofile: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "add-profile?"
    }
    
    // MARK:- get Make Appoint Request Api
    
    
    static var getMakeInstantAppointmentRequest: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "make-appointment-request?"
    }
    
    // MARK:- get-All profile Api
    static var getallprofile: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "get-all-profile"
    }
    
    // MARK:- get- Uuid Api
    static var startbookingprocess: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "start-booking-process?"
    }
    
    // MARK:- get- Request-Summary
    static var requestsummary: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "request-summary"
    }
    
    // MARK:- get getmakedoctorappointmentrequest
    static var getmakedoctorappointmentrequest: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "make-doctor-appointment-request"
    }
    
    
    // MARK:- get Confirm Appointment
    static var confirmappointment: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "confirm-appointment?"
    }
    
    
    // MARK:- get Cancel Appointment
    static var cancelappointment: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "cancel-appointment?"
    }
    
    // MARK:- get History Details Appointment
    static var historydetails: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "history-details?"
    }
    
    // MARK:- get FAQ
    static var getFaqs: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "faqs"
    }
    
    
    // MARK:- Socail Register
    static var socialRegister: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "social-signup"
    }
    // MARK:- Socail Login
    static var socialLogin: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "social-signin"
    }
    
    // MARK:- Get Send Otp
    static var getSendotp: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "send-otp"
    }
    
    // MARK:- Get Forgot Password Send Otp
    static var forgotpassword: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "forgot-password"
    }
    
    // MARK:- Get reset
    static var resetPassword: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "reset"
    }
    
    
    // MARK:- Check-otp
    static var checkOtp: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "check-otp"
    }
    
    // MARK:- Resend-otp
    static var resendOtp: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "resend-otp"
    }
    
    // MARK:- Set-default-card
    static var setDefaultCard: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "set-default-card"
    }
    
    // MARK:- Itemised bill
    static var getItemisedBill: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "itemised-bill"
    }
    
    // MARK:- Itemised bill
    static var getNotifications: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "notifications?"
    }
    
    // MARK:- id-verification
    static var getIdVerification: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "id-verification"
    }
    
    
    // MARK:- Add - Verification
    static var getAddDocument: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "add-document"
    }
 
    
    // MARK:- Add - Verification
    static var getUpdatePassword: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "update-password"
    }
    
    
    // MARK:- get-Country Api
    static var getCountrylist: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "get-country-list"
    }
    
    // MARK:- get- Match OTP
    static var getMatchtoken: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "match-token"
    }
    
    // MARK:- get- Match OTP
    static var getHealthBlog: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "health-blog"
    }
    
    
    // MARK:- get clinic-available-days Api
    static var clinicAvailableDays: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "clinic-available-days"
    }
    
    // MARK:- get clinic-available-slots Api
    static var clinicAvailableSlots: String {
        let domain = "\(UserDefaults.standard.object(forKey: Key.UserDefaults.stagingURL) ?? "")"
        let api = domain + Route.api.rawValue
        return api + "clinic-available-slots"
    }
}
