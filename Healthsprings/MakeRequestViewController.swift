//
//  MakeRequestViewController.swift
//  Healthsprings
//
//  Created by Personal on 14/07/22.
//

import UIKit
import Alamofire
import SwiftyJSON

class MakeRequestViewController: UIViewController ,ScheduleVCDelegate{
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var requestTableView : UITableView!
    @IBOutlet weak var nextButton: UIButton!
    
    var makeRequestViewModel = MakeRequestViewModel()
    private var selected = [String]()
    private var selectedIds = [String]()
    
    var getDoctorUuid: String!
    var getDoctorName: String!
    var getDoctorId: String!
    //    var getBookingType: String!
    var getDate: String!
    var getTIme: String!
    var getDeliveryType = ""
    var symptomsSelectArray = NSMutableArray()
    var getCardId: Int!
    var getProfileId: String!
    var getCartBool: Bool = false
    var requestDetailModel: RequestDetailModel!
    var requestdetail: [Requestdetail]?
    
    var getScheduleDate: String = ""
    var getScheduleTime: String = ""
    var getMedicineRequired: String = "no"
    
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var popupView: UIView!

    @IBOutlet weak var requiredView: UIView!
    @IBOutlet weak var notRequiredView: UIView!

    @IBOutlet weak var medicineRequiedLabel: UILabel!
    @IBOutlet weak var medicineRequiedDetailLabel: UILabel!

    @IBOutlet weak var notMedicineRequiedLabel: UILabel!
    @IBOutlet weak var notMedicineRequiedDetailLabel: UILabel!

    @IBOutlet weak var medicineRequiedButton: UIButton!
    @IBOutlet weak var notMedicineRequiedDetailButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //        initializeHideKeyboard()
        buttonSuccess()
        loadSetup()
        loadProfileSetup()
        tableViewSetup()
        alertViewLoadSetup()
    }
    
    //MARK: Load Setup
    func alertViewLoadSetup()  {
        alertView.backgroundColor = UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.7)
        alertView.backgroundColor = .clear
        alertView.layer.cornerRadius = 5
        popupView.layer.cornerRadius = 5
        doOnMain {
            self.popupView.layer.masksToBounds = false
            self.popupView.layer.shadowRadius = 4
            self.popupView.layer.shadowOpacity = 0.5
            self.popupView.layer.shadowColor = UIColor.black.cgColor
            self.popupView.layer.shadowOffset = CGSize(width: 0 , height:0.9)
            self.popupView.layer.cornerRadius = 0
        }
        
        alertView.isHidden = true
        requiredView.layer.cornerRadius = 5
        notRequiredView.layer.cornerRadius = 5
        requiredView.layer.borderWidth = 1
        requiredView.layer.borderColor = UIColor.lightGray.cgColor
        notRequiredView.layer.borderWidth = 1
        notRequiredView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    @IBAction func didTapMedicineAction(_ sender: UIButton) {
        if sender.tag == 500{
            medicineRequiedButton.setImage(UIImage(named: "radiocheck"), for: .normal)
            notMedicineRequiedDetailButton.setImage(UIImage(named: "radiouncheck"), for: .normal)
            requiredView.layer.borderWidth = 1
            requiredView.layer.borderColor = UIColor.themeColor.cgColor
            notRequiredView.layer.borderWidth = 1
            notRequiredView.layer.borderColor = UIColor.lightGray.cgColor
        }else if sender.tag == 501{
            medicineRequiedButton.setImage(UIImage(named: "radiouncheck"), for: .normal)
            notMedicineRequiedDetailButton.setImage(UIImage(named: "radiocheck"), for: .normal)
            requiredView.layer.borderWidth = 1
            requiredView.layer.borderColor = UIColor.lightGray.cgColor
            notRequiredView.layer.borderWidth = 1
            notRequiredView.layer.borderColor = UIColor.themeColor.cgColor
        }
     }
    
    @IBAction func didTapCloseAction(_ sender: UIButton) {
        alertView.isHidden = true
    }

    @IBAction func didTapConfirmAction(_ sender: UIButton) {
        if self.getDeliveryType == "asap"{
            alertView.isHidden = true
        }else if self.getDeliveryType == "time_based"{
            alertView.isHidden = true
            self.reDirectSchedule()
        }
    }
    
    
    func medicineRequiedView(){
        medicineRequiedButton.setImage(UIImage(named: "radiouncheck"), for: .normal)
        notMedicineRequiedDetailButton.setImage(UIImage(named: "radiouncheck"), for: .normal)
        requiredView.layer.borderWidth = 1
        requiredView.layer.borderColor = UIColor.lightGray.cgColor
        notRequiredView.layer.borderWidth = 1
        notRequiredView.layer.borderColor = UIColor.lightGray.cgColor
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: Load Setup
    func loadSetup(){
        setdelegate()
        makeRequestViewModel.getSymptomListApi( apitokenStr: "",loadview: self)
    }
    
    //MARK: Load Setup
    func loadProfileSetup(){
        setdelegate()
        makeRequestViewModel.getAllProfileListApi( apitokenStr: UserDetails.shared.getApiToken(),loadview: self)
    }
    
    //MARK: Setup Delegate
    func setdelegate(){
        makeRequestViewModel.vc = self
    }
    
    //MARK: Get Uuid
    func getUuidSetup(){
        self.getUuidApi(cardIdStr: getCardId,loadview: self)
    }
    
    func getUuidApi(cardIdStr: Int,loadview: UIViewController) {
        LoadingIndicator.shared.show(forView: loadview.view)
        var paramsDictionary = [String:Any]()
        paramsDictionary["apiToken"] = UserDetails.shared.getApiToken()
        paramsDictionary["card_id"] = cardIdStr
        HttpClientApi.instance().makeAPICall(url: "\(URL.startbookingprocess)", params:paramsDictionary, method: .POST, success: { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dataDict = json as! NSDictionary
                let response = response
                if  response?.statusCode == 200 {
                    print(dataDict)
                    doOnMain {delay(0.1) {
                        self.getDoctorUuid  = "\(dataDict.object(forKey:"Uuid") as? String ?? "")"
                        LoadingIndicator.shared.hide()
                        //MakeInstant Appointment Request
                        self.makeInstantAppointmentRequestApi()
                    }}
                }else{
                    delay(0.1) {
                        self.showCustomToast(toastTitle: "Error",toastMeassage: "The profile id field is required or The symptom id field is required.",status: 1)
                        LoadingIndicator.shared.hide()
                    }
                }
            }
        }, failure: { (data, response, error) in
            // API call Failure
        })
    }
    
    
    //MARK: Make instant appointment request
    func makeInstantAppointmentRequestApi()  {
        let parameters: [String: Any] = [
            "apiToken" : UserDetails.shared.getApiToken(),
            "Uuid" : getDoctorUuid ?? "",
            "profile_id" : self.getProfileId ?? "",
            "appointment_date" : getScheduleDate,
            "appointment_time" : getScheduleTime,
            "booking_type" : "video_consultation",
            "symptom_id" : symptomsSelectArray ,
            "type": getDeliveryType,
            "medicine_required": getMedicineRequired,
        ]
        print("MakeInstant parameters = ",parameters)
        
        AF.request("\(URL.getMakeInstantAppointmentRequest)", method:.post, parameters: parameters,encoding: JSONEncoding.default) .responseJSON { (response) in
            print(response)
            if response.response?.statusCode == 200{
                let json = try? JSONSerialization.jsonObject(with: response.data!, options: [])
                let dataDict = json as! NSDictionary
                self.requestDetailModel = RequestDetailModel(JSON(dataDict))
                self.requestdetail = self.requestDetailModel?.requestdetail ?? []
                print("requestdetail",self.requestdetail?.count ?? 0)
                doOnMain {
                    delay(0.1) {
                        LoadingIndicator.shared.hide()
                        if self.getDeliveryType == "asap"{
                            self.getMedicineRequired = "yes"
                            //self.reDirectConfirmPopupViewPage()
                        }else if self.getDeliveryType == "time_based"{
                            self.getMedicineRequired = "no"
                        }
                        self.reDirectSummaryPage()
                    }}
            }else if response.response?.statusCode == 401{
                self.showCustomToast(toastTitle: "Error",toastMeassage: "The profile id field is required or The symptom id field is required.",status: 1)
            }
        }
    }
    
    //MARK: re Direct Summary Page
   private func reDirectSummaryPage()  {
        let requestSummaryVC = RequestSummaryViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        requestSummaryVC.hidesBottomBarWhenPushed = true
        requestSummaryVC.getRequestDetailModel = self.requestDetailModel
        requestSummaryVC.getRequestdetail = self.requestdetail
       requestSummaryVC.getDate = getScheduleTime
       requestSummaryVC.getTIme = getScheduleDate
       requestSummaryVC.getDoctorUuid = getDoctorUuid
        self.navigationController?.pushViewController(requestSummaryVC, animated: false)
    }
    
    //MARK: reDirect Confirm PopupView
    private func reDirectConfirmPopupViewPage()  {
        let confirmPopupVC = ConfirmPopupViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        confirmPopupVC.getRequestDetailModel = self.requestDetailModel
        confirmPopupVC.getRequestdetail = self.requestdetail
        confirmPopupVC.selectTime = getScheduleTime
        confirmPopupVC.selectDate = getScheduleDate
//        confirmPopupVC.view.backgroundColor = .alphaColor
//        confirmPopupVC.popupView.backgroundColor = .alphaColor
        self.navigationController?.pushViewController(confirmPopupVC, animated: false)
    }
    
    //MARK: TableView Setup
    func tableViewSetup()  {
        setStatusBar(color: UIColor.convertRGB(hexString: "#F4F4F4"))
        requestTableView.register(PatientCell.nib, forCellReuseIdentifier: PatientCell.identifier)
        requestTableView.register(VideoCell.nib, forCellReuseIdentifier: VideoCell.identifier)
        requestTableView.register(AddReasonCell.nib, forCellReuseIdentifier: AddReasonCell.identifier)
        requestTableView.register(SelectSymptomsCell.nib, forCellReuseIdentifier: SelectSymptomsCell.identifier)
        requestTableView.register(CommonSymptomsCell.nib, forCellReuseIdentifier: CommonSymptomsCell.identifier)
        requestTableView.register(ConsultationTypeCell.nib, forCellReuseIdentifier: ConsultationTypeCell.identifier)
        requestTableView.delegate = self
        requestTableView.dataSource = self
        requestTableView.tableFooterView = UIView()
        requestTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        requestTableView.showsHorizontalScrollIndicator = false
        requestTableView.showsVerticalScrollIndicator = false
        requestTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        requestTableView.backgroundColor = .white
        requestTableView.contentInsetAdjustmentBehavior = .never
    }
    
    //MARK: - Back
    @IBAction func didTapLeftAction(_ sender: Any) {
        //        if getCartBool == true{
        //            self.navigationController?.popToViewController(ofClass: MakeInstantAddCardViewController.self)
        //        }else{
        //            self.navigationController?.popToViewController(ofClass: TimeSlotViewController.self)
        //        }
        self.navigationController?.popViewController(animated: false)
    }
    
    //MARK: - Back
    @IBAction func didAddPatientAction(_ sender: UIButton) {
        let findDoctorVC = AddProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        findDoctorVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(findDoctorVC, animated: false)
    }
    //MARK: - Back
    @IBAction func didTabNextAction(_ sender: Any) {
        if getProfileId != "" && symptomsSelectArray.count != 0{
            getUuidSetup()
        }
    }
}

extension MakeRequestViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2{
            if selected.count == 0{
                return 0.0
            }else{
                return UITableView.automaticDimension
            }
        }else  if indexPath.row == 3{
            return CGFloat(Int(makeRequestViewModel.symtomList.count) / 2 * 55) + CGFloat(Int(makeRequestViewModel.symtomList.count ) % 2 == 0 ? 0 : 130)
        }else{
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            return getPatientTableViewCell(tableView: tableView, indexPath: indexPath)
        }else  if indexPath.row == 1{
            return getConsultationTypeTableCell(tableView: tableView, indexPath: indexPath)
        }else  if indexPath.row == 2{
            return getSelectSymptomsTableViewCell(tableView: tableView, indexPath: indexPath)
        }else  if indexPath.row == 3{
            return getCommonSymptomsTableViewCell(tableView: tableView, indexPath: indexPath)
        }
        return UITableViewCell()
    }
    func getPatientTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PatientCell.identifier) as? PatientCell else {
            return UITableViewCell()
        }
        doOnMain { [self] in
            cell.patientDropDown.text = UserDetails.shared.userName ?? ""
            //cell.patientDropDown.optionArray = [UserDetails.shared.userName ?? ""]
            let detailArray = NSMutableArray()
            for i in (0..<(self.makeRequestViewModel.profileData?.count ?? 0)){
                detailArray.add(self.makeRequestViewModel.profileData?[i].name ?? "")
            }
            cell.patientDropDown.optionArray = detailArray  as! [String]
            if self.makeRequestViewModel.profileData?.count ?? 0 > 0{
                self.getProfileId = self.makeRequestViewModel.profileData?[0].profileId ?? ""
            }
            cell.patientDropDown.textAlignment = .center
            cell.patientDropDown.resignFirstResponder()
            cell.patientDropDown.font = UIFont(name:Font.FontName.Latoregular.rawValue, size: Utility.dynamicSize(14.0))
            cell.patientDropDown.didSelect{(selectedText , index ,id) in
                self.getProfileId = self.makeRequestViewModel.profileData?[index].profileId ?? ""
                self.buttonSuccess()
                //self.barTypeInt = index
                //self.setupBarType(isType: self.barTypeInt)
                //self.valueLabel.text = "Selected String: \(selectedText) \n index: \(index)"
            }
            cell.addButton.addTarget(self, action: #selector(didAddPatientAction), for: .touchUpInside)
        }
        return cell
    }
    func getConsultationTypeTableCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ConsultationTypeCell.identifier) as? ConsultationTypeCell else {
            return UITableViewCell()
        }
        cell.immediateButton.tag = 10001
        cell.scheduleButton.tag = 10002
        
        if getDeliveryType == "asap"{
            cell.immediateBgView.backgroundColor = UIColor.convertRGB(hexString: "#EAFBFF")
            cell.immediateInerView.backgroundColor = UIColor.themeColor
            cell.immediateBgView.layer.borderWidth = 1
            cell.immediateBgView.layer.borderColor = UIColor.themeColor.cgColor
            
            cell.scheduleBgView.backgroundColor = UIColor.white
            cell.scheduleInerView.backgroundColor = UIColor.convertRGB(hexString: "#EFEFEF")
            cell.scheduleBgView.layer.borderWidth = 1
            cell.scheduleBgView.layer.borderColor = UIColor.lightGray.cgColor

            doOnMain {
                cell.immediateImageView.setImageColor(color: .white)
                cell.scheduleImageView.setImageColor(color: .darkGray)
            }
        }else if getDeliveryType == "time_based"{
                cell.scheduleBgView.backgroundColor = UIColor.convertRGB(hexString: "#EAFBFF")
                cell.scheduleInerView.backgroundColor = UIColor.themeColor
                cell.scheduleBgView.layer.borderWidth = 1
                cell.scheduleBgView.layer.borderColor = UIColor.themeColor.cgColor
            
            cell.immediateBgView.backgroundColor = UIColor.white
            cell.immediateInerView.backgroundColor = UIColor.convertRGB(hexString: "#EFEFEF")
            cell.immediateBgView.layer.borderWidth = 1
            cell.immediateBgView.layer.borderColor = UIColor.lightGray.cgColor
                doOnMain {
                    cell.scheduleImageView.setImageColor(color: .white)
                    cell.immediateImageView.setImageColor(color: .darkGray)
                }
            }

//        if getDeliveryType == "asap"{
//            cell.immediateBgView.backgroundColor = UIColor.convertRGB(hexString: "#EAFBFF")
//            cell.immediateInerView.backgroundColor = UIColor.themeColor
//            cell.scheduleBgView.backgroundColor = UIColor.white
//            cell.immediateBgView.layer.borderWidth = 1
//            cell.immediateBgView.layer.borderColor = UIColor.themeColor.cgColor
//        }else if getDeliveryType == "time_based"{
//            cell.scheduleBgView.backgroundColor = UIColor.convertRGB(hexString: "#EAFBFF")
//            cell.scheduleInerView.backgroundColor = UIColor.themeColor
//            cell.immediateBgView.backgroundColor = UIColor.white
//            cell.immediateInerView.backgroundColor = UIColor.lightGray
//            cell.scheduleBgView.layer.borderWidth = 1
//            cell.scheduleBgView.layer.borderColor = UIColor.themeColor.cgColor
//            cell.immediateBgView.layer.borderWidth = 1
//            cell.immediateBgView.layer.borderColor = UIColor.lightGray.cgColor
//            cell.scheduleImageView.setImageColor(color: .white)
//        }
        cell.immediateButton.addTarget(self, action: #selector(didTapConsultationButtonAction), for: .touchUpInside)
        cell.scheduleButton.addTarget(self, action: #selector(didTapConsultationButtonAction), for: .touchUpInside)
        return cell
    }
    
    @objc func didTapConsultationButtonAction(_ sender: UIButton) {
        if sender.tag == 10001{
            self.getDeliveryType = "asap"
            self.getScheduleDate = ""
            self.getScheduleTime = ""
        }else if sender.tag == 10002{
            alertView.isHidden = false
            self.getDeliveryType = "time_based"
        }
        alertView.isHidden = false
        let indexPathRow:Int = 1
        let indexPosition = IndexPath(row: indexPathRow, section: 0)
        self.requestTableView.reloadRows(at: [indexPosition], with: .none)
    }
    
    private func reDirectSchedule(){
        medicineRequiedView()
        let scheduleTimeVC = ScheduleTimeViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        scheduleTimeVC.hidesBottomBarWhenPushed = true
        scheduleTimeVC.delegate = self
        navigationController?.pushViewController(scheduleTimeVC, animated: false)
    }
    
    func didSelectData(date: String, time: String) {
        getScheduleDate = "\(date)"
        getScheduleTime = "\(time)"
    }
    
    func getSelectSymptomsTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SelectSymptomsCell.identifier) as? SelectSymptomsCell else {
            return UITableViewCell()
        }
        cell.selectSymptomsCollectionView.register(UINib(nibName: "SelectViewCell", bundle: .main), forCellWithReuseIdentifier: "SelectViewCell")
        cell.selectSymptomsCollectionView.delegate = self
        cell.selectSymptomsCollectionView.dataSource = self
        cell.selectSymptomsCollectionView.tag = 100
        cell.selectSymptomsCollectionView.reloadData()
        return cell
    }
    func getCommonSymptomsTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CommonSymptomsCell.identifier) as? CommonSymptomsCell else {
            return UITableViewCell()
        }
        cell.commonSymptomsCollectionView.register(UINib(nibName: "CommonCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "CommonCollectionViewCell")
        if makeRequestViewModel.symtomList.count > 0{
            cell.commonSymptomsCollectionView.delegate = self
            cell.commonSymptomsCollectionView.dataSource = self
            cell.commonSymptomsCollectionView.tag = 200
            let layout = TagFlowLayout()
            layout.estimatedItemSize = CGSize(width: 140, height: 40)
            cell.commonSymptomsCollectionView.collectionViewLayout = layout
            cell.commonSymptomsCollectionView.reloadData()
        }
        return cell
    }
}

//extension MakeRequestViewController: UITableViewDelegate,UITableViewDataSource {
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 7{
//            return UITableView.automaticDimension
//        }
////        else if indexPath.row == 4{
////            return 0.0
////        }
//        else if indexPath.row == 6{
//            if selected.count > 0{
//                return UITableView.automaticDimension
//            }else{
//                return 0.1
//            }
//        }else{
////            return 29
//            return 20
//        }
//    }
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 8
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if indexPath.row == 0{
//            return getPatientTableViewCell(tableView: tableView, indexPath: indexPath)
//        }else  if indexPath.row == 2{
//            return getVideoconsultationTableViewCell(tableView: tableView, indexPath: indexPath)
//        }else  if indexPath.row == 4 {
//            return getAddReasonTableViewCell(tableView: tableView, indexPath: indexPath)
//        }else  if indexPath.row == 6 {
//            return getSelectSymptomsTableViewCell(tableView: tableView, indexPath: indexPath)
//        }else  if indexPath.row == 7 {
//            return getCommonSymptomsTableViewCell(tableView: tableView, indexPath: indexPath)
//        }
//        return UITableViewCell()
//    }
//    func getPatientTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: PatientCell.identifier) as? PatientCell else {
//            return UITableViewCell()
//        }
//        doOnMain { [self] in
//            cell.patientDropDown.text = UserDetails.shared.userName ?? ""
//            //cell.patientDropDown.optionArray = [UserDetails.shared.userName ?? ""]
//
//            let detailArray = NSMutableArray()
//            for i in (0..<(self.makeRequestViewModel.profileData?.count ?? 0)){
//                detailArray.add(self.makeRequestViewModel.profileData?[i].name ?? "")
//            }
//            cell.patientDropDown.optionArray = detailArray  as! [String]
//            if self.makeRequestViewModel.profileData?.count ?? 0 > 0{
//                self.getProfileId = self.makeRequestViewModel.profileData?[0].profileId ?? ""
//            }
//            cell.patientDropDown.textAlignment = .center
//            cell.patientDropDown.resignFirstResponder()
//            cell.patientDropDown.font = UIFont(name:Font.FontName.Latoregular.rawValue, size: Utility.dynamicSize(14.0))
//            cell.patientDropDown.didSelect{(selectedText , index ,id) in
//                self.getProfileId = self.makeRequestViewModel.profileData?[index].profileId ?? ""
//                //self.barTypeInt = index
//                //self.setupBarType(isType: self.barTypeInt)
//                //self.valueLabel.text = "Selected String: \(selectedText) \n index: \(index)"
//            }
//            cell.addButton.addTarget(self, action: #selector(didAddPatientAction), for: .touchUpInside)
//        }
//        return cell
//    }
//    func getVideoconsultationTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: VideoCell.identifier) as? VideoCell else {
//            return UITableViewCell()
//        }
//        doOnMain {
//            cell.videoDropDown.text = "ASAP"
//            cell.videoDropDown.optionArray = ["ASAP","Time Based"]
//            cell.videoDropDown.textAlignment = .left
//            cell.videoDropDown.resignFirstResponder()
//            cell.videoDropDown.font = UIFont(name:Font.FontName.Latoregular.rawValue, size: Utility.dynamicSize(14.0))
//            cell.videoDropDown.didSelect{(selectedText , index ,id) in
//                if index == 1{
//                    self.getDeliveryType = "time_based"
//                }else{
//                    self.getDeliveryType = "asap"
//                }
//                //self.barTypeInt = index
//                //self.setupBarType(isType: self.barTypeInt)
//                //self.valueLabel.text = "Selected String: \(selectedText) \n index: \(index)"
//            }
//        }
//        cell.backgroundColor = .red
//        return cell
//    }
//    func getAddReasonTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: AddReasonCell.identifier) as? AddReasonCell else {
//            return UITableViewCell()
//        }
//        cell.backgroundColor = .red
//        return cell
//    }
//    func getSelectSymptomsTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: SelectSymptomsCell.identifier) as? SelectSymptomsCell else {
//            return UITableViewCell()
//        }
//        cell.selectSymptomsCollectionView.register(UINib(nibName: "SelectViewCell", bundle: .main), forCellWithReuseIdentifier: "SelectViewCell")
//        cell.selectSymptomsCollectionView.delegate = self
//        cell.selectSymptomsCollectionView.dataSource = self
//        cell.selectSymptomsCollectionView.tag = 100
//        cell.selectSymptomsCollectionView.reloadData()
//        return cell
//    }
//    func getCommonSymptomsTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: CommonSymptomsCell.identifier) as? CommonSymptomsCell else {
//            return UITableViewCell()
//        }
//
//        cell.commonSymptomsCollectionView.register(UINib(nibName: "CommonCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "CommonCollectionViewCell")
//        if makeRequestViewModel.symtomList.count > 0{
//            cell.commonSymptomsCollectionView.delegate = self
//            cell.commonSymptomsCollectionView.dataSource = self
//            cell.commonSymptomsCollectionView.tag = 200
//            let layout = TagFlowLayout()
//            layout.estimatedItemSize = CGSize(width: 140, height: 40)
//            cell.commonSymptomsCollectionView.collectionViewLayout = layout
//            cell.commonSymptomsCollectionView.reloadData()
//        }
//        return cell
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//    }
//}

extension MakeRequestViewController: UICollectionViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 100{
            return selected.count
        }else{
            return makeRequestViewModel.symtomList.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 100{
            let cell : SelectViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectViewCell", for: indexPath as IndexPath) as! SelectViewCell
            cell.titleLbl.text = selected[indexPath.row]
            return cell
        }else{
            let cell : CommonCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommonCollectionViewCell", for: indexPath as IndexPath) as! CommonCollectionViewCell
            cell.titleLbl.text = makeRequestViewModel.symtomList[indexPath.row].symptom
            cell.titleLbl.preferredMaxLayoutWidth = collectionView.frame.width - 16
            cell.subBgView.layer.cornerRadius = 20
            cell.subBgView.clipsToBounds = true
            cell.titleLbl.tag = makeRequestViewModel.symtomList[indexPath.row].id!
            if selected.contains(makeRequestViewModel.symtomList[indexPath.row].symptom!) {
                cell.subBgView.backgroundColor = .themeColor
                cell.titleLbl.textColor = .white
                cell.plusButton.setTitleColor(.white, for: .normal)
                cell.plusButton.setTitle("-", for: .normal)
            } else {
                cell.subBgView.backgroundColor = .white
                cell.titleLbl.textColor = .black
                cell.plusButton.setTitleColor(.black, for: .normal)
                cell.plusButton.setTitle("+", for: .normal)
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView.tag == 100{
            return 3
        }else{
            return 15
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView.tag == 100{
            return 3
        }else{
            return 15
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag != 100{
            guard let cell = collectionView.cellForItem(at: indexPath) as? CommonCollectionViewCell, let text = cell.titleLbl.text else {return}
            print("Tag =",cell.titleLbl.tag)
            if selected.contains(text) {
                selected = selected.filter{$0 != text}
                symptomsSelectArray.remove(cell.titleLbl.tag)
            } else {
                selected.append(text)
                symptomsSelectArray.add(cell.titleLbl.tag)
            }
            collectionView.reloadData()
            print(symptomsSelectArray)
            //symptomsSelectArray.add(makeRequestViewModel.symtomList[indexPath.row].id!)
            let indexPathRow:Int = 2
            let indexPosition = IndexPath(row: indexPathRow, section: 0)
            self.requestTableView.reloadRows(at: [indexPosition], with: .none)
        }
        buttonSuccess()
    }
    
    private func buttonSuccess(){
        if getProfileId != "" && symptomsSelectArray.count != 0{
            nextButton.backgroundColor = .themeColor
            nextButton.tintColor = .white
            nextButton.titleLabel?.textColor = .white
            nextButton.setTitleColor(.white, for: .normal)
        }else{
            nextButton.backgroundColor = .loginGeryColor
            nextButton.tintColor = UIColor.convertRGB(hexString: "#A0A0A0")
            nextButton.titleLabel?.textColor = UIColor.convertRGB(hexString: "#A0A0A0")
        }
    }
}



extension UITableView {
    var contentSizeHeight: CGFloat {
        var height = CGFloat(0)
        for section in 0..<numberOfSections {
            height = height + rectForHeader(inSection: section).height
            let rows = numberOfRows(inSection: section)
            for row in 0..<rows {
                height = height + rectForRow(at: IndexPath(row: row, section: section)).height
            }
        }
        return height
    }
}
