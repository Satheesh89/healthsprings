//
//  ClinicEvening.swift
//

import Foundation
import SwiftyJSON

struct ClinicEvening {
	let time: String?
	init(_ json: JSON) {
		time = json["time"].stringValue
	}
}
