import Foundation

struct NotificationsModel : Codable {
    let id : Int?
    let patient_id : Int?
    let type : String?
    let sub_type : String?
    let message : String?
    let doctor_id : Int?
    let appointment_id : Int?
    let requested_data : String?
    let created_at : String?
    let updated_at : String?
    let status : String?
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case patient_id = "patient_id"
        case type = "type"
        case sub_type = "sub_type"
        case message = "message"
        case doctor_id = "doctor_id"
        case appointment_id = "appointment_id"
        case requested_data = "requested_data"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case status = "status"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        patient_id = try values.decodeIfPresent(Int.self, forKey: .patient_id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        sub_type = try values.decodeIfPresent(String.self, forKey: .sub_type)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        doctor_id = try values.decodeIfPresent(Int.self, forKey: .doctor_id)
        appointment_id = try values.decodeIfPresent(Int.self, forKey: .appointment_id)
        requested_data = try values.decodeIfPresent(String.self, forKey: .requested_data)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }
}


//class NotificationsModel : NSObject {
//    let id: Int?
//    let patientId: Int?
//    let type: String?
//    let subType: String?
//    let message: String?
//    let doctorId: Int?
//    let appointmentId: Int?
//    let requestedData: String?
//    let createdAt: String?
//    let updatedAt: String?
//    let status: String?
//
//    init(id: Int, patientid: Int, type: String, subtype: String,message: String,doctorid: Int,appointmentid: Int,requesteddata: String,createdat: String,updatedat: String,status: String) {
//        self.id = id
//        self.patientId = patientid
//        self.type = type
//        self.subType = subtype
//        self.message = message
//        self.doctorId = doctorid
//        self.appointmentId = appointmentid
//        self.requestedData = requesteddata
//        self.createdAt = createdat
//        self.updatedAt = updatedat
//        self.status = status
//    }
//}
