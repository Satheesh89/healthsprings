//
//  AppointmentDetails.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on December 14, 2022
//
import Foundation
import SwiftyJSON

struct HistoryModelDetailsAppointmentDetails {

	let doctorName: String?
	let speciality: String?
	let appointmentDate: String?
	let appointmentTime: String?
	let patientName: String?
	let videoId: String?
	let symptom: String?
	let historyModelDetailsItems: [HistoryModelDetailsItems]?
	let subTotal: Double?
	let taxService: String?
	let totalPaid: Double?
	let currency: String?
	let paymentDate: String?
	let cardNo: Int?
	let streetName: String?
	let areaLocation: String?
	let state: String?
	let city: String?
	let country: String?
	let pinCode: String?
	let avatar: String?
	let bill: String?
	let prescription: String?
	let medicalCertificate: String?
	let fullAddress: String?

	init(_ json: JSON) {
		doctorName = json["doctor_name"].stringValue
		speciality = json["speciality"].stringValue
		appointmentDate = json["appointment_date"].stringValue
		appointmentTime = json["appointment_time"].stringValue
		patientName = json["patient_name"].stringValue
		videoId = json["video_id"].stringValue
		symptom = json["symptom"].stringValue
        historyModelDetailsItems = json["items"].arrayValue.map { HistoryModelDetailsItems($0) }
		subTotal = json["sub_total"].doubleValue
		taxService = json["tax_service"].stringValue
		totalPaid = json["total_paid"].doubleValue
		currency = json["currency"].stringValue
		paymentDate = json["payment_date"].stringValue
		cardNo = json["card_no"].intValue
		streetName = json["street_name"].stringValue
		areaLocation = json["area_location"].stringValue
		state = json["state"].stringValue
		city = json["city"].stringValue
		country = json["country"].stringValue
		pinCode = json["pin_code"].stringValue
		avatar = json["avatar"].stringValue
		bill = json["bill"].stringValue
		prescription = json["prescription"].stringValue
		medicalCertificate = json["medical_certificate"].stringValue
		fullAddress = json["full_address"].stringValue
	}

}
