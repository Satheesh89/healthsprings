//
//  SocialLoginDetails .swift
//  Healthsprings
//

import Foundation

class SocialLoginDetails: NSObject {
    var email: String
    var name: String
    var token: String
    var loginType: Int
    init(email: String,name: String,token: String,loginType: Int){
        self.email = email
        self.name = name
        self.token = token
        self.loginType = loginType
    }
}


class UpdateProfileLoginDetails: NSObject {
    var person_name: String
    var profile_person: String
    var email: String
    var loginType: Int
    var mobile_code: String
    var mobile_number: String
    var street_name: String
    var country: String
    var state_id: String
    var city: String
    var pin_code: String
    var gender: String
    var emergency_contact: String
    var marital_status: String
    var dob: String
    var height: String
    var weight: String
    var blood_group: String
    var nric_id: String
    var full_address: String
    
    init(person_name: String,profile_person: String,email: String,loginType: Int,mobile_code: String,mobile_number: String,street_name: String,country: String,state_id: String,city: String,pin_code: String,gender: String,emergency_contact: String,marital_status: String,dob: String,height: String,weight: String,blood_group: String,nric_id: String,full_address: String){
        self.person_name = person_name
        self.profile_person = profile_person
        self.email = email
        self.loginType = loginType
        self.mobile_code = mobile_code
        self.mobile_number = mobile_number
        self.street_name = street_name
        self.country = country
        self.state_id = state_id
        self.city = city
        self.pin_code = pin_code
        self.gender = gender
        self.emergency_contact = emergency_contact
        self.marital_status = marital_status
        self.dob = dob
        self.height = height
        self.weight = weight
        self.blood_group = blood_group
        self.nric_id = nric_id
        self.full_address = full_address
    }
}


class confirmDetails: NSObject {
    var getUuidStr: String
    var profileIdStr: String
    var consultationfee: String
    var currency: String
    var date: String
    var time: String
    
    init(getUuidStr: String,profileIdStr: String,consultationfee: String,currency: String,date: String,time: String){
        self.getUuidStr = getUuidStr
        self.profileIdStr = profileIdStr
        self.consultationfee = consultationfee
        self.currency = currency
        self.date = date
        self.time = time
    }
}

//                self.getConfirmApi(getUuidStr: "\(self.getDoctorUuid ?? "")",profileIdStr: "\(self.getProfileId ?? "")",loadview: self)
