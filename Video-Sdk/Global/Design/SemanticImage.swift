//
//  SemanticImage.swift
//  VideoCallingApp
//
//  Created by Balamurugan c on 05/08/22.
//

import UIKit

/// Strongly typed way of working with resources from the bundle
enum SemanticImage: String {
    // Loader
    case icActivityIndicator = "activity_indicator"
    
    // Address
    case icDropdown = "icon_dropdown"

    // Internet connection
    case icOffline = "offline_image"
    
    // video screen
    case abortSwap = "abort_swap"
    case icSwitchCamera = "ic_switch_camera"
}

extension SemanticImage {

    /// Image corresponding to the Semantic name
    public var image: UIImage? {
        return UIImage(named: self.rawValue)
    }
}

extension UIImage {
    convenience init?(withSemantic semanticImage: SemanticImage) {
        self.init(named: semanticImage.rawValue)
    }
}


