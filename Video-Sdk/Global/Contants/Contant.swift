//
//  Contant.swift
//  AntVideoCallApp
//
//  Created by Balamurugan c on 27/08/22.
//

import UIKit

let videoSocketURL = "ws://stee-dev.roundesk.io:5080/testing/websocket"



let emptyMessage = NSLocalizedString("This filed is required", comment: "")
let invaildEmailMessage = NSLocalizedString("Enter valid email ID", comment: "")
let phoneNoEmptyMessage = NSLocalizedString("Phone number should not be less than 6", comment: "")
let invaildPhoneNoMessage = NSLocalizedString("Enter valid mobile number", comment: "")
let nricMessage = NSLocalizedString("NRIC number should start with 'F', 'G', 'M', 'S', or 'T' followed by 7 numeric digits and ends with a character", comment: "")
let maritialStatusMessage = NSLocalizedString("Marital status should be Married or Unmarried", comment: "")
let newAddCardMessage = NSLocalizedString("Account Number should be of 16 digits", comment: "")
let invaildCardAddCardMessage = NSLocalizedString("Account Number should be of 16 digits", comment: "")
let InvalidAccountMessage = NSLocalizedString("Account Number should be of 16 digits", comment: "")

let InvalidCVVMessage = NSLocalizedString("CVV should be of 3 digits", comment: "")
let InvalidPasswordMessage = NSLocalizedString("Password length should be > 8 & must contain alphaNumeric and special characters", comment: "")
let InvalidConfirmPasswordMessage = NSLocalizedString("Password doesn't match", comment: "")
let uploadedIDVerificationMessage = NSLocalizedString("Please upload a document for verification", comment: "")


//Case: If NRIC/Fin is selected from id type & then NRIC check should be there and if NRIC is invalid then show below message
//Msg: "NRIC/Fin number should start with 'F', 'G', 'M', 'S', or 'T' followed by 7 numeric digits and ends with a character"
//
//
//Case: Valid Till field while adding new card
//    1) Invalid Month
//    Msg: "Month should not be > 12"
//
//    2) Invalid Year
//    Msg: "Year should not be < 2022"
//
//    3) Invalid both Month and Year
//    Msg: "Enter Valid Month and Year"
//
//    4) Invalid format for Valid Till field
//    Msg: "Enter valid Exp format MM/YY"
//
//Case: Invalid CVV
//Msg:

