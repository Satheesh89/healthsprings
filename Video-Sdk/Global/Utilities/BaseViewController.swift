//
//  BaseViewController.swift
//  VideoCallingApp
//
//  Created by Balamurugan c on 03/08/22.
//
import Foundation
import UIKit
import CallKit
import SocketIO

class BaseViewController: UIViewController, CXProviderDelegate {
    
    /// Singleton instance
    let provider = CXProvider(configuration: CXProviderConfiguration(localizedName: "My App"))
    @Published var manager = SocketManager(socketURL: URL(string:"https://docis.live:3000/")!, config:[.log(false), .compress])
    var meetingId = 0
    //    var roomId = 0
    var streamId = ""
    var roomIds = 0
    let myCallerIdUUID = UUID()

    override func viewDidLoad() {
        super.viewDidLoad()
        let socket = manager.defaultSocket
        sendMsg()
        socket.connect()
        provider.setDelegate(self, queue: nil)
//        self.tabBarController?.hidesBottomBarWhenPushed = true
    }
    
    func receiverCall() {
        let update = CXCallUpdate()
        update.remoteHandle = CXHandle(type: .generic, value: "Healthsprings Doctor calling you!")
        //provider.reportNewIncomingCall(with: UUID(), update: update, completion: { error in })
        provider.reportNewIncomingCall(with: myCallerIdUUID, update: update, completion: { error in })
    }
    
    private func sendMsg() {
        let socket = manager.defaultSocket
        print("Connected1")
        socket.on(clientEvent: .connect){ (data, ack) in
            print("Connected")
        }
        //        let receiverId = DataManager.instance.selectedReceivedDetail ?? ""
        //        print("i am working pa","sendCallToClient\(receiverId)")
        
        // satheesh
        let receiverId = UserDetails.shared.getProfileuuid ?? ""
        print("i am working pa","sendCallToClient\(receiverId)")
        socket.on("sendCallToClient\(receiverId)"){ [weak self] (callDetails, ack) in //receive
            if let callDetails = callDetails[0] as? [String: Any]{
                
//                let callType = callDetails["type"] as? String
//                let meetingId = callDetails["meeting_id"] as? Int
//                let roomId = callDetails["room_id"] as? Int
//                
                //satheesh
                let callType = callDetails["type"] as? String
                let meetingIdStr = callDetails["meeting_id"] as? String
                let roomIdStr = callDetails["room_id"] as? String

                let meetingId = Int(meetingIdStr ?? "0")
                let roomId = Int(roomIdStr ?? "0")
                
                let transferType = ReceivedCallType(rawValue: callType ?? "") ?? .none
                switch transferType {
                case .new:
//                    DispatchQueue.main.async {
//                        guard let window = UIApplication.shared.delegate?.window,  let rootVc = window!.rootViewController else {
//                            return
//                        }
//                        let topVC = rootVc.topViewController()
//                        if topVC.isKind(of: ConferenceViewController.self) {
//                            // account is already created, but the user is still waiting for approval, so no need to force pin entry
//                            return
//                        }
                    DispatchQueue.main.async {
                        self?.receiverCall()
                        self?.meetingId = meetingId ?? 0
                        self?.roomIds = roomId ?? 0
                    }
                case .accepted:
                    print("call accepected")
                case .none:
                    print("none")
                }
            }
        }
    }
    
    private func endCall() {
        provider.reportCall(with: myCallerIdUUID, endedAt: Date(), reason: .unanswered)
    }
    
    private func navigateToVideoCall(_ streamId: String) {
        endCall()
        guard let window = UIApplication.shared.delegate?.window,  let rootVc = window!.rootViewController else {
            return
        }
        let topVC = rootVc.topViewController()
        if topVC.isKind(of: ConferenceViewController.self) {
            // account is already created, but the user is still waiting for approval, so no need to force pin entry
            let conferenceViewController = topVC as? ConferenceViewController
            conferenceViewController?.loadedVideoDetails(clientUrl: videoSocketURL, roomId: streamId)
            return
        }
        let controller = UIStoryboard.init(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "Conference") as! ConferenceViewController
        controller.clientUrl = videoSocketURL
        controller.roomId = streamId
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func providerDidReset(_ provider: CXProvider) {
    }
    
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        print("Answer call",meetingId,roomIds)
        self.navigationController?.dismiss(animated: true, completion: nil)
        DataManager.instance.acceptCallAPI(meetingId: meetingId, roomId: roomIds) { [weak self] error, result in
            guard let strongSelf = self else { return }
            if let createCallModelDetails = result {
                strongSelf.streamId = createCallModelDetails.streamId ?? ""
                let streamId = createCallModelDetails.callerStreamId ?? ""
                print("i am workimnh samma",createCallModelDetails.callerStreamId)
                print("i am workimnh roomIds","\(strongSelf.roomIds)")
//                strongSelf.navigateToVideoCall(streamId)
                //satheesh
                strongSelf.navigateToVideoCall("\(strongSelf.roomIds)")
            }
            if let error = error {
                print(error)
            }
        }
        action.fulfill()
        endCall()
    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        action.fulfill()
        endCall()
    }
}
