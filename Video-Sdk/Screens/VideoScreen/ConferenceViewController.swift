//
//  ConferenceViewController.swift
//  AntMediaReferenceApplication
//
//  Created by mekya on 13.08.2020.
//  Copyright © 2020 AntMedia. All rights reserved.
//

import UIKit
import Foundation

class ConferenceViewController: BaseViewController {
    
    var conferenceClient: ConferenceClient!
    var clientUrl: String = ""
    var roomId: String?
    var publisherStreamId = ""
    var joinedStreamId = ""
    var swapView = true
    @IBOutlet var localView: UIView!
    @IBOutlet var mainVideoView: UIView!
    @IBOutlet weak var waitingLabel: UILabel!
    @IBOutlet weak var dropButton: UIButton!
    @IBOutlet weak var callDetailsView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var actionsBar: CallActionsBar!
    var isPublishedVideo = true
    var publisherClient: AntMediaClient?
    var playerClient: AntMediaClient?
    
    
    var details =  [confirmDetails]()

    
    
    class AntMediaClientConference {
        var playerClient: AntMediaClient
        var viewIndex: Int
        
        init(player: AntMediaClient, index: Int) {
            self.playerClient = player
            self.viewIndex = index
        }
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isIdleTimerDisabled = true
        dropButton.setTitle("", for: .normal)
        callDetailsView.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableView.registerCells([CallHistoryTableViewCell.self])
        mainVideoView.clipsToBounds = true
        mainVideoView.layer.cornerRadius = 30
        mainVideoView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        localView?.roundCorners([.allCorners], radius: 20)
        localView?.layer.zPosition = 1
        actionsBar.setup(withActions: [
            
            (.audio, action: { [weak self] sender in
                guard let self = self else { return }
                self.publisherClient?.toggleAudio()
            }),
            (.decline, action: { [weak self] sender in
                guard let self = self else { return }
                //sender?.isEnabled = false
                //self.navigationController?.popViewController(animated: true)
                let refreshAlert = UIAlertController(title: "End call", message: "Are you sure you want to end a call?", preferredStyle: UIAlertController.Style.alert)
                refreshAlert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action: UIAlertAction!) in
                    self.redirectSucessPage()
                    sender?.isEnabled = false
                }))
                refreshAlert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { (action: UIAlertAction!) in
                }))
                self.present(refreshAlert, animated: true, completion: nil)
            }),
            (.video, action: { [weak self] sender in
                guard let self = self else { return }
                self.publisherClient?.toggleVideo()
            }),
            (.switchCamera, action: { [weak self] sender in
                guard let self = self else { return }
                self.publisherClient?.switchCamera()
            })
        ])
    }
    
    
    private func redirectSucessPage(){
        let videoConsulationVC = VideoConsulationViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        videoConsulationVC.hidesBottomBarWhenPushed = true
        videoConsulationVC.getconfirmdetails =  details
        navigationController?.pushViewController(videoConsulationVC, animated: false)
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        AntMediaClient.setDebug(true)
        publishVideo()
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.publisherClient?.stop()
        self.playerClient?.stop()
        if roomId != "" && isPublishedVideo  {
            conferenceClient.leaveRoom()
        }
    }
    
    @IBAction func dropDownButtonAction(_ sender: UIButton) {
        UIView.animate(withDuration: 1.0, //1
                       delay: 0.5, //2
                       usingSpringWithDamping: 0.3, //3
                       initialSpringVelocity: 1, //4
                       options: UIView.AnimationOptions.curveLinear, //5
                       animations: ({ //6
            self.callDetailsView.isHidden = !self.callDetailsView.isHidden
        }), completion: nil)
    }
    
    func loadedVideoDetails(clientUrl: String, roomId: String) {
        self.clientUrl = clientUrl
        self.roomId = roomId
        publishVideo()
    }
    
    func publishVideo() {
//        roomId = "room1"
//        isPublishedVideo = true
        if let roomId = roomId, roomId != "" && isPublishedVideo  {
            waitingLabel.text = roomId
            conferenceClient = ConferenceClient.init(serverURL: self.clientUrl, conferenceClientDelegate: self)
            conferenceClient.joinRoom(roomId: roomId, streamId: "")
            isPublishedVideo = false
        } else {
//            let receiverId = DataManager.instance.selectedReceivedDetail
//            let callerId = DataManager.instance.selectedCallerDetail
//            DataManager.instance.createCallAPI(receiverId: receiverId ?? "", callerId: callerId ?? "") { [weak self] error, result in
//                guard let strongSelf = self else { return }
//                if let createCallModelDetails = result {
//                    if let streamId = createCallModelDetails.streamId {
//                        strongSelf.isPublishedVideo = true
//                        strongSelf.roomId = streamId
//                        strongSelf.publishVideo()
//                    }
//                }
//            }
        }
        
    }
    
    //MARK: - Utils
    private func iconView(withNormalImage normalImage: UIImage?, selectedImage: UIImage?) -> UIImageView? {
        let iconView = UIImageView(image: normalImage, highlightedImage: selectedImage)
        iconView.contentMode = .scaleAspectFit
        return iconView
    }
    
    private func loadLocalClientView(_ islocalView : Bool = true) {
        Run.onMainThread {
            AntMediaClient.printf("stream id to publish \(self.publisherStreamId)")
            self.publisherClient =  AntMediaClient.init()
            self.publisherClient?.delegate = self
            self.publisherClient?.setOptions(url: self.clientUrl, streamId: self.publisherStreamId, token: "", mode: AntMediaClientMode.publish, enableDataChannel: false)
            self.publisherClient?.setLocalView(container: islocalView ? self.localView : self.mainVideoView, mode: .scaleAspectFill)
            self.publisherClient?.initPeerConnection()
            self.publisherClient?.setExternalAudio(externalAudioEnabled: false)
            self.publisherClient?.start()
        }
    }
    
    private func loadRemoteClientView(_ isRemoteView : Bool = true, _ swapFlow: Bool = false) {
        Run.onMainThread {
            self.swapView = !isRemoteView
            if !isRemoteView {
                self.localView.subviews.forEach({ $0.removeFromSuperview() })
            }
            self.playerClient = AntMediaClient.init()
            self.playerClient?.delegate = self
            self.playerClient?.setRemoteView(remoteContainer: isRemoteView ? self.mainVideoView : self.localView ,mode: .scaleAspectFill)
            self.loadRemoteClient()
            if swapFlow {
                self.loadLocalClientView(isRemoteView)
                UIView.transition(from: self.localView, to: self.localView,
                                  duration: 0.0, options: [.curveLinear,
                                                           .showHideTransitionViews]) { _ in
                                                           }
            }
        }
    }
    
    private func loadRemoteClient() {
        AntMediaClient.printf("stream in the room: \(self.joinedStreamId)")
        self.playerClient?.setOptions(url: self.clientUrl, streamId: self.joinedStreamId, token: "", mode: AntMediaClientMode.play, enableDataChannel: false)
        self.playerClient?.initPeerConnection()
        self.playerClient?.start()
    }
}

extension ConferenceViewController: ConferenceClientDelegate
{
    public func streamIdToPublish(streamId: String) {
        self.publisherStreamId = streamId
        loadLocalClientView()
    }
    
    public func newStreamsJoined(streams: [String]) {
        guard let joinedStreamId = [streams.first ?? ""].first else {
            return
        }
        self.joinedStreamId = joinedStreamId
        loadRemoteClientView()
    }
    
    public func streamsLeaved(streams: [String]) {
        Run.onMainThread {
            self.playerClient?.stop()
        }
    }
}

extension ConferenceViewController: AntMediaClientDelegate{
    func swapVideoView() {
        if publisherStreamId != "" , joinedStreamId != "" {
            loadRemoteClientView(self.swapView,true)
        }
    }
    public func clientDidConnect(_ client: AntMediaClient) {
        AntMediaClient.printf("Websocket is connected")
    }
    public func clientDidDisconnect(_ message: String) {
    }
    public func clientHasError(_ message: String) {
    }
    public func remoteStreamStarted(streamId: String) {
    }
    public func remoteStreamRemoved(streamId: String) {
    }
    public func localStreamStarted(streamId: String) {
    }
    public func playStarted(streamId: String) {
        print("play started")
    }
    public func playFinished(streamId: String) {
    }
    public func publishStarted(streamId: String) {
    }
    public func publishFinished(streamId: String) {
    }
    public func disconnected(streamId: String) {
    }
    public func audioSessionDidStartPlayOrRecord(streamId: String) {
        self.publisherClient?.speakerOn()
    }
    public func dataReceivedFromDataChannel(streamId: String, data: Data, binary: Bool) {
    }
    public func streamInformation(streamInfo: [StreamInformation]) {
    }
}

extension ConferenceViewController: UITableViewDataSource, UITableViewDelegate  {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CallHistoryTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.updateData(accountNumberString: "mqqw", titleLabelString: "dewer",.callDetails)
        cell.backgroundColor = #colorLiteral(red: 0.8997184634, green: 0.9547008872, blue: 0.9623613954, alpha: 1)
        cell.selectionStyle = .none
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(CallHistoryViewController(), animated: true)
    }
}

