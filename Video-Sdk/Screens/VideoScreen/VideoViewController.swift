//
//  VideoViewController.swift
//  WebRTC
//

import UIKit
import WebRTC

class VideoViewController: BaseViewController {
    
    @IBOutlet weak var waitingLabel: UILabel!
    @IBOutlet weak var dropButton: UIButton!
    @IBOutlet weak var callDetailsView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var actionsBar: CallActionsBar!
    @IBOutlet weak var mainVideoView: UIView!
    @IBOutlet private weak var localVideoView: UIView?
    
//    private let webRTCClient: WebRTCClient
//    fileprivate let swapButton: ActionButton = {
//        let button = ActionButton(frame: ActionsBarConstants.rect)
//        button.addTarget(self, action: #selector(swapAction), for: .touchUpInside)
//        return button
//    }()
//    var localRenderer = RTCMTLVideoView()
//    init(webRTCClient: WebRTCClient) {
//        self.webRTCClient = webRTCClient
//        super.init(nibName: String(describing: VideoViewController.self), bundle: Bundle.main)
//    }
//    
//    @available(*, unavailable)
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        dropButton.setTitle("", for: .normal)
//        callDetailsView.isHidden = true
//        tableView.delegate = self
//        tableView.dataSource = self
//        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
//        tableView.registerCells([CallHistoryTableViewCell.self])
//        
//        mainVideoView.addSubview(swapButton)
//        mainVideoView.clipsToBounds = true
//        mainVideoView.layer.cornerRadius = 30
//        mainVideoView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
//        localVideoView?.roundCorners([.allCorners], radius: 20)
//        localRenderer = RTCMTLVideoView(frame: self.localVideoView?.frame ?? CGRect.zero)
//        let remoteRenderer = RTCMTLVideoView(frame: self.mainVideoView.frame)
//        localRenderer.contentMode = .scaleAspectFill
//        remoteRenderer.contentMode = .scaleAspectFill
//        // flip button
//        swapButton.pushed = true
//        swapButton.iconView = iconView(withNormalImage: SemanticImage.icSwitchCamera.image, selectedImage: SemanticImage.abortSwap.image)
//        swapButton.anchor(bottom: mainVideoView.bottomAnchor, trailing: mainVideoView.trailingAnchor, padding: UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 40))
//        
//        self.webRTCClient.startCaptureLocalVideo(renderer: localRenderer)
//        self.webRTCClient.renderRemoteVideo(to: remoteRenderer)
//        if let localVideoView = self.localVideoView {
//            self.embedView(localRenderer, into: localVideoView)
//        }
//        self.embedView(remoteRenderer, into: self.mainVideoView)
//        self.mainVideoView.sendSubviewToBack(remoteRenderer)
//        actionsBar.setup(withActions: [
//            
//            (.audio, action: { [weak self] sender in
//                guard let self = self else { return }
//                //                self.media.audioEnabled = !self.media.audioEnabled
//            }),
//            
//            (.decline, action: { [weak self] sender in
//                guard let self = self else { return }
//                sender?.isEnabled = false
//                //                self.hangUp?(self.callInfo.callId)
//            }),
//            
//            (.video, action: { [weak self] sender in
//                guard let self = self else { return }
//                //                self.media.videoEnabled = !self.media.videoEnabled
//                //                self.camera(enable: self.media.videoEnabled)
//            })
//        ])
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        //        rfhuzkfvodpajiletqss
//                // "caller_id": "agjqticlbhvredpouzkf"
//        
//        DataManager.instance.createCallAPI(receiverId: "agjqticlbhvredpouzkf", callerId: "rfhuzkfvodpajiletqss") { [weak self] error, result in
////        DataManager.instance.createCallAPI(receiverId: "rfhuzkfvodpajiletqss", callerId: "agjqticlbhvredpouzkf") { [weak self] error, result in
//            guard let strongSelf = self else { return }
//            
//            if let createCallModelDetails = result {
//                
//            }
//            if let error = error {
//                
//            }
//        }
//        self.actionsBar.select(false, type: .share)
//    }
//    
//    @IBAction func dropDownButtonAction(_ sender: UIButton) {
//        UIView.animate(withDuration: 1.0, //1
//                       delay: 0.5, //2
//                       usingSpringWithDamping: 0.3, //3
//                       initialSpringVelocity: 1, //4
//                       options: UIView.AnimationOptions.curveLinear, //5
//                       animations: ({ //6
//            self.callDetailsView.isHidden = !self.callDetailsView.isHidden
//        }), completion: nil)
//    }
//    
//    @objc fileprivate func swapAction(sender: ActionButton) {
//        if sender.pressed {
//            swapCameraToFront()
//        } else {
//            swapCameraToBack()
//        }
//    }
//    
//    private func swapCameraToFront() {
//        self.webRTCClient.startCaptureLocalVideo(renderer: localRenderer)
//    }
//    
//    private func swapCameraToBack() {
//        self.webRTCClient.startCaptureLocalVideo(renderer: localRenderer,cameraPosition: .back)
//    }
//    
//    private func embedView(_ view: UIView, into containerView: UIView) {
//        containerView.addSubview(view)
//        view.translatesAutoresizingMaskIntoConstraints = false
//        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|",
//                                                                    options: [],
//                                                                    metrics: nil,
//                                                                    views: ["view":view]))
//        
//        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|",
//                                                                    options: [],
//                                                                    metrics: nil,
//                                                                    views: ["view":view]))
//        containerView.layoutIfNeeded()
//    }
//    
//    //MARK: - Utils
//    private func iconView(withNormalImage normalImage: UIImage?, selectedImage: UIImage?) -> UIImageView? {
//        let iconView = UIImageView(image: normalImage, highlightedImage: selectedImage)
//        iconView.contentMode = .scaleAspectFit
//        return iconView
//    }
    
}

extension VideoViewController: UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CallHistoryTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.updateData(accountNumberString: "mqqw", titleLabelString: "dewer",.callDetails)
        cell.backgroundColor = #colorLiteral(red: 0.8997184634, green: 0.9547008872, blue: 0.9623613954, alpha: 1)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(CallHistoryViewController(), animated: true)
    }
}
