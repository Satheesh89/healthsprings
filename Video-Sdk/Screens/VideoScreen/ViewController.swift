//
//  ViewController.swift
//  AntVideoCallApp
//

import UIKit

class ViewController: BaseViewController {
    let selectCaller = ["drttbzfjiouhqkaegcvtl","agjqticerwlbhvredpou"]
    var receiverCaller = [String]()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if receiverCaller.count == 0 {
            title = "Select Caller"
        } else {
            title = "Receiver Caller"
        }
        tableView.register(UITableViewCell.self,
                           forCellReuseIdentifier: "TableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if receiverCaller.count == 0 {
            return self.selectCaller.count
        } else {
            return receiverCaller.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath)
        cell.textLabel?.text = receiverCaller.count == 0 ? self.selectCaller[indexPath.row] : self.receiverCaller[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if receiverCaller.count != 0 {
            
            AlertHelper.showOkCancelAlert(
                controller: self,
                title: "Alert",
                okayText: "Okay",
                cancelText: "Cancel",
                showCancel: true,
                message: "if u want to be an caller please press yes . if u are receiver press no",
                positiveAction: {
                    DataManager.instance.selectedReceivedDetail = self.receiverCaller[indexPath.row]
                    let controller = UIStoryboard.init(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "Conference") as! ConferenceViewController
                    controller.clientUrl = videoSocketURL
                    controller.roomId = ""
                    self.navigationController?.pushViewController(controller, animated: true)
                },
                negativeAction: {}
            )
            
            
        } else {
            var defaultSelectedCaller  = selectCaller
            if let index = defaultSelectedCaller.firstIndex(of: selectCaller[indexPath.row]) {
                defaultSelectedCaller.remove(at: index)
            }
            DataManager.instance.selectedCallerDetail = selectCaller[indexPath.row]
            let controller = UIStoryboard.init(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
            controller.receiverCaller = defaultSelectedCaller
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
