//
//  CallHistoryViewController.swift
//  VideoCallingApp
//


import UIKit

final class CallHistoryViewController: BaseViewController {
    private lazy var cardVisibilityList: UITableView = {
        let tableView = UITableView()
        tableView.registerCells([CallHistoryTableViewCell.self])
        tableView.backgroundColor = .white
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    private func setupTableView() {
        view.backgroundColor = .white
        view.addSubview(cardVisibilityList)
        cardVisibilityList.fillSuperview()
        cardVisibilityList.reloadData()
    }

}

extension CallHistoryViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func numberOfSections(in _: UITableView) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CallHistoryTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.updateData(accountNumberString: "mqqw", titleLabelString: "dewer")
        cell.selectionStyle = .none
        return cell
    }

    func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return 84.0
    }

}
