//
//  CallHistoryTableViewCell.swift
//  VideoCallingApp
//


import UIKit

final class CallHistoryTableViewCell: UITableViewCell {
    
    /// Namespace for all constants, used for layout of the Cell
    private struct ContentSizes {
        static let leadingMargin: CGFloat = 22.0
        static let stackLeadingTrailing: CGFloat = 14
        static let labelSize = CGSize(width: 0, height: 17.0)
        static let sourceTypeCardImageSize = CGSize(width: 25, height: 25.0)
    }
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20.0)
        label.anchor(size: ContentSizes.labelSize)
        label.textColor = .black
        return label
    }()
    
    private let accountNumber: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20.0)
        label.anchor(size: ContentSizes.labelSize)
        label.textColor = .black
        return label
    }()
    
    private let videoImage: UIImageView = {
        let imgView = UIImageView()
        imgView.image = #imageLiteral(resourceName: "ic_video_call")
        imgView.anchor(size: ContentSizes.sourceTypeCardImageSize)
        imgView.contentMode = .scaleAspectFit
        return imgView
    }()
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [titleLabel, accountNumber])
        stack.axis = .vertical
        stack.spacing = 3
        stack.alignment = .fill
        stack.distribution = .fillEqually
        return stack
    }()
    
   
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setupUi()
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateData(accountNumberString: String, titleLabelString: String,_ callType: CallType = .callHistory) {
        accountNumber.text = accountNumberString
        titleLabel.text = titleLabelString
        stackView.axis = callType == .callHistory ? .vertical : .horizontal
        videoImage.isHidden = callType != .callHistory
        accountNumber.textAlignment = callType == .callHistory ? .left : .right
    }
    
    private func setupUi() {
        selectionStyle = .none
        contentView.addSubview(stackView)
        let stackPadding = UIEdgeInsets(top: 0, left: ContentSizes.stackLeadingTrailing, bottom: 0, right: ContentSizes.stackLeadingTrailing)
        stackView.anchor(leading: contentView.leadingAnchor, trailing: contentView.trailingAnchor,padding: stackPadding)
        stackView.centerYTo(contentView.centerYAnchor)
        contentView.addSubview(videoImage)
        let videoImagePadding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ContentSizes.leadingMargin)
        videoImage.anchor(trailing: contentView.trailingAnchor, padding: videoImagePadding)
        videoImage.centerYTo(contentView.centerYAnchor)
    }
    
}

