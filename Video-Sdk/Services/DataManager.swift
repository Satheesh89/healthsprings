//
//  DataManager.swift
//  postbank-mwallet-ios
//
//  Created by Dimitar Mihailov on 2.06.20.
//  Copyright © 2020 SoftwareGroup. All rights reserved.
//

import Alamofire
import Foundation
import ObjectMapper

class DataManager {
    private let selectedCaller = "selectedCaller"
    private let receivedCaller = "receivedCaller"
    
    static let instance = DataManager()
    private var alamofireManager: Session
    private static let BASE_URL = BackendEndpoint.dev.rawValue
    
    private init() {
        let sessionConfig = URLSessionConfiguration.ephemeral
        sessionConfig.timeoutIntervalForRequest = 30
        let evaluators: [String: ServerTrustEvaluating]
        let domain = DataManager.BASE_URL
        // Do not enable certificate pinning, for environments different than UAT and PROD
        evaluators = [domain: DisabledTrustEvaluator()]
        
        let manager = ServerTrustManager(allHostsMustBeEvaluated: false,
                                         evaluators: evaluators)
        alamofireManager = Session(configuration: sessionConfig, serverTrustManager: manager)
    }
    
    // MARK: - Set & Get Caller and Receive details
    
    var selectedCallerDetail: String? {
        get {
            UserDefaults.standard.value(forKey: selectedCaller) as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: selectedCaller)
        }
    }
    
    var selectedReceivedDetail: String? {
        get {
            UserDefaults.standard.value(forKey: receivedCaller) as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: receivedCaller)
        }
    }
    
    // MARK: - BACKEND REQUESTS
    
    // MARK: - get Header method
    
    static func getHeader(contentType: String? = nil) -> HTTPHeaders {
        var headers = [HTTPHeader]()
        if let contentType = contentType {
            headers.append(HTTPHeader(name: RequestHeaderKeys.contentType, value: contentType))
        }
        return HTTPHeaders(headers)
    }
    
    // MARK: - Request method
    
    static func request(method: Alamofire.HTTPMethod = .post,
                        _ endPointurl: Endpoint,
                        performerDict: [String: Any]? = [:],
                        encoding: ParameterEncoding = JSONEncoding.default,
                        completionHandler: @escaping (_ result: [String: AnyObject]?,
                                                      _ error: Error?) -> Void) {
        var parameters: Parameters?
        parameters = performerDict
        let url: URLConvertible = BASE_URL + endPointurl.rawValue
        let headers = DataManager.getHeader(contentType: "application/json")
        DataManager.instance.alamofireManager.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    completionHandler(value as? [String: AnyObject], nil)
                case .failure(let error):
                    completionHandler(nil, error)
                }
            }
    }
    
    // MARK: - Make Call
    typealias CreateCallHandler = (_ error: Error?, _ user: CreateCallModel?) -> Void
    func createCallAPI(receiverId: String, callerId: String,completionHandler: @escaping CreateCallHandler) {
        let participantsParams: [[String: Any]] = [["receiver_id": receiverId,
                                                    "role": "paramedic"]]
        var params: [String: Any] = ["caller_id": callerId, "role": "paramedic", "audio": "on", "video": "on", "apiToken": "eyJ0eXAiOiJLV1PiLOJhbK1iOiJSUzI1NiJ9", "case_id": "a3dt3ffdesd3d"]
        params["participants"] = participantsParams
        print("samma ah params",params)
        DataManager.request(.makeCall, performerDict: params) { response, error in
            if let error = error {
                completionHandler(error, nil)
            } else {
                if let result = response, let createCallModel = Mapper<CreateCallModel>().map(JSON: result) {
                    completionHandler(nil, createCallModel)
                }
            }
        }
    }
    
    // MARK: - Accept Call
    
    typealias AcceptCallHandler = (_ error: Error?, _ user: CreateCallModel?) -> Void
    
    func acceptCallAPI(meetingId: Int, roomId: Int, completionHandler: @escaping AcceptCallHandler) {
//        let receiverId = DataManager.instance.selectedReceivedDetail ?? ""
        //satheesh
        let receiverId = UserDetails.shared.getProfileuuid ?? ""
        let params: [String: Any] = ["receiver_id": receiverId, "audio": "on", "video": "on", "meeting_id": meetingId, "roomId": roomId]
        print("params = ",params)
        
        DataManager.request(.acceptCall, performerDict: params) { response, error in
            print(response)
            if let error = error {
                completionHandler(error, nil)
            } else {
                if let result = response, let createCallModel = Mapper<CreateCallModel>().map(JSON: result) {
                    completionHandler(nil, createCallModel)
                }
            }
        }
    }
    
}
