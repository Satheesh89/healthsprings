//
//  Enumerations.swift
//  VideoCallingApp
//
import UIKit
// MARK: - Date Format type Enum
enum DateFormatType: String {
    /// Date in format 3 November 2020
    case dateWithLiteralMonth = "dd MMM yyyy"
    /// Date in format 3/11/2020
    case date = "dd/MM/yyyy"
    case serverDateTime = "yyyy-MM-dd"
    case dateTime = "dd MMM yyyy, HH:mm"
    case dateTimeFullMonth = "dd MMMM yyyy"
    case dateAndTimeUTC = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    case dateFullTimeShort = "dd/MM/yy HH:mm"
    case time = "HH:mm"
}
/// Namespace of the header keys used in backend requests
enum RequestHeaderKeys {
    static let authorization = "Authorization"
    static let contentType = "Content-type"
    static let acceptLanguage = "Accept-Language"
}
enum CallType {
    case callHistory
    case callDetails
}
/// Possible  methods returns call type
enum ReceivedCallType: String {
    case new = "new"
    case accepted = "accepted"
    case none
}
extension CaseIterable where Self: RawRepresentable {
    /// Array of all rawValues
    static var allValues: [RawValue] {
        return allCases.map { $0.rawValue }
    }
}
enum BackendEndpoint: String {
    case dev = "https://www.docis.live/docis_api/public/api"
    case test = "https://www.docis.live/docis_api/public/api1"
    case uat = "https://www.docis.live/docis_api/public/api2"
    case prod = "https://www.docis.live/docis_api/public/api4"
}
/// Enum containing all endpoins
enum Endpoint: String {
    // makecall
    case makeCall = "/make-call"
    // acceptcall
    case acceptCall = "/accept-call"
}
