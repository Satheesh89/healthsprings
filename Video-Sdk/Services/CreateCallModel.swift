//
//  CreateCallModel.swift
//  VideoCallingApp
//

import Foundation
import ObjectMapper

struct CreateCallModel : Mappable {
    var roomId : Int?
    var caller_name : String?
    var meetingId : Int?
    var streamId : String?
    var rtmp_url : String?
    var callerRtmpUrl: String?
    var callerStreamId: String?

    init?(map: Map) {
    }

    mutating func mapping(map: Map) {
        roomId <- map["roomId"]
        caller_name <- map["caller_name"]
        meetingId <- map["meetingId"]
        streamId <- map["streamId"]
        rtmp_url <- map["rtmp_url"]
        callerRtmpUrl <- map["caller_rtmp_url"]
        callerStreamId <- map["caller_streamId"]
    }

}

struct HandshakeMessage:Codable {
    var command:String?
    var streamId:String?
    var token:String?
    var video:Bool?
    var audio:Bool?
    var mode:String?
    var multiPeer:Bool?
}
