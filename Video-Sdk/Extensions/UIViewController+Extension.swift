//
//  UIViewController+Extension.swift
//  AntVideoCallApp
//
//  Created by Balamurugan c on 31/08/22.
//

import UIKit

extension UIViewController {
    /// Getting the top most view controller, which is visible to the user
    /// - Returns: Top most view controller,
    func topViewController() -> UIViewController {
        if let tabbarController = self as? UITabBarController {
            return tabbarController.selectedViewController!.topViewController()
        } else if let navigationController = self as? UINavigationController {
            return navigationController.visibleViewController!.topViewController()
        } else if self.presentedViewController != nil {
            let controller = self.presentedViewController
            return controller!.topViewController()
        } else {
            return self
        }
    }
}
