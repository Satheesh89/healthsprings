//
//  Afternoon.swift
//

import Foundation
import SwiftyJSON

struct ClinicAfternoon {
	let time: String?
	init(_ json: JSON) {
		time = json["time"].stringValue
	}
}
