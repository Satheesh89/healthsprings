//
//  Items.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on December 14, 2022
//
import Foundation
import SwiftyJSON


struct HistoryModelDetailsItems {

	let name: String?
	let qty: String?
	let price: Int?
	let currency: String?

	init(_ json: JSON) {
		name = json["name"].stringValue
		qty = json["qty"].stringValue
		price = json["price"].intValue
		currency = json["currency"].stringValue
	}

}
