//
//  ItemBillModeView.swift
//  Healthsprings
//
//  Created by Personal on 13/10/22.
//

import Foundation
import ObjectMapper
import Alamofire

class ItemBillModeView {
    
    typealias itemBillDetails = (_ itemBillDetails: ItemsBillModel?,_ status: Bool,_ message: String) -> Void
    var callBackHistoryDetails: itemBillDetails?
    
    func getAppoinmentDetails(appointmentId: String) {
        let parameters: [String: Any] = [
            "apiToken": UserDetails.shared.getApiToken(),
            "appointment_id": "436"
        ]
        print(parameters)
        
        AF.request(URL.getItemisedBill, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil, interceptor: nil).response { (responseData) in
            guard let data = responseData.data else {
                self.callBackHistoryDetails?(nil, false, "")
                return}
            do {
                let jsonDecoder = JSONDecoder()
                let responseModel = try jsonDecoder.decode(ItemsBillModel.self, from: data)
                self.callBackHistoryDetails?(responseModel, true,"")
            } catch {
                self.callBackHistoryDetails?(nil, false, error.localizedDescription)
            }
        }
    }
    
    func completionHandler(callBack: @escaping itemBillDetails) {
        self.callBackHistoryDetails = callBack
    }
    
}
