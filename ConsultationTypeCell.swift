//
//  ConsultationTypeCell.swift
//  Healthsprings
//
//  Created by Personal on 11/12/22.
//

import UIKit

class ConsultationTypeCell: UITableViewCell {
    @IBOutlet weak var immediateBgView: UIView!
    @IBOutlet weak var immediateInerView: UIView!
    @IBOutlet weak var immediateLabel: UILabel!
    @IBOutlet weak var immediateImageView: UIImageView!
    @IBOutlet weak var scheduleBgView: UIView!
    @IBOutlet weak var scheduleInerView: UIView!
    @IBOutlet weak var scheduleImageView: UIImageView!
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var immediateButton: UIButton!
    @IBOutlet weak var scheduleButton: UIButton!
    @IBOutlet weak var hStackView: UIStackView!

    override func awakeFromNib() {
        super.awakeFromNib()
        immediateBgView.layer.cornerRadius = 5
        immediateInerView.layer.cornerRadius = 5
        
        immediateBgView.layer.borderWidth = 1
        immediateBgView.layer.borderColor = UIColor.lightGray.cgColor
        
        scheduleBgView.layer.borderWidth = 1
        scheduleBgView.layer.borderColor = UIColor.lightGray.cgColor
        scheduleBgView.backgroundColor = .white
    
        scheduleBgView.layer.cornerRadius = 5
        scheduleInerView.layer.cornerRadius = 5

        doOnMain {
            self.immediateImageView.setImageColor(color: .darkGray)
            self.scheduleImageView.setImageColor(color: .darkGray)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    static var nib: UINib {
      return UINib(nibName: "ConsultationTypeCell", bundle: nil)
    }
    static var identifier: String {
      return String(describing: self)
    }
}
