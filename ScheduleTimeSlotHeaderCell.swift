//
//  ScheduleTimeSlotHeaderCell.swift
//  Healthsprings
//

import UIKit

class ScheduleTimeSlotHeaderCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var slotCollectionView: UICollectionView!
    @IBOutlet weak var selectTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: "ScheduleTimeSlotHeaderCell", bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
