//
//  ItemisedBillViewController.swift
//  Healthsprings
//


import UIKit
import SDWebImage
import CRNotifications

class ItemisedBillViewController: UIViewController {
    
    @IBOutlet weak var historyDetailTableView: UITableView!
    var itemsBillModel: ItemsBillModel!
    var getappointmentid: String = ""
    var getprofileid: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setStatusBar(color: UIColor.themeColor)
    }
    
    func  loadViewModel()  {
        let historyDetailViewModel = ItemBillModeView()
        historyDetailViewModel.getAppoinmentDetails(appointmentId: getappointmentid)
        historyDetailViewModel.completionHandler{[weak self] (details, status, message)in
            if status {
                guard let self = self else {return}
                guard let _itemBillList = details else {return}
                self.itemsBillModel = _itemBillList
                self.tableViewSetup()
            }
        }
    }
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        loadViewModel()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
 

    //MARK: TableView Setup
    func tableViewSetup()  {
        historyDetailTableView.register(ItemsBillDetailHeaderCell.nib, forCellReuseIdentifier: ItemsBillDetailHeaderCell.identifier)
        historyDetailTableView.register(PaymentHeaderCell.nib, forCellReuseIdentifier: PaymentHeaderCell.identifier)
        historyDetailTableView.register(PaymentPriceDetailsCell.nib, forCellReuseIdentifier: PaymentPriceDetailsCell.identifier)
        historyDetailTableView.register(PaymentDetailsCell.nib, forCellReuseIdentifier: PaymentDetailsCell.identifier)
        
        historyDetailTableView.delegate = self
        historyDetailTableView.dataSource = self
        historyDetailTableView.tableFooterView = UIView()
        historyDetailTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        historyDetailTableView.showsHorizontalScrollIndicator = false
        historyDetailTableView.showsVerticalScrollIndicator = false
        historyDetailTableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        historyDetailTableView.backgroundColor = .clear
        historyDetailTableView.contentInsetAdjustmentBehavior = .never
    }
    
    //MARK: - Back
    @IBAction func didTapLeftAction(_ sender: Any) {
        self.navigationController?.popToViewController(ofClass: HistoryViewController.self)
    }
    
    func savePdf(urlString:String, fileName:String) {
            DispatchQueue.main.async {
                let url = URL(string: urlString)
                let pdfData = try? Data.init(contentsOf: url!)
                let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
                let pdfNameFromUrl = "Healthspring-\(fileName).pdf"
                let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
                print("actualPath =",actualPath,"actualPath")
                do {
                    try pdfData?.write(to: actualPath, options: .atomic)
                    print("pdf successfully saved!")
                    self.showCRNotificationsToast(toastType: CRNotifications.success, toastTitle: "success", toastMeassage: "Healthspring - \(fileName) successfully saved!")
                } catch {
                    print("Pdf could not be saved")
                    self.showCRNotificationsToast(toastType: CRNotifications.error, toastTitle: "Error", toastMeassage: "Healthspring - \(fileName) could not be saved")
                }
            }
        }

        func showSavedPdf(url:String, fileName:String) {
            if #available(iOS 10.0, *) {
                do {
                    let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                    for url in contents {
                        if url.description.contains("\(fileName).pdf") {
                           // its your file! do what you want with it!
                    }
                }
            } catch {
                print("could not locate pdf file !!!!!!!")
            }
        }
    }

    // check to avoid saving a file multiple times
    func pdfFileAlreadySaved(url:String, fileName:String)-> Bool {
        var status = false
        if #available(iOS 10.0, *) {
            do {
                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                for url in contents {
                    if url.description.contains("Healthspring-\(fileName).pdf") {
                        status = true
                    }
                }
            } catch {
                print("could not locate pdf file !!!!!!!")
            }
        }
        return status
    }
    
}

extension ItemisedBillViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2{
            return itemsBillModel.itemised_bill?.items?.count ?? 0
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            return availableTableViewCell(tableView: tableView, indexPath: indexPath)
        }else  if indexPath.section == 1{
            return paymentHeaderCell(tableView: tableView, indexPath: indexPath)
        }else  if indexPath.section == 2{
            return paymentPriceCell(tableView: tableView, indexPath: indexPath)
        }else{
            return paymentDetailCell(tableView: tableView, indexPath: indexPath)
        }
    }
    
    
    func availableTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ItemsBillDetailHeaderCell.identifier) as? ItemsBillDetailHeaderCell else {
            return UITableViewCell()
        }
        cell.videoIdLabel.text = "\(itemsBillModel.itemised_bill?.video_id ?? "")"
        cell.closeButton.addTarget(self, action: #selector(didTaptoClose), for: .touchUpInside)

        return cell
    }
    
    func paymentHeaderCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentHeaderCell.identifier) as? PaymentHeaderCell else {
            return UITableViewCell()
        }
        return cell
    }
    
    func paymentPriceCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentPriceDetailsCell.identifier) as? PaymentPriceDetailsCell else {
            return UITableViewCell()
        }
        let itemList = itemsBillModel.itemised_bill?.items
        cell.titleLabel.text = "\(itemList?[indexPath.row].name ?? "")"
        cell.qtyLabel.text = "\(itemList?[indexPath.row].qty ?? "")"
        cell.priceLabel.text = "\(itemList?[indexPath.row].price ?? "")"
        return cell
    }
    
    func paymentDetailCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentDetailsCell.identifier) as? PaymentDetailsCell else {
            return UITableViewCell()
        }
        cell.subTotalPriceLabel.text = "\(itemsBillModel.itemised_bill?.sub_total ?? "$ 0.00")"
        cell.totalPriceLabel.text = "\(itemsBillModel.itemised_bill?.total_paid ?? 0)"
        cell.taxPriceLabel.text = "\(itemsBillModel.itemised_bill?.tax_service ?? 0)"
        cell.dateandtimeLabel.text = "Date & Time  : \(itemsBillModel.itemised_bill?.payment_date ?? "$ 0.00")"
        cell.cardNoLabel.text = "****       ****        ****        \(itemsBillModel.itemised_bill?.card_no ?? 1234)"
        cell.downloadButton.addTarget(self, action: #selector(didTaptoDownloadButton), for: .touchUpInside)
        cell.downloadPrescriptionView.isHidden = true
        cell.downloadMedicalCertificateView.isHidden = true
        return cell
    }
    @objc func didTaptoClose() {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func didTaptoDownloadButton() {
//        savePdf(urlString:"\(self.historyAppointmentDetail.appointment_details?.bill ?? "https://www.docis.live/docis_api/public/api/generate-bill")", fileName:"Medical Bill")
    }
}
