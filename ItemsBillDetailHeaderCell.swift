//
//  ItemsBillDetailHeaderCell.swift
//  Healthsprings
//
import UIKit


class ItemsBillDetailHeaderCell: UITableViewCell {
    
    @IBOutlet weak var videoIdLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    static var nib: UINib {
      return UINib(nibName: "ItemsBillDetailHeaderCell", bundle: nil)
    }
    static var identifier: String {
      return String(describing: self)
    }
}
