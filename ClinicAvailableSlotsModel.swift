//
//  ClinicAvailableSlotsModel.swift
//

import Foundation
import SwiftyJSON

struct ClinicAvailableSlotsModel {
	let morning: [ClinicMorning]?
	let afternoon: [ClinicAfternoon]?
	let evening: [ClinicEvening]?
	let night: [ClinicNight]?
	init(_ json: JSON) {
		morning = json["morning"].arrayValue.map { ClinicMorning($0) }
		afternoon = json["afternoon"].arrayValue.map { ClinicAfternoon($0) }
		evening = json["evening"].arrayValue.map { ClinicEvening($0) }
		night = json["night"].arrayValue.map { ClinicNight($0) }
	}
}
