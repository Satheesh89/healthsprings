//
//  ClinicNight.swift
//

import Foundation
import SwiftyJSON

struct ClinicNight {
	let time: String?
	init(_ json: JSON) {
		time = json["time"].stringValue
	}
}
