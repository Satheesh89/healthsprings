//
//  PasswordChangedViewController.swift
//  Healthsprings
//
//  Created by Personal on 02/10/22.
//

import UIKit

class PasswordChangedViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    @IBAction func didTapBackToLoginAction(_ sender: Any) {
        doOnMain{
            let mainstoryboard = UIStoryboard(name: "Menu", bundle: nil)
            let mainview = mainstoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            UserDefaults.standard.hasOnboarded = true
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(mainview, animated: false)
        }
    }
}
