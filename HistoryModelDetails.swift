//
//  HistoryModelDetails.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on December 14, 2022
//
import Foundation
import SwiftyJSON

struct HistoryModelDetails {

	let historyModelDetailsAppointmentDetails: HistoryModelDetailsAppointmentDetails?

	init(_ json: JSON) {
        historyModelDetailsAppointmentDetails = HistoryModelDetailsAppointmentDetails(json["appointment_details"])
	}

}
