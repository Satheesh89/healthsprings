import Foundation

struct GetCountryList : Codable {
	let country_id : Int?
	let country_phone_code : Int?
	let country_code : String?
	let country : String?

	enum CodingKeys: String, CodingKey {
		case country_id = "country_id"
		case country_phone_code = "country_phone_code"
		case country_code = "country_code"
		case country = "country"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		country_id = try values.decodeIfPresent(Int.self, forKey: .country_id)
		country_phone_code = try values.decodeIfPresent(Int.self, forKey: .country_phone_code)
		country_code = try values.decodeIfPresent(String.self, forKey: .country_code)
		country = try values.decodeIfPresent(String.self, forKey: .country)
	}
}
