//
//  Faq.swift
//

import Foundation
import SwiftyJSON

struct Faq {
    let question: String?
    let answer: String?
    init(_ json: JSON) {
        question = json["question"].stringValue
        answer = json["answer"].stringValue
    }
}
